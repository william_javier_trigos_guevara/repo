# -*- coding: utf-8 -*-

import numpy as np

import gpstk
import matplotlib.pyplot as plt
from IPython.display import display, HTML 

def get_sat_elv_az(svs, prs, rx):
    """
    http://www.naic.edu/aisr/GPSTEC/drewstuff/MATLAB/elavazim.m
    https://goo.gl/2Kt8Pf
    """
    svs = apply_earth_rotation_to_svs_position(svs, prs)

    #d_svs = r_com[:,:3]/r_mag[:, None]
    svs_enu = ecef2enu(svs, rx)
    
    east, north, up  = svs_enu[0], svs_enu[1], svs_enu[2]
    
    elevation = up/np.linalg.norm(svs_enu, axis=0)
    azimuth = np.arctan2(east,north)#radians
    idx = np.where(azimuth<0)
    azimuth[idx] += 2*np.pi;
    return elevation, azimuth # radians

def plot_from_gps_dataframe(data, x_var, y_var, legend, title_plot=None, ylim=None, fig_size=(20, 5)):
    import matplotlib.pyplot as plt
    from matplotlib.pyplot import cm
    import numpy as np
    from matplotlib import lines
    
    # https://anaconda.org/BjornFJohansson/mpldatacursor
    from mpldatacursor import datacursor#import datacursor

    #fig = plt.figure(figsize=fig_size)
    #ax = fig.add_subplot(111)

    info = {}
    gps_time = []

    for i in xrange(len(data)):

        obs = data.iloc[i]
        #print obs.gps_sow, obs.prns

        sats = obs[legend] #obs.prns
        elev = obs[y_var] #obs.prns_elev
        
        tup = zip(sats, [obs.gps_sow]*len(elev), elev)

        for k, v1, v2 in tup:
            #info[k] = info.get(k, ()) + ((v1, v2),)   # con tupla de tuplas

            # lo mismo pero con lista de tuplas
            if k not in info.keys():
                info.setdefault(k, [(v1, v2)])
            else:
                info[k].append((v1, v2))

        gps_time.append(obs[x_var]) #(obs.gps_sow)

    linestyles = [str(x) for x in lines.lineStyles.keys()]
    

    c = 0
    for satid, dat in info.iteritems():

        if satid in ["GPS31", "GPS21", "GPS32", "GPS9", "GPS14"]:#obs.prns:
            x_vals = [x[0] for x in dat]
            y_vals = [x[1] for x in dat]

            #print x_vals
            c += 1
            if ylim == None:
                ylim = [-1.1*np.min(y_vals), 2*np.max(y_vals)]

            plt.ylim(ylim)
            lines = ax.plot(x_vals, y_vals, 
                    marker = '.',
                    markerfacecolor='None',
                    #markeredgecolor=color,
                    linestyle='none',
                    label=satid)
        
        # http://stackoverflow.com/questions/12236566/setting-different-color-for-each-series-in-scatter-plot-on-matplotlib
        #ax.scatter(x_vals, y_vals, c=x_vals, label=satid, marker='.', cmap=plt.cm.Blues)
    
    #print c
    datacursor(lines)
    
    ax.set_title(title_plot)
    ax.set_xlabel(x_var)
    ax.set_ylabel(y_var)
    
    # http://stackoverflow.com/questions/4700614/how-to-put-the-legend-out-of-the-plot
    
    ax.legend(loc='upper center', bbox_to_anchor=(0.5, 1.), ncol=8, fancybox=True, shadow=True)
    # Put a legend below current axis
    #ax.legend(loc='upper center'L, bbox_to_anchor=(0.5, -0.1), fancybox=True, shadow=True, ncol=8)

    #ax.legend(fontsize='medium', fancybox=True, framealpha=0.8, loc=9) #loc='best', fontsize='small', bbox_to_anchor=(1, 0.), ncol=2,
    
