# Widget related imports
import ipywidgets as widgets
from IPython.display import display, clear_output, Javascript
from traitlets import Unicode

# nbconvert related imports
from nbconvert import get_export_names, export_by_name
from nbconvert.writers import FilesWriter
from nbformat import read, NO_CONVERT
from nbconvert.utils.exceptions import ConversionException

def get_notebook_name():
    # http://stackoverflow.com/questions/12544056/how-to-i-get-the-current-ipython-notebook-name
    """ get current notebook name."""
    
    display(Javascript(
            """
            var kernel = IPython.notebook.kernel;
            var thename = window.document.getElementById("notebook_name").innerHTML;
            var command = "theNotebook = " + "'"+thename+"'";
            """))

filename = get_notebook_name()
print filename

exporter_names = widgets.Dropdown(options=get_export_names(), value='pdf')
export_button = widgets.Button(description="Export")
download_link = widgets.HTML(visible=False)

file_writer = FilesWriter()

def export(name, nb):
    
    # Get a unique key for the notebook and set it in the resources object.
    notebook_name = name[:name.rfind('.')]
    resources = {}
    resources['unique_key'] = notebook_name
    resources['output_files_dir'] = '%s_files' % notebook_name

    # Try to export
    try:
        output, resources = export_by_name(exporter_names.value, nb)
    except ConversionException as e:
        download_link.value = "<br>Could not export notebook!"
    else:
        write_results = file_writer.write(output, resources, notebook_name=notebook_name)
    
        download_link.value = "<br>Results: <a href='files/{filename}'><i>\"{filename}\"</i></a>".format(filename=write_results)
        download_link.visible = True
        
def handle_export(widget):
    import os
    with open(filename, 'r') as f:
        print "Saving... ", filename, " in ", os.getcwd()
        export(filename, read(f, NO_CONVERT))

export_button.on_click(handle_export)

display(exporter_names, export_button, download_link)