function A = Omega(w)
% --------------------------------------------------
% For the quaternion differential equation:
% q_dot = Omega(w)*q;
%
% Author:   Harald N�kland
% Date:     June 2011
% --------------------------------------------------
x = w(1);
y = w(2);
z = w(3);

A = [ 0  -x  -y  -z
      x   0   z  -y
      y  -z   0   x
      z   y  -x   0 ];