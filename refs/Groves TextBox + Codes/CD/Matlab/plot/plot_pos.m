%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Position plot
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure

% x
subplot(3,1,1)
title('Position')
hold on
plot(t, act_pos(1:N,1),'-b')
plot(t, mes_pos(1:N,1),'--g')
plot(t, est_pos(1:N,1),'--r')
legend('True', 'GPS', 'EKF')
ylabel('North [m]')

% y
subplot(3,1,2)
hold on
plot(t, act_pos(1:N,2),'-b')
plot(t, mes_pos(1:N,2),'--g')
plot(t, est_pos(1:N,2),'--r')
legend('True', 'GPS', 'EKF')
ylabel('East [m]')

% z
subplot(3,1,3)
hold on
plot(t, act_pos(1:N,3),'-b')
plot(t, mes_pos(1:N,3),'--g')
plot(t, est_pos(1:N,3),'--r')
legend('True', 'GPS', 'EKF')
ylabel('Down [m]')
xlabel('Time [sec]')


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Position plot  xy + height
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure
title('Position')
hold on
plot(act_pos(1:N,2),act_pos(1:N,1),'-b')
plot(mes_pos(1:N,2),mes_pos(1:N,1),'--g')
plot(est_pos(1:N,2),est_pos(1:N,1),'--r')
legend('True', 'GPS', 'EKF')
xlabel('East [m]')
ylabel('North [m]')


