#!/bin/bash

#cd /home/diego/Proyecto/datos

INPUT=./047/*
OUTPUT_DIR=./rinex/047
RINEX_VER=2.11
FORMAT=nvs #ubx  #
COM1="Data adquired using PildoBox-Edison by Diego Acosta"
COM2="Laboratorio de SuperComputación y Cálculo Cientifico"
COM3="2015 November 17 UTC, Bucaramanga, Colombia"
OBSERV="SC3 UIS"
AGENCY = "SC3 UIS"
REC_NUMBER="PildoBox001"
REC_TYPE="NV08C-CSM"
REC_VER="v4.1 1214"

exec ./convbin "$INPUT" -d "$OUTPUT_DIR" -r "$FORMAT" -hc "$COM1" -hc "$COM2" -hc "$COM3" -ho "$OBSERV"/"$AGENCY" -hr "$REC_NUMBER"/"$REC_TYPE"/"$REC_VER"
