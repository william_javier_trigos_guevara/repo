#!/usr/bin/env python

import pylab
import matplotlib.pyplot as plt
import matplotlib.dates as md
import plotConstants as mPlt
from SSN import ssnConstants as mSSN
from GNSS import gpstime


def suplabel(axis, label, label_prop=None, labelpad=3, ha='center', va='center'):
    '''
    Add super ylabel or xlabel to the figure
    Similar to matplotlib.suptitle
        axis       - string: "x" or "y"
        label      - string
        label_prop - keyword dictionary for Text
        labelpad   - padding from the axis (default: 5)
        ha         - horizontal alignment (default: "center")
        va         - vertical alignment (default: "center")
    '''
    fig = pylab.gcf()
    xmin = []
    ymin = []
    for ax in fig.axes:
        xmin.append(ax.get_position().xmin)
        ymin.append(ax.get_position().ymin)
    xmin, ymin = min(xmin), min(ymin)
    dpi = fig.dpi
    if axis.lower() == "y":
        rotation = 90.
        x = xmin-float(labelpad)/dpi
        y = 0.5
    elif axis.lower() == 'x':
        rotation = 0.
        x = 0.5
        y = ymin - float(labelpad)/dpi
    else:
        raise Exception("Unexpected axis: x or y")
    if label_prop is None:
        label_prop = dict()
    pylab.text(x, y, label, rotation=rotation,
               transform=fig.transFigure,
               ha=ha, va=va,
               **label_prop)


def TOW2UTC(WkNr, TOW):
    '''
    TOW2UTC transforms an list expressed in TOW to UTC list

    Parameters:
        WkNr: week number of TOW
        TOW: list of TOWs to transform

    Return:
        UTC: list of UTCs
    '''
    # transform TOW to UTC representation
    UTC = []
    for i in range(0, len(TOW)):
        UTC.append(gpstime.UTCFromWT(float(WkNr), float(TOW[i])))
    print ("UTC = %s to %s" % (UTC[0], UTC[-1]))

    return UTC


def plotCN0(SVID, signalTypes, dataMeasSVID, UTCall, Last):
    """
    plotC/N0 creates a plot of the C/NO ratio
    """
    # plt.style.use('BEGPIOS')
    plt.style.use('ggplot')
    # print "datameas 0 ", dataMeasSVID[0]['MEAS_CN0']
    # print "datameas 1 ", dataMeasSVID[1]['MEAS_CN0']
    plt.figure(1)
    # f, axes = plt.subplots(2, 1)
    plt.suptitle('C/N0 Ratio', fontsize=20)

    # ax1 = plt.subplot(len(signalTypes), 1, 1)
    subPlots = []
    for index, signalType in enumerate(signalTypes):
        subPlots.append(plt.subplot(len(signalTypes), 1, index+1))

        # transform TOW to UTC representation
        utc = []
        for i in range(0, len(dataMeasSVID[index])):
            utc.append(gpstime.UTCFromWT(float(dataMeasSVID[index]['MEAS_WNC'][i]), float(dataMeasSVID[index]['MEAS_TOW'][i])))
        # print ("UTC = %s" % utc[0])

        plt.plot(dataMeasSVID[index]['MEAS_TOW'], dataMeasSVID[index]['MEAS_CN0'], label=str(mSSN.svPRN(SVID)[1])+str(mSSN.svPRN(SVID)[2]), marker='.', linestyle='', markersize=1.5, color=mPlt.rgbColors[SVID % len(mPlt.rgbColors)])

        # print "SIG", mSSN.GNSSSignals[1]
        plt.title('Signaltype:  %s' % mSSN.GNSSSignals[signalType]['name'])

        # adjust the X-axis to represent readable time
        ax = plt.gca()
        ax.xaxis.set_major_formatter(md.DateFormatter('%H:%M:%S'))
        dateString = gpstime.UTCFromWT(float(dataMeasSVID[0]['MEAS_WNC'][0]), float(dataMeasSVID[0]['MEAS_TOW'][0])).strftime("%d/%m/%Y")

    #     print dateString

    # print "index", index
    # print "AX", subPlots[index]

    if Last:
        mPlt.annotateText(r'$\copyright$ Alain Muls (alain.muls@rma.ac.be)', subPlots[index], 0, -0.22, 'left')
        mPlt.annotateText(r'$\copyright$ Frederic Snyers (fredericsn@gmail.com)', subPlots[index], 1, -0.22, 'right')

        # leg = plt.legend(shadow=True, bbox_to_anchor=(1.01, 1), loc=3)
        suplabel('y', 'C/N0')
        dateString = gpstime.UTCFromWT(float(dataMeasSVID[0]['MEAS_WNC'][0]), float(dataMeasSVID[0]['MEAS_TOW'][0])).strftime("%d/%m/%Y")
        plt.xlabel('Time of ' + dateString)
        plt.show()
        plt.close()
    # f.savefig('test.CNO.png')
    # plt.draw()
