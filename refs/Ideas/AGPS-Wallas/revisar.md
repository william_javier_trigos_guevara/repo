
## Titulos:

- ambiguity resolution in precise point positioning single frequency receiver
- Evaluación de ASR para PPP




## Justificacion Desde Pág 160 (http://eprints.qut.edu.au/60960/1/Jun_Wang_Thesis.pdf) 

Li  et al.  (2009)have  demonstrated  that  using  three  frequency  signals  can  efficiently determine   the   ambiguities   over   several   minutes   without   distance   constraints. Verhagen  et  al.  (2010)  have  investigated  the  high-precision  relative  positioning performance of low-cost single-frequency RTK receiver in the dual-constellation and presented that instantaneous ASR above 99% can be obtained with 15km baseline in clear sky conditions. However, these performance benefits do not come without cost. Generally  speaking,  multi-GNSS  hardware  will  consume  more  power  to  deal  with more satellites and signals which may be a problem for many low-cost applications. High   computational   burden   is   another   issue   for   high   timeliness   applications. Transmission  of  GNSS  corrections  for  a  large  number  of  satellites  and  signals  will cause  bandwidth  congestion  problems,  for  instance,  potential disadvantagesdue  to the  coexistence  of  multiple  GNSS systems  in  the  same  frequency  bands.  Benefits 
that  multi-GNSS  and  multi-frequency  signals  can  bring  to  users  may  be  maximized by  selective  use  of  satellite  systems,  or  signals,  or  subset  of  visible  satellites  from different systems in order to achieve required positioning performance.


Desde Pág 161 (http://eprints.qut.edu.au/60960/1/Jun_Wang_Thesis.pdf) 

This  is certainly  the  case  for  real time  kinematic  positioning  or  other  precise positioning  based  on  successful  resolutions  of  carrier  phase  ambiguities  of  satellite signals. This research work will prove that it is possible to select a subset of satellites from two  constellations  in  order  to  achieve  higher  reliability  of  carrier  phase ambiguity resolutions, thus assuring the reliability and accuracy of the RTK solutions (Grejner-Brzezinska  et  al.  2005;  Feng  and  Wang  2008).  In  the  contrast,  use  of  too many  redunda
nt  satellites  may  likely  cause  low  success  probability  of  ambiguities, thus affecting the reliability of the RTK solutions
