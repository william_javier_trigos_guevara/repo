
def read_kmz_file2(fname, max_distancia):
    import sys
    import zipfile
    import glob
    from xml.dom import minidom

    #fname = "sites.kmz"
    path_cnt = 0
    zf = zipfile.ZipFile(fname, 'r')

    #print zf, type(zf)
    for fn in zf.namelist():
        #print fn
        if fn.endswith('.kml'):
            content = zf.read(fn)
            xmldoc = minidom.parseString(content)
            placemarks = xmldoc.getElementsByTagName('Placemark')

            nodes = {}
            for placemark in placemarks:
                nodename = placemark.getElementsByTagName("name")[0].firstChild.data
                coords = placemark.getElementsByTagName("coordinates")[0].firstChild.data
                lst1 = coords.split(",")
                longitude = float(lst1[0])
                latitude = float(lst1[1])
                nodes[nodename] = (latitude, longitude)

            from itertools import combinations
            #print nodes
            parejas = [{j: nodes[j] for j in i} for i in combinations(nodes, 2)]

            d_min = 1e8

            GPS_Stations = {}
            data_map = {}
            c = 0
            for n in parejas:
                #print n, type(n), n.keys()[0], n.values()[0]
                rx_1 = n.values()[0]
                rx_2 = n.values()[1]
                #print rx_1, rx_2
                d,_ = get_dist(rx_1, rx_2)
                #if (d_min > d):
                data_map[str(n.keys()[0])] = rx_1
                data_map[str(n.keys()[1])] = rx_2
                    
                if (d < max_distancia):

                    print d_min, d, "entre", n.keys()[0], "y", n.keys()[1]
                    #d_min = d
                    GPS_Stations[c] = [n.keys()[0], n.keys()[1]]
                    c += 1
                    
    return GPS_Stations, data_map
    
def dict2kml(opath_kml, parejas, data_map):
    from os import (listdir, getcwd, system)
    from os.path import (basename, splitext, abspath,
                         dirname, split, isfile)

    dirname, zipname = split(opath_kml)
    fname, ext = splitext(zipname)
    
    header = '''<?xml version='1.0' encoding='UTF-8'?>
    <kml xmlns="http://earth.google.com/kml/2.2"
    xmlns:gx="http://www.google.com/kml/ext/2.2"
    xmlns:kml="http://www.opengis.net/kml/2.2"
    xmlns:atom="http://www.w3.org/2005/Atom">
    <Document>
       <name>%s</name>
       <Style id="line_label">
       <LabelStyle>
         <scale>1.3</scale>
       </LabelStyle>
       <LineStyle>
         <color>ff00ffff</color>
         <width>5</width>
         <gx:labelVisibility>1</gx:labelVisibility>
       </LineStyle>
       </Style>
    '''%(zipname)

    finish_header = '''</Document>\n</kml>'''


    with open(opath_kml, 'w') as f:
        
        f.write(header)
        # Create objects into map
        for pareja_id, pareja in parejas.iteritems():
            
            dat = {station:v for station in pareja 
                   for i, (k,v) in enumerate(data_map.iteritems()) 
                   if k.lower()==station.lower()}
            
            #print dat

            # Placemarks
            for station_name, coords in dat.iteritems():
                f.write("<Placemark>\n")
                f.write(" <name>%s</name>\n"%(station_name))
                f.write(" <description>%s</description>\n"%(station_name))
                f.write(" <Point>\n")                              # lon,    lat,       alt
                f.write("  <coordinates> %s, %s, %s </coordinates>\n"%(coords[1], coords[0], 0))
                f.write(" </Point>\n")
                f.write("</Placemark>\n")
            """   
            dat = {i:v for i, station in enumerate(pareja) 
                   for (k,v) in data_map.iteritems()
                   if k.lower()==station.lower()}
            print dat
            """
            # Lines

            for station in pareja[1:]:
                for k,v in data_map.iteritems():
                    if k.lower()==station.lower():
                        rx_1 = dat[pareja[0]]
                        rx_2 = dat[k]
                        distance,_ = get_dist(rx_1, rx_2)
                        #print rx_1, rx_2, distance
    
                        f.write("<Placemark>\n")
                        distance,_ = get_dist(rx_1, rx_2)
                        f.write("   <name> %0.2f [m]</name>\n"%(distance))
                        #f.write("   <description>dist_%s-%s</description>\n"%(pareja[0], k))
                        f.write("   <styleUrl>#line_label</styleUrl>\n")
                        f.write("   <LineString>\n")
                        f.write("     <tessellate>1</tessellate>\n")
                        f.write("     <coordinates>%s,%s,%s %s,%s,%s</coordinates>"
                                %(rx_1[1],rx_1[0],0, rx_2[1],rx_2[0], 0))
                        f.write("   </LineString>\n")
                        f.write("</Placemark>")

        # finish kml file
        f.write("\n"+finish_header)