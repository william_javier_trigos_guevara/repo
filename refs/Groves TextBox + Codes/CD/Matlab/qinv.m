function qinv = qinv(q)
% --------------------------------------------------
% Inverse quaternion
%
% Author:   Harald N�kland
% Date:     June 2011
% --------------------------------------------------
q(2:4) = -q(2:4);
qinv = q;
