clc;
close all;

h     = 0.01;               % Sampling frequency [sec] 
N     = size(act_q,1);      % Number of samples 

est_q    = zeros(N,4);
act_e    = zeros(N,3);
est_e    = zeros(N,3);
est_deps = zeros(N,3);
est_bgyr = zeros(N,3);
mes_pos  = zeros(N,3);
est_pos  = zeros(N,3);
mes_vel  = zeros(N,3);
est_vel  = zeros(N,3);
est_bacc = zeros(N,3);

old_gps_age = inf;
old_prs_age = inf;

tic
for i = 1:N
    
    % IMU input
    gyr = y_imu_gyr(i,:)';
    mag = y_imu_mag(i,:)';
    acc = y_imu_acc(i,:)';
    
    % GPS input
    gps_llh = y_gps_llh(i,:)';
    gps_vel = y_gps_vel(i,:)';
    gps_age = y_gps_age(i);
    gps_fix = y_gps_fix(i); % && (i<2000 || i>7000);
    
    % Check for new data
    new_imudata = 1;
    new_gpsdata = gps_age < old_gps_age;
    old_gps_age = gps_age;
  
    % Flags
    init = (i==1);
    valid_mag = new_imudata;
    valid_acc = new_imudata;
    valid_pos = new_gpsdata && gps_fix;
    valid_vel = new_gpsdata && gps_fix;
    
    % Magnetometer test
    % Accelerometer test
%     if norm(acc) > 9.81*1.01
%         valid_acc = 0
%     end
%     if norm(acc) < 9.81/1.01
%         valid_acc = 0
%     end
    
    % Transform GPS coordinates to NED coordinates
    if init, llh0=gps_llh; end
    ecef0 = llh2ecef(llh0);
    ecef  = llh2ecef(gps_llh);
    gps_pos = Rll(llh0(1),llh0(2))'*(ecef-ecef0);
    
%     % Attitude & Position - Kalman filter 
%     [q deps bgyr pos vel bacc] = AP_MEKF(gyr, mag, acc, gps_pos, gps_vel,...
%         init, 1*valid_mag, 1*valid_acc, 1*valid_pos, 1*valid_vel);

%     % Attitude & Position - Kalman filter qmethod
%     [q deps bgyr pos vel bacc] = AP_MEKF_qmethod(gyr, mag, acc, gps_pos, gps_vel,...
%         init, 1*valid_mag, 1*valid_acc, 1*valid_pos, 1*valid_vel);

    % Attitude & Position - HuaMahony
    [q bgyr pos vel] = AP_HuaMahony(gyr, mag, acc, gps_pos, gps_vel,...
        init, 1*valid_mag, 1*valid_acc, 1*valid_pos, 1*valid_vel);
    deps = [0 0 0]';
    bacc = [0 0 0]';
    
%     % Attitude - Kalman filter
%     [q deps bgyr] = A_MEKF(gyr, mag, acc,...
%         init, 1*valid_mag, 1*valid_acc);
%     pos = [0 0 0]';
%     vel = [0 0 0]';
%     bacc = [0 0 0]';

%     % Attitude - Kalman filter
%     [q bgyr] = A_EKF(gyr, mag, acc,...
%         init, 0*valid_mag, 0*valid_acc);
%     deps = [0 0 0]';
%     pos = [0 0 0]';
%     vel = [0 0 0]';
%     bacc = [0 0 0]';


   
    % Store values
    est_q(i,:)    = q';
    act_e(i,:)    = 180/pi*q2euler(act_q(i,:))';
    est_e(i,:)    = 180/pi*q2euler(q)';
    est_deps(i,:) = deps';
    est_bgyr(i,:) = bgyr';
    mes_pos(i,:)  = gps_pos';
    est_pos(i,:)  = pos';
    mes_vel(i,:)  = gps_vel';
    est_vel(i,:)  = vel';
    est_bacc(i,:) = bacc';
    
end
toc/N

run 'plot/euler_corrector'
euler_rms = RMS(est_e, act_e)
pos_rms = RMS(est_pos, act_pos)
vel_rms = RMS(est_vel, act_vel)

t  = (0:N-1)*h;
%run 'plot/plot_deps'
run 'plot/plot_quat'
run 'plot/plot_euler'
run 'plot/plot_bias_gyro'
run 'plot/plot_pos'
run 'plot/plot_vel'
run 'plot/plot_bias_acc'

