function q = qmethod(a1, a2, r1, r2, b1, b2)
% ------------------------------------------------------------------------
% The q-method computes the unit quaternion q = [eta eps1 eps2 eps3]'
% from a set of vector measurements {b1 b2} in the body frame. The
% set {r1 r2} is the corresponding reference vectors in the reference 
% frame, and {a1 a2} are weights. It is the solution to an optimization
% problem of minimizing the objective function:
%
% f(q) = a1*norm(r1-R(q)*b1) + a2*norm(r2-R(q)*b2)
%
% Input:
% a - weight
% r - reference vector   (NED)
% b - observation vector (BODY)
%
% Author:   Harald N�kland
% Date:     June 2011
% ------------------------------------------------------------------------
Z = a1*cross(r1,b1) + a2*cross(r2,b2);
B = a1*r1*b1' + a2*r2*b2';
S = B + B';
sigma = trace(B);

% Compute the K matrix
K = [ -sigma     Z'
       Z        -S+sigma*eye(3) ]; 

% Find the eigenvector for the smallest eigenvalue of K
[V,E] = eig(K);
[m,i] = min([E(1,1) E(2,2) E(3,3) E(4,4)]);
q = V(:,i);
q = q/norm(q);
