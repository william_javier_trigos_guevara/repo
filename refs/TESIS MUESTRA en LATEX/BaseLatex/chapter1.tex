% Chapter 1: Introduction to GPS

\bigskip
\begin{quote}\emph{
No matter where you go, there you are.
\small
\begin{tabbing}
  \hspace{117pt}%
  --- \= Buckaroo Banzai\\
\end{tabbing}
}\end{quote}

This chapter introduces the Global Positioning System (GPS) and the technical operation and applications of GPS receivers.

\section{The Global Positioning System}

The Global Positioning System, or GPS, is the world's first Global Navigation Satellite System (GNSS). A constellation of more than 24 satellites in medium earth orbit continuously broadcasts radio frequency signals which allow GPS receivers to determine their position, velocity, and time.

GPS receivers are becoming ubiquitous. Although best known as navigational aides for drivers and recreational hikers, GPS receivers are quickly becoming embedded in consumer technology such as cell phones, small electronic hand-held organizers, and, recently, wrist watches. As the integrated circuits for GPS receivers shrink in size and require fewer external components, we can expect GPS receivers to become increasingly embedded in our everyday experience.

\subsection{Types of GPS Receivers}

\noindent The term ``GPS receiver'' covers a wide range of hardware and software forms:

\begin{description}
    \item[Consumer receivers] are inexpensive (typically under US~\$200) GPS receivers that include a display screen, input device (e.g., a keyboard), and an integrated microwave patch antenna.
    \item[GPS receiver boards] are original equipment manufacturer (OEM) boards that are intended for applications requiring positioning or time data but not a direct user interface. Consumer receivers include GPS board functionality as well as user interface hardware.
    \item[Software receivers] (often known as ``software defined radios'') minimize hardware by using software to do the majority of signal processing.
\end{description}

In this thesis, the term ``GPS receiver'' will refer to a printed circuit board with all the electronic components necessary to receive and decode GPS signals but no user interface or extraneous hardware. GPS receivers have only a GPS chipset, a general purpose processor, memory (ROM and RAM), and the radio frequency components necessary to operate the receiver. These receivers are also known as ``OEM receivers'', ``GPS sensors'', and even ``GPS engines''.

\subsection{Radio Frequency Trilateration}

Triangulation is the most common form of beacon-based position determination. Measuring the angles between a receiver and three beacons at known positions allows a receiver to determine its location. How accurately the receiver's location can be determined depends on how accurately the receiver can measure the angles involved.

GPS works by trilateration; knowing the distance to three beacons at known locations allows the receiver to determine its position. The accuracy of trilateration depends upon how accurately the receiver can determine the distance to the beacon. In the case of GPS, each satellite carries an atomic clock to synchronize the broadcast of a microwave signal. When the signal is received, the arrival time is recorded and compared to the time of transmission. The time between the transmission and reception of the broadcast is the ``time of flight'': multiplying the time of flight by the propagation speed (in this case, the speed of light) provides the receiver with the distance to the beacon. Consequently, for a time-of-flight, trilateration-based positioning system, accuracy is limited by how accurately the receiver can measure time.

Measuring the time of flight requires that the receiver's and the beacon's clocks are precisely synchronized. Since radio frequency signals propagate at roughly the speed of light (about 0.3~m per nanosecond), meter-level positioning accuracy requires timing with nanosecond accuracy. GPS satellites and their ground-based control centers solve this problem with redundant atomic clocks. Unfortunately it's not feasible to have atomic clocks in GPS receivers due to their expense and size. However, a GPS receiver's inexpensive, temperature-dependent, frequency-drifting, and noisy crystal-based clock can be quickly and continuously synchronized with the navigation system's time by adding time as a fourth unknown to the navigation problem. Solving this four dimensional (X, Y, Z, t) problem requires at least four independent measurements. With one measurement per satellite, a stand-alone GPS receiver must receive signals from four satellites, not three, to synchronize its clock and acquire position.

If the receiver is given more information, fewer satellites are necessary to solve the position equation. For example, GPS receivers with altimeters can navigate with only three satellites since their position is now three dimensional (X,Y,t). And GPS receivers with very stable clocks can navigate with only three satellites until their clock drift gives unacceptable errors.

\subsection{Satellites: The Space Segment}

The core of GPS is a constellation of 24 satellites in medium earth orbit. Developed and deployed by the United States government, these GPS satellites are the ``Space Segment'' of the Global Positioning System.

The 24 satellites (or more, including in-orbit spares) are equally distributed in six longitudinally distributed orbital planes, each inclined 55 degrees to the equator at an altitude of 20,335~km (Figure~\ref{fig:constellation}). Depending on latitude, an average of 5 to 7 satellites are visible 5 degrees above the horizon at any given time \cite{bluebook}. Because the beamwidth of the GPS signal extends about 7.4 degrees past the limb of the earth, and because of unavoidable back propagation of the satellites' antennas, GPS can also be used in low and medium earth orbit \cite{oscar}.

\begin{figure}[h] % place the figure right here
    \begin{center}
          \includegraphics[keepaspectratio,width=0.33\linewidth]{pictures/gps_constellation.eps}
        \caption{The GPS constellation (reproduced from \cite{strang}).}
        \label{fig:constellation}
    \end{center}
\end{figure}

Each GPS satellite carries redundant Cesium and Rubidium atomic frequency standards (AFS) which steer a precision 10.23~MHz oscillator. The AFS, along with adjustments from the GPS ground controllers, keep the 10.23 MHz time base to within 6 ns of the GPS system time. This accuracy is required to keep systemic timing errors below 1.2~m. The satellites also use this precision time base to control the frequency of their broadcast signals: the GPS uses the L1 frequency of 1.57542~GHz, which is 154 times the 10.23~MHz timebase.

\subsection{GPS Signal Description}

GPS satellites use code division multiple access (CDMA) to share the L1 frequency between multiple satellites. Specifically, GPS uses Gold codes, a class of nearly orthogonal codes which can be used for CDMA transmissions due to their low cross-correlation. Each orbital satellite is assigned a pseudorandom index number (PRN) which denotes its Gold code and uniquely identifies the satellite. GPS satellites also have satellite vehicle designations, but are most often referred to by their PRN \cite{bluebook}.

The Gold code sequence is periodic therefore each symbol conveys a negligible amount of information. This is why the Gold code symbols are called ``chips'', rather than bits. Each chip of the 1,023~chip GPS Gold code sequence is binary phase shift keyed (BPSK) onto the L1 carrier at a chip rate of 1.023~MHz making the sequence repeat once each millisecond. Modulated on top of this 1.023~MHz signal is a 50 bit-per-second message that is also binary phase shift keyed onto the carrier. This 50~bps signal contains a 1,500~bit navigation message which includes the satellite's health, clock corrections, a detailed description of its orbit (ephemeris data), and a  general orbital description of the entire GPS constellation (almanac data). Using the ephemeris data, the receiver can calculate where the satellite was (to $<$~0.5~m) at the time of transmission. Using the almanac data and the receiver's current position, the receiver can predict which satellites may be currently visible.

The satellite signals are broadcast at 50~W, but are attenuated by distance and the atmosphere to a guaranteed minimum level of a few microvolts (-130~dBm) at the Earth's surface. Since this is below the typical noise floor of roughly -90~dB, GPS signals are ``under the noise floor'' and can only be recovered by using noise rejecting techniques. For conventional GPS receivers, a local copy of the Gold code sequence is correlated with the received signal to recover the original signal while rejecting un-correlated noise \cite{bluebook}.

\section{GPS Receivers: the User Segment}

A conventional GPS receiver, part of the GPS ``user segment'', requires five components: an antenna, a radio frequency front end (RF front end), an analog to digital converter (ADC), a correlator with at least four channels (although in some cases this can be done in software), and a general purpose processor.

\subsection{Antenna}

Antennas for GPS receivers must acquire the right-hand circularly polarized signals from the satellites while minimizing multipath signals. Multipath signals are those signals which have been `bounced' off of local features, increasing their propagation path length and thus changing their phase. These multipath signals `smear' the phase of GPS signals, making precise phase tracking difficult and thus adding noise to the receiver's position measurements.

\subsection{Prefilter and LNA}

The 1.57542~GHz signal from the antenna is typically prefiltered and amplified using a low noise amplifier (LNA) on the receiver board.

\subsection{RF Front End (Down-converter and ADC)}

Typically, GPS receivers have a ``radio frequency front end'' chip that down-converts and digitizes the 1.57542~GHz carrier into a signal a few megahertz wide. For example, the Zarlink GP2015 RF front end (Figure~\ref{fig:frontend}) uses a 10~MHz temperature-controlled crystal oscillator (TCXO) as the local oscillator for a three-stage down-converter. After down-conversion, the resulting 4.3~MHz signal is over-sampled by a two bit flash analog to digital converter at a sample rate of 5.7~MHz. This aliases the digitized signal, resulting in a final output frequency centered around 1.4~MHz. The 5.7~MHz digitized stream is sent to a correlator chip to be further processed.

\begin{figure}[h!] % place the figure right here
\begin{center}
  \includegraphics[keepaspectratio,width=0.75\linewidth]{pictures/gp2015_block_diagram.eps}
  \caption{Block diagram of the Zarlink GP2015 GPS front end (reproduced from \cite{gp2015}).}
  \label{fig:frontend}
\end{center}
\end{figure}

\subsection{GPS Correlator}

Conventional GPS receivers are usually able to track 6--12 satellites. Each tracked satellite requires a `channel', which is a set of correlation hardware that includes a Gold code generator, binary multipliers, and two digitally controlled oscillators (DCO)s, one for the carrier frequency generator and one for the Gold code generator.

The purpose of each channel is to replicate a satellite's Gold code sequence and multiply it with the incoming signal. The summation of this product (or correlation) is very low if the code phase and frequency of the local replica doesn't match the received signal's phase and frequency. However, if the signals are synchronized, the correlation value spikes and the signal is detected. An example of a one-dimensional correlation in code phase only is shown in Figure~\ref{fig:correlation-1d}, where the X axis is the relative phase of the two Gold code sequences.

\begin{figure}[p] % place the figure on a separate page
    \begin{center}
        \includegraphics[keepaspectratio,width=0.4\linewidth]{pictures/GoldCode-correlation.eps}
        \caption{One dimensional (code phase only) Gold code correlation.}
        \label{fig:correlation-1d}
    \end{center}
\end{figure}

\begin{figure}[p] % place the figure on a separate page
    \begin{center}
        \includegraphics[keepaspectratio,width=0.4\linewidth]{pictures/contour.eps}
        \caption{Two dimensional (code phase and carrier frequency) Gold code correlation: contour map.}
        \label{fig:correlation-countour}
    \end{center}
\end{figure}

\begin{figure}[p] % place the figure on a separate page
    \begin{center}
        \includegraphics[keepaspectratio,width=0.4\linewidth]{pictures/mtdoom.eps}
        \caption{Two dimensional (code phase and carrier frequency) Gold code correlation: 3D plot.}
        \label{fig:mt-doom}
    \end{center}
\end{figure}


The search for GPS signals --- `acquiring' the satellite signal --- is a two dimensional search problem. First, the variable distance to the transmitting satellite causes an unknown phase shift in the incoming signal's Gold code. Second, the satellite vehicle's motion relative to the user causes an unknown Doppler shift in the carrier frequency. There are also second and third order effects on the code frequency due to dispersion of the signal in the ionosphere, but these smaller effects can safely be ignored during acquisition.  Figures~\ref{fig:correlation-countour} and \ref{fig:mt-doom} show full two dimensional correlation in code phase and carrier frequency.

For a practical example, a channel in the Zarlink GP4020 baseband correlator (Figure~\ref{fig:channel}) uses a carrier frequency DCO to match the incoming carrier frequency and a code generator (run by a separate code frequency DCO) to match the incoming code phase and frequency. Each correlator channel has four accumulators, which are registers meant to hold the correlation's sum of products. Two of the registers accumulate the in-phase component ($I$) of the incoming signal, and two registers accumulate the quadrature component ($Q$) of the incoming signal. These accumulators are further divided into ``prompt'' and ``tracking'' sets. The prompt accumulators ($I_{prompt}$ and $Q_{prompt}$) follow the incoming signal as closely as possible. The tracking accumulators ($I_{tracking}$ and $Q_{tracking}$), generate a signed error signal by subtracting an `early' version of the code phase from a `late' version of the code phase.

\begin{figure}[h] % place the figure right here
    \begin{center}
        \includegraphics[keepaspectratio,width=0.75\linewidth]{pictures/gp4020_channel_block.eps}
        \caption{Block diagram of a channel in the Zarlink GP4020 (reproduced from \cite{gp4020}).}
        \label{fig:channel}
    \end{center}
\end{figure}

GPS receivers usually require a general purpose processor to control the channel's DCOs. This control loop is the processor's most time-intensive task, requiring typically 20\% to 50\% of the processor's bandwidth \cite{tsui}. After every full Gold code cycle (every 1~ms), the processor must check the accumulators. If searching for satellites, the processor analyzes the values in the accumulators and decides whether to continue scanning code phase and carrier frequency for satellites, or to try and ``pull in'' a possible candidate signal. Once a satellite signal is found, the processor must run two control loops:

\begin{enumerate}
    \item Monitor the error signal of the tracking accumulators every code cycle in order to control the code frequency DCO.
    \item Maximize the value of the in-phase ($I$) prompt accumulator and minimize the quadrature phase ($Q$) prompt accumulator over some integration period in order to adjust the carrier frequency DCO.
\end{enumerate}

Figure~\ref{fig:tracking-loops} shows a generic software tracking loop: toward the top of the figure, the code generator generates two Gold code sequences, one a half-chip early and one a half-chip late in phase. Subtracting the sum of products from these two phase-shifted codes produces an error signal which is averaged, summed and used to control the code generator. The resulting prompt, or in-phase code is multiplied against the input signal to remove the Gold code from the input to the the carrier tracking loop. The carrier loop generates a phase-locked carrier signal by maximizing the in-phase ($I$) component and minimizing the quadrature ($Q$) phase of the signal. This carrier signal is then used to remove the carrier from the code for the code loop.

\begin{figure}[h] % place the figure right here
    \begin{center}
        \includegraphics[keepaspectratio,width=0.75\linewidth]{pictures/tsui_tracking.eps}
        \caption{Tracking loops for a generic software receiver (reproduced from \cite{tsui}).}
        \label{fig:tracking-loops}
    \end{center}
\end{figure}

The control of the DCOs can be done with a frequency-locked loop (FLL) during pull-in of a signal and a phase-locked loop (PLL) during lock. The FLL allows a wider capture range for acquiring the signal, but does not track the phase closely enough for the precision phase alignment necessary for accurate time-of-flight measurements. Switching to the PLL after pull-in allows the correlators to directly match the signal's phase.

\subsection{Extracting the Navigation Message}

Every 20~ms (50~Hz) the satellite binary phase shift keys another satellite navigation message bit onto the GPS signal. If the navigation message bit changes, there is a subsequent sign change in the output of the correlator's accumulators. The receiver's processor looks for these sign flips, synchronizes to a likely bit edge, and decodes them into a 1,500 bit satellite navigation message. The navigation message is broken into words of 30 bits (0.6~s long), subframes of 10 words (6~s long), and frames of 5 subframes (30~s long). The full satellite almanac data is distributed over multiple frames, called a superframe, which repeats every 12.5 minutes. The processor must check the words for errors using a Hamming block code (32 total bits with 26 information bits), assemble them into subframes, and extract the message data from a packed bit field.

Once subframes one through three have been assembled, the complete ephemeris is available to the receiver. With only a rough idea of the system time (down to a few milliseconds), the ephemeris enables the receiver to calculate the satellites position and, if the receiver's approximate position is known, the azimuth and elevation from the receiver to the satellite. The azimuth and elevation can then be used to correct the satellite's signal propagation time using generic ionospheric and tropospheric models. This model can be further refined with increasingly precise coefficients that are uploaded by the control segment and inserted into subframe four.

\subsection{Taking Measurements}

A bank of channels is usually referred to as a GPS correlator (Figure~\ref{fig:correlator}). The correlator takes an instantaneous sample of each channel's code phase, code DCO phase, and the carrier phase. This instantaneous sample, sometimes called a `snapshot', contains the timing information to make a precise measurement of how far away each satellite under observation was at the moment of measurement.

\begin{figure}[h] % place the figure right here
    \begin{center}
        \includegraphics[keepaspectratio,width=0.75\linewidth]{pictures/gp4020_correlator_block.eps}
        \caption{Zarlink GP4020 correlator peripheral block diagram.}
        \label{fig:correlator}
    \end{center}
\end{figure}

Since all the satellites transmit the navigation messages at precisely the same time, the variation in the received message's total phase is a measure of the signal propagation time. This phase is directly measured as the bit number of the message, the chip number of the Gold code generator, and the phase of the Gold code generator. For example, with the Zarlink GP4020 the satellite vehicle time is calculated as:

\begin{equation}
t_{tx} = ( B \times 0.2~\textrm{s}) + (G \times 1~\textrm{ms}) + (C \times 488.758~\textrm{ns}) + (\phi_c \times 477.3~\textrm{ps}),
\label{eq:satellite-time}
\end{equation}

\noindent where $t_{tx}$ is the time of transmission from the satellite, $B$ is the number of 20~ms long bits since the the start of the GPS week (weeks are the largest time epoch in the GPS and begin every Sunday morning at 00:00), $G$ is the number of 1~ms Gold code cycles since the last data bit transition, $C$ is the number of Gold code chips since the last cycle (counted in half chips), and $\phi_c$ is the phase of the code frequency DCO which has 1,024 counts per half chip.

\subsection{Calculating Pseudoranges}

The local clock of the GPS receiver is set to within roughly 100~ms of the system time after receiving any valid navigation message subframe, since all subframes are timestamped from the beginning of the GPS week. Given this rough synchronization, the receiver can now calculate the ``pseudorange'' to the satellite, which is the apparent time-of-flight of the signal measured without a precisely synchronized clock:

\begin{equation}
\rho_i = t_R - (t_T + \delta t_i) * c - b,
\label{eq:pseudorange}
\end{equation}

\noindent where $\rho_i$ is the pseudorange from the satellite to the receiver measured in meters, $t_R$ is the time of the measurement, $t_T$ is the time of transmission based on equation~(\ref{eq:satellite-time}), $\delta t_i$ is the satellite clock correction factor (including terms for satellite clock drift correction, atmospheric modeling, and relativistic corrections for the satellite's motion), $c$ is the speed of light in a vacuum, and $b$ is the unknown clock bias expressed in units of distance.

With four pseudoranges known, the receiver can solve for the three position coordinates as well as the clock bias term. The clock bias term is applied to the receiver's local clock, precisely synchronizing it with GPS time.

\subsection{Calculating Position and Time}

In order to use the four pseudoranges for positioning, the location of each satellite must be known. This can be calculated to sub-meter accuracy using the precise ephemeris data obtained from the navigation messages and a standard orbital model.

When four pseudoranges and four satellite positions are available from a given correlator measurement, a position-time solution can be found using a least squares method. The fundamental positioning equation is a simple statement of Euclidean geometry:

\begin{equation}
\rho_i = \sqrt{\bf \vec x_i - \vec x_r } + b,
\label{eq:range}
\end{equation}

\noindent where $\rho_i$ is the pseudorange from the receiver location $\bf \vec x_r$ to the location of the $i_{th}$ satellite $\bf \vec x_i$. Like equation~(\ref{eq:pseudorange}), the unknown clock bias $b$ has been included.

Although straightforward in principle, this equation has several problems in application. The first is the need to invert it and solve for $\bf \vec x_r$. The resulting non-linear vector equation is usually linearized around an estimated solution, and then iterated until the error drops below a tolerable accuracy.

\subsection{Carrier Phase Velocity and Position}

Because the carrier frequency DCO is locked in phase with the incoming signal, a very accurate measurement can be made of the change in phase of the GPS carrier. This change in phase is the number of 19~cm wavelengths of the 1.57542~GHz L1 carrier passing per unit time, and is an independent measurement of the relative velocity between satellite and receiver. Tracking carrier phase, or ``accumulated delta range'', is a very accurate method of determining receiver velocity \cite{bluebook}.

If the integer number of carrier cycles between the satellite and the receiver can be determined, then carrier phase can be used for precise positioning. Also known as the integer ambiguity problem, solving the integer carrier phase cycles usually requires position aiding such as differential GPS (discussed below in Section \ref{sec:specialized-apps}) for single frequency receivers.

\subsection{Filtering Position Output}

Covering the gamut from simple double differenced values through Kalman filtering, position filtering allows the receiver to reduce sample-to-sample errors in position. Filtering is an entire extended GPS topic in itself, and will not be covered in this thesis.

\subsection{GPS Receiver Software}

To further understand the operation of a GPS receiver, it is helpful to break down the previous receiver description into a series of generic tasks that the processor must perform independent of the underlying hardware (see also Figure \ref{fig:generic-data-flow}):

\begin{description}
    \item[Tracking task] --- a high repetition rate, ($<$ 1~ms) high priority task that reads the accumulators and adjusts the DCOs to acquire and track the satellite signals for each channel. This task also demodulates bits from satellite navigation messages.
    \item[Satellite navigation message task] --- a medium frequency, medium priority task which assembles the bits from the satellite navigation message found by the tracking task. The message is checked for errors, assembled into subframes, and the ephemeris and almanac data extracted.
    \item[Positioning task] --- a low frequency, preemptible task that uses measurements and ephemerides to calculate position and time.
    \item[Housekeeping task] --- a very low frequency, low priority task which takes care of miscellaneous chores, such as allocating satellites to correlator channels, managing external communication channels, etc.
\end{description}

\begin{figure}[h] % place the figure right here
    \begin{center}
        \includegraphics[keepaspectratio,width=0.75\linewidth]{pictures/generic_data_flow.eps}
        \caption{Generic GPS receiver tasks.}
    \label{fig:generic-data-flow}
    \end{center}
\end{figure}

\subsection{GPS Receiver Performance}

% 10 m see Tbl.2 v1p.481 but note p.213 cited just below.
Current GPS receivers have horizontal position accuracy of around 10~m (1~$\sigma$) \cite{bluebook}. The spherical error probable (the 3D sphere in which 50\% of the position estimates falls within), or SEP, is also 10~m. The system ``user range error'' (URE) error budget is shown in Table~\ref{tab:errorbudget}.

\begin{table}[h]
    \centering
    \begin{smalltabular}{lc}
        Error Sources   & 1$\sigma$ User Range Error (URE) [m] \\
        \hline
        Space Segment \\
        \ \ Clock Stability                 & 3.0 \\
        \ \ L-Band phase uncertainty        & 1.5 \\
        \ \ SV parameter predictability     & 1.0 \\
        \ \ Other                           & 0.5 \\
        Control Segment \\
        \ \ Ephemeris prediction and model  & 4.2 \\
        \ \ Other                           & 0.9 \\
        User Segment \\
        \ \ Ionospheric delay               & 2.3 \\
        \ \ Tropospheric delay              & 2.0 \\
        \ \ Receiver noise/resolution       & 1.5 \\
        \ \ Multipath                       & 1.2 \\
        \ \ Other                           & 0.5 \\
        \hline
        TOTAL (RSS) URE                     & 6.6 \\
        \end{smalltabular}
    \caption{GPS error budget (from \cite{bluebook}).} % Bluebook page 213
    \label{tab:errorbudget}
\end{table}

Ionospheric refraction and multipath errors are the main error sources in a modern GPS receiver. Antenna design and correlator timing can help with multipath signals, and models of the ionosphere can help subtract some of the ionospheric delay errors. To completely take into account these effects, however, requires external aiding (see Section \ref{sec:specialized-apps}).

\section{GPS Receivers: Applications}

Commercially available GPS receivers fall into general categories, based on the application's demands.

\subsection{Standard GPS Applications}

Commercial GPS receivers are usually intended to provide position, velocity and time (PVT) updates once a second in an ASCII encoded message. Other data, such as satellite ephemeris and pseudoranges, are difficult, or impossible, to obtain from standard commercial receivers, and if available, are usually formatted in a propriety binary protocol. The ASCII encoded message, usually a National Marine Electronics Association (NMEA) standard, is either stored for future processing or is used by an external processor. These standard 1~Hz PVT messages are applicable for low dynamic conditions such as handheld receivers, in-vehicle navigation aides, and most commercial aircraft receivers.

\subsection{Specialized GPS Applications}
\label{sec:specialized-apps}

Non-standard applications require specialized code and, often, specialized receiver hardware. There are many off-the-shelf commercial receivers that are designed for specialized applications:

\begin{description}
    \item[Timing] Specialized GPS receivers exist which are intended only to synchronize a local precision clock with GPS time. Such time transfer systems can be within a few tens of nanoseconds of GPS time, which is kept within a microsecond of Coordinated Universal Time (UTC), plus or minus an integer number of leap seconds.
    \item[High Dynamics] Most GPS receivers have an internal model of their dynamics; e.g., they have filtered position and velocity estimates which assume bounded dynamic movement (acceleration and velocity) and smoothness assumptions. These assumptions may cause the receiver to lose lock in high dynamic environments. High dynamic receivers, meant for military aircraft or launch vehicles, cost upwards of thousands of dollars and often need to be finely tuned for the end vehicle's dynamics.
    \item[DGPS] A stationary receiver at a surveyed location can derive propagation-time corrections for each visible satellite to compensate for ionospheric and tropospheric delays. These correction signals, called ``differential GPS corrections'' (DGPS) can then be broadcast to local (within 100~km) roving receivers. These pseudorange corrections can give standard GPS receivers sub-meter accuracy.
    \item[RTK] DGPS coupled with carrier phase positioning (carrier phase tracking with the integer ambiguity solved) is often called ``real time kinematic'' positioning and is used for centimeter-level accurate land surveying.
    \item[E911] Cellular phones with embedded GPS receivers enable position-based emergency services (as well as covert surveillance) with extremely low satellite signal strengths (e.g., -160 dBm), which enables these services to be used inside buildings. The GPS time and ephemeris are transmitted to the chipset from the cellular base station, allowing the correlators to be directly set without having to search for signals. The provided ephemeris data also means the correlators do not have to decode the satellite navigation messages, letting them instead concentrate on tracking the weak signals.
\end{description}

\subsection{Extended GPS Applications}

Many specialized GPS applications require custom modifications to the receiver software. Although some off the shelf products may work in these applications, they are usually extremely expensive, forcing most non-governmental users to create their own solutions. Some of these applications are:

\begin{description}
\item[Attitude Determination] Multiple GPS receivers with multiple antennas on a vehicle can be coordinated to produce an estimate of the heading (attitude) of the vehicle. This requires coordinated carrier phase tracking, where multiple receivers track the change in carrier phase caused by satellite and receiver motion.
\item[Integrated Navigation Systems] Integrating inertial, magnetic and other position and attitude sensors via data fusion (e.g., Kalman filtering) can greatly enhance the receiver's ability to track and improve positioning accuracy. This integration is almost always a custom solution that cannot be purchased off the shelf, with the exception of military systems which cost hundreds of thousands of dollars. Small unmanned aerial vehicles (UAVs) can greatly benefit from these kind of integrated navigation systems.
\item[Terrestrial Physics] A tremendous amount of information about the troposphere and ionosphere can be gleaned by observing how the GPS signals propagation through the atmosphere. This can be done by running the system ``in reverse'' --- a stationary receiver in a known position with precision satellite ephemerides calculates the signal delay in the atmosphere, which is a function of the total electron content in the atmosphere along with second order effects such as water content.
\item[Academic Applications] Academic research often requires stand-alone receivers running custom software. A programmable receiver board makes an excellent teaching tool, and is necessary for in-situ prototyping and testing of algorithms.
\end{description}

While there is little need for a GPS development system for standard GPS applications, there is no current solution that reduces the barriers to entry for extended applications. It is these extended GPS applications --- and those yet to be developed --- which will most benefit from an inexpensive and open GPS development system.








