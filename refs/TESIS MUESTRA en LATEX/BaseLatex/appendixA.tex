% Appendix A: Initial results

\section{Introduction}

As of May 2nd, 2005, GPL-GPS successfully calculates position and clock bias if four or more satellites have valid pseudoranges and ephemerides. However, without atmospheric corrections, a more robust locking algorithm, carrier phase positioning, and position filtering, the output position is very rough. Range errors are on order 100's of meters with infrequent excursions to 1,000's of meters.

To put the GPL-GPS results in context, 15 hours worth of data were recorded from a SigTec MG5001 receiver running with its commercial software. Later, 10 hours worth of data were taken on a MG5001 running GPL-GPS (May 13, 2005 CVS image).

A Connexant (now Navman) ``Jupiter'' GPS receiver board \cite{navman} was run using the same antenna. The positions from the Jupiter board were averaged to choose an independent measurement of the antenna position. The reference antenna position, within a few meters, is 45.47030$^\circ$ latitude, -122.62490$^\circ$ longitude, and 30~m altitude. In Earth Centered, Earth Fixed (ECEF) coordinates, that position is (-2,415,600~m, -3,773,550~m, 4,524,190~m). ECEF is a non-inertial right-handed reference frame with the origin at the center of the earth, the Z axis through the geodetic north pole, and the X axis through the zero meridian on the equator.

Unfortunately, the testing environment is far from ideal: Figure~\ref{fig:comparison-setup} shows the large trees blocking off a large fraction of the southern sky and most likely causing severe multipath interference. Future tests should be run at a site with a clearer view of the sky and less possibility of multipath interference.

\begin{figure}[h] % place the figure right here
    \begin{center}
        \includegraphics[keepaspectratio,width=0.8\linewidth]{pictures/setup.eps}
        \caption{Comparison testing setup: roof-mounted antenna, antenna splitter and two MG5001 receivers on development boards}
        \label{fig:comparison-setup}
    \end{center}
\end{figure}

\section{SigTec OEM Software Positioning Results}

The \texttt{\$GPGPQ,XYZ,1} command was sent to the MG5001 running the SigTec OEM software to produce 1~Hz ECEF coordinate messages. Surprisingly, the initial 500 points (approximately) of the ECEF position were an average of 26.6~km off of the antenna reference position (Figure~\ref{fig:sigtec-initial}).

\begin{figure}[h] % place the figure right here
    \begin{center}
        \includegraphics[keepaspectratio,width=0.5\linewidth]{pictures/sigtec-initial.eps}
        \caption{Initial ECEF positions of the SigTec OEM software}
        \label{fig:sigtec-initial}
    \end{center}
\end{figure}

\begin{figure}[p] % place the figure right here
    \begin{center}
        \includegraphics[keepaspectratio,width=0.5\linewidth]{pictures/sigtec-hairball.eps}
        \caption{54,747 ECEF positions at 1~Hz from the SigTec OEM software}
        \label{fig:sigtec-position}
    \end{center}
\end{figure}

\begin{figure}[p] % place the figure right here
    \begin{center}
        \includegraphics{pictures/sigtec-histogram.eps}
        \caption{Histogram of range to the sample mean for the SigTec OEM software}
        \label{fig:sigtec-histogram}
    \end{center}
\end{figure}

After the first 500 points, the SigTec software settled down and produce a more sane output (Figure~\ref{fig:sigtec-position}). The mean of the SigTec data set, minus the first 500 points, was (-2,415,601~m,  -3,773,553~m, 4,524,189~m) which is 3.4~m away from the antenna reference position. The maximum deviation from the sample mean was 94.3~m and the sample standard deviation was 7.9~m.

A histogram of the distance from each position to the data set average is shown in Figure~\ref{fig:sigtec-histogram}. One would think that the position set would be grouped near or on the mean, but the SigTec output seems to actively avoid the mean with a 12.8~m bias. A hypothesis to explain this behavior is that the output of the receiver tends to ``clump''; an average of clumps might not necessarily fall in a clump, which would explain why there are almost no data points at the mean.

\section{GPL-GPS Software Positioning Results}

GPL-GPS was run for 10.1 hr to collect 14,225 points. Removing the farthest outliers, similar to the commercial software test, produced a data set of 14,049 points which represents a loss of 176 points (1.2\% of the total sample). Since there is no position filtering and the solution validity is not checked, some outliers are to be expected.

\begin{figure}[h!] % place the figure right here
    \begin{center}
        \includegraphics[keepaspectratio,width=0.5\linewidth]{pictures/may13-1km.eps}
        \caption{GPL-GPS data bounded by a 1~km bounding box (98.8\% of points)}
        \label{fig:gplgps-box-1km}
    \end{center}
\end{figure}

The average of the data set was (-9.1521~m, 1.91896~m, 5.08915~m),  which is 10.7~m from the reference position. The mean of the data set from the average position is 41.7~m and the standard deviation is 80.3~m. Figure~\ref{fig:gplgps-box-1km} shows the data points with a 1~km bounding box. Note the trends (``offshoots'') in the data, most likely related to satellite geometry. Figure~\ref{fig:gplgps-box-100m} shows 90.5\% of the data points falling in a 100~m bounding box. The 100~m box shows that the points are clustered in a sphere-like fashion around the mean, as expected.

\begin{figure}[p] % place the figure right here
    \begin{center}
        \includegraphics[keepaspectratio,width=0.33\linewidth]{pictures/may13-100m.eps}
        \caption{GPL-GPS data bounced by a 100~m bounding box (90.5\% of points)}
        \label{fig:gplgps-box-100m}
    \end{center}
\end{figure}

\begin{figure}[p] % place the figure right here
    \begin{center}
        \includegraphics{pictures/may13-hist.eps}
        \caption{Histogram of range to the sample mean for GPL-GPS (1~km bounded data set)}
        \label{fig:gpl-gps-hist}
    \end{center}
\end{figure}

Figure~\ref{fig:gpl-gps-hist} shows a histogram of the 1~km GPL-GPS dataset. Note the data centered around the 10~m range error.

\section{Receiver Time Series}

Figure~\ref{fig:sigtec-time} shows a time series of the distance to the sample mean from the SigTec software. Clearly, some kind of filtering is taking places since the adjacent points are highly correlated.

\begin{figure}[h] % place the figure right here
    \begin{center}
        \includegraphics{pictures/sigtec-timegraph.eps}
        \caption{Time series graph from the SigTec OEM Software}
        \label{fig:sigtec-time}
    \end{center}
\end{figure}

Conversely, the GPL-GPS time series of the distance to the sample mean (Figure~\ref{fig:gpl-gps-time}) shows no filtering. Note the large gap in points (the horizontal lines) indicate a time with less than four satellites in lock.

\begin{figure}[h] % place the figure right here
    \begin{center}
        \includegraphics{pictures/gpl-gps-timegraph.eps}
        \caption{Time series graph from GPL-GPS}
        \label{fig:gpl-gps-time}
    \end{center}
\end{figure}

Figure~\ref{fig:gpl-gps-time-zoom} shows a zoom in of the GPL-GPS time series. 
 
\begin{figure}[h] % place the figure right here
    \begin{center}
        \includegraphics[keepaspectratio,width=0.75\linewidth]{pictures/gpl-gps-zoomedtimegraph.eps}
        \caption{Zoomed-in time series from GPL-GPS}
        \label{fig:gpl-gps-time-zoom}
    \end{center}
\end{figure}

\section{Comparison Conclusion}

While it is clear that the GPL-GPS positioning code needs further refinement, initial results are promising. Further improvements will be made with the addition of convergence checking, atmospheric modeling, carrier phase tracking, and position filtering.

