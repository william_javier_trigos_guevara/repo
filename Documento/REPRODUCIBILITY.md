### Como desarrollar codigo, ejecutar y redactar ideas
Notebooks 

### Como almacenar los datos
Dataframes y diccionarios

### Como restaurar los resultados de una ejecucion
HDF5

### Como automatizar la ejecución
Makefile o Scons

### Como generar el documento, distintos formatos
Nbconvert, pdflatex, jinja2

### Como poner disponible el experimento online
nbviewer o https://notebooks.azure.com/ https://tmpnb.org/ http://mybinder.org/