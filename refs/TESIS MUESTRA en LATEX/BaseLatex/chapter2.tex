% Chapter 2: Motivation and Principles behind GPL-GPS

This chapter describes the motivations and general design principles behind GPL-GPS.

\section{Motivations for an Open GPS Development System}

\subsection{The Embedded System Development Model}

A software developer starting a development project on a new embedded processor generally does the following:

\begin{enumerate}
    \item Orders a hardware development board, costing a few hundred dollars.
    \item Downloads the specifications of the chipset and the development board, which is usually published on the Internet.
    \item Chooses a software development system, most likely one they already own and use. Often example code is freely downloaded from the chip manufacturer's site, or increasingly often, from independent sites on the Internet devoted to sharing technical information.
    \item Develops software for which there are no intellectual property restrictions. They may share their code as they wish.
\end{enumerate}

Unfortunately, developing for GPS chipsets is not as seamless. A software developer starting a development project on a commercial GPS chipset --- in this example, one of the more popular chipsets --- does the following:

\begin{enumerate}
    \item Orders a hardware/software development kit for more than US \$20,000.
    \item Requests specifications on the chipset and development board, forcing them to sign a non-disclosure agreement prohibiting them from sharing their work on the chipset with others --- even coworkers --- who have not signed the NDA.
    \item Requests development tools, which are usually expensive, proprietary tools with libraries or code bases that only have an application programming interface (API) to a binary program, rather than source code. Obtaining the source code is another expense and license agreement.
    \item Develops code for the new GPS chipset, which they may not share with others given their NDA. Thus, the developer is forced into ``reinventing the wheel'' for their application since no software is available except  some generic binary interface provided by the chipset manufacturer.
\end{enumerate}

Why this is standard for an industry trying to sell chipsets --- not development systems --- baffles the GPS developer community. Developers assume that the chipset manufacturer is there to help, not hinder their use of a chipset. These restrictions also mean that the academic community has been all but barred from developing a teaching infrastructure because of the intellectual property restrictions on the development software. Clearly, a more streamlined and open GPS development environment, in line with the embedded development model, is needed.

\subsection{Debugging Critical Flight Hardware}

\subsubsection{Background: PSAS}

In October 2000, I directed a team of students that designed, constructed and launched an advanced sounding rocket avionics system for the Portland State Aerospace Society \cite{psas}. The vehicle, dubbed launch vehicle number 1b, or LV1b, flew to 3.6~km with a custom RISC microcontroller-based flight computer, a custom micro-electro-mechanical (MEMs) strap down inertial measurement unit, telemetry and uplink radio links, and a commercial GPS receiver board. The data from the sensors (including the GPS) was sent down a 900~MHz 19.2~kbps telemetry system and also stored in battery-backed up SRAM.

\subsubsection{PSAS LV1b GPS Flight Data}

The commercial GPS receiver's standard 1~Hz position data from the October 2000 flight is shown in Figure~\ref{fig:lv1bgps}. The receiver was locked and positioning correctly until the vehicle reached apogee and the recovery parachute was deployed. It was extremely disheartening to realize that after the recovery system deployment shock, the receiver gave grossly incorrect position data while asserting ``position locked and valid'' flags. Multiple calls to the manufacturer went unheeded, and there was no access to the receiver's software to discover what had happened. Later discussions with other GPS developers led to the hypothesis that the errors were due to a software bug in the receiver's Kalman filter. The need to fix this bug, along with the desire to create an integrated navigation system (GPS and inertial sensors), led us to realize that we needed to develop our own GPS software.


\begin{figure}[h] % place the figure right here
\begin{center}
  \includegraphics[keepaspectratio,width=0.5\linewidth]{pictures/lv1b_gps_data.eps}
  \caption{PSAS Launch Vehicle No. 1b (LV1b) GPS flight data from October 2000}
  \label{fig:lv1bgps}
\end{center}
\end{figure}

\section{Moving Towards Open Source Solutions}

This began my investigation into GPS software development. It became abundantly clear that no commercially available receiver was suitable for our project, and that no existing development system fit our needs due to budget and license restrictions.

We discovered, and became inspired by, Dr. Clifford Kelley's work on the OpenSource GPS (OSGPS) project. Kelley took an inexpensive commercial GPS receiver, ``hacked'' off the processor, and connected the correlator chip directly to a 486 PC using an ISA prototyping board. Kelley's work, published in 2002 \cite{kelley}, was the first open source GPS receiver software written. Although his receiver system couldn't easily be used in a power, space, and weight constrained environment like a launch vehicle, it did provide a code base from which to start an open source ``non-hacked'' receiver project.

In 2002, I proposed creating an open source GPS receiver project designed for OEM GPS boards. To my knowledge no one had yet proposed this. I began part time work on the idea starting in early 2003. I widely published the idea on the web and through the open source GPS mailing list which I formed in mid 2004. In July of 2004, Takuji Ebinuma started a parallel project by taking Kelley's OSGPS code and porting it to a commercial receiver. Although there were several problems with the project, his work became the world's first open source stand-alone GPS receiver board. Work began in earnest on GPL-GPS in late 2004, and the GPL-GPS's first position fix was on May 2nd, 2005.

Over the years, GPL-GPS was transformed from just open source software for GPS receivers into a complete GPS receiver development system. By encompassing open source methodologies, tool suites, operating systems, and open hardware designs, the GPL-GPS development system promises to provide an affordable and open GPS development environment available to all.

\section{GPL-GPS Design Principles}

GPL-GPS is based on the ``open source'' design philosophy: the more open a system is, the more dynamic and vibrant it becomes as it is adopted by multiple end users with different needs. Linux is a prime example of such an open system; thousands of people around the world have embraced and extended Linux from a small educational project to a modern, full featured desktop and enterprise-level operating system. GPL-GPS has five design principles:

\begin{description}
    \item[Open Source:] All software used in this project must be open source. This includes the application code, any operating system, and the software development tools. Using proprietary code or tools would be expensive, overly restrict end users, and require tedious and potentially litigious management of intellectual property.
    \item[Open Hardware:] The required hardware (the GPS chipset and receiver board) must have open and available documentation to avoid having to ``reverse engineer'' and/or ``hack'' hardware.
    \item[Portable:] The software design should be as modular and portable as possible; improvements to GPL-GPS code should be transferable to future GPL-GPS ports, as well as back to the OSGPS project. The development system should run on as many platforms (Linux, Windows, Macintosh) as possible.
    \item[Inexpensive] The hardware cost of the system must be as low as possible (a few hundred dollars, with a US \$300 upward limit) and be widely available.
    \item[Available] All software and documentation must be made available on the internet. Any developer with a bit of experience who is willing to read the documentation should be able to quickly and easily start development.
\end{description}

The GNU General Public License (GPL) was chosen as the software license because of its general acceptance as an open source license. Some industry lobbyists claim that the GPL itself is a restrictive license agreement since any source changes must be distributed along with the binary form of the software, making it impossible to have proprietary source code based on the original open source code. The free software community claims that this restriction isn't a restriction, but rather a requirement for participation. People who benefit from an open system should expect that their modifications should be open as well, if they choose to distribute them.

Note that licensing under the GPL does not require that independently developed code be distributed openly. Also, code that simply interacts with GPL-licensed code does not fall under the GPL \cite{gpl}.

\section{Immediate Applications for GPL-GPS}

GPL-GPS is primarily aimed at small academic and/or commercial projects which require extended GPS applications. For example:

\begin{description}
    \item[UAVs] Small unmanned aerial vehicles (UAVs) requiring integrated navigation systems such as the PSAS' next generation launch vehicle, LV2.
    \item[Robotics] Autonomous robotic navigation includes both autonomous agricultural equipment and autonomous vehicles such as those entered in the Defense Advanced Research Project's Grand Challenge competition.
    \item[Nanosatellites] GPS receivers on satellites make satellite navigation and attitude control smaller and less expensive compared to larger, more costly star sensors. Several academic nanosatellite projects have already expressed interest in using GPL-GPS.
    \item[GPS coursework] GPL-GPS should make a dramatically inexpensive and available educational development system.
\end{description}
