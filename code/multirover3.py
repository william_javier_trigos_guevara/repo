# -*- coding: utf-8 -*-
import os
from os import (listdir, getcwd, system)
from os.path import (basename, splitext, abspath, 
                     dirname, split, isfile)
from re import search, findall, compile
import re, urllib2, sys, copy, time

import gpstk
import numpy as np
import pandas as pd
from numba import autojit

def new_section(title):
    style = "text-align:center;background:#66aa33;padding:30px;color:#ffffff;font-size:3em;"
    return HTML('<div style="{}">{}</div>'.format(style, title))

# SIMBOLOS DE ERROR
def tick_equis(c):
    if c == 0:  #imprime chulito
        return "✔ "
    else:
        return "✘ "


def is_number(s):
    # http://stackoverflow.com/questions/354038/how-do-i-check-if-a-string-is-a-number-float-in-python
    try:
        float(s) # for int, long and float
    except ValueError:
        try:
            complex(s) # for complex
        except ValueError:
            return False

    return True

def isempty(var):
    
    if var != None:# or str(var) != '':
        return True
    else:
        return False
    
import pyproj
ecef = pyproj.Proj(proj='geocent', ellps='WGS84', datum='WGS84')
lla = pyproj.Proj(proj='latlong', ellps='WGS84', datum='WGS84')
def lla2ecef(lat,lon,alt, isradians=True):
    return pyproj.transform(lla, ecef, lon, lat, alt, radians=isradians)

def ecef2lla(X,Y,Z, isradians=True):
    lon, lat, alt = pyproj.transform(ecef, lla, X,Y,Z, radians=isradians)
    return lat, lon, alt

# https://github.com/scottyhq/insar_scripts/blob/master/ALOS/estimate_alos_baselines.py
def ecef2enu(pos, ref):
    """
    http://en.wikipedia.org/wiki/Geodetic_datum#Geodetic_versus_geocentric_latitude
    """
    xm,ym,zm = ref.flat #extracts elements of column or row vector
    # duplicate reference vector rows into matrix
    ref = np.vstack((ref,)*pos.shape[0])
       
    # get geodetic lat/lon/height (above wgs84) of satellite
    ecef = pyproj.Proj(proj='geocent',  ellps='WGS84', datum='WGS84')
    wgs84 = pyproj.Proj(proj='latlong', ellps='WGS84', datum='WGS84')
    lon, lat, h = pyproj.transform(ecef, wgs84, xm, ym, zm, radians=True)

    # make transformation matrix
    transform = np.array([
        [-np.sin(lon), np.cos(lon), 0.0],
        [-np.sin(lat)*np.cos(lon), -np.sin(lat)*np.sin(lon), np.cos(lat)],
        [np.cos(lat)*np.cos(lon), np.cos(lat)*np.sin(lon), np.sin(lat)]
    ])
    
    # do matrix multiplication
    enu = np.dot(transform, pos.T - ref.T)
    return enu


def rinex_to_dataframe_ionosfe(obsfile, navfile, silent_mode=False, ion_max=60):
    c = 299792458.

    observation_types=["P1", "P2", "L1", "L2", "C1", "C2", "S1", "S2"]
    obsHeader, obsData = gpstk.readRinex3Obs(obsfile)
    navHeader, navData = gpstk.readRinex3Nav(navfile)
    
    nh=gpstk.readRinexNav(navfile)[0]
    alphas = nh.ionAlpha
    betas  = nh.ionBeta
    gamma=gpstk.GAMMA_GPS ##CARGANDO GAMMA
    
    # setup ephemeris store to look for satellite positions
    bcestore = gpstk.GPSEphemerisStore()
    for navDataObj in navData:
        ephem = navDataObj.toGPSEphemeris()
        bcestore.addEphemeris(ephem)
    bcestore.SearchNear()
    navData.close()

    rec_pos = [obsHeader.antennaPosition[0], obsHeader.antennaPosition[1], obsHeader.antennaPosition[2]]
    
    requested_obstypes = observation_types
    obsidxs = []
    obstypes = []
    obsdefs = np.array([i for i in obsHeader.R2ObsTypes])
    for i in requested_obstypes:
        w = np.where(obsdefs==i)[0]
        if len(w)!=0:
            obsidxs.append(w[0])
            obstypes.append(i)
        else:
            if silent_mode == False:
                print ("WARNING! observation `"+i+"` no present in file "+obsfile)
            #else:
            #    print ""
    #obsidxs, obstypes

    r = []
    for obsObject in obsData:
        prnlist = []
        obsdict = {}
        prnspos = []
        prns_clockbias = []
        prns_relcorr   = []
        prnselev       = []
        prnsaz         = []
        iono_delay     = []
        for i in obstypes:
            obsdict[i]=[]

        gpsTime = gpstk.GPSWeekSecond(obsObject.time)

        for satID, datumList in obsObject.obs.iteritems():
            if satID.system == satID.systemGPS:
                prnlist.append("".join(str(satID).split()))
                eph   = bcestore.findEphemeris(satID, obsObject.time)

                for i in range(len(obsidxs)):
                    obsdict[obstypes[i]].append(obsObject.getObs(satID, obsidxs[i]).data)
                
                P1 = obsObject.getObs(satID, obsidxs[0]).data

                # para reemplazar los valores de P1 que son cero en el rinex
                # por los valores de C1, que deberian ser semejantes
                if np.mean(P1) < 1:
                	#print obstypes[4]
                	P1 = obsObject.getObs(satID, obsidxs[4]).data

                P2 = obsObject.getObs(satID, obsidxs[1]).data
                Id = (1*1/(1-gamma)*(P1-P2)) 
                if np.abs(Id) < ion_max:
                    iono_delay.append(Id)
                else:
                    iono_delay.append(ion_max)
                
                svTime = obsObject.time - P1/c
                svXvt = eph.svXvt(svTime)
                svTime += - svXvt.getClockBias() + svXvt.getRelativityCorr()
                svXvt = eph.svXvt(svTime)
                
                prnspos.append([svXvt.x[0], svXvt.x[1], svXvt.x[2]])
                prns_clockbias.append(svXvt.getClockBias())
                prns_relcorr.append(svXvt.getRelativityCorr())

                prnselev.append(obsHeader.antennaPosition.elvAngle(svXvt.getPos()))
                prnsaz.append(obsHeader.antennaPosition.azAngle(svXvt.getPos()))
                
        r.append([gpsTime.getWeek(), gpsTime.getSOW(), alphas, betas, gamma, np.array(prnlist), np.array(prnspos), np.array(prns_clockbias), 
                  np.array(prns_relcorr), np.array(prnselev), np.array(prnsaz), np.array(iono_delay)] + [np.array(obsdict[i]) for i in obstypes])

    names=["gps_week", "gps_sow", "alphas", "betas", "gamma", "prns", "prns_pos", "prns_clockbias", "prns_relcorr", "prns_elev", "prns_az", "Iono"] + obstypes
    r = pd.DataFrame(r, columns=names)
    obsData.close()
    return r, bcestore, np.array(rec_pos)


from numba import autojit
from numpy.linalg import norm

@autojit
def compute_distances(rc, svs):
    # return np.array( [np.sqrt((rc[0]-sv[0])**2 + (rc[1]-sv[1])**2) for sv in svs] )
    return np.linalg.norm(rc-svs, axis=1)

@autojit
def predict_pseudoranges(x, prns_pos, prns_clockbias):
    c = 299792458
    rhos    = compute_distances(x[:3], prns_pos)
    pranges = rhos + x[3]-c*prns_clockbias
    return rhos, pranges

@autojit
def apply_earth_rotation_to_svs_position(svs, prs):
      c = 299792458
      we = 7.2921159e-5
      rpos = np.zeros(svs.shape)
      pos = np.array(svs)
      for i in range(len(pos)):
        dt = prs[i]/c
        theta = we*dt
        R = np.array([[np.cos(theta), np.sin(theta),0.],[-np.sin(theta), np.cos(theta),0.],[0.,0.,1.]])
        rpos[i] = R.dot(pos[i])
      svs = np.array(rpos)
      return svs

def compute_least_squares_position(svs, svs_clocks, prs, max_iters=200, apply_earth_rotation=True):

    if apply_earth_rotation:
        #print type(svs), type(prs)
        svs = apply_earth_rotation_to_svs_position(svs, prs)
    
    if len(svs)==0 or len(prs)==0:
        return np.array([0.,0.,0.,0.]),None, None, None

    ri = np.array([0.,0.,0.,0.])

    #for i in range(max_iters):
    delta,i = 1,0
    while (norm(delta)>1e-8 and i<max_iters):
        rhos, pranges = predict_pseudoranges(ri, svs, svs_clocks)
        b = prs - pranges
        A = np.hstack(((ri[:3]-svs)/rhos[:,None],np.ones((len(b), 1))))
        delta =  np.linalg.pinv(A.T.dot(A)).dot(A.T).dot(b)
        ri += delta
        i+=1
    return ri, A, b, delta



def d_teta_calc(rx_1, rx_2):
    import numpy as np
    import nvector as nv
    from nvector import deg

    wgs84n = dict(a=6378137.0, f=1.0/298.257223563)
    p_EB_E1 = 6371e3 * np.vstack((rx_1))
    p_EB_E2 = 6371e3 * np.vstack((rx_2))

    n_EB_E1, z_EB1 = nv.p_EB_E2n_EB_E(p_EB_E1, **wgs84n)
    n_EB_E2, z_EB2 = nv.p_EB_E2n_EB_E(p_EB_E2, **wgs84n)

    lat_EB1, lon_EB1 = nv.n_E2lat_lon(n_EB_E1)
    lat_EB2, lon_EB2 = nv.n_E2lat_lon(n_EB_E2)
    h = -z_EB1
    lat1, lon1 = deg(lat_EB1), deg(lon_EB1)
    lat2, lon2 = deg(lat_EB2), deg(lon_EB2)
    #print lat1, lon1, lat2, lon2

    wgs84 = nv.FrameE(name='WGS84')

    pointA = wgs84.GeoPoint(latitude=lat1, longitude=lon1, degrees=True)
    pointB = wgs84.GeoPoint(latitude=lat2, longitude=lon2, degrees=True)

    p_AB_E = nv.diff_positions(pointA, pointB)

    frame_N = nv.FrameN(pointA)
    p_AB_N = p_AB_E.change_frame(frame_N)
    p_AB_N = p_AB_N.pvector.ravel()

    azimuth = np.arctan2(p_AB_N[1], p_AB_N[0])
    #teta = np.rad2deg(azimuth)
    #print 'azimuth = {%4.2f} deg' %(teta)
    
    d = np.linalg.norm(p_AB_E.pvector)
    
    return d, azimuth, p_AB_E.pvector.ravel()

def get_dist(rx_1, rx_2):
    import numpy as np
    import nvector as nv
    from nvector import deg

    lat1, lon1 = rx_1[0], rx_1[1]
    lat2, lon2 = rx_2[0], rx_2[1]
    #print lat1, lon1, lat2, lon2

    wgs84 = nv.FrameE(name='WGS84')

    pointA = wgs84.GeoPoint(latitude=lat1, longitude=lon1, degrees=True)
    pointB = wgs84.GeoPoint(latitude=lat2, longitude=lon2, degrees=True)

    p_AB_E = nv.diff_positions(pointA, pointB)

    frame_N = nv.FrameN(pointA)
    p_AB_N = p_AB_E.change_frame(frame_N)
    p_AB_N = p_AB_N.pvector.ravel()

    azimuth = np.arctan2(p_AB_N[1], p_AB_N[0])
    #teta = np.rad2deg(azimuth)
    #print 'azimuth = {%4.2f} deg' %(teta)
    
    d = np.linalg.norm(p_AB_E.pvector)
    
    return d, azimuth#, p_AB_E.pvector.ravel()

def read_kmz_file(fname, max_distancia):
    import sys
    import zipfile
    import glob
    from xml.dom import minidom

    #fname = "sites.kmz"
    path_cnt = 0
    zf = zipfile.ZipFile(fname, 'r')

    #print zf, type(zf)
    for fn in zf.namelist():
        #print fn
        if fn.endswith('.kml'):
            content = zf.read(fn)
            xmldoc = minidom.parseString(content)
            placemarks = xmldoc.getElementsByTagName('Placemark')

            nodes = {}
            for placemark in placemarks:
                nodename = placemark.getElementsByTagName("name")[0].firstChild.data
                coords = placemark.getElementsByTagName("coordinates")[0].firstChild.data
                lst1 = coords.split(",")
                longitude = float(lst1[0])
                latitude = float(lst1[1])
                nodes[nodename] = (latitude, longitude)

            from itertools import combinations
            #print nodes
            parejas = [{j: nodes[j] for j in i} for i in combinations(nodes, 2)]

            d_min = 1e8

            GPS_Stations = {}
            c = 0
            for n in parejas:
                #print n, type(n), n.keys()[0], n.values()[0]
                rx_1 = n.values()[0]
                rx_2 = n.values()[1]

                d,_ = get_dist(rx_1, rx_2)
                if (d_min > d):
                    if (d < max_distancia):

                        print d_min, d, "entre", n.keys()[0], "y", n.keys()[1]
                        #d_min = d
                        GPS_Stations[c] = [n.keys()[0], n.keys()[1]]
                        c += 1
    return GPS_Stations


def get_data_stations(f_in, max_dis):

    List_Stations = read_kmz_file(f_in, max_dis)

    stations = {}
    
    for (idx, two) in enumerate(List_Stations.iteritems()):
        #print two, type(two[1])
        two = [str(x) for x in two[1]]
        #print tuple(two)
        stations[idx] = tuple(two)
        
    return stations



def read_Rinex_Compri(data_folder, parejas):
    import os, re
    
    # Nombre de la carpeta donde se guardara la descarga
    stations = [x.lower() for par in parejas.values() for x in par]
    #verificando las parejas de estaciones de las que se tiene archivos rinex
    
    estaciones = {}

    dir_files = [fn for fn in listdir(data_folder)]

    for filename in listdir(data_folder):
        base, ext = splitext(filename)
        
        key = base[:-1]
        for par in parejas.values(): 
            for station in par:

                if base[:4].lower() == station.lower():
                    
                    path = data_folder+"/"
                    
                    obstype = str(base[-1]+'-zip').lower()
                    estaciones.setdefault(key, {})[obstype] = path+filename

    return estaciones


def readAllStored_Rinex_Compri(data_folder, parejas_estaciones):
    import os, re
    
    # Nombre de la carpeta donde se guardara la descarga
    stations = [x.lower() for par in parejas_estaciones.values() for x in par]
    #verificando las parejas de estaciones de las que se tiene archivos rinex
    
    estaciones = {}

    dir_files = [fn for fn in listdir(data_folder)]
    #print dir_files
    
    for filename in dir_files: #os.listdir(data_folder):
        base, ext = splitext(filename)

        if base[:4].lower() in stations:
            station, ObsType = splitext(base)
           
            #print station
            path = data_folder+"/"
            
            # GRACIAS A ESTA RESPUESTO, EL CODIGO FUNCIONA
            # http://stackoverflow.com/questions/21613038/adding-new-keys-to-a-python-dictionary-from-variables?answertab=votes#tab-top
            # http://stackoverflow.com/questions/14012918/passing-dictionary-keys-to-a-new-dictionary
            if base[-1].lower() == 'o':
                estaciones.setdefault(station, {})['name_station'] = station[:4]
                estaciones.setdefault(station, {})['obs-zip'] = path+filename

            else:

                estaciones.setdefault(station, {})['name_station'] = station[:4]
                estaciones.setdefault(station, {})['nav-zip'] = path+filename
    
    return estaciones

@autojit
def process_day(day):
    if   day<10:
         day="00"+str(day)
    elif day<100:
         day="0"+str(day)
    else:
         day=str(day)+str("0")
       
    return day

@autojit
def process_day2(day):
    if   day<10:
         day="00"+str(day)+"0"
    elif day>9 and day<100:
         day="0"+str(day)+"0"
    else:
         day=str(day)+str("0")
       
    return day


def descarga_Rinex_Compri(data_folder, parejas_kmz, dia1, dian, ano):
    #import descargador_RINEX as dl_RINEX
    #import os
    #import urllib2
    #import re

    # Nombre de la carpeta donde se guardara la descarga
    #data_folder = os.getcwd() +"/" + data_folder      #"new_data"
    cmd1 = "mkdir -p " + data_folder    # COMANDO CREA CARPETA DE DESCARGA
    system(cmd1)

    dir_files = [fn for fn in listdir(data_folder)]
    #print dir_files
    
    # http://stackoverflow.com/questions/2486145/python-check-if-url-to-jpg-exists
    def file_exists(url):
        request = urllib2.Request(url)
        request.get_method = lambda : 'HEAD'
        try:
            response = urllib2.urlopen(request)
            return True
        except:
            return False
    
    def valid_extension(x, valid={'.gz', '.Z'}):
        return splitext(x)[1].lower() in valid
                            
    # MAIN FUNCTION
    ###########################################################################S
    print "\n Descargando Nuevos Archivos ...."
    print "************************\n"
    
    #rint "Buscando Archivos para: "
    print "\t [Estacion] \t [dia GPS] \t\t [RESULTADO]"
            
    for key, value in parejas_kmz.iteritems():
        #print k, v[0], v[1], type(v[0]), str(v[0])
        for station in value:
            #print stat, type(stat)
            # BASADO EN EL CALENDARIO http://gps.topografia.upm.es/www/2015G.htm

            
            # RED Gadner tiene met, tropo, rinex files de casi todo
            # USADOS EN LA TESIS DE 
            # http://www.inpe.br/pos_graduacao/cursos/geo/arquivos/dissertacoes/dissertacao_olusegun_folarin_2013.pdf


            # ftp://garner.ucsd.edu/pub/troposphere/0990/
            # ftp://garner.ucsd.edu/pub/met/2017/010/
            # ftp://garner.ucsd.edu/pub/rinex/2017/010/
            # ftp://garner.ucsd.edu/pub/
            # http://madrigal.haystack.mit.edu/madrigal/siteSpecific/tec_sources.html

            Servers = {
                'garner':{    'ftp':'ftp://garner.ucsd.edu/pub/rinex/', 
                         },
                'lox':{    'ftp':'ftp://lox.ucsd.edu/pub/rinex/', 
                         },
                #
                'cors1':{    'ftp':'ftp://geodesy.noaa.gov/cors/rinex/', 
                         },
                'cors2':{    'ftp':'ftp://alt.ngs.noaa.gov/cors/rinex/', 
                         },
                #
                'igac-gov':{  'ftp':'ftp://anonimo:anonimo@132.255.20.140/', 
                        },
                'nasa'  :{    'ftp':'ftp://cddis.gsfc.nasa.gov/pub/gps/data/daily/', 
                         },
                'unavco':{    'ftp':'ftp://data-out.unavco.org/pub/rinex/', 
                         }
            }             
                                    
            tipos_Rinex  = ['obs', 'nav', 'd']
            station = str(station.lower())
            
            print "\t %s" %(station)

            
            for i in range(int(dia1), int(dian)+1):
                """
                if i<10:
                    dia="00"+str(i)
                elif i<100:
                    dia="0"+str(i)
                else:
                    dia=str(i)
                """
                dia = process_day(i)

                c = 0
                flag = 0
                for server in Servers.keys():
                    c += 1
                    if flag == 1 or flag == 2:
                        # hacer que se acabe la busqueda en los servidores
                        c == len(Servers[server])
                    
                    else:
                        fail = []     # guarda tipo de archivo que no se encontro para descarga
                        f_exist = []  # contar los archivos que existen
                        for tipo in tipos_Rinex:

                            #print "\t", tipo,

                            f_name = station+dia+'0.'+ano[-2:]+tipo[:1]
                            
                            #http://stackoverflow.com/questions/17777311/filter-file-list-in-python-lowercase-and-uppercase-extension-files
                            pat = compile(r'[.](gz|Z)$', re.IGNORECASE)
                            filenames = [filename for filename in listdir(data_folder)
                                         if search(pat, filename)]

                            #matches = [re.search("("+f_name+".*)", f, flags=re.I) for f in filenames]
                            matches = [f for f in filenames if findall("("+f_name+".*)", f, flags=re.I)]
                            
                            #print matches
                            if len(matches) != 0:
                                f_exist.append(tipo)
                                #print "esta en dir"

                                if len(f_exist) == len(tipos_Rinex):
                                    flag = 2
                                    #break
                                    
                            else:
                                #print "No esta en dir"
                                
                                #Servers['igac-gov']['ftp']    = 'ftp://anonimo:anonimo@132.255.20.140/'

                                Servers['igac-gov']['path']    = dia+'/'+ano[-2:]+tipo[:1]+'/'
                                Servers['igac-gov']['archivo'] = str(f_name).upper()+'.gz'

                                #Servers['nasa']['ftp']         ='ftp://cddis.gsfc.nasa.gov/pub/gps/data/daily/'
                                Servers['nasa']['path']        = ano+'/'+dia+'/'+ano[-2:]+tipo[:1]+'/'
                                Servers['nasa']['archivo']     = f_name+'.Z'

                                #Servers['unavco']['ftp']       ='ftp://data-out.unavco.org/pub/rinex/'
                                Servers['unavco']['path']      = tipo+'/'+ano+'/'+dia+'/'
                                Servers['unavco']['archivo']   = f_name+'.Z'

                                Servers['garner']['path']      = ano+'/'+dia+'/'
                                Servers['garner']['archivo']   = f_name+'.Z'

                                Servers['lox']['path']         = ano+'/'+dia+'/'
                                Servers['lox']['archivo']      = f_name+'.Z'

                                #""
                                Servers['cors1']['path']      = ano+'/'+dia+'/'+station[:4]
                                Servers['cors1']['archivo']   = f_name+'.Z'

                                Servers['cors2']['path']      = ano+'/'+dia+'/'+station[:4]
                                Servers['cors2']['archivo']   = f_name+'.Z'
                                #""

                                url = Servers[server]['ftp']+Servers[server]['path']
                                archivo = Servers[server]['archivo']

                                # Red gps new zeland
                                #https://apps.linz.govt.nz/ftp/positionz/2016/026/
                                #ftp://ftp.geonet.org.nz/gps/rinex/2016/003/

                                # GLONAS DESDE SERVIDOR NASA
                                #ftp://igscb.jpl.nasa.gov/igscb/glonass/products/1920/

                                #https://code-maven.com/urllib-vs-urllib2
                                if file_exists(url+archivo):
                                    #print "existe url"

                                    html_content = urllib2.urlopen(url).read()
                                    matches = re.findall(archivo, html_content, flags=re.I)

                                    if len(matches) != 0:

                                        #print archivo, url+archivo
                                        if flag == 0:

                                            cmd = "wget " + str(url+archivo) + " -P " + data_folder
                                            #print "\t", cmd
                                            system(cmd)

                                        f_exist.append(tipo)

                                else:

                                    #print "No encontrado el archivo en ", server
                                    if tipo not in fail:
                                        fail.append(tipo)
                                    #print fail
                            
                        if len(f_exist) == len(tipos_Rinex) and len(fail) == 0:
                            flag = 1


                    if c == len(Servers[server]):
                        if flag == 0:
                            print "\t\t\t %s \t\t No encontrados Ficheros %s" %(dia, fail)
                            #print dia, " 

                        elif flag ==2:
                            print "\t\t\t %s \t\t Archivos ya disponibles!!!" %(dia)
                        else:
                            print "\t\t\t %s \t\t Descarga Exitosa!!!" %(dia)
                
    print "Descarga Finalizada!! \n"
    
    DB_Rinex_Compri = readAllStored_Rinex_Compri(data_folder, parejas_kmz)

    return DB_Rinex_Compri



#######################################################################################################
#######################################################################################################
#######################################################################################################



