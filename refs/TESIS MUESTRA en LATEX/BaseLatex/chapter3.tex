% Chapter 3: Selecting a Receiver Chipset and Board

\section{Choosing a GPS Receiver Chipset}

A GPS ``chipset'' is the active electronics necessary to decode GPS signals. Most GPS receivers require one to three silicon integrated circuits (ICs, or `chips'): a radio frequency (RF) front end, a baseband correlator, and some kind of general purpose processor. In practice, commercial GPS chipsets come in four configurations:

\begin{description}
    \item[Correlator only] A single chip that only does GPS correlation. It requires a separate RF front end and  processor.
    \item[Combined correlator/processor] A single chip that contains both the correlator and the processor. It only requires a RF front end.
    \item[Single chip receiver] A single chip receiver that combines a RF front end, a correlator, and a processor.
    \item[Analog to Digital Converter (ADC)] Software-defined radio (SDR) receivers replace the RF front end and correlator chips with a single ADC and signal processing software running on a fast processor.
\end{description}

Current trends seem to be bifurcating the GPS chipset market: the system-on-chip (SoC) trend is pushing receiver chipsets towards highly-integrated single chip receivers while, at the same time, trends in software defined radio (SDR) are pushing the correlator features into small ``correlator peripheral'' chipsets that use the processing power of a host processor for much of the signal processing besides tracking. The latter is most evident in deeply embedded receivers, such as those in GPS-enabled cellular phones.

\subsection{GPL-GPS Chipset Requirements}

The GPS chipset used in the GPL-GPS receiver will determine how the initial code infrastructure is defined, and deeply influences how project resources are spent. Does the chip need to be reverse engineered? Is that even possible? Are there operating systems already ported to the processor of the chipset? Choosing a chipset sets the tone and direction for the rest of this project, thus considerable research was invested in existing chipsets.

It can be argued that an open source GPS receiver should move away from a hardware-dependent GPS chipset towards a SDR system. This would increase the flexibility of the receiver's functionality while reducing the the cost and complexity of the receiver hardware. Unfortunately, moving from a GPS correlator chipset to a general purpose processor or field programmable gate array (FPGA) currently incurs unacceptable power and size penalties for many embedded applications: chipset-based receivers generally require 6~in$^2$ and consume less than 1~W, while general purpose processors and FPGAs require at least 16~in$^2$ and consume 10--100~W of power. Thus to serve power, weight and size restricted applications, GPL-GPS must use a chipset-based receiver rather than a SDR general purpose platform. As a stand-alone receiver, the chipset for GPL-GPS requires:

\begin{enumerate}
    \item Open and accessible chipset documentation (it cannot require a non-disclosure agreement)
    \item Support of the processor architecture (if there is one) by open and available tools.
    \item Available in a commercial, off-the-shelf (COTS) receiver board.
\end{enumerate}

\noindent Important but not required features of that chipset are that it:

\begin{enumerate}
    \item Has at least eight correlator channels.
    \item Is intended to be used as a general purpose receiver and is not as an ultra-miniaturized SoC cell-phone chip or a timing-only chip.
    \item Has a convenient and usable hardware architecture (i.e., does not require an extraordinary amount of effort in either software or hardware development).
    \item Has an existing open code base
\end{enumerate}

\subsection{Applicable GPS chipsets}

There are currently about a dozen commercial GPS chipsets on the market. Of these, most were not user-modifiable or programmable as a stand-alone receiver and thus were discarded. Discarded chipsets included:

\begin{itemize}
    \item Older chipsets with significantly less processing power or less than 8 correlator channels.
    \item ``Position peripheral'' chipsets, meant for deeply embedded consumer applications such as cellular phones. In such applications, a receiver chip provides a standard interface over a parallel or serial link to the host processor. The positioning peripheral chip --- despite the fact that it may contain a general purpose processor and may even require the host to send it a firmware --- must be considered to be non-programmable since only its application interface is described. Data sheets on the internal workings of the chipsets do not seem to be available.
    \item Intellectual property-only (``IP-based'') receiver designs, consisting mostly of hardware description language files for Field Programmable Gate Arrays (FPGAs), were not considered  for GPL-GPS. As mentioned earlier, FPGA boards are larger and consume more power than conventional GPS chipsets, and further, as of fall 2004, there were no commercial FPGA-based GPS receiver boards available.
\end{itemize}


Table~\ref{tab:chipsets} compares available chipsets that fit the stand-alone receiver model. The chipsets have been divided into two groups: the first has integrated processor and correlator chipsets, and the second group has separate process and correlator chipsets.


\begin{table}[p]
    \begin{center}
    
        \begin{smalltabular}{l|ccc|cc}
            Mfg. and model.                & Arch.    & Open & Rec & Sane  & OSS \\
            \hline
            Atmel ATR0620                  & ARM7TDMI & N    & Y   & ?     & N \\
            NemeriX NJ1030 \cite{nemerix}   & SPARC V8 & Y    & N   & Y     & N \\
            SiRF SiRFStar II \cite{sirf2}   & ARM v?   & N    & Y   & Y     & N \\
            Thales Baldur \cite{thales}     & ARM7     & N    & Y   & ?     & N \\
            u-Nav uN8031B \cite{unav}       & V-DSP?   & N    & Y   & N     & N \\
            Zarlink GP4020 \cite{gp4020}    & ARM7TDMI & Y    & Y   & Y     & Y \\
        \end{smalltabular}
        
        \addvspace{12pt}
        
        Processor with integrated correlator peripheral chipsets.
        
        \addvspace{12pt}
                
        \begin{smalltabular}{l|cc|cc}
            Mfg. and model.                 & Open & Rec & Sane & OSS \\
            \hline
            Navman Zodiac \cite{navman}      & N    & Y   & Y    & N \\
            Trimble FirstGPS \cite{trimble}  & N    & Y   & ?    & N \\
            Zarlink GP2021 \cite{gp2021}     & Y    & N   & Y    & Y \\
        \end{smalltabular}
        
        \addvspace{12pt}
        
        Correlator only chipsets.
        
        \addvspace{12pt}
                
        \begin{smalltabular}{lll}
            Key: & Arch & Architecture of processor if known \\
                 & Open & Open documentation available without NDA or other excessive \\
                 &      & licensing agreement \\
                 & Rec  & Inexpensive, commercial GPS receiver boards exist for this chipset \\
                 & Sane & Receiver architecture is not overcomplicated \\
                 & OSS & Open source software exists for this chipset \\
        \end{smalltabular}

        \caption{Comparison of GPS chipsets.}
        \label{tab:chipsets}
        
    \end{center}
\end{table}

Unlike the rest of the semiconductor industry, finding an ``open'' GPS chipset --- one which had open and available documentation and did not require a restrictive licensing agreement to use --- was a major challenge. Exhaustive research uncovered only two open chipsets. This seems like extreme short-sightedness on behalf of the GPS chipset manufacturers who are operating on an older model of intellectual property retention than the more open, fluid --- and successful --- general semiconductor industry.

I spent some time trying to convince chipset manufacturers to open up their chipset designs, if only for nonprofit projects. All refused, and when queried, answered that they were protecting their intellectual property from competition. In light of the openness and success of the general semiconductor industry, this seemed to be poor business strategy. The only explanation offered by other frustrated GPS developers is that the military origins of the Global Positioning System have left a corporate culture still steeped in secrecy and closed intellectual property rights.

\subsection{Choosing a Chipset}

The NemeriX NJ1030 \cite{nemerix} is an open GPS chipset featuring a 16 channel correlator, an open source ``LEON'' (SPARC V8) processor, a large onboard SRAM cache, and very low power consumption ( $<$ 8~mA at 3.3~V). Unfortunately, this promising new chipset was too recently released to have any commercially-available receivers as of spring of 2005. Future versions of GPL-GPS will undoubtedly be ported to the NemeriX chipset as  NJ1030-based receivers begin to appear on the market.

The only open and available chipset at the start of the GPL-GPS project was the Zarlink GP4020 Baseband Processor \cite{gp4020}. All architectural and application notes are available on Zarlink's web site, including reference designs for receiver boards. A major feature of choosing the GP4020 is that its correlator peripheral is the same architecture as the stand-alone Zarlink GP2021 correlator chip used in Kelley's OpenSource GPS receiver project. This provided a rich pre-existing software base for any GP4020 design. Another plus of the GP4020 is that its general purpose processor is an ARM7TDMI, a widely used 32 bit RISC architecture with available open source tools.

The GP4020 has a variety of peripherals, including two timers, two asynchronous serial ports (UARTs), a high speed synchronous serial port (SPI), a watchdog timer, and a sophisticated memory peripheral controller (MPC) to interface the GP4020 to external RAM, ROM and peripheral devices. A typical GP4020-based receiver is shown in Figure~\ref{fig:generic-gp4020-receiver}. Note the antenna, TCXO, and filters on the GP2015 RF front end, and the 2 bit ADC output (sign, magnitude) from the RF front end to the GP4020 correlator block. The 16 bit RAM and ROM (usually flash) is shown in the upper right.

\begin{figure}[h] % place the figure right here
    \begin{center}
        \includegraphics[keepaspectratio,width=0.9\linewidth]{pictures/gp4020_receiver_block.eps}
        \caption{Generic GP4020-based GPS receiver block diagram (reproduced from \cite{gp4020}).}
        \label{fig:generic-gp4020-receiver}
    \end{center}
\end{figure}

\section{Choosing a GP4020-based GPS Receiver}
\subsection{GP4020-based GPS Receiver Requirements}
\noindent The GPL-GPS receiver requires a receiver board that:

\begin{enumerate}
    \item Uses the Zarlink GP4020 Baseband Processor chip.
    \item Has enough RAM and ROM ($>$ 128~kB).
    \item Does not need to be modified in any way to be used as a GPL-GPS receiver.
\end{enumerate}

This last requirement is important because many developers do not have the skills necessary to precisely modify a four or more layer printed circuit board. Relying on ``hacking'' a receiver board severely diminishes the number of software-only developers willing to work on a project. Other important considerations for the receiver are:

\begin{enumerate}
    \item Access to GP4020 pins, in particular the MULTI\_FNIO pin for easy use of the GP4020's built-in boot loader and the GPIO pins for LED debugging.
    \item Compact size and on-board power management.
    \item Standard RF connector (e.g., MCX, SMA, SMB)
    \item Widely available.
    \item Relatively inexpensive (less than US~\$150).
\end{enumerate}

\subsection{Available GP4020-based GPS Receivers}

See Table~\ref{tab:gp4020receivers} for a comparison of the three commercially available boards based on the GP4020.

\begin{table}
    \begin{center}
        \begin{smalltabular}{l|ccc}

            Manufacturer   & SigTec           & SigTec            & Novatel \\
            Model          & MG5001           & MG5003            & SuperStar II \\
            \hline
            RF Front End   & GP2015           & GP2015            & GP2015 \\
            External SRAM  & 256~kB           & 512~kB            & 128~kB \\
            Flash          & 256~kB           & 256~kB  (serial)           & 256~kB \\
            Cost (Qty. 1)  & US~\$250         & US~\$225          & US~\$125 \\
            Notes          & 51 pin connector & Small form factor & 32~kB EEPROM \\
        \end{smalltabular}
        \caption{Commercially available GP4020-based GPS receivers.}
        \label{tab:gp4020receivers}
    \end{center}
\end{table}

\subsection{Choosing a Receiver}

\noindent The SigTec MG5001 is the clear choice as a development receiver for a number of reasons:

\begin{itemize}
    \item Larger RAM size
    \item The high density 51 pin connector allows the MG5001 to be mounted as a daughter board on a development board, providing direct access to the GP4020's serial ports (UARTs), JTAG debugging pins, MULTI\_FNIO serial boot pin, GPIO pins, data lines and a small subset of address lines.
    \item The MG5001 has a RF section that is separated from the digital section in a way that facilitates using separate metal ground shields, a useful option when operating in high noise applications.
\end{itemize}

The MG5003 has the advantage that it has an extremely small form factor and has a large amount of SRAM. But the lack of connectors make it necessary to hack the board to get access to the GPIO, and the serial flash make it impossible to run the software infrastructure directly from flash, making the larger SRAM not as advantageous.

The Novatel SuperStar II is half the cost of the MG5001, but the added expense of the MG5001 seems worth the added development flexibility of the 51 pin connector and the larger memory. Not surprisingly, however, the SigTec MG5001 and the Novatel SuperStar II are very similar to each other, and there are almost no differences between them from a software development point of view. With the exception of RAM size, code developed on the MG5001 should work exactly the same on the SuperStar II. Projects with budget constraints should still be able to use the SuperStar II for GPL-GPS development. In-situ GPL-GPS installations should also consider using the SuperStar II due to the lower cost, since only development activities require a large SRAM size.

An important possible difference between the boards is the sensitivity of their RF front ends. While they employ the same RF front end chip (the Zarlink GP2015), they have very different component values and board layouts. Future side-by-side testing using a single antenna with a 2-to-1 splitter will enable a direct comparison of the two receiver boards.

\subsection{Creating a GPL-GPS Development Board}

It's almost always necessary to have some kind of a development board that aids embedded development work. To have something similar for GPL-GPS, I designed a 4.5 x 3.5 inch carrier board for the MG5001. It includes:

\begin{itemize}
    \item Two DB-9 connectors with a 5 V to RS-232C signal level converter chip, allowing a PC to connect to both serial channels.
    \item Three switches connected to the GP4020's reset, MULTI\_FNIO (serial bootloader), and JTAG debugging pins.
    \item Five separate 0.1 inch headers for the address, data, JTAG, GPIO and serial pins.
    \item Eight LEDs connected to the GPIO.
    \item Linear 5 V and 3.3V power supplies.
    \item A small prototyping area.
\end{itemize}

The schematic and board layout was created in EAGLE CAD, a freely available (but not open source) PCB CAD development tool. The design is posted on the GPL-GPS web site (\url{http://gps.psas.pdx.edu}) for others to download and manufacture. A commercial circuit board manufacturing run costs roughly US~\$75 for two carrier boards, and there is approximately US~\$25 worth of components on the board. See Appendix~\ref{app:carrierboard} for a detailed schematic.

