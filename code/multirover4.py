# -*- coding: utf-8 -*-
import os
from os import (listdir, getcwd, system)
from os.path import (basename, splitext, abspath,
                     dirname, split, isfile)
from re import search, findall, compile
import re, urllib2, sys, copy, time

import gpstk
import numpy as np
import pandas as pd
from numba import autojit

import matplotlib.pyplot as plt
from IPython.display import HTML

global lab
lab = 0


# PARA EL MANEJO DE CONVERSION DATAFRAMES A LATEX
# https://stackoverflow.com/questions/20685635/pandas-dataframe-as-latex-or-html-table-nbconvert
import pandas as pd
pd.set_option('display.notebook_repr_html', True)
pd.set_option('precision', 3)
pd.set_option('max_colwidth',44)

def _repr_latex_(self):
    return "\centering{%s}" % self.to_latex()
pd.DataFrame._repr_latex_ = _repr_latex_  # monkey patch pandas DataFrame

# Make figures to pdf
# https://goo.gl/DhQHFL
# http://blog.rtwilson.com/how-to-get-nice-vector-graphics-in-your-exported-pdf-ipython-notebooks/
from IPython.display import set_matplotlib_formats
set_matplotlib_formats('png','pdf')


#https://stackoverflow.com/questions/34106622/how-to-convert-html-output-for-pandas-dataframes-to-latex-when-using-nbconvert
#https://stackoverflow.com/questions/20685635/pandas-dataframe-as-latex-or-html-table-nbconvert
#from IPython.display import Latex
#Latex(df_stations.to_latex())

# https://stackoverflow.com/questions/32855828/how-to-add-latex-figure-captions-below-jupyter-matplotlib-figures
from IPython.display import display, clear_output, Javascript, Latex
def makeplot_latex( plt, figlabel, figcaption, bShowInline = False):
    figname = figlabel+'.png'

    #plt.savefig(figname, format='pdf')
    plt.savefig(figname, format='png')

    if bShowInline:
        plt.show()
    else:
        plt.close()

    strLatex="""
    \\begin{figure}[b]
    \centering
        \includegraphics[totalheight=10.0cm]{%s}
        \caption{%s}
        \label{fig:%s}
    \end{figure}"""%(figname, figcaption, figlabel)

    strLatex2="""
    \\begin{figure}
        \\begin{center}
        	\\adjustimage{max size={0.9\linewidth}{0.6\paperheight}}{%s}
        \\end{center}
        \\caption{%s}
        \\label{%s}
    \\end{figure}"""%(figname, figcaption, figlabel)

    #print figname, figcaption, figlabel
    return display(Latex(strLatex2))


from IPython.display import display, Math, Latex, Image

def save(path, ext='png', close=True, verbose=True, axis_state='off'):
    """Save a figure from pyplot.
    http://www.jesshamrick.com/2012/09/03/saving-figures-from-pyplot/

    Parameters
    ----------
    path : string
        The path (and filename, without the extension) to save the
        figure to.
    ext : string (default='png')
        The file extension. This must be supported by the active
        matplotlib backend (see matplotlib.backends module).  Most
        backends support 'png', 'pdf', 'ps', 'eps', and 'svg'.
    close : boolean (default=True)
        Whether to close the figure after saving.  If you want to save
        the figure multiple times (e.g., to multiple formats), you
        should NOT close it in between saves or you will have to
        re-plot it.
    verbose : boolean (default=True)
        Whether to print information about when and where the image
        has been saved.
    """

    # Extract the directory and filename from the given path
    directory = os.path.split(path)[0]
    filename = "%s.%s" % (os.path.split(path)[1], ext)
    if directory == '':
        directory = '.'

    # If the directory does not exist, create it
    if not os.path.exists(directory):
        os.makedirs(directory)

    # The final path to save to
    savepath = os.path.join(directory, filename)

    if verbose:
        print("Saving figure to '%s'..." % savepath),

    plt.axis(axis_state)
    # Actually save the figure
    plt.savefig(savepath, bbox_inches='tight', dpi=300)

    # Close it
    if close:
        plt.close()

    if verbose:
        print("Done")

def section_title(placement, scenario=0):

    title_x_section = [r"""\subsection{\'Indice de mejora para escenario %s}"""%(scenario),
                       r"""\subsection{Gr\'aficas para escenario %s}"""%(scenario)]

    if isinstance(placement, str):
        return placement
    else:
        return title_x_section[placement]


class Table_latex():
    def __init__(self, table, ref, cap, landscape=False):
        self.cap = cap
        self.tab = table
        self.ref = ref
        self.landscape = landscape
    def _repr_html_(self):
        return '<center>{0}</center>'.format(self.cap)
    def _repr_latex_(self):
        #return '\\begin{figure}\n \includegraphics{'+self.fig+'}\n \caption{'+self.s+'}\n\\end{figure}'
        # \\adjustimage{max size={0.9\linewidth}{0.6\paperheight}}{%s}
        if self.landscape==True:
            self.begin = r"""
                            \begin{landscape}
                            \global\pdfpageattr\expandafter{\the\pdfpageattr/Rotate 90}"""
            self.end   = r"""
                            \global\pdfpageattr\expandafter{\the\pdfpageattr/Rotate 0}
                            \end{landscape}"""
        else:
            self.begin = ""
            self.end   = ""

        a = r"""
            %s
            \begin{table}[h!]
            \begin{minipage}{\linewidth}
            \begin{center}
            %s
            \caption{Tabla \ref{tab:%s} %s}
            \label{tab:%s}
            \end{center}
            \end{minipage}
            \end {table}
            %s"""%(self.begin, self.tab, self.ref, self.cap, self.ref, self.end)
        return a

def Ecuacion(Equation):
    display(Latex(latex(expr=Equation, mode='equation')))
    
class Ecuacion2():
    def __init__(self, eq, ref):
        self.eq = eq
        self.ref = ref
    def _repr_html_(self):
        return '<center>{0}</center>'.format(self.ref)
    def _repr_latex_(self):
        a = r"""
            \begin{equation}
            %s
            \label{eq:%s}
            \end{equation}
            """%(self.eq, self.ref)
        return a
        
class Caption():
    def __init__(self, fig, ref, cap, size=(1.05, 0.17)):
        self.cap = cap
        self.fig = fig
        self.ref = ref
        self.w   = size[0]
        self.h   = size[1]
    def _repr_html_(self):
        return '<center>{0}</center>'.format(self.cap)
    def _repr_latex_(self):
        #return '\\begin{figure}\n \includegraphics{'+self.fig+'}\n \caption{'+self.s+'}\n\\end{figure}'
        # \\adjustimage{max size={0.9\linewidth}{0.6\paperheight}}{%s} {width={%s\linewidth}, height={%s\paperheight}}{%s}
        a = r"""
            \begin{figure}[h!]
            \begin{center}
            \adjustimage{max size={%s\linewidth}{%s\paperheight}}{%s}
            \caption{Figura \ref{fig:%s} %s}
            \label{fig:%s}
            \end{center}
            \end{figure}"""%(self.w, self.h, self.fig, self.ref, self.cap, self.ref)
        return a


def new_section(title):
    style = "text-align:center;background:#66aa33;padding:30px;color:#ffffff;font-size:3em;"
    return HTML('<div style="{}">{}</div>'.format(style, title))

# SIMBOLOS DE ERROR
def tick_equis(c):
    if c == 0:  #imprime chulito
        return "✔ "
    else:
        return "✘ "


def is_number(s):
    # http://stackoverflow.com/questions/354038/how-do-i-check-if-a-string-is-a-number-float-in-python
    try:
        float(s) # for int, long and float
    except ValueError:
        try:
            complex(s) # for complex
        except ValueError:
            return False

    return True

def isempty(var):

    if var != None:# or str(var) != '':
        return True
    else:
        return False

import pyproj
ecef = pyproj.Proj(proj='geocent', ellps='WGS84', datum='WGS84')
lla = pyproj.Proj(proj='latlong', ellps='WGS84', datum='WGS84')
def lla2ecef(lat,lon,alt, isradians=True):
    return pyproj.transform(lla, ecef, lon, lat, alt, radians=isradians)

def ecef2lla(X,Y,Z, isradians=True):
    lon, lat, alt = pyproj.transform(ecef, lla, X,Y,Z, radians=isradians)
    return lat, lon, alt

# https://github.com/scottyhq/insar_scripts/blob/master/ALOS/estimate_alos_baselines.py
def ecef2enu(pos, ref):
    """
    http://en.wikipedia.org/wiki/Geodetic_datum#Geodetic_versus_geocentric_latitude
    """
    xm,ym,zm = ref.flat #extracts elements of column or row vector
    # duplicate reference vector rows into matrix
    ref = np.vstack((ref,)*pos.shape[0])

    # get geodetic lat/lon/height (above wgs84) of satellite
    ecef = pyproj.Proj(proj='geocent',  ellps='WGS84', datum='WGS84')
    wgs84 = pyproj.Proj(proj='latlong', ellps='WGS84', datum='WGS84')
    lon, lat, h = pyproj.transform(ecef, wgs84, xm, ym, zm, radians=True)

    # make transformation matrix
    transform = np.array([
        [-np.sin(lon), np.cos(lon), 0.0],
        [-np.sin(lat)*np.cos(lon), -np.sin(lat)*np.sin(lon), np.cos(lat)],
        [np.cos(lat)*np.cos(lon), np.cos(lat)*np.sin(lon), np.sin(lat)]
    ])

    # do matrix multiplication
    enu = np.dot(transform, pos.T - ref.T)
    return enu


    
def check_get_data(data1, data2, d_c):
    variables = ["gps_sow", "prns", "C1", "Iono"]

    c = 0
    for epoc in range(len(data1)):
        if epoc % res_plot == 0:
            for var in variables[:2]:
                print np.array(data1.iloc[epoc][var])
                print np.array(data2.iloc[epoc][var])
                d = d_c[c]
                print np.array(d.iloc[0][var])

            for var in variables[2:]:
                print np.array(data1.iloc[epoc][var])
                print np.array(data2.iloc[epoc][var])
                d = d_c[c]
                print np.array(d.iloc[0][var+"_master"])
                print np.array(d.iloc[0][var+"_"+pareja[1]])

            c += 1
            print "*"*70
    print len(data1), len(data2), len(d_c)

#check_get_data(data1, data2, d_c)

def d_teta_calc(rx_1, rx_2):
    import numpy as np
    import nvector as nv
    from nvector import deg

    wgs84n = dict(a=6378137.0, f=1.0/298.257223563)
    p_EB_E1 = 6371e3 * np.vstack((rx_1))
    p_EB_E2 = 6371e3 * np.vstack((rx_2))

    n_EB_E1, z_EB1 = nv.p_EB_E2n_EB_E(p_EB_E1, **wgs84n)
    n_EB_E2, z_EB2 = nv.p_EB_E2n_EB_E(p_EB_E2, **wgs84n)

    lat_EB1, lon_EB1 = nv.n_E2lat_lon(n_EB_E1)
    lat_EB2, lon_EB2 = nv.n_E2lat_lon(n_EB_E2)
    h = -z_EB1
    lat1, lon1 = deg(lat_EB1), deg(lon_EB1)
    lat2, lon2 = deg(lat_EB2), deg(lon_EB2)
    #print lat1, lon1, lat2, lon2

    wgs84 = nv.FrameE(name='WGS84')

    pointA = wgs84.GeoPoint(latitude=lat1, longitude=lon1, degrees=True)
    pointB = wgs84.GeoPoint(latitude=lat2, longitude=lon2, degrees=True)

    p_AB_E = nv.diff_positions(pointA, pointB)

    frame_N = nv.FrameN(pointA)
    p_AB_N = p_AB_E.change_frame(frame_N)
    p_AB_N = p_AB_N.pvector.ravel()

    azimuth = np.arctan2(p_AB_N[1], p_AB_N[0])
    #teta = np.rad2deg(azimuth)
    #print 'azimuth = {%4.2f} deg' %(teta)

    d = np.linalg.norm(p_AB_E.pvector)

    return d, azimuth, p_AB_E.pvector.ravel()

def is_number(s):
    # http://stackoverflow.com/questions/354038/how-do-i-check-if-a-string-is-a-number-float-in-python
    try:
        float(s) # for int, long and float
    except ValueError:
        try:
            complex(s) # for complex
        except ValueError:
            return False

    return True

import re
# http://stackoverflow.com/questions/2669059/how-to-sort-alpha-numeric-set-in-python
def sorted_nicely( l ):
    """ Sort the given iterable in the way that humans expect."""
    convert = lambda text: int(text) if text.isdigit() else text
    alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ]
    return sorted(l, key = alphanum_key)

def get_data(dat_common, data_rx, epoc, csats, station_name):
        import types
        requested_obstypes = ["C1", "C2", "P1", "P2", "L1", "L2", "S1", "S2"]
        columns2search = ["prns", "alphas", "betas", "prns_elev", "prns_az", "prns_clockbias", "prns_pos", "Iono", "svTimeCtes"]

        col_rename = {k:k+"_master" for k in columns2search+requested_obstypes}
        col_rename["prns"] = "prns"

        r = []
        data = {}	
        
        # http://thomas-cokelaer.info/blog/2014/06/pandas-how-to-compare-dataframe-with-none/
        if len(dat_common.columns) == 0: #isinstance(dat_common, types.NoneType) == True: #dat_common.empty:
            #print('DataFrame is empty!')
            obstypes = [c for c in data_rx.columns if c in requested_obstypes]

            idx1 = []
            for k, sat in enumerate(data_rx.iloc[epoc].prns):
                if sat in csats:
                    idx1.append(k)

            columns = ["prns", "prns_elev", "prns_az", "prns_clockbias", "prns_pos", "Iono", "svTimeCtes"]+obstypes
            names = []

            for col in data_rx.columns:
                if col in columns:
                    names.append(col)
                    values = data_rx.iloc[epoc][col][idx1]
                elif col == "prns":
                    names.append(col)
                    values = csats
                else:
                    names.append(col)
                    values = np.array(data_rx.iloc[epoc][col])

                r.append(values)

            r = pd.Series(r, index=names).to_frame()
            df_temp =pd.DataFrame(r.T, columns=names)
            df_temp.rename(columns=col_rename, inplace=True)
            return df_temp

        else:

            obstypes = [c for c in data_rx.columns if c in requested_obstypes]

            idx1 = []
            for k, sat in enumerate(data_rx.iloc[epoc].prns):
                if sat in csats:
                    idx1.append(k)

            for obstype in obstypes:
                dat_common[obstype+"_"+station_name] = pd.Series([data_rx.iloc[epoc][obstype][idx1]], index=dat_common.index)

            columns = ["prns_elev", "prns_az", "prns_clockbias", "prns_pos", "Iono", "svTimeCtes"]

            for col in columns2search:
                if col in columns:
                    values = data_rx.iloc[epoc][col][idx1]
                    key = col+"_"+station_name
                elif col == "prns":
                    values = csats
                    key = "csats"
                else:
                    key = col+"_"+station_name
                    values = np.array(data_rx.iloc[epoc][col])

                dat_common[key] = pd.Series([values], index=dat_common.index)

        return dat_common

def inspect_rinex(obsfile, navfile, silent_mode=False):
    obsHeader, obsData = gpstk.readRinex3Obs(obsfile)
    navHeader, navData = gpstk.readRinex3Nav(navfile)
    # setup ephemeris store to look for satellite positions
    bcestore = gpstk.GPSEphemerisStore()
    for navDataObj in navData:
        ephem = navDataObj.toGPSEphemeris()
        bcestore.addEphemeris(ephem)
    bcestore.SearchNear()
    navData.close()

    rec_pos = np.array([obsHeader.antennaPosition[0], obsHeader.antennaPosition[1], obsHeader.antennaPosition[2]])
    lat, lon, alt = ecef2lla(rec_pos[0], rec_pos[1], rec_pos[2], isradians=True)
    print "receiver position lat=%f, lon=%f, alt=%f"%(np.rad2deg(lat), np.rad2deg(lon), alt)
    silent_mode = False
    requested_obstypes = ["P1", "P2", "L1", "L2", "C1", "C2", "S1", "S2"]
    obsidxs = []
    obstypes = []
    obsdefs = np.array([i for i in obsHeader.R2ObsTypes])
    for i in requested_obstypes:
        w = np.where(obsdefs==i)[0]
        if len(w)!=0:
            obsidxs.append(w[0])
            obstypes.append(i)
        else:
            if silent_mode == False:
                print ("WARNING! observation `"+i+"` no present in file "+obsfile)
            #else:
                #print ""

    obsObject = obsData.next()
    print "Time of observation", obsObject.time
    print "SatID GPS  GLON",
    for P1_idx, otype in zip(obsidxs, obstypes):
            print "\t  ", otype,
    print ""

    for satID, datumList in obsObject.obs.iteritems():

        isGPS     = True if satID.system==satID.systemGPS else False
        isGlonass = True if satID.system==satID.systemGlonass else False

        print "  %2d %5s %5s"%(satID.id, isGPS, isGlonass),

        for P1_idx, otype in zip(obsidxs, obstypes):
            P1 = obsObject.getObs(satID, P1_idx).data
            print "%10.3f"%( P1),

        print ""



        #print "  %2d %5s %5s %10.3f %10.3f %10.3f %10.3f"%(satID.id, isGPS, isGlonass, P1, P2, L1, L2)

#inspect_rinex(obsfile1, navfile1)

def rinex_to_dataframe_ionosfe(obsfile, navfile, silent_mode=False, ion_max=60):
    c = 299792458.

    observation_types=["P1", "P2", "L1", "L2", "C1", "C2", "S1", "S2"]
    obsHeader, obsData = gpstk.readRinex3Obs(obsfile)
    navHeader, navData = gpstk.readRinex3Nav(navfile)

    nh=gpstk.readRinexNav(navfile)[0]
    alphas = nh.ionAlpha
    betas  = nh.ionBeta
    gamma=gpstk.GAMMA_GPS ##CARGANDO GAMMA

    # setup ephemeris store to look for satellite positions
    bcestore = gpstk.GPSEphemerisStore()
    for navDataObj in navData:
        ephem = navDataObj.toGPSEphemeris()
        bcestore.addEphemeris(ephem)
    bcestore.SearchNear()
    navData.close()

    rec_pos = [obsHeader.antennaPosition[0], obsHeader.antennaPosition[1], obsHeader.antennaPosition[2]]

    requested_obstypes = observation_types
    obsidxs = []
    obstypes = []
    obsdefs = np.array([i for i in obsHeader.R2ObsTypes])
    for i in requested_obstypes:
        w = np.where(obsdefs==i)[0]
        if len(w)!=0:
            obsidxs.append(w[0])
            obstypes.append(i)
        else:
            if silent_mode == False:
                print ("WARNING! observation `"+i+"` no present in file "+obsfile)
            #else:
            #    print ""
    #obsidxs, obstypes

    r = []
    for obsObject in obsData:
        prnlist = []
        obsdict = {}
        prnspos = []
        #prns_Timecorr  = []
        prns_clockbias = []
        prns_relcorr   = []
        prnselev       = []
        prnsaz         = []
        iono_delay     = []
        for i in obstypes:
            obsdict[i]=[]

        gpsTime = gpstk.GPSWeekSecond(obsObject.time)

        for satID, datumList in obsObject.obs.iteritems():
            if satID.system == satID.systemGPS:
                prnlist.append("".join(str(satID).split()))
                eph   = bcestore.findEphemeris(satID, obsObject.time)

                for i in range(len(obsidxs)):
                    obsdict[obstypes[i]].append(obsObject.getObs(satID, obsidxs[i]).data)

                P1 = obsObject.getObs(satID, obsidxs[0]).data

                # para reemplazar los valores de P1 que son cero en el rinex
                # por los valores de C1, que deberian ser semejantes
                if np.mean(P1) < 1:
                    #print obstypes[4]
                    P1 = obsObject.getObs(satID, obsidxs[4]).data

                P2 = obsObject.getObs(satID, obsidxs[1]).data
                Id = (1*1/(1-gamma)*(P1-P2))
                if np.abs(Id) < ion_max:
                    iono_delay.append(Id)
                else:
                    iono_delay.append(ion_max)

                svTime = obsObject.time - P1/c
                svXvt = eph.svXvt(svTime)
                
                # svTimeCorr: from http://math.tut.fi/posgroup/icl-gnss2012.pdf
                svClkRelative = svXvt.getRelativityCorr()
                svClkBias = svXvt.getClockBias()
                svToc = gpsTime.getSOW() - P1/c - svClkBias
                svClkDrift = eph.af1
                svFreqDrift = eph.af2
                svTimeCorr = svClkBias + svClkDrift*(svToc) + svFreqDrift*(svToc**2) + svClkRelative - eph.Tgd  
                svTime += svTimeCorr
                #Raul
                #svTime -= svXvt.getClockBias() + svClkRelative
                svXvt = eph.svXvt(svTime)

                #prns_Timecorr.append(svTimeCorr)
                prnspos.append([svXvt.x[0], svXvt.x[1], svXvt.x[2]])
                prns_clockbias.append(svXvt.getClockBias())
                prns_relcorr.append(svXvt.getRelativityCorr())

                prnselev.append(obsHeader.antennaPosition.elvAngle(svXvt.getPos()))
                prnsaz.append(obsHeader.antennaPosition.azAngle(svXvt.getPos()))

        r.append([gpsTime.getWeek(), gpsTime.getSOW(), svTimeCorr, alphas, betas, gamma, np.array(prnlist), np.array(prnspos), np.array(prns_clockbias),
                  np.array(prns_relcorr), np.array(prnselev), np.array(prnsaz), np.array(iono_delay)] + [np.array(obsdict[i]) for i in obstypes])

    names=["gps_week", "gps_sow", "gps_time_corr", "alphas", "betas", "gamma", "prns", "prns_pos", "prns_clockbias", "prns_relcorr", "prns_elev", "prns_az", "Iono"] + obstypes
    r = pd.DataFrame(r, columns=names)
    obsData.close()
    return r, bcestore, np.array(rec_pos)

def rinex_to_dataframe_ionosfe2(obsfile, navfile, silent_mode=False, ion_max=60):
    c = gpstk.C_MPS

    observation_types=["P1", "P2", "L1", "L2", "C1", "C2", "S1", "S2"]
    obsHeader, obsData = gpstk.readRinex3Obs(obsfile)
    navHeader, navData = gpstk.readRinex3Nav(navfile)

    nh=gpstk.readRinexNav(navfile)[0]
    alphas = nh.ionAlpha
    betas  = nh.ionBeta
    gamma=gpstk.GAMMA_GPS ##CARGANDO GAMMA

    # setup ephemeris store to look for satellite positions
    bcestore = gpstk.GPSEphemerisStore()
    for navDataObj in navData:
        ephem = navDataObj.toGPSEphemeris()
        bcestore.addEphemeris(ephem)
    bcestore.SearchNear()
    navData.close()

    rec_pos = [obsHeader.antennaPosition[0], obsHeader.antennaPosition[1], obsHeader.antennaPosition[2]]

    requested_obstypes = observation_types
    obsidxs = []
    obstypes = []
    obsdefs = np.array([i for i in obsHeader.R2ObsTypes])
    for i in requested_obstypes:
        w = np.where(obsdefs==i)[0]
        if len(w)!=0:
            obsidxs.append(w[0])
            obstypes.append(i)
        else:
            if silent_mode == False:
                print ("WARNING! observation `"+i+"` no present in file "+obsfile)
            #else:
            #    print ""
    #obsidxs, obstypes

    r = []
    for obsObject in obsData:
        prnlist = []
        obsdict = {}
        prnspos = []
        #prns_Timecorr  = []
        svTimeCtes = []
        prns_clockbias = []
        prns_relcorr   = []
        prnselev       = []
        prnsaz         = []
        iono_delay     = []
        for i in obstypes:
            obsdict[i]=[]

        gpsTime = gpstk.GPSWeekSecond(obsObject.time)

	#fg = 0
	#print ""
        for satID, datumList in obsObject.obs.iteritems():
            if satID.system == satID.systemGPS:
                prnlist.append("".join(str(satID).split()))
                eph   = bcestore.findEphemeris(satID, obsObject.time)

                for i in range(len(obsidxs)):
                    obsdict[obstypes[i]].append(obsObject.getObs(satID, obsidxs[i]).data)

                P1 = obsObject.getObs(satID, obsidxs[0]).data

                # para reemplazar los valores de P1 que son cero en el rinex
                # por los valores de C1, que deberian ser semejantes
                if np.mean(P1) < 1:
                    #print obstypes[4]
                    P1 = obsObject.getObs(satID, obsidxs[4]).data

                P2 = obsObject.getObs(satID, obsidxs[1]).data
                Id = (1*1/(1-gamma)*(P1-P2))
                if np.abs(Id) < ion_max:
                    iono_delay.append(Id)
                else:
                    iono_delay.append(ion_max)

                svTime = obsObject.time - P1/c
                svXvt = eph.svXvt(svTime)
                
                # svTimeCorr: from http://math.tut.fi/posgroup/icl-gnss2012.pdf
                svClkRelative = svXvt.getRelativityCorr()
                svClkBias = svXvt.getClockBias()
                svToc = gpsTime.getSOW() - P1/c - svClkBias
                svClkDrift = eph.af1
                svFreqDrift = eph.af2
                svTimeCorr = svClkBias + svClkDrift*(svToc) + svFreqDrift*(svToc**2) + svClkRelative - eph.Tgd  
                svTime += svTimeCorr
                svTimeCtes.append([svClkDrift, svFreqDrift, eph.Tgd])
                
        	#print fg, svTimeCtes
        	#fg += 1
                #Raul
                #svTime -= svXvt.getClockBias() + svClkRelative
                svXvt = eph.svXvt(svTime)

                #prns_Timecorr.append(svTimeCorr)
                prnspos.append([svXvt.x[0], svXvt.x[1], svXvt.x[2]])
                prns_clockbias.append(svXvt.getClockBias())
                prns_relcorr.append(svXvt.getRelativityCorr())

                prnselev.append(obsHeader.antennaPosition.elvAngle(svXvt.getPos()))
                prnsaz.append(obsHeader.antennaPosition.azAngle(svXvt.getPos()))

        r.append([gpsTime.getWeek(), gpsTime.getSOW(), svTimeCorr, alphas, betas, gamma, np.array(prnlist), np.array(prnspos), np.array(prns_clockbias),
                  np.array(prns_relcorr), np.array(prnselev), np.array(prnsaz), np.array(iono_delay), np.array(svTimeCtes)] + [np.array(obsdict[i]) for i in obstypes])

    names=["gps_week", "gps_sow", "gps_time_corr", "alphas", "betas", "gamma", "prns", "prns_pos", "prns_clockbias", "prns_relcorr", "prns_elev", "prns_az", "Iono", "svTimeCtes"] + obstypes
    r = pd.DataFrame(r, columns=names)
    obsData.close()
    return r, bcestore, np.array(rec_pos)

def read_Rinex_Compri(data_folder, parejas):
    import os, re
    
    # Nombre de la carpeta donde se guardara la descarga
    stations = [x.lower() for par in parejas.values() for x in par]
    #verificando las parejas de estaciones de las que se tiene archivos rinex
    
    estaciones = {}

    dir_files = [fn for fn in listdir(data_folder)]

    for filename in listdir(data_folder):
        base, ext = splitext(filename)
        
        key = base[:-1]
        for par in parejas.values(): 
            for station in par:

                if base[:4].lower() == station.lower():
                    
                    path = data_folder+"/"
                    
                    obstype = str(base[-1]+'-zip').lower()
                    estaciones.setdefault(key, {})[obstype] = path+filename

    return estaciones


def readAllStored_Rinex_Compri(data_folder, parejas_estaciones):
    import os, re

    # Nombre de la carpeta donde se guardara la descarga
    stations = [x.lower() for par in parejas_estaciones.values() for x in par]
    #verificando las parejas de estaciones de las que se tiene archivos rinex

    estaciones = {}

    dir_files = [fn for fn in listdir(data_folder)]
    #print dir_files

    for filename in dir_files: #os.listdir(data_folder):
        base, ext = splitext(filename)

        if base[:4].lower() in stations:
            station, ObsType = splitext(base)

            #print station
            path = data_folder+"/"

            # GRACIAS A ESTA RESPUESTO, EL CODIGO FUNCIONA
            # http://stackoverflow.com/questions/21613038/adding-new-keys-to-a-python-dictionary-from-variables?answertab=votes#tab-top
            # http://stackoverflow.com/questions/14012918/passing-dictionary-keys-to-a-new-dictionary
            if base[-1].lower() == 'o':
                estaciones.setdefault(station, {})['name_station'] = station[:4]
                estaciones.setdefault(station, {})['obs-zip'] = path+filename

            else:

                estaciones.setdefault(station, {})['name_station'] = station[:4]
                estaciones.setdefault(station, {})['nav-zip'] = path+filename

    return estaciones

@autojit
def process_day(day):
    if   day<10:
         day="00"+str(day)
    elif day<100:
         day="0"+str(day)
    else:
         day=str(day)+str("0")

    return day

@autojit
def process_day2(day):
    if   day<10:
         day="00"+str(day)+"0"
    elif day>9 and day<100:
         day="0"+str(day)+"0"
    else:
         day=str(day)+str("0")

    return day


def get_dist(rx_1, rx_2):
    import numpy as np
    import nvector as nv
    from nvector import deg

    lat1, lon1 = rx_1[0], rx_1[1]
    lat2, lon2 = rx_2[0], rx_2[1]
    #print lat1, lon1, lat2, lon2

    wgs84 = nv.FrameE(name='WGS84')

    pointA = wgs84.GeoPoint(latitude=lat1, longitude=lon1, degrees=True)
    pointB = wgs84.GeoPoint(latitude=lat2, longitude=lon2, degrees=True)

    p_AB_E = nv.diff_positions(pointA, pointB)

    frame_N = nv.FrameN(pointA)
    p_AB_N = p_AB_E.change_frame(frame_N)
    p_AB_N = p_AB_N.pvector.ravel()

    azimuth = np.arctan2(p_AB_N[1], p_AB_N[0])
    #teta = np.rad2deg(azimuth)
    #print 'azimuth = {%4.2f} deg' %(teta)
    
    d = np.linalg.norm(p_AB_E.pvector)
    
    return d, azimuth#, p_AB_E.pvector.ravel()

def read_kmz_file(fname, max_distancia):
    import sys
    import zipfile
    import glob
    from xml.dom import minidom

    #fname = "sites.kmz"
    path_cnt = 0
    zf = zipfile.ZipFile(fname, 'r')

    #print zf, type(zf)
    for fn in zf.namelist():
        #print fn
        if fn.endswith('.kml'):
            content = zf.read(fn)
            xmldoc = minidom.parseString(content)
            placemarks = xmldoc.getElementsByTagName('Placemark')

            nodes = {}
            for placemark in placemarks:
                nodename = placemark.getElementsByTagName("name")[0].firstChild.data
                coords = placemark.getElementsByTagName("coordinates")[0].firstChild.data
                lst1 = coords.split(",")
                longitude = float(lst1[0])
                latitude = float(lst1[1])
                nodes[nodename] = (latitude, longitude)

            from itertools import combinations
            #print nodes
            parejas = [{j: nodes[j] for j in i} for i in combinations(nodes, 2)]

            d_min = 1e8

            GPS_Stations = {}
            c = 0
            for n in parejas:
                #print n, type(n), n.keys()[0], n.values()[0]
                rx_1 = n.values()[0]
                rx_2 = n.values()[1]

                d,_ = get_dist(rx_1, rx_2)
                if (d_min > d):
                    if (d < max_distancia):

                        print d_min, d, "entre", n.keys()[0], "y", n.keys()[1]
                        #d_min = d
                        GPS_Stations[c] = [n.keys()[0], n.keys()[1]]
                        c += 1
    return GPS_Stations


def get_data_stations(f_in, max_dis):

    List_Stations = read_kmz_file(f_in, max_dis)

    stations = {}
    
    for (idx, two) in enumerate(List_Stations.iteritems()):
        #print two, type(two[1])
        two = [str(x) for x in two[1]]
        #print tuple(two)
        stations[idx] = tuple(two)
        
    return stations

#######################################################

# Full Guide to open files with python
# https://goo.gl/DWxWqR

def download(url, filename, dest_dir=os.getcwd()+"/"+"new_data/"):
    import urllib2

    if not os.path.exists(dest_dir):
        print dest_dir
        os.makedirs(dest_dir)

    filename = dest_dir+filename
    #page=urllib2.urlopen(url)
    #open(filename,'wb').write(page.read())

    testfile = urllib.URLopener()
    testfile.retrieve(url, filename)

def extract_from_zip(zip_file, in_path, outpath):
    """
        Descompresor funcional
    """
    import zipfile

    in_path = str(in_path)+"/"+zip_file
    #print in_path
    archive = zipfile.ZipFile(in_path, 'r')

    for f in archive.namelist():
        if f.endswith(('n', 'o')):
            makedir(Simu_Folder)
            archive.extract(f, Simu_Folder)
            fname, ext = splitext(zip_file)
            cmd = "mv "+Simu_Folder+"/"+str(f)+" "+Simu_Folder+"/"+fname[:-1]+f[-1]
            #print cmd
            system(cmd)


def unzip(filename, source_dir, dest_dir):
    """
	Descompresor no probado
    """
    import zipfile, os.path

    source_filename = source_dir+filename
    print source_filename

    if not os.path.exists(dest_dir):
        print dest_dir
        os.makedirs(dest_dir)

    with zipfile.ZipFile(source_filename) as zf:
        for member in zf.infolist():
            words = member.filename.split('/')
            path = dest_dir
            for word in words[:-1]:
                drive, word = os.path.splitdrive(word)
                head, word = os.path.split(word)
                if word in (os.curdir, os.pardir, ''): continue
                path = os.path.join(path, word)
            zf.extract(member, path)

def makedir(path):
    if not os.path.exists(path):
        print path
        os.makedirs(path)

def file_url_exist(location):
    request = urllib2.Request(location)
    request.get_method = lambda : 'HEAD'
    try:
        response = urllib2.urlopen(request)
        return True
    except urllib2.URLError, e:
        #print(e.args)
        return False
    except urllib2.HTTPError:
        return False

def urlretrieve(urllib2_request, filepath, reporthook=None, chunk_size=4096):
    """
    https://stackoverflow.com/questions/51212/how-to-write-a-download-progress-indicator-in-python
    """
    req = urllib2.urlopen(urllib2_request)

    if reporthook:
        # ensure progress method is callable
        if hasattr(reporthook, '__call__'):
            reporthook = None

        try:
            # get response length
            total_size = req.info().getheaders('Content-Length')[0]
        except KeyError:
            reporthook = None

    data = ''
    num_blocks = 0

    with open(filepath, 'w') as f:
        while True:
            data = req.read(chunk_size)
            num_blocks += 1
            if reporthook:
                # report progress
                reporthook(num_blocks, chunk_size, total_size)
            if not data:
                break
            f.write(data)

    # return downloaded length
    return len(data)

    #######################################################




def descarga_Rinex_Compri(data_folder, parejas_kmz, dia1, dian, ano):
    #import descargador_RINEX as dl_RINEX
    #import os
    #import urllib2
    #import re

    # Nombre de la carpeta donde se guardara la descarga
    #data_folder = os.getcwd() +"/" + data_folder      #"new_data"
    cmd1 = "mkdir -p " + data_folder    # COMANDO CREA CARPETA DE DESCARGA
    system(cmd1)

    dir_files = [fn for fn in listdir(data_folder)]
    #print dir_files

    # http://stackoverflow.com/questions/2486145/python-check-if-url-to-jpg-exists
    def file_exists(url):
        request = urllib2.Request(url)
        request.get_method = lambda : 'HEAD'
        try:
            response = urllib2.urlopen(request)
            return True
        except:
            return False

    def valid_extension(x, valid={'.gz', '.Z'}):
        return splitext(x)[1].lower() in valid

    # MAIN FUNCTION
    ###########################################################################S
    print "\n Descargando Nuevos Archivos ...."
    print "************************\n"

    #rint "Buscando Archivos para: "
    print "\t [Estacion] \t [dia GPS] \t\t [RESULTADO]"

    for key, value in parejas_kmz.iteritems():
        #print k, v[0], v[1], type(v[0]), str(v[0])
        for station in value:
            #print stat, type(stat)
            # BASADO EN EL CALENDARIO http://gps.topografia.upm.es/www/2015G.htm


            # RED Gadner tiene met, tropo, rinex files de casi todo
            # USADOS EN LA TESIS DE
            # http://www.inpe.br/pos_graduacao/cursos/geo/arquivos/dissertacoes/dissertacao_olusegun_folarin_2013.pdf


            # ftp://garner.ucsd.edu/pub/troposphere/0990/
            # ftp://garner.ucsd.edu/pub/met/2017/010/
            # ftp://garner.ucsd.edu/pub/rinex/2017/010/
            # ftp://garner.ucsd.edu/pub/
            # http://madrigal.haystack.mit.edu/madrigal/siteSpecific/tec_sources.html

            Servers = {
                'garner':{'ftp':'ftp://garner.ucsd.edu/pub/rinex/',
                        },
                'lox':{   'ftp':'ftp://lox.ucsd.edu/pub/rinex/',
                        },
                #
                'cors1' :{'ftp':'ftp://geodesy.noaa.gov/cors/rinex/',
                        },
                'cors2' :{'ftp':'ftp://alt.ngs.noaa.gov/cors/rinex/',
                        },
                #
                'igac-gov':{'ftp':'ftp://anonimo:anonimo@132.255.20.140/',
                        },
                'nasa'  :{'ftp':'ftp://cddis.gsfc.nasa.gov/pub/gps/data/daily/',
                        },
                'unavco':{'ftp':'ftp://data-out.unavco.org/pub/rinex/',
                        },
                'rbmc'  :{'ftp':'ftp://geoftp.ibge.gov.br/informacoes_sobre_posicionamento_geodesico/rbmc/dados/',
                        }
            }

            tipos_Rinex  = ['obs', 'nav', 'd']
            station = str(station.lower())

            print "\t %s" %(station)


            for i in range(int(dia1), int(dian)+1):
                """
                if i<10:
                    dia="00"+str(i)
                elif i<100:
                    dia="0"+str(i)
                else:
                    dia=str(i)
                """
                dia = process_day(i)

                c = 0
                flag = 0
                for server in Servers.keys():
                    c += 1
                    if flag == 1 or flag == 2:
                        # hacer que se acabe la busqueda en los servidores
                        c == len(Servers[server])

                    else:
                        fail = []     # guarda tipo de archivo que no se encontro para descarga
                        f_exist = []  # contar los archivos que existen

                        for tipo in tipos_Rinex:
                            #print tipo

                            #print "\t", tipo,
                            f_name = station+dia+'0.'+ano[-2:]+tipo[:1]

                            #http://stackoverflow.com/questions/17777311/filter-file-list-in-python-lowercase-and-uppercase-extension-files
                            pat = compile(r'[.](gz|Z)$', re.IGNORECASE)
                            filenames = [filename for filename in listdir(data_folder)
                                         if search(pat, filename)]

                            #matches = [re.search("("+f_name+".*)", f, flags=re.I) for f in filenames]
                            matches = [f for f in filenames if findall("("+f_name+".*)", f, flags=re.I)]

                            #print matches
                            if len(matches) != 0:
                                f_exist.append(tipo)
                                #print "esta en dir"

                                if len(f_exist) == len(tipos_Rinex):
                                    flag = 2
                                    #break

                            else:
                                #print "No esta en dir"

                                #Servers['igac-gov']['ftp']    = 'ftp://anonimo:anonimo@132.255.20.140/'

                                Servers['igac-gov']['path']    = dia+'/'+ano[-2:]+tipo[:1]+'/'
                                Servers['igac-gov']['archivo'] = str(f_name).upper()+'.gz'

                                #Servers['nasa']['ftp']         ='ftp://cddis.gsfc.nasa.gov/pub/gps/data/daily/'
                                Servers['nasa']['path']        = ano+'/'+dia+'/'+ano[-2:]+tipo[:1]+'/'
                                Servers['nasa']['archivo']     = f_name+'.Z'

                                #Servers['unavco']['ftp']       ='ftp://data-out.unavco.org/pub/rinex/'
                                Servers['unavco']['path']      = tipo+'/'+ano+'/'+dia+'/'
                                Servers['unavco']['archivo']   = f_name+'.Z'

                                Servers['garner']['path']      = ano+'/'+dia+'/'
                                Servers['garner']['archivo']   = f_name+'.Z'

                                Servers['lox']['path']         = ano+'/'+dia+'/'
                                Servers['lox']['archivo']      = f_name+'.Z'

                                #""
                                Servers['cors1']['path']      = ano+'/'+dia+'/'+station[:4]
                                Servers['cors1']['archivo']   = f_name+'.Z'

                                Servers['cors2']['path']      = ano+'/'+dia+'/'+station[:4]
                                Servers['cors2']['archivo']   = f_name+'.Z'
                                #""

                                ##################################################################################
                                if server == 'rbmc':

                                    #2017/010/ifsc0101.zip
                                    f_name = station+dia+'0.'+ano[-2:]+tipo[:1]
                                    Servers[server]['path'] = str(ano+'/'+dia+'/')
                                    Servers[server]['archivo'] = str(f_name).lower()+'.zip'

                                    ## convirtiendo a la convencion de nombres standar que yo manejo
                                    ## dentro de mi carpeta new_data

                                    filename = Servers['rbmc']['archivo']
                                    url = Servers[server]['ftp']+Servers[server]['path']+station+dia+'1'+".zip"

                                    ## Check if not exist in new_data folder
                                    if not os.path.exists(data_folder+filename):

                                        # Check if exist in webpage
                                        if file_url_exist(url):

                                            #print url
                                            makedir(data_folder)
                                            #makedir(simu_path)

                                            # FIRST VERSION
                                            #download(url, filename, dest_dir=folderpath)

                                            # OPTIMIZED VERSION
                                            urlretrieve(urllib2_request=url, filepath=data_folder+filename)
                                            #print "finish"
                                            #unzip(filename=filename, source_dir=down_path, dest_dir=simu_path)

                                            f_exist.append(tipo)

                                        else:
                                            if tipo not in fail:
                                                fail.append(tipo)
                                                pass

                                    else:
                                        flag = 2
                                        #print "Red RBMC"
                                        break

                                ##################################################################################


                                url = Servers[server]['ftp']+Servers[server]['path']
                                archivo = Servers[server]['archivo']

                                # Red gps new zeland
                                #https://apps.linz.govt.nz/ftp/positionz/2016/026/
                                #ftp://ftp.geonet.org.nz/gps/rinex/2016/003/

                                # GLONAS DESDE SERVIDOR NASA
                                #ftp://igscb.jpl.nasa.gov/igscb/glonass/products/1920/
				
				#print url

                                if not os.path.exists(data_folder+archivo):
                                    #https://code-maven.com/urllib-vs-urllib2
                                    if file_exists(url+archivo):
                                        #print "existe url"
                                        #print url
                                        html_content = urllib2.urlopen(url).read()
                                        matches = re.findall(archivo, html_content, flags=re.I)

                                        if len(matches) != 0:

                                            #print archivo, url+archivo
                                            if flag == 0:

                                                cmd = "wget " + str(url+archivo) + " -P " + data_folder
                                                #print "\t", cmd
                                                system(cmd)

                                            f_exist.append(tipo)

                                    else:
                                        if tipo not in fail:
                                            fail.append(tipo)
                                            pass
                                        #print "No encontrado el archivo en ", server
                                        #print fail

                                else:
                                    flag = 2
                                    #print "Red RBMC"
                                    break
                                
                        #if len(f_exist) == len(tipos_Rinex) and len(fail) == 0:
                        #    flag = 1


                    if c == len(Servers[server]):
                        if flag == 0:
                            print "\t\t\t %s \t\t No encontrados Ficheros %s" %(dia, fail)
                            #print dia, "

                        elif flag ==2:
                            print "\t\t\t %s \t\t Archivos ya disponibles!!!" %(dia)
                        else:
                            print "\t\t\t %s \t\t Descarga Exitosa!!!" %(dia)

    print "Descarga Finalizada!! \n"

    DB_Rinex_Compri = readAllStored_Rinex_Compri(data_folder, parejas_kmz)

    return DB_Rinex_Compri



#######################################################################################################
#######################################################################################################
#######################################################################################################

from numba import autojit
from numpy.linalg import norm

@autojit
def compute_distances(rc, svs):
    # return np.array( [np.sqrt((rc[0]-sv[0])**2 + (rc[1]-sv[1])**2) for sv in svs] )
    return np.linalg.norm(rc-svs, axis=1)

@autojit
def predict_pseudoranges(x, prns_pos, prns_clockbias):
    c = 299792458
    rhos    = compute_distances(x[:3], prns_pos)
    pranges = rhos + x[3]-c*prns_clockbias
    return rhos, pranges

@autojit
def apply_earth_rotation_to_svs_position(svs, prs):
      c = 299792458
      we = 7.2921159e-5
      rpos = np.zeros(svs.shape)
      pos = np.array(svs)
      for i in range(len(pos)):
        dt = prs[i]/c
        theta = we*dt
        R = np.array([[np.cos(theta), np.sin(theta),0.],[-np.sin(theta), np.cos(theta),0.],[0.,0.,1.]])
        rpos[i] = R.dot(pos[i])
      svs = np.array(rpos)
      return svs

def compute_least_squares_position(svs, svs_clocks, prs, max_iters=200, apply_earth_rotation=True):

    if apply_earth_rotation:
        #print type(svs), type(prs)
        svs = apply_earth_rotation_to_svs_position(svs, prs)

    if len(svs)==0 or len(prs)==0:
        return np.array([0.,0.,0.,0.]),None, None, None

    ri = np.array([0.,0.,0.,0.])

    #for i in range(max_iters):
    delta,i = 1,0
    while (norm(delta)>1e-8 and i<max_iters):
        rhos, pranges = predict_pseudoranges(ri, svs, svs_clocks)
        b = prs - pranges
        A = np.hstack(((ri[:3]-svs)/rhos[:,None],np.ones((len(b), 1))))
        delta =  np.linalg.pinv(A.T.dot(A)).dot(A.T).dot(b)
        ri += delta
        i+=1
    return ri, A, b, delta



def d_teta_calc(rx_1, rx_2):
    import numpy as np
    import nvector as nv
    from nvector import deg

    wgs84n = dict(a=6378137.0, f=1.0/298.257223563)
    p_EB_E1 = 6371e3 * np.vstack((rx_1))
    p_EB_E2 = 6371e3 * np.vstack((rx_2))

    n_EB_E1, z_EB1 = nv.p_EB_E2n_EB_E(p_EB_E1, **wgs84n)
    n_EB_E2, z_EB2 = nv.p_EB_E2n_EB_E(p_EB_E2, **wgs84n)

    lat_EB1, lon_EB1 = nv.n_E2lat_lon(n_EB_E1)
    lat_EB2, lon_EB2 = nv.n_E2lat_lon(n_EB_E2)
    h = -z_EB1
    lat1, lon1 = deg(lat_EB1), deg(lon_EB1)
    lat2, lon2 = deg(lat_EB2), deg(lon_EB2)
    #print lat1, lon1, lat2, lon2

    wgs84 = nv.FrameE(name='WGS84')

    pointA = wgs84.GeoPoint(latitude=lat1, longitude=lon1, degrees=True)
    pointB = wgs84.GeoPoint(latitude=lat2, longitude=lon2, degrees=True)

    p_AB_E = nv.diff_positions(pointA, pointB)

    frame_N = nv.FrameN(pointA)
    p_AB_N = p_AB_E.change_frame(frame_N)
    p_AB_N = p_AB_N.pvector.ravel()

    azimuth = np.arctan2(p_AB_N[1], p_AB_N[0])
    #teta = np.rad2deg(azimuth)
    #print 'azimuth = {%4.2f} deg' %(teta)

    d = np.linalg.norm(p_AB_E.pvector)

    return d, azimuth, p_AB_E.pvector.ravel()

def get_dist(rx_1, rx_2):
    import numpy as np
    import nvector as nv
    from nvector import deg

    lat1, lon1 = rx_1[0], rx_1[1]
    lat2, lon2 = rx_2[0], rx_2[1]
    #print lat1, lon1, lat2, lon2

    wgs84 = nv.FrameE(name='WGS84')

    pointA = wgs84.GeoPoint(latitude=lat1, longitude=lon1, degrees=True)
    pointB = wgs84.GeoPoint(latitude=lat2, longitude=lon2, degrees=True)

    p_AB_E = nv.diff_positions(pointA, pointB)

    frame_N = nv.FrameN(pointA)
    p_AB_N = p_AB_E.change_frame(frame_N)
    p_AB_N = p_AB_N.pvector.ravel()

    azimuth = np.arctan2(p_AB_N[1], p_AB_N[0])
    #teta = np.rad2deg(azimuth)
    #print 'azimuth = {%4.2f} deg' %(teta)

    d = np.linalg.norm(p_AB_E.pvector)

    return d, azimuth#, p_AB_E.pvector.ravel()

def read_kmz_file(fname, max_distancia):
    import sys
    import zipfile
    import glob
    from xml.dom import minidom

    #fname = "sites.kmz"
    path_cnt = 0
    zf = zipfile.ZipFile(fname, 'r')

    #print zf, type(zf)
    for fn in zf.namelist():
        #print fn
        if fn.endswith('.kml'):
            content = zf.read(fn)
            xmldoc = minidom.parseString(content)
            placemarks = xmldoc.getElementsByTagName('Placemark')

            nodes = {}
            for placemark in placemarks:
                nodename = placemark.getElementsByTagName("name")[0].firstChild.data
                coords = placemark.getElementsByTagName("coordinates")[0].firstChild.data
                lst1 = coords.split(",")
                longitude = float(lst1[0])
                latitude = float(lst1[1])
                nodes[nodename] = (latitude, longitude)

            from itertools import combinations
            #print nodes
            parejas = [{j: nodes[j] for j in i} for i in combinations(nodes, 2)]

            d_min = 1e8

            GPS_Stations = {}
            c = 0
            for n in parejas:
                #print n, type(n), n.keys()[0], n.values()[0]
                rx_1 = n.values()[0]
                rx_2 = n.values()[1]

                d,_ = get_dist(rx_1, rx_2)
                if (d_min > d):
                    if (d < max_distancia):

                        print d_min, d, "entre", n.keys()[0], "y", n.keys()[1]
                        #d_min = d
                        GPS_Stations[c] = [n.keys()[0], n.keys()[1]]
                        c += 1
    return GPS_Stations


@autojit
def predict_coderanges(x, prns_pos, prns_clockbias):
    c = 299792458.0
    f1 = 1575420000.0
    lamda = c/f1

    rhos    = compute_distances(x[:3], prns_pos)
    pcodes  = rhos + x[3]-c*prns_clockbias + x[4]*lamda
    return rhos, pcodes

def least_squares_position_codephase(svs, svs_clocks, prs, codes=None, elevs=None, snr=None, max_iters=200, apply_earth_rotation=True):

    c = 299792458.0
    f1 = 1575420000.0
    lamda = c/f1

    """
        Para emplear la solucion con codigo+fase se necesitan minimo 5 satelites u observaciones.
    """
    if apply_earth_rotation:
        #print type(svs), type(prs)
        svs = apply_earth_rotation_to_svs_position(svs, prs)

    if len(svs)==0 or len(prs)==0:
        return np.array([0.,0.,0.,0.]),None, None, None

    if len(snr) == 0 or len(elevs)== 0:
        print "SNR observable is needed"
        return np.array([0.,0.,0.,0.]),None, None, None

    ri = np.array([0.,0.,0.,0.])

    if len(codes) != 0 and len(codes)>5:
        ri = np.array([0.]*4 + [0.]*len(codes))
        bandera = True

    #for i in range(max_iters):
    delta,i = 1,0
    while (norm(delta)>1e-8 and i<max_iters):

        rhos, pranges = predict_pseudoranges(ri[:4], svs, svs_clocks)
        b = prs - pranges
        ones = np.ones((len(b), 1))
        zero = np.zeros((len(b), len(b))) #np.zeros((len(b),1))
        A = np.hstack(((ri[:3]-svs)/rhos[:,None], ones, zero))

        if bandera:
            # Considerando observables de fase
            rhos2, pcodes  = predict_coderanges(ri, svs, svs_clocks)
            bc = codes - pcodes
            #print np.shape(b), np.shape(bc)
            b = np.hstack((b, bc))
            #print np.shape(b), np.shape(bc)
            lambdas = lamda*np.eye(len(bc)) # *ones
            #print np.shape((ri[:3]-svs)/rhos[:,None]), np.shape(ones), np.shape(b), np.shape(pcodes)
            ai = np.hstack(((ri[:3]-svs)/rhos[:,None], ones, lambdas))
            #print np.shape(ai), np.shape(b)
            A = np.vstack((A, ai))

        if len(snr) != 0 or len(elevs) != 0:
            #wp = len(pranges)*(0.03 + 0.9*np.exp(-snr/elevs))#elevs/30.))
            wp = len(pranges)*(0.03 + 0.9*np.exp(-snr/elevs))#elevs/30.))
            wc = len(pranges)*(0.07 + 0.6*np.exp(-snr/elevs))#elevs/30.))
            w = np.hstack((wp, wc))
            #print np.shape(w), np.shape((A.T.dot(w*np.eye(len(w))))), np.shape(A.T), np.shape(b)
             #  + 1e-3*np.eye(len(ri))
            delta =  np.linalg.pinv((A.T.dot(w*np.eye(len(w)))).dot(A)).dot(A.T.dot(w*np.eye(len(w)))).dot(b)
        else:
            delta =  np.linalg.pinv(A.T.dot(A)).dot(A.T).dot(b)

        ri += delta
        i+=1
    return ri, A, b, delta

def weight_least_squares_position(svs, svs_clocks, prs, elevs, snr, max_iters=200, apply_earth_rotation=True):

    if apply_earth_rotation:
        svs = apply_earth_rotation_to_svs_position(svs, prs)

    if len(svs)==0 or len(prs)==0 or len(elevs)==0 or len(snr)==0:
        return np.array([0.,0.,0.,0.]),None, None, None

    ri = np.array([0.,0.,0.,0.])

    #for i in range(max_iters):
    delta,i = 1,0
    while (norm(delta)>1e-8 and i<max_iters):
        rhos, pranges = predict_pseudoranges(ri, svs, svs_clocks)
        b = prs - pranges
        A = np.hstack(((ri[:3]-svs)/rhos[:,None],np.ones((len(b), 1))))

        w= (0.03 + 0.9*np.exp(-snr/elevs))#len(pranges)*
        #wc = len(pranges)*(0.07 + 0.6*np.exp(-snr/elevs))#elevs/30.))
        #w = np.hstack((wp, wc))
        delta =  np.linalg.pinv((A.T.dot(w*np.eye(len(w)))).dot(A)).dot(A.T.dot(w*np.eye(len(w)))).dot(b)
        #delta =  np.linalg.pinv(A.T.dot(A)).dot(A.T).dot(b)
        ri += delta
        i+=1
    return ri, A, b, delta


def simulation_multirover(Obser, error_max, parejas_kmz, RinexDB, dia1, dian, ano, distances=None,
                         Simu_Folder=None,
                         res_plot=1000,
                         iono_model_master=None,
                         iono_model_rover=None,
                         silent_exec=True,
                         wls = False,
                         k_a=0.6, k_b=0.009):

    def clean_folder(abspath_Folder, silent_mode=True):
            cmd = "rm -rf "+abspath_Folder+"/*"
            if silent_mode == False:
                print ("\n"+cmd)
            system(cmd)


    import re
    # http://stackoverflow.com/questions/2669059/how-to-sort-alpha-numeric-set-in-python
    def sorted_nicely( l ):
        """ Sort the given iterable in the way that humans expect."""
        convert = lambda text: int(text) if text.isdigit() else text
        alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ]
        return sorted(l, key = alphanum_key)

    def extract_rinex_files(RinexDB, k, RX, Simu_Folder, required_files=['n', 'o']):

        Temp = {}
        matches = [x for x in RinexDB.keys() if RX.lower() == x]
        #print matches

        if len(matches)> 0:

            RX = matches[0]
            for Obstype in RinexDB[RX].keys():
                station, ObsType = splitext(RinexDB[RX][Obstype])
                path = RinexDB[RX][Obstype]
                dirname, zipname = split(path)
                fname, ext = splitext(zipname)

                #print fname, ext, path
                cmd = "cp "+path+" "+Simu_Folder+"/"+zipname
                system(cmd)
                cmd2 = "gunzip "+Simu_Folder+"/"+zipname

		if zipname.endswith('zip'):
		     extract_from_zip(zipname, dirname, Simu_Folder)
		else:
		     system(cmd2)
		#print fname
		#break
                if isfile(Simu_Folder+"/"+fname):
                    if k==0:
                        #Temp.setdefault("master_"+RX,{})[Obstype[0]] = Simu_Folder+"/"+fname
                        #Temp["master_"+RX].update({Obstype:path})
                        Temp.setdefault(RX,{})[Obstype[0]] = Simu_Folder+"/"+fname
                        Temp[RX].update({Obstype:path})
                    else:
                        #Temp.setdefault("rover"+str(k)+"_"+RX,{})[Obstype[0]] = Simu_Folder+"/"+fname
                        #Temp["rover"+str(k)+"_"+RX].update({Obstype:path})
                        Temp.setdefault(RX,{})[Obstype[0]] = Simu_Folder+"/"+fname
                        Temp[RX].update({Obstype:path})
                else:
                    if k==0:
                        #Temp.setdefault("master_"+RX,{})[Obstype[0]] = ""
                        #Temp["master_"+RX].update({Obstype:""})
                        Temp.setdefault(RX,{})[Obstype[0]] = ""
                        Temp[RX].update({Obstype:""})
                    else:
                        #Temp.setdefault("rover"+str(k)+"_"+RX,{})[Obstype[0]] = ""
                        #Temp["rover"+str(k)+"_"+RX].update({Obstype:""})
                        Temp.setdefault(RX,{})[Obstype[0]] = ""
                        Temp[RX].update({Obstype:""})

            station = Temp.keys()[0]
            files = [f for f in required_files if f in Temp[station].keys()]

            if (len(files) >= len(required_files)):
                return Temp
            else:
                return {}
        else:
            return {}

    if Simu_Folder==None:
        Simu_Folder = getcwd()+"/Temp_Simul_data"
        cmd1 = "mkdir -p " + Simu_Folder    # COMANDO CREA CARPETA temporal
        system(cmd1)

        #print parejas_kmz
        #print "\t[Station Pair]\t\t[GPS day]\t[year]\t\t[Process status]\n"
        print "[GPS day]   [year]\t[Station Pair]\t\t[Process status]\n"

        Results = {}
        dat_com = {}

        for day in range(int(dia1), int(dian)+1):
            print " %s\t%s" %(str(process_day2(day)[:3]), ano),
            tag = 0

            for k, agrupacion in parejas_kmz.iteritems():

                if tag == 0:
                    print "\t   %s\t"%(str(agrupacion)),
                    tag = 1
                else:
                    print " \t\t %s\t"%(str(agrupacion)),

                agrupacion = [x.lower() for x in agrupacion]
                #print agrupacion
                
                Obsfiles = {}
                for k, station in enumerate(agrupacion):

                    #fname = str(station)+str(process_day2(day))+"."+ano[-2:]
                    fname = str(station[:4])+str(process_day2(day))+"."+ano[-2:]

                    fname = fname.lower()
                    Obsfiles.update(extract_rinex_files(RinexDB, k, fname, Simu_Folder))
                    #print day, agrupacion, station, fname

                    if len([Obsfiles[fname][x] for x in ['n', 'o']]) < 2:
                        error = True
                        #print Obsfiles[fname].keys()
                        if silent_exec==False:
                            print "WARNING! Aborting execution: missing rinex files for %s \n"%(str(agrupacion))

                        break
                    else:
                        error = False
    
                #agrupacion = Obsfiles.keys()
                agrupacion = [y for x in agrupacion for y in Obsfiles.keys() if y[:4]==x]
                #print agrupacion
                
                ## LOS DATOS DE TODA LA AGRUPACION
                data = []
                realpos = []
                for k, station in enumerate(agrupacion):

                    p = station
                    data1, b1, rec_pos1 = rinex_to_dataframe_ionosfe(Obsfiles[p]['o'],
                                                                  Obsfiles[p]['n'],
                                                                  silent_mode=silent_exec)
                    data.append(data1)
                    # almacenando posicion real para calcular error dentro simple_test
                    realpos.append(rec_pos1)

                    
                ## RECORRIENDO LAS EPOCAS DE LOS 
                print " Running... ",
                results = []
                for epoc in range(len(data[0])):

                    if epoc % res_plot == 0:
                        csats = data[0].iloc[epoc]['prns']
                        for k, (d, station) in enumerate(zip(data, agrupacion)): # enumerate(zip(data[1:], pareja[1:])):
                            csats = list( set(csats) & set(d.iloc[epoc]['prns']))

                        csats = sorted_nicely(csats)
                        d_common = pd.DataFrame()
                        #print "c", len(csats)#, csats
                        #print ("\t\t Rinex Observation: "), epoc,
                        gps_time = data[0].iloc[epoc].gps_sow

                        # EXTRAYENDO LOS DATOS EN COMÚN PARA LA EPOCA
                        for k, (d, station) in enumerate(zip(data, agrupacion)):

                            #print gps_time,  d.iloc[epoc].gps_sow
                            if gps_time == d.iloc[epoc].gps_sow:
                                #print station
                                error = False
                                d_common = get_data(d_common, d, epoc, csats, station)

                            else:
                                error = True
                                if silent_exec==False:
                                    print " no existe epoca común para %s la agrupación %s" %(station, agrupacion)
                                
                                #print station, error
                                break
                                
                        if error:
                            break
                        else:
                            d_common = d_common.iloc[0]
                                
                            r,n = multirover_test(d_common, agrupacion, realpos,
                                                  iono_model_master=ion_mas,
                                                  iono_model_rover=ion_rov, k_a=k_a, k_b=k_b, wls_flag = wls)

                            #print ""
                            #for x,y in zip(r[:4], n[:4]):
                            #    print y, np.shape(x)
                            results.append(r)


                print "Finished!!!"
                dat_com.setdefault(day, {})[str(agrupacion)] = d_common
                Results.setdefault(day, {})[str(agrupacion)] = pd.DataFrame(results, columns=n)
                #print Results.keys()

                tag += 1

    else:
        #print "no existe master"
        print "no existe primer estacion"

    print ("\n\tSimulation Finished!")
    print ("\t "+"--"*10)
    #print ("\n")
    #clean_folder(Simu_Folder, silent_mode=False)
    #clean_folder(Simu_Folder)

    return Results, dat_com


"""
def coop_ls_code_phase_multirover(gpstime, receivers, d_est, dat,
                              Obser="C1",
                              max_iters=50,
                              apply_earth_rotation=True,
                              iono_model_master=None,
                              iono_model_rover =None,
                              k_a=0.6, k_b=0.009, wls=False):

    #print receivers, d_est
    def get_sat_elv_az(svs, rx):
        
        #http://www.naic.edu/aisr/GPSTEC/drewstuff/MATLAB/elavazim.m
        #https://goo.gl/2Kt8Pf
        # https://sourceforge.isae.fr/projects/building-blocks-for-a-leo-ground-station/wiki/Distance_Elevation_and_Azimuth_Calculation
        
        #svs = apply_earth_rotation_to_svs_position(svs, prs)

        #d_svs = r_com[:,:3]/r_mag[:, None]
        svs_enu = ecef2enu(svs, rx)

        east, north, up  = svs_enu[0], svs_enu[1], svs_enu[2]

        elevation = up/np.linalg.norm(svs_enu, axis=0)
        azimuth = np.arctan2(east,north)#radians
        idx = np.where(azimuth<0)
        azimuth[idx] += 2*np.pi;
        return elevation, azimuth # radians

    def apply_earth_rotation_to_svs_position(svs, prs):
        c = 299792458
        we = 7.2921159e-5
        rpos = np.zeros(svs.shape)
        pos = np.array(svs)
        for i in range(len(pos)):
            dt = prs[i]/c
            theta = we*dt
            R = np.array([[np.cos(theta), np.sin(theta),0.],[-np.sin(theta), np.cos(theta),0.],[0.,0.,1.]])
            rpos[i] = R.dot(pos[i])
        svs = np.array(rpos)
        return svs

    def compute_distances(rc, svs):
        return np.linalg.norm(rc-svs, axis=1)

    def Klobuchar(A,E,time, rx_pos, alphas, betas):
        ####klobuchar  model

        lat, lon, alt = ecef2lla(X=rx_pos[0], Y=rx_pos[1], Z=rx_pos[2])
        C=2.99792458e8

        ######bla bla
        phi=(0.0137/(E-0.11))-0.022
        psi=lat+phi*np.cos(A)#2
        if psi > 0.416:
            psi=psi+0.416
        elif psi < -0.416:
            psi=psi-0.416

        lampda=lon+(phi*np.sin(A))/np.cos(psi)#3
        phi_m=lampda+0.064*np.cos(psi-1.617)#4
        t=43200*lampda+time#.getSecondOfDay()#5
        if t > 86400:
            t=t-86400
        elif t < 0:
            t=t+86400
        ##basados en el modelo expuesto en el link, se cambian el exponente de la sumatoria.
        ##http://what-when-how.com/gps/ionospheric-effects-physical-influences-of-gps-surveying-part-2/
        A_I=alphas[0]+alphas[1]*phi_m+alphas[2]*phi_m**2+alphas[3]*phi_m**3 #6
        if A_I < 0:
            A_I=0

        P_I=betas[0]+betas[1]*phi_m+betas[2]*phi_m**2+betas[3]*phi_m**3 #7
        if P_I < 72000:
            P_I=72000

        X_I=2*np.pi*(t-50400)/P_I #8
        F=1+16*(0.53-E)**3#9

        if np.fabs(X_I) > 1.57:#10
            IL1_GPS=((5e-9)+A_I*(1-(((X_I)**2)/2)+((X_I)**4)/24))*F*C
        elif np.fabs(X_I) < 1.57:
            IL1_GPS=(5e-9)*F*C
        else :
            IL1_GPS=(5e-9)*F*C
        return IL1_GPS#/F

    def predict_pseudoranges(x, prns_pos, prns_clockbias):
        c = 299792458
        rhos    = compute_distances(x[:3], prns_pos)
        pranges = rhos + x[3]-c*prns_clockbias
        return rhos, pranges

    def predict_pseudoranges_iono(x, svs, prns_clockbias, dat, apply_iono_model=None):
        c = 299792458
        R_earth = 6367444.657  # 6,371 km
        h_iono  = 400e3

        def iono_3dmodel(rs, d_iono, elevs):
            num1 = d_iono**2*(np.sqrt(rs**2*np.cos(elevs))*np.sin(elevs)**2)
            den1 = np.power(2*rs*np.cos(elevs)**2, 3) #8*rs**3*np.cos(elev)**6 #

            num2 = rs
            den2 = rs*np.cos(elevs)

            return num2/den2 + 1/8*(d_iono/rs)*(d_iono/rs)*np.tan(elevs) #num1/den1

        if apply_iono_model=='taylor':
            #print "a", apply_iono_model
            rs, d_iono = R_earth+h_iono, 10e3
            elv, az = get_sat_elv_az(svs, x[:3])
            io1 = iono_3dmodel(rs, d_iono, elv)

        elif apply_iono_model=='standard':
            #print apply_iono_model
            R = R_earth
            elevs, az = get_sat_elv_az(svs, x[:3])
            io1 = np.array(1/np.cos(np.arcsin(R/(R+350000))*np.sin(elevs)))
            #io2 = np.array(1/np.cos(np.arcsin(R/(R+350000))*np.sin(elv2)))

        elif apply_iono_model=='empiric2':
            e, a = get_sat_elv_az(svs, x[:3])
            #print len(S1), len(e), len(a)
            #io1 = (a/40.3)*(dat["S1_master"]/e)
            #io1 = (a/40.3)*(0.1 + np.exp(-((e/dat["S1_master"]))))
            N = np.shape(svs)[0]
            SNR1 = dat["S1_master"]
            io1 = (1./2)*(N - e/SNR1 + np.random.normal(N, 1)*np.exp(-e/SNR1))
            #print len(io1), io1

        elif apply_iono_model=='wallas':
            e, a = get_sat_elv_az(svs, x[:3])
            #print len(S1), len(e), len(a)
            #io1 = (a/40.3)*(dat["S1_master"]/e)
            #io1 = (a/40.3)*(0.1 + np.exp(-((e/dat["S1_master"]))))
            N = np.shape(svs)[0]
            SNR1 = dat["S1_master"]
            io1 = 0.7*(-N + np.random.normal(SNR1, 1)*np.exp(-e/SNR1))
            #print len(io1), io1

        elif apply_iono_model=='klobu':
            elv, az = get_sat_elv_az(svs, x[:3])
            io1 = np.array([Klobuchar(a,e, gpstime, ri[:3], dat.alphas_master, dat.betas_master) for a, e in zip(az, elv)])

        elif apply_iono_model=='dual_freq':
            #print apply_iono_model, dat["Iono_master"][:4], dat["P1_master"][:4], dat["P2_master"][:4]
            io1 = dat["Iono_master"]

        else:
            io1 = np.zeros(len(prns_clockbias))
            #print len(io1)
            pass

        rhos    = compute_distances(x[:3], svs)
        #print np.shape(rhos), np.shape(x[3]), np.shape(svs), np.shape(prns_clockbias)
        pranges = rhos + x[3]-c*prns_clockbias# - io1 #iono_3dmodel(elevs)
        return rhos, pranges, io1


    # initialize vector of variables
    n_rx = len(receivers)  #numero de receptores en cooperativo
    ri = np.hstack((np.zeros(4), np.ones(4*(n_rx-1))))

    svs = dat['prns_pos_master']
    n_sats = len(svs)
    n_vars = 4
    #n, m = 2*n_sats+1, 2*n_vars
    n, m = (n_rx*n_sats + n_rx-1), n_rx*n_vars

    if apply_earth_rotation:

        svs = dat['prns_pos_master']
        prs = dat[Obser+'_master']
        #print np.shape(svs), np.shape(prs)
        svs = apply_earth_rotation_to_svs_position(svs, prs)

    if len(svs)==0 or len(dat[Obser+'_master'])==0:
        return np.zeros(4*n_rx),None, None, None

    delta,i = 1,0
    iono_delays = []
    #print "iono model"
    #print "rov", iono_model_rover, "mas", iono_model_master

    #while (norm(delta)>1e-12 and i<max_iters):
    A = np.zeros((n, m))
    prs = np.zeros(n)
    pranges = np.zeros(n)
    
    for i in range(1, max_iters):

        rhos_a, pranges_a, io1 = predict_pseudoranges_iono(ri[:4], svs,
                                                      dat['prns_clockbias_master'],
                                                      dat,
                                                      apply_iono_model=iono_model_master)
        iono_delays.append(io1)

        Aa = np.hstack(((ri[:3] - svs)/rhos_a[:,None], np.ones((n_sats, 1))))

        # component asociated to position of local receiver
        A[:n_sats, :n_vars] = Aa
        #print "A"
        #print Aa
        #A[:n_sats, n_vars:] = np.zeros(np.shape(A[:n_sats,n_vars:]))

        prs[:n_sats]     = dat[Obser+'_master'] #np.hstack((dat[Obser+'_master'])).reshape(-1,1)
        pranges[:n_sats] = pranges_a + io1
        #print prs
        #print pranges

        w = len(pranges)*(k_a + k_b*np.exp(-dat['S1_master']/dat['prns_elev_master']))
        #w = len(pranges)*(0.003 + 0.09*np.exp(-dat['S1_master']/dat['prns_elev_master']))

        c = -1#0
        for k, station in enumerate(receivers):
            if k != 0:

                # MODELO DISTANCIA.
                #OJO INVERTIR ESTO PARA CAMBIAR LA DIRECCION DEL VECTOR DISTANCIAS
                
                d_model = np.linalg.norm(ri[4*k:4*(k+1)-1] - ri[:3])
                Ad = np.hstack(((ri[4*k:4*(k+1)-1] - ri[:3])/d_model))#[:,None]))

                # MODELO PSEUDORANGES.
                
                #print station
                #svs = dat['prns_pos_'+station]
                rhos_b, pranges_b, io2 = predict_pseudoranges_iono(ri[4*k:4*(k+1)], svs,
                                                              dat['prns_clockbias'+"_"+station],
                                                              dat,
                                                              apply_iono_model=iono_model_rover)
                # prs y pranges construction.
                prs2 = dat[Obser+"_"+station]
                #ini = k*n_sats + c
                #fin = (k+1)*n_sats+1 + c


                ini = k*n_sats + k-1
                fin = (k+1)*n_sats + k
                #print k, n_sats, ini, fin
                #print np.shape(prs2), np.shape(prs[ini:fin]), np.shape(np.array(d_est[k-1]))

                pranges_b += io2
                iono_delays.append(io2)

                prs[ini:fin]     = np.hstack((prs2, np.array(d_est[k-1])))
                pranges[ini:fin] = np.hstack((pranges_b, d_model))
                #print station+"prs", prs
                #print station+"pranges", pranges

                # A Matrix construction.
                
                # get cosine director from each receiver to sats
                Ab = np.hstack(((ri[4*k:4*(k+1)-1]- svs)/rhos_b[:,None], np.ones((n_sats, 1))))

                # component asociated to position of remote receiver
                A[ini:fin, k*n_vars:(k+1)*n_vars] = np.vstack((Ab, np.hstack((Ad[:3], np.zeros(1)))))
                c += 1 # para avanzar la posicion de Ad en cada rover
                #print A


                wp = len(pranges)*(k_a + k_b*np.exp(-dat['S1'+"_"+station]/dat['prns_elev'+"_"+station]))
                #wp = 4+(np.exp(-0.4*dat['prns_elev'+"_"+station]/dat['S1'+"_"+station]))
                w = np.hstack((w, wp, np.ones(1)))

                #print "Ab"
                #print Ab
        #print A
        #return
        # LS computation.
        

        b     = prs - pranges

        if wls:
            #delta =  np.linalg.pinv((A.T.dot(w*np.eye(len(w)))).dot(A)).dot(A.T.dot(w*np.eye(len(w)))).dot(b)
            #append_wb(b)
            #x, y = np.sort(np.mean(wb, 0)), np.sort(np.var(wb, 0))# np.log(
            #x, y = np.sort(np.mean(b)), np.sort(np.log(np.var(b)))

            delta =  np.linalg.pinv((A.T.dot(w*np.eye(len(w)))).dot(A)).dot(A.T.dot(w*np.eye(len(w)))).dot(b)

        else:
            delta =  np.linalg.pinv(A.T.dot(A)).dot(A.T).dot(b)
        

        #delta =  np.linalg.pinv((A.T.dot(w*np.eye(len(w)))).dot(A)).dot(A.T.dot(w*np.eye(len(w)))).dot(b)
        #delta =  np.linalg.pinv(A.T.dot(A)).dot(A.T).dot(b)
        ri    += delta#.flatten('F')
        i     += 1

        #if i%5 == 0:
        #    print i, norm(delta), ri

        if norm(delta)<1e-12:
            break

    return ri, A, b, delta, iono_delays

"""




def coop_ls_code_phase_multirover(gpstime, receivers, d_est, dat,
                                  Obser="C1",max_iters=50, max_error=1,
                                  apply_earth_rotation=True,
                                  iono_model_master=None,
                                  iono_model_rover =None,
                                  k_a=0.6, k_b=0.009, wls=False):
    
    #print max_error, wls

    #print receivers, d_est
    def get_sat_elv_az(svs, rx):
        """
        http://www.naic.edu/aisr/GPSTEC/drewstuff/MATLAB/elavazim.m
        https://goo.gl/2Kt8Pf
        # https://sourceforge.isae.fr/projects/building-blocks-for-a-leo-ground-station/wiki/Distance_Elevation_and_Azimuth_Calculation
        """
        #svs = apply_earth_rotation_to_svs_position(svs, prs)

        #d_svs = r_com[:,:3]/r_mag[:, None]
        svs_enu = ecef2enu(svs, rx)

        east, north, up  = svs_enu[0], svs_enu[1], svs_enu[2]

        elevation = up/np.linalg.norm(svs_enu, axis=0)
        azimuth = np.arctan2(east,north)#radians
        idx = np.where(azimuth<0)
        azimuth[idx] += 2*np.pi;
        return elevation, azimuth # radians

    def apply_earth_rotation_to_svs_position(svs, prs):
        c = 299792458
        we = 7.2921159e-5
        rpos = np.zeros(svs.shape)
        pos = np.array(svs)
        for i in range(len(pos)):
            dt = prs[i]/c
            theta = we*dt
            R = np.array([[np.cos(theta), np.sin(theta),0.],[-np.sin(theta), np.cos(theta),0.],[0.,0.,1.]])
            rpos[i] = R.dot(pos[i])
        svs = np.array(rpos)
        return svs

    def compute_distances(rc, svs):
        return np.linalg.norm(rc-svs, axis=1)

    def Klobuchar(A,E,time, rx_pos, alphas, betas):
        ####klobuchar  model

        lat, lon, alt = ecef2lla(X=rx_pos[0], Y=rx_pos[1], Z=rx_pos[2])
        C=2.99792458e8

        ######bla bla
        phi=(0.0137/(E-0.11))-0.022
        psi=lat+phi*np.cos(A)#2
        if psi > 0.416:
            psi=psi+0.416
        elif psi < -0.416:
            psi=psi-0.416

        lampda=lon+(phi*np.sin(A))/np.cos(psi)#3
        phi_m=lampda+0.064*np.cos(psi-1.617)#4
        t=43200*lampda+time#.getSecondOfDay()#5
        if t > 86400:
            t=t-86400
        elif t < 0:
            t=t+86400
        ##basados en el modelo expuesto en el link, se cambian el exponente de la sumatoria.
        ##http://what-when-how.com/gps/ionospheric-effects-physical-influences-of-gps-surveying-part-2/
        A_I=alphas[0]+alphas[1]*phi_m+alphas[2]*phi_m**2+alphas[3]*phi_m**3 #6
        if A_I < 0:
            A_I=0

        P_I=betas[0]+betas[1]*phi_m+betas[2]*phi_m**2+betas[3]*phi_m**3 #7
        if P_I < 72000:
            P_I=72000

        X_I=2*np.pi*(t-50400)/P_I #8
        F=1+16*(0.53-E)**3#9

        if np.fabs(X_I) > 1.57:#10
            IL1_GPS=((5e-9)+A_I*(1-(((X_I)**2)/2)+((X_I)**4)/24))*F*C
        elif np.fabs(X_I) < 1.57:
            IL1_GPS=(5e-9)*F*C
        else :
            IL1_GPS=(5e-9)*F*C
        return IL1_GPS#/F

    def predict_pseudoranges(x, prns_pos, prns_clockbias):
        c = 299792458
        rhos    = compute_distances(x[:3], prns_pos)
        pranges = rhos + x[3]-c*prns_clockbias
        return rhos, pranges

    def predict_pseudoranges_iono(x, svs, prns_clockbias, dat, apply_iono_model=None):
        c = 299792458
        R_earth = 6367444.657  # 6,371 km
        h_iono  = 400e3

        def iono_3dmodel(rs, d_iono, elevs):
            num1 = d_iono**2*(np.sqrt(rs**2*np.cos(elevs))*np.sin(elevs)**2)
            den1 = np.power(2*rs*np.cos(elevs)**2, 3) #8*rs**3*np.cos(elev)**6 #

            num2 = rs
            den2 = rs*np.cos(elevs)

            return num2/den2 + 1/8*(d_iono/rs)*(d_iono/rs)*np.tan(elevs) #num1/den1

        if apply_iono_model=='taylor':
            #print "a", apply_iono_model
            rs, d_iono = R_earth+h_iono, 10e3
            elv, az = get_sat_elv_az(svs, x[:3])
            io1 = iono_3dmodel(rs, d_iono, elv)

        elif apply_iono_model=='standard':
            #print apply_iono_model
            R = R_earth
            elevs, az = get_sat_elv_az(svs, x[:3])
            io1 = np.array(1/np.cos(np.arcsin(R/(R+350000))*np.sin(elevs)))
            #io2 = np.array(1/np.cos(np.arcsin(R/(R+350000))*np.sin(elv2)))

        elif apply_iono_model=='empiric2':
            e, a = get_sat_elv_az(svs, x[:3])
            #print len(S1), len(e), len(a)
            #io1 = (a/40.3)*(dat["S1_master"]/e)
            #io1 = (a/40.3)*(0.1 + np.exp(-((e/dat["S1_master"]))))
            N = np.shape(svs)[0]
            SNR1 = dat["S1_master"]
            #io1 = (1./2)*(N - e/SNR1 + np.random.normal(N, 1)*np.exp(-e/SNR1))
            #io1 = (- e/SNR1 + np.power(np.random.normal(SNR1, 2), .5)*np.exp(-e/SNR1)**3)
            io1 = (np.power(np.random.normal(SNR1, 2), 3.5)*np.exp(-e/SNR1)**3)
            #print len(io1), io1

        elif apply_iono_model=='wallas':
            e, a = get_sat_elv_az(svs, x[:3])
            #print len(S1), len(e), len(a)
            #io1 = (a/40.3)*(dat["S1_master"]/e)
            #io1 = (a/40.3)*(0.1 + np.exp(-((e/dat["S1_master"]))))
            N = np.shape(svs)[0]
            SNR1 = dat["S1_master"]
            io1 = 0.7*(-N + np.random.normal(SNR1, 1)*np.exp(-e/SNR1))
            #print len(io1), io1

        elif apply_iono_model=='klobu':
            elv, az = get_sat_elv_az(svs, x[:3])
            io1 = np.array([Klobuchar(az,elv, gpstime, x[:3], dat.alphas_master, dat.betas_master) for az, elv in zip(az, elv)])

        elif apply_iono_model=='dual_freq':
            #print apply_iono_model, dat["Iono_master"][:4], dat["P1_master"][:4], dat["P2_master"][:4]
            io1 = dat["Iono_master"]

        else:
            io1 = np.zeros(len(prns_clockbias))
            #print len(io1)
            pass

        rhos    = compute_distances(x[:3], svs)
        #print np.shape(rhos), np.shape(x[3]), np.shape(svs), np.shape(prns_clockbias)
        pranges = rhos + x[3]-c*prns_clockbias# - io1 #iono_3dmodel(elevs)
        return rhos, pranges, io1


    # initialize vector of variables
    n_rx = len(receivers)  #numero de receptores en cooperativo
    ri = np.hstack((np.zeros(4), np.ones(4*(n_rx-1))))

    svs = dat['prns_pos_master']
    n_sats = len(svs)
    n_vars = 4
    #n, m = 2*n_sats+1, 2*n_vars
    n, m = (n_rx*n_sats + n_rx-1), n_rx*n_vars

    if apply_earth_rotation:

        svs = dat['prns_pos_master']
        prs = dat[Obser+'_master']
        #print np.shape(svs), np.shape(prs)
        svs = apply_earth_rotation_to_svs_position(svs, prs)

    if len(svs)==0 or len(dat[Obser+'_master'])==0:
        return np.zeros(4*n_rx),None, None, None

    delta,i = 1,0
    iono_delays = []

    A = np.zeros((n, m))
    prs = np.zeros(n)
    pranges = np.zeros(n)
    
    #P = np.ones(n)/(max_error)**2
    
    for i in range(1, max_iters):

        rhos_a, pranges_a, io1 = predict_pseudoranges_iono(ri[:4], svs,
                                                      dat['prns_clockbias_master'],
                                                      dat,
                                                      apply_iono_model=iono_model_master)
        iono_delays.append(io1)

        Aa = np.hstack(((ri[:3] - svs)/rhos_a[:,None], np.ones((n_sats, 1))))

        # component asociated to position of local receiver
        A[:n_sats, :n_vars] = Aa

        prs[:n_sats]     = dat[Obser+'_master'] #np.hstack((dat[Obser+'_master'])).reshape(-1,1)
        pranges[:n_sats] = pranges_a - io1

        #w = len(pranges)*(k_a + k_b*np.exp(-dat['S1_master']/dat['prns_elev_master']))
        #w = len(pranges)*(0.003 + 0.09*np.exp(-dat['S1_master']/dat['prns_elev_master']))

        c = -1#0
        for k, station in enumerate(receivers):
            if k != 0:

                """ MODELO DISTANCIA.
                OJO INVERTIR ESTO PARA CAMBIAR LA DIRECCION DEL VECTOR DISTANCIAS
                """
                d_model = np.linalg.norm(ri[4*k:4*(k+1)-1] - ri[:3])
                Ad = np.hstack(((ri[4*k:4*(k+1)-1] - ri[:3])/d_model))#[:,None]))

                """ MODELO PSEUDORANGES.
                """
                #print station
                #svs = dat['prns_pos_'+station]
                rhos_b, pranges_b, io2 = predict_pseudoranges_iono(ri[4*k:4*(k+1)], svs,
                                                              dat['prns_clockbias'+"_"+station],
                                                              dat,
                                                              apply_iono_model=iono_model_rover)
                """ prs y pranges construction."""
                prs2 = dat[Obser+"_"+station]
                #ini = k*n_sats + c
                #fin = (k+1)*n_sats+1 + c


                ini = k*n_sats + k-1
                fin = (k+1)*n_sats + k

                pranges_b -= io2
                iono_delays.append(io2)

                prs[ini:fin]     = np.hstack((prs2, np.array(d_est[k-1])))
                pranges[ini:fin] = np.hstack((pranges_b, d_model))
                #print station+"prs", prs
                #print station+"pranges", pranges

                """ A Matrix construction.
                """
                # get cosine director from each receiver to sats
                Ab = np.hstack(((ri[4*k:4*(k+1)-1]- svs)/rhos_b[:,None], np.ones((n_sats, 1))))

                # component asociated to position of remote receiver
                A[ini:fin, k*n_vars:(k+1)*n_vars] = np.vstack((Ab, np.hstack((Ad[:3], np.zeros(1)))))
                c += 1 # para avanzar la posicion de Ad en cada rover
                #print A

                #wp = len(pranges)*(k_a + k_b*np.exp(-dat['S1'+"_"+station]/dat['prns_elev'+"_"+station]))
                #wp = 4+(np.exp(-0.4*dat['prns_elev'+"_"+station]/dat['S1'+"_"+station]))
                #w = np.hstack((w, wp, np.ones(1)))

        """ LS computation.
        """

        b     = prs - pranges
        
        if wls:
            #w = np.ones(len(b))/max_error**2
            W = np.eye(len(b))/np.var(b)**2
            #delta =  np.linalg.pinv((A.T.dot(w*np.eye(len(w)))).dot(A)).dot(A.T.dot(w*np.eye(len(w)))).dot(b)
            delta =  np.linalg.pinv((A.T.dot(W)).dot(A)).dot(A.T.dot(W)).dot(b)
        else:
            delta =  np.linalg.pinv(A.T.dot(A)).dot(A.T).dot(b)
        

        #delta =  np.linalg.pinv((A.T.dot(w*np.eye(len(w)))).dot(A)).dot(A.T.dot(w*np.eye(len(w)))).dot(b)
        #delta =  np.linalg.pinv(A.T.dot(A)).dot(A.T).dot(b)
        ri    += delta#.flatten('F')
        i     += 1

        #if i%5 == 0:
        #    print i, norm(delta), ri

        if norm(delta)<1e-3:
            break

    return ri, A, b, delta, iono_delays















def multirover_test(data_common, pareja, realpos,
                    Obser = "C1", dist = 1.,
                    iono_model_master = None,
                    iono_model_rover = None,
                    k_a=0.6, k_b=0.009,
                    decimal_places=3, wls_flag = False):

    c = 0
    r = []
    station_list = []
    coop_pos, coop_err = [], []
    stdalone_pos, stdalone_err = [], []
    dc_est, d_real  = [], []

    dat = data_common#.iloc[0]
    gps_sow = data_common.gps_sow


    #print pareja
    #print data_common.index

    if len(dat['prns'])>=4:

        # Metodos desarrollados
        ## Compute standalone positions
        for k, station in enumerate(pareja):
            if k == 0:

                #print np.shape(dat.loc['prns_pos_master']), np.shape(dat[Obser+'_master']), np.shape(dat['prns_clockbias_master'])

                #print "d", np.shape(dat.loc['prns_clockbias_master'])
                posstd,_,_,_ = compute_least_squares_position(data_common.loc['prns_pos_master'],
                                                              data_common.loc['prns_clockbias_master'],
                                                              data_common.loc[Obser+'_master'])
                err = np.linalg.norm(posstd[:3] - realpos[k])

                stdalone_pos.append(posstd[:3])
                stdalone_err.append(err)

            else:
                #print "e", np.shape(dat.loc['prns_clockbias_'+station])
                posstd,_,_,_ = compute_least_squares_position(data_common.loc['prns_pos_'+station],
                                                              data_common.loc['prns_clockbias_'+station],
                                                              data_common.loc[Obser+"_"+station])
                err = np.linalg.norm(posstd[:3] - realpos[k])

                stdalone_pos.append(posstd[:3])
                stdalone_err.append(err)

        # compute distances between receivers
        for k, station in enumerate(pareja):
            if k != 0:
                #print k, stdalone_pos[0], stdalone_pos[k]
                dc, teta, dc_vec = d_teta_calc(stdalone_pos[0], stdalone_pos[k])
                dr, teta, dc_vec = d_teta_calc(realpos[0], realpos[k])
                dc_est.append(dc)
                d_real.append(dr)

        start_time = time.time()

        if is_number(dist):
            noise = np.random.normal(-1, 1, len(d_real))
            dist = np.array(d_real) + (dist/100)*noise
            """
            ri,_,_,_ = coop_ls_solver_multirover(data_common["gps_sow"], pareja, dist, data_common,
                                             iono_model_master=iono_model_master,
                                             iono_model_rover =iono_model_rover)
            """
            ri,_,_,_, ionos = coop_ls_code_phase_multirover(data_common["gps_sow"], pareja, dist, data_common,
                                                 iono_model_master=iono_model_master,
                                                 iono_model_rover =iono_model_rover, k_a=k_a, k_b=k_b, wls=wls_flag)

        elif dist == None:
            dist = dc_est
            """
            ri,_,_,_ = coop_ls_solver_multirover(data_common["gps_sow"], pareja, dist, data_common,
                                                 iono_model_master=iono_model_master,
                                                 iono_model_rover =iono_model_rover)
            """
            ri,_,_,_, ionos = coop_ls_code_phase_multirover(data_common["gps_sow"], pareja, dist, data_common,
                                                 iono_model_master=iono_model_master,
                                                 iono_model_rover =iono_model_rover, k_a=k_a, k_b=k_b, wls=wls_flag)
        coop_t = time.time() - start_time

        c = 0

        #print iono_model_rover, iono_model_master
        station_list, iono_model = [], []
        for k, (station, iono) in enumerate(zip(pareja, ionos)):
            station_list.append(station)
            if k == 0:
                coop_pos.append(ri[4*k:4*(k+1)-1])
                coop_err.append(np.linalg.norm(ri[4*k:4*(k+1)-1] - realpos[k]))
                iono_model.append(iono)
            else:
                ini = 4*k #+ c
                fin = 4*(k+1)-1 # + c
                #print
                #print k, ini, fin, ri, np.shape(ri[ini:fin]), np.shape(real_pos[k])
                coop_pos.append(ri[ini:fin])
                coop_err.append(np.linalg.norm(ri[ini:fin] - realpos[k]))
                c += 1
                iono_model.append(iono)

        #print dc_est
        #print "st", stdalone_err
        #print "cp", coop_err

        r = [gps_sow,
                    np.round(realpos, decimal_places),
                    station_list,
                    iono_model,
                    np.round(dist, decimal_places),
                    np.round(d_real, decimal_places),
                    np.round(dc_est, decimal_places),
                    np.round(stdalone_pos, decimal_places),
                    np.round(stdalone_err, decimal_places),
                    np.round(coop_pos, decimal_places),
                    np.round(coop_err, decimal_places),
                    np.round(coop_t, decimal_places),
                    np.round(ri, decimal_places),
                    data_common]
    else:
        return

    names = ["gps_sow" ,"real_pos", "station_list", "iono_model", "d", "dreal", "d_calc",
             "stdlone_pos", "stdlone_err", "coop_pos", "coop_err",
             "coop_time[s]", "rc_sol", "data_common"]

    return r, names #pd.DataFrame(r, columns=names)# Results




def gen_df_results(df_results, dict_results, use_case,
                   xvar="gps_sow",
                   extract_cols = ["stdlone_err", "coop_err"],
                   columns = ["stdlone_err", "coop_err", "dreal", "d_calc", "d", "gps_sow"],
                   decimal_places=4):


    #plots = pd.DataFrame()
    vals, cols = [], []
    plots = []
    df_plots = pd.DataFrame()
    for pareja, v in dict_results.iteritems():
        for day in v.keys():
            #print day

            df = dict_results[pareja][day][columns]
            #df["scenario"] = pd.Series([str(use_case)], index=df.index)

            diff = [np.array(i)-np.array(j) for i,j in zip(df['stdlone_err'], df['coop_err'])]
            df = df.assign(diff_e = diff)
            df = df.assign(mean_e = df['diff_e'].apply(lambda x: np.mean(x[0:])))
            df = df.assign(std_e = df['diff_e'].apply(lambda x: np.std(x[0:])))

            #print type(pareja), [x.split("_")[1][:4] for x in pareja.split(",")]
            #pareja = [x.split("_")[1][:4] for x in pareja.split(",")] # its neccesary comment this line
            cols.append('mean_std '+str(pareja))

            var = tuple((np.array(df[xvar]), np.round(df["mean_e"].mean(), decimal_places), np.round(df["std_e"].std(), decimal_places), str(use_case)))
            #vals.append((df["mean_e"].mean(), df["std_e"].std()))
            vals.append(var[1:-1])
            plots.append((np.array(df[xvar]), diff, str(pareja), str("scenario "+use_case)))

    vals.append(str(use_case))
    cols.append("scenario")

    vals = pd.DataFrame([vals], columns=cols)
    #print len(plots)

    plots = pd.DataFrame(plots, columns=[xvar] + ["diff_e", "estaciones", "use_case"])
    #print plots.columns
    df_plots = pd.concat([df_plots, plots], axis=0)

    df_results = pd.concat([df_results, vals], axis=0)

    return df_results, df_plots


def gen_df_results2(df_results, dict_results, use_case,
                   xvar="gps_sow",
                   extract_cols = ["stdlone_err", "coop_err"],
                   columns = ["stdlone_err", "coop_err", "dreal", "d_calc", "d", "gps_sow"],
                   decimal_places=4):


    #plots = pd.DataFrame()
    vals, cols = [], []
    plots = []
    df_plots = pd.DataFrame()
    for day, v in dict_results.iteritems():
        for agrupacion in v.keys():
            #print day

            df = dict_results[day][agrupacion][columns]
            #df["scenario"] = pd.Series([str(use_case)], index=df.index)

            diff = [np.array(i)-np.array(j) for i,j in zip(df['stdlone_err'], df['coop_err'])]
            df = df.assign(day = day)
            df = df.assign(diff_e = diff)
            df = df.assign(mean_e = df['diff_e'].apply(lambda x: np.mean(x[0:])))
            df = df.assign(std_e = df['diff_e'].apply(lambda x: np.std(x[0:])))

            #print type(agrupacion), [x.split("_")[1][:4] for x in agrupacion.split(",")]
            #agrupacion = [x.split("_")[1][:4] for x in agrupacion.split(",")] # its neccesary comment this line
            cols.append('mean_std '+str(agrupacion))

            var = tuple((np.array(df[xvar]), np.round(df["mean_e"].mean(), decimal_places), np.round(df["std_e"].std(), decimal_places), str(use_case)))
            #vals.append((df["mean_e"].mean(), df["std_e"].std()))
            vals.append(var[1:-1])
            plots.append((np.array(df[xvar]), diff, str(agrupacion), str("scenario "+use_case)))

    vals.append(str(use_case))
    cols.append("scenario")

    vals = pd.DataFrame([vals], columns=cols)
    #print len(plots)

    plots = pd.DataFrame(plots, columns=[xvar] + ["diff_e", "estaciones", "use_case"])
    #print plots.columns
    df_plots = pd.concat([df_plots, plots], axis=0)

    df_results = pd.concat([df_results, vals], axis=0)

    return df_results, df_plots


def create_multi(df, index=['day'], cols=["mean_std_e", "d", "dr", "dc"]):
    #multi=df.set_index(['stations', 'day', 'scenario'], inplace=False)
    multi=df.set_index(index, inplace=False).sort_index()
    #multi.rename(columns={"index_gen":"index_gen"+"(mean,std)"}, inplace=True)
    return multi[cols]


def gen_df_results3(df_results, dict_results, use_case,
                   xvar="gps_sow",
                   columns = ["stdlone_err", "coop_err", "dreal", "d_calc", "d", "gps_sow", "iono_model"],
                   decimal_places=3):

    df2 = pd.DataFrame()

    for day, data in dict_results.iteritems():

        gps_time, days, dist = [], [], []
        index_improvement, diffs, stations, scheme = [], [], [], []
        mean_station, improve_station = [], []
        iono_delays = []
        
        for pareja in data.keys():
            #print pareja
            header = [x[:4] for x in pareja.translate(None, "[']").replace(" ", "").split(",")]
            header = ','.join(header)
            days.append(day)

            df = dict_results[day][pareja][columns] 
            display(df)

            diff = [np.array(i)-np.array(j) for i,j in zip(df['stdlone_err'], df['coop_err'])]
            print diff
            #print diff, np.mean(diff, axis=0)
            index_improvement.append((np.mean(diff), np.std(diff)))

            # mejora para cada estacion
            mean_station.append((np.mean(diff, axis=0), np.std(diff, axis=0)))

            diffs.append(diff)
            stations.append(str(header))
            scheme.append(str(use_case))
            gps_time.append([x for x in df['gps_sow']])

            dist.append((df['d'].mean(), df['dreal'].mean(), df['d_calc'].mean()))

        #print np.shape(gps_time), np.shape(diffs)
        #print diffs
        #print "m", mean_station
        #print "gm", [np.mean(x) for x in np.mean(mean_station, axis=1)]#,mean_station
        #print mean_station, np.mean(mean_station, axis=1)
        
        Index = [(np.mean(x), np.std(x)) for x in np.mean(mean_station, axis=1)]
        
        df2 = df2.assign(gps_day = days)
        df2 = df2.assign(scenario = scheme)
        df2 = df2.assign(stations = stations)
        df2 = df2.assign(improve_by_station = list(np.mean(mean_station, axis=1)))
        df2['improve_by_station'] = df2["improve_by_station"].apply(lambda tupla: tuple(''.join(format(f, '.4f')) for f in tupla))
        
        #df2 = df2.assign(index_gen = mean_std)
        df2 = df2.assign(index_gen = Index)
        df2['index_gen'] = df2["index_gen"].apply(lambda tupla: tuple(''.join(format(f, '.4f')) for f in tupla))
        df2 = df2.assign(mean_e_by_station = mean_station)
        df2 = df2.assign(gps_time = gps_time)
        df2 = df2.assign(d_dreal_dcalc = dist)
        df2 = df2.assign(iono_model = df['iono_model'])
        df2 = df2.assign(diff_e_std_e_coop = diffs)

        df_results = pd.concat([df_results, df2], axis=0)
        #display(df_results)
    
    return df_results


def plot_from_dict(dict_results,
                   xvar = "gps_sow",
                   tags = "station_list",
                   scenario="",
                   vars2plot = ["stdlone_err", "coop_err"],
                   axis_labels = [""]*3,
                   figsize=(20,5),
                   plot_inline = True,
                   plot_in_doc = False):

    def plotter(ax, df, xvar, tags, vars2plot, axis_labels, figsize):

        class Dictlist(dict):
            def __setitem__(self, key, value):
                try:
                    self[key]
                except KeyError:
                    super(Dictlist, self).__setitem__(key, [])
                self[key].append(value)

        #fig = plt.figure(figsize=figsize)
        #ax = fig.add_subplot(111)

        d = Dictlist(dict())
        for i, station in enumerate(df.iloc[0][tags]):

            if station not in d.keys():
                for var in vars2plot:
                    d.setdefault(station, {})[var] = []
                    for epoc in range(len(df)):
                        d[station][var].append((df.iloc[epoc][xvar], df.iloc[epoc][var][i]))


        station_lst = []
        for station, vars2plot in d.iteritems():
            #print station
            station_lst.append(station)
            for k, v in vars2plot.iteritems():
                x, y =  zip(*v)
                #ax.plot(x, y, label=station[7:]+"_"+k)
                ax.plot(x, y, label=station[:7]+"_"+k)

        #station_lst = [x.split("_")[1][:4] for x in station_lst]
        title = axis_labels[2] + " escenario " + str(scenario)

        plt.legend()
        ax.set_xlabel(axis_labels[0])
        ax.set_ylabel(axis_labels[1])
        ax.set_title(title)

        # generated for export to  latex document
        figlabel = str(scenario + '-'.join(station_lst))
        figcaption = "Resultados para " + str('-'.join(station_lst) + " escenario " + scenario)

        # Generate latex code associated to figure plot
        #makeplot_latex(fig, figlabel, figcaption, bShowInline = plot_inline)
        filename = figlabel #+'.png'

        #fig.savefig(filename)
        #plt.close()

        return fig, filename, figlabel, figcaption

    for day, results in dict_results.iteritems():
        for pareja, data in results.iteritems():
            
            #display(day)
            df = dict_results[day][pareja]

            fig = plt.figure(figsize=figsize)
            ax = fig.add_subplot(111)
            #axis_labels = [xvar, "error [m]", vars2plot[0]+" vs "+vars2plot[1]]
            fig, filename, figlabel, figcaption = plotter(ax, df, xvar=xvar, tags=tags, vars2plot=vars2plot,
                                           axis_labels=axis_labels, figsize=figsize)

            """
            fig.savefig(filename+".pdf", format='pdf', bbox_inches='tight')
            plt.close()
            display(Image(filename),Caption(filename+".pdf", figlabel, str(figcaption)))
            """

            if plot_inline:
                plt.show()
            else:
                plt.close()

            fig.savefig(filename+".pdf", format='pdf', bbox_inches='tight')

            if plot_in_doc:
                display(Image(filename),Caption(filename+".pdf", figlabel, str(figcaption)))

            #LatexFigure(figlabel, figcaption)


def plotting_df(plots_mean_std, scenario="",
            title="difference between error stdlone - error coop position: scenario ",
            xlabel="gps_sow", # ["+str(resolucion)+"x30s]",
            ylabel="position error [m]",
            figsize=(20,5),
            plot_inline = False,
            plot_in_doc = False):

    df = plots_mean_std
    import re

    fig = plt.figure(figsize=figsize)
    ax = fig.add_subplot(111)

    for i in range(len(df.diff_e)):
        labels = [x.translate(None, "[']") for x in df.iloc[i].estaciones.split(",")]
        n_rx = len(df.iloc[i].estaciones.split(","))
        x = df.iloc[i].gps_sow
        for station, tag in zip(range(n_rx), labels):
            y = [s[station] for s in df.iloc[i].diff_e]
            ax.plot(x, y, label=tag)

    ax.legend(bbox_to_anchor=(1.02, 1), loc=1, borderaxespad=0.)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.set_title(title+scenario)

    figlabel = str(scenario)# + "_" + title
    figcaption = "Resultados " + title[:14] + " para escenario " + scenario
    # #escenario + mean_diference
    filename = scenario + "_" + title[:14] #+'.png'

    if plot_inline:
        plt.show()
    else:
        plt.close()

    fig.savefig(filename+".pdf", format='pdf', bbox_inches='tight')

    if plot_in_doc:
        display(Image(filename),Caption(filename+".pdf", figlabel, str(figcaption)))

    # Generate latex code associated to figure plot
    #LatexFigure(figlabel, figcaption)
    #makeplot_latex(fig, label, caption, bShowInline = plot_inline)



def plotting_df3(df_res, scenario,
                 title="diference err_stdlone - err_coop: ",
                 xlabel="gps_sow",
                 ylabel="err_stdlone - err_coop [m]",
                 figsize=(20,5),
                 plot_inline=True,
                 plot_in_doc=True):

    days = set(df_res.gps_day)

    for day in days:
        #
        #Select rows from a DataFrame based on values in a column in pandas
        #https://goo.gl/CW8cZA
        
        df_a = df_res[(df_res['scenario']==scenario) & (df_res['gps_day']==day)]
        #display(df_a)
        
        fig = plt.figure(figsize=figsize)
        ax = fig.add_subplot(111)
        
        labels = df_a["stations"].to_dict()
        labels = {k:v.split(',') for k,v in labels.iteritems()}
        y_var = df_a["diff_e_std_e_coop"].to_dict()
        y_var = {k:np.array(v).T for k,v in y_var.iteritems()}
        x_var = df_a["gps_time"].to_dict()
        
        for idx, idy, idtag in zip(x_var,y_var, labels):
            x = x_var[idx]
            for i, y in enumerate(y_var[idy]):
                ax.plot(x, y, label=labels[idtag][i])

        subtitle = "(scheme {} gpsday {})".format(scenario, str(day))
        ax.set_title(title+" - "+subtitle)
        ax.set_xlabel(xlabel)
        ax.set_ylabel(ylabel)
        ax.legend(bbox_to_anchor=(1.02, 1), loc=1, borderaxespad=0.)

        figlabel = str(day)+"_"+str(scenario)
        figcaption = title+ " " +subtitle
        # #escenario + mean_diference
        filename = figlabel #+'.png'
        
        if plot_inline:
            plt.show()
        else:
            plt.close()

        fig.savefig(filename+".pdf", format='pdf', bbox_inches='tight')

        if plot_in_doc:
            display(Image(filename),Caption(filename+".pdf", figlabel, str(figcaption)))
