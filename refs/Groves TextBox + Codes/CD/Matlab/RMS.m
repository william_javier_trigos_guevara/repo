function rms = RMS(est, true)
% --------------------------------------------------
% Calculate the root mean square error.
% Each column is a time-serie.
%
% Author:   Harald N�kland
% Date:     June 2011
% --------------------------------------------------
E = est(4000:end,:)-true(4000:end,:);
n = size(E,1);

var = diag(E'*E)'/n;
rms = sqrt(var);
