def plot_df_errors(results, stlone_vars=None, cooper_vars=None, e_max=100):
                    
    #from matplotlib.lines import Line2D
    from mpldatacursor import HighlightingDataCursor
    
    import matplotlib.cm as cm
    import itertools
    from mpldatacursor import datacursor
    
    escenarios = [k for k in results.keys()]
    
    if "standalone" in escenarios and "cooperative" in escenarios:
        fig = plt.figure(figsize=(20,15))
        ax1 = fig.add_subplot(211)
        ax2 = fig.add_subplot(212)
        
    elif "cooperative" in escenarios and "standalone" not in escenarios:
        fig = plt.figure(figsize=(20, 8))
        ax2 = fig.add_subplot(111)
        
    elif "standalone" in escenarios:
        fig = plt.figure(figsize=(20, 8))
        ax1 = fig.add_subplot(111)
        
    for k, v in results.iteritems():
        
        if k == "standalone":

            # http://stackoverflow.com/questions/12236566/setting-different-color-for-each-series-in-scatter-plot-on-matplotlib
            colors    = cm.rainbow(np.linspace(0, 1, len(results[k].keys())))
            # http://matplotlib.org/api/markers_api.html#module-matplotlib.markers
            marker    = itertools.cycle(('o', 'v', '^', '<', '>', '8', 's', 'p', '*', 'h', 'H', 'D', 'd'))
            # http://matplotlib.org/api/lines_api.html#matplotlib.lines.Line2D.set_linestyle
            linestyle = itertools.cycle(('-', '--', '-.', ':')) 

            curves = [] 
            #
            for station, color in zip(results[k].keys(), colors):

                epochs = results[k][station].index

                for var in stlone_vars:

                    #if axisNum < len(linestyles):
                    df = results[k][station]
                    variable = np.where(df[var] > e_max, 0, df[var])

                    #http://widu.tumblr.com/post/43624348228/making-unfilled-hollow-markers-in-matplotlib
                    curve, = ax1.plot(epochs , variable,
                                     marker=marker.next(), markersize=7, markeredgewidth=1,
                                     markeredgecolor=color, markerfacecolor='None',
                                     linestyle=linestyle.next(),
                                     label=station+" "+var)

                    curves.append(curve)



            ax1.set_ylim([-1, e_max])
            ax1.set_title(k+" Positioning")
            ax1.set_xlabel("gps_sow")
            ax1.set_ylabel("Position error [m]")
            ax1.legend()

            # para poder resaltar las curvas
            HighlightingDataCursor(curves)
            datacursor(display='multiple', draggable=True)

        elif k == "cooperative":

            # http://stackoverflow.com/questions/12236566/setting-different-color-for-each-series-in-scatter-plot-on-matplotlib
            colors    = cm.rainbow(np.linspace(0, 1, len(results[k].keys())))
            # http://matplotlib.org/api/markers_api.html#module-matplotlib.markers
            marker    = itertools.cycle(('o', 'v', '^', '<', '>', '8', 's', 'p', '*', 'h', 'H', 'D', 'd', 'P', 'X'))
            # http://matplotlib.org/api/lines_api.html#matplotlib.lines.Line2D.set_linestyle
            linestyle = itertools.cycle(('-', '--', '-.', ':')) 

            curves = []
            #ax = fig.add_subplot(212)
            for pareja, color in zip(results[k].keys(), colors):

                epochs = results[k][pareja].index

                for var in cooper_vars:

                    if var.split("_")[-1] == "local":

                        local = pareja.split(",")[0]
                        df = results[k][pareja]
                        variable = np.where(df[var] > e_max, 0, df[var])

                        curve, = ax2.plot(epochs, variable,
                                         marker=marker.next(), markersize=7, markeredgewidth=1,
                                         markeredgecolor=color, markerfacecolor='None',
                                         linestyle=linestyle.next(),
                                         label="coop_err_"+local+"(local)")
                        curves.append(curve)


                    elif var.split("_")[-1] == "remote":

                        remot = pareja.split(",")[1]
                        df = results[k][pareja]
                        variable = np.where(df[var] > e_max, 0, df[var])# df[df[var] < e_max]

                        curve, = ax2.plot(epochs, variable,
                                         marker=marker.next(), markersize=7, markeredgewidth=1,
                                         markeredgecolor=color, markerfacecolor='None',
                                         linestyle=linestyle.next(),
                                         label="coop_err_"+remot+"(remote)")
                        curves.append(curve)

            ax2.set_ylim([-1, e_max])
            ax2.set_title(k+" Positioning")
            ax2.set_xlabel("gps_sow")
            ax2.set_ylabel("Position error [m]")
            ax2.legend()

            # para poder resaltar MULTPLES curvas
            HighlightingDataCursor(curves)
            # https://pypi.python.org/pypi/mpldatacursor/
            datacursor(display='multiple', draggable=True)

        else:
            pass

def show_results(results):
    
    for k, v in results.iteritems():
        
        if k == "standalone":
            
            print ("*"*40)
            print ("%s Positioning results"%(k))
            print ("*"*40)
            
            for station in results[k].keys():
                
                print ("\n\t * %s"%(station))
                
                df = results[k][station]
                columnas = ['gps_sow', 'ls_pos', 'optim_pos', 'raim_err', 'ls_err', 'optim_err', 
                            'raim_time[s]', 'ls_time[s]', 'op_time[s]']
                display(df[columnas])
        
        elif k == "cooperative":
            
            print ("*"*40)
            print ("%s Positioning results"%(k))
            print ("*"*40)
            
            for pareja in results[k].keys():
                
                print ("\n\t * %s"%(pareja))
                
                df = results[k][pareja]
                columnas = ['gps_sow', 'real_pos_local', 'real_pos_remote', 'coop_pos_local', 
                            'coop_pos_remote', 'coop_err_local', 'coop_err_remote', 
                            'coop_time[s]', 'dc_vec']
                display(df[columnas])
                
