%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Euler angle plot
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure

% roll
subplot(3,1,1)
title('Euler angles')
hold on
plot(t, act_e(1:N,1),'-b')
plot(t, est_e(1:N,1),'--r')
axis([0 N*h -180 180])
legend('True', 'EKF')
ylabel('Roll \phi [deg]')

% pitch
subplot(3,1,2)
hold on
plot(t, act_e(1:N,2),'-b')
plot(t, est_e(1:N,2),'--r')
axis([0 N*h -90 90])
legend('True', 'EKF')
ylabel('Pitch \theta [deg]')

% yaw
subplot(3,1,3)
hold on
plot(t, act_e(1:N,3),'-b')
plot(t, est_e(1:N,3),'--r')
axis([0 N*h -180 180])
legend('True', 'EKF')
ylabel('Yaw \psi [deg]')
xlabel('Time [sec]')
