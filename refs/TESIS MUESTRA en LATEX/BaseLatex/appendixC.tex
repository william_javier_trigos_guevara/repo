% APPENDIX C: eCos/RedBoot Instructions

The following instructions detail setting up the GPL-GPS development system on a host Linux PC and on a SigTec MG5001 receiver as of May $5^{th}$, 2005. Please see \url{http://gps.psas.pdx.edu/} for the latest version of these instructions.

\section{``{RedBooting}'' a GP4020-based Receiver}

\subsection{Overview}

In order to get {eCos} and thus the OSGPS code on your receiver, you first need to prep your receiver with a ROM monitor. We're going to use RedBoot, {eCos}'s bootstrap environment for embedded systems. I'm assuming that you're working on a Linux box here, not a Windows/Cygwin system. However, I think it should be pretty easy to translate to that environment. Please let me know if you have patches for these instructions, including how to get it to work under cygwin.

Here are the steps to install RedBoot:
\begin{enumerate}
   \item Install {eCos} on the host computer and get the latest {eCos} sources from {CVS}.
   \item Configure and build {RedBoot} (a RAM and a ROM version) out of the {eCos} sources for your receiver.
   \item Cross compile a small bootstrapping program for the GP4020.
   \item Set up minicom for downloading to the GP4020.
   \item Load a bootstrap program into the GP4020's memory via the GP4020's boot-via-UART feature.
   \item Load {RedBoot} into RAM.
   \item (Optional but recommended) Download and save the OEM firmware bits.
   \item Burn {RedBoot} into ROM (flash, really).
   \item (Optional and not recommended) Burning back the OEM firmware bits.
\end{enumerate}

\subsection{Install {eCos} on the host computer and get the latest {eCos} sources from CVS}

Go to \url{http://ecos.sourceware.org/getstart.html} and follow the directions to download and install {eCos}. I don't recommend getting it any other way (e.g., Debian packages) because it tends to put things in odd places.

You'll find that it put everything in \texttt{/opt/ecos} which is a fine place to put it. All of your tools are in \texttt{/opt/ecos/gnutools/} and all of the released {eCos} sources (version 2.0 as of 3/2005) is in \texttt{/opt/ecos/ecos-2.0}.

Go to \url{http://ecos.sourceware.org/anoncvs.html} and follow the directions to get the latest version of {eCos} from the {eCos} CVS repository. This is necessary because (as of 3/2005) the GP4020 sources are only in CVS. Annoyingly, you'll need to do the checkout and all future updates as root since it's located in the \texttt{/opt}directory. Don't forget to update your sources often - like once a month or so - by running the CVS update command specified on that page.

Run \texttt{configtool}. Choose \texttt{Build}, \texttt{Repository}, and point your repository to the packages menu in the CVS sources you just downloaded previously.

You also want to make sure that your environment has \texttt{ECOS\_REPOSITORY} set to your CVS directory's \texttt{packages} directory.

OK, now you've got {eCos} (and thus RedBoot) sources, the {eCos} configuration tools, and some GNU tools (gcc, etc).

\subsection{Configure and build {RedBoot} (a RAM and a ROM version) out of the {eCos} sources}

First we'll build the RAM version of RedBoot. Then we'll build the ROM version.

\begin{itemize}
   \item Make a directory, called for example \texttt{redboot\_ram}, and start \texttt{configtool}from there.
   \item Choose \texttt{Build}, \texttt{Template}and choose \texttt{GPS 4020}from the hardware drop down list. Then choose \texttt{redboot}from the \texttt{Packages}drop down. Hit \texttt{OK}. and then \texttt{Continue}when the conflicts dialog pops up. It'll handle the conflicts automatically.
   \item Choose \texttt{Import}from the \texttt{File}menu and import the file \texttt{/opt/ecos/ecos-cvs/packages/hal/arm/gps4020/current/misc/redboot\_RAM.ecm}. This makes the final necessary tweaks to the gps4020/redboot template.
   \item Choose \texttt{Save}from the \texttt{File}menu and save that file in the directory you made above. I called it \texttt{redboot\_ram.ecc}.
   \item Choose \texttt{Build}, \texttt{Library}and now the configtool will build you {RedBoot} for GP4020 to run out in RAM. The binary file, Motorola S-record file and elf file are all located in the \texttt{redboot\_ram/redboot\_ram\_install/bin/} directory.
\end{itemize}

Now we need to make the ROM version of redboot that we can burn into flash. 

\begin{itemize}
   \item Make a directory, called for example \texttt{redboot\_rom}, and start the \texttt{configtool} from there.
   \item Choose \texttt{Build}, \texttt{Template} and choose \texttt{GPS 4020} from the hardware drop down list. Then choose \texttt{redboot} from the \texttt{Packages} drop down. Hit \texttt{OK}. and then \texttt{Continue} when the conflicts dialog pops up. It'll handle the conflicts automatically.
   \item Choose \texttt{Import} from the \texttt{File} menu and import the file \texttt{/opt/ecos/ecos-cvs/packages/hal/arm/gps4020/current/misc/redboot\_ROM.ecm}. This makes the final necessary tweaks to the gps4020/redboot template.
   \item Choose \texttt{Save} from the \texttt{File} menu and save that file in the directory you made above. I called it \texttt{redboot\_rom.ecc}.
   \item Choose \texttt{Build}, \texttt{Library} and now the configtool will build you {RedBoot} for GP4020 to run out in ROM. The binary file, Motorola S-record file and elf file are all located in the \texttt{redboot\_rom/redboot\_rom\_install/bin/} directory.
\end{itemize}

\subsection{Cross compile a small bootstrapping program for the GP4020.}

First of all, thanks to Gary Thomas for writing all of the GP4020 download tools and putting them into {eCos} - thanks Gary!

When the GP4020 boots, it has a special UART boot mode where it can load a small program into SRAM and run it. We're going to compile a small program to do exactly that: load it into the SRAM as raw binary, and then use that program to load the s-record version of the {RedBoot} RAM image into the GP4020's RAM.

At the same time, we'll grab some Python programs that we'll install into minicom in the next step.

First, get the download directory from CVS, then build the small program.

\begin{verbatim}
\$ cp -r /opt/ecos/ecos-cvs/packages/hal/arm/gps4020/current/support/download .
\$ cd download
\$ make
\end{verbatim}


... and after a bit of grinding, you should have the bootstrapping program, \texttt{gps4020\_download.bin}.

\subsection{1.4: Configure Minicom and add some downloading protocols}

We're going to use minicom to interact with the GP4020, including bootstrapping ourselves into RedBoot. We'll need a few special "minicom extensions" that Gary wrote that we copied over in section 1.3 above. \texttt{download\_bin.py} is a python script for minicom which allows you to load a binary file (in our case, the binary \texttt{gps4020\_download.bin}) into the GP4020 when it's in boot mode. \texttt{download.py} is a program to send Motorola s-record formatted files to that bootstrapping binary.

First, put the python scripts where \texttt{minicom} can get at them:


\begin{verbatim}
\$ cd download
\$ sudo cp download*.py /usr/local/bin
\$ chmod +x /usr/local/bin/download*.py
\end{verbatim}


Now run \texttt{minicom}.

\begin{itemize}
   \item Press \texttt{ctrl-a} then \texttt{o} to get the configuration menu.
   \item Choose \texttt{File Transfer Protocols}
   \item Configure an empty option:
      \item \texttt{Name} can be whatever you want; I used \texttt{gps\_bootloader}
      \item \texttt{Program} is \texttt{/usr/local/bin/download\_bin.py}
      \item The next five columns are \texttt{Name = Y}, \texttt{U/D = U}, \texttt{FullScr = N}, \texttt{IO-Red. = Y}, \texttt{Multi = N}
    \item Now configure another empty option:
      \item \texttt{Name} can be whatever you want; I used \texttt{gps\_uploader}
      \item \texttt{Program} is \texttt{/usr/local/bin/download.py}
      \item The next five columns are \texttt{Name = Y}\texttt{U/D = U}, \texttt{FullScr = N}, \texttt{IO-Red. = Y}, \texttt{Multi = N}
   \item Hit return to exit that menu and go back to the configuration menu
   \item Choose \texttt{Serial Port Setup}
   \item Make sure you have the right device (e.g., \texttt{/dev/ttyS0}), it's set to \texttt{57600 8N1}, and BOTH hardware and software flow control are OFF.
   \item Either save the setup as default (recommended) or save it as a setup somewhere which you'll load when you need to do this again.
\end{itemize}

Now \texttt{minicom} is set up to transfer files in two modes: one to talk to the GP4020's UART bootloader, and one to talk to the program we compiled in 1.3.

\subsection{Load the bootstrap program into the GP4020's memory via the GP4020's boot-via-UART}

\begin{itemize}
   \item Run \texttt{minicom}
   \item Turn on your GP4020 receiver with the boot loader pin set to "load". Or better yet, do a hardware reset with it set to "load".
   \item Press \texttt{ctrl-a} then \texttt{s} to send a file; choose the \texttt{gps\_bootloader} protocol.
   \item Navigate to and select  \texttt{gps4020\_download.bin}.
   \item Press any key to continue and then you should see the message \texttt{download>>}. It might actually only show \texttt{d>>} or some fraction of the message; don't worry about it, it still works.
\end{itemize}

\subsection{Load redboot into RAM using the gps4020\_download.bin bootloader program}

Now we have \texttt{gps4020\_download.bin} loaded into RAM; we can now upload an s-records file.

\begin{itemize}
   \item In \texttt{minicom}, press \texttt{ctrl-a} then \texttt{s} to send a file; choose the \texttt{gps\_uploader} protocol.
   \item Select \texttt{redboot\_ram/redboot\_ram\_install/bin/redboot\_ram.srec}.
   \item You should see addresses spin in a dialog as the {RedBoot} srec file loads.
   \item After you press any key, you should see the {RedBoot} welcome screen!
\end{itemize}

\begin{verbatim}
RedBoot(tm) bootstrap and debug environment (RAM)
Non-certified release, version UNKNOWN - built 07:21:45, Nov 15 2003

Platform: GPS-4020 (ARM7TDMI)
Copyright (C) 2000, 2001, 2002, Red Hat, Inc.

RAM: 0x20000000-0x20040000, 0x20005908-0x2003f000 available
RedBoot>
\end{verbatim}

Note the \texttt{(RAM)} note on the first line; this means you have successfully loaded {RedBoot} into the GP4020's RAM.

\subsection{(Optional but recommended) Save the current flash image (usually the OEM firmware)}

It's always good to keep all of your bits, and the OEM firmware on your GP4020 board is no exception. To get a copy, get {RedBoot} up and running in RAM (see above). Once you're there, then by far the easiest thing to do is to use gdb to grab the flash. First of all, the \texttt{arm-elf-gdb} version of gdb doesn't seem to like the \texttt{set baud} command, so run minicom and then quit without resetting the serial port; that'll leave it configured correctly. DON'T FORGET to quit minicom, by the way: even with redboot running, gdb will take of the serial console so minicom is useless anyway.

Finally, you might be wondering why the below addresses are in the 0x60000000 range instead of the 0x00000000 range. That's because {RedBoot} by necessity swaps the SRAM and flash memory areas (1 and 4 in the GP4020) so that 0x60000000 becomes flash and 0x00000000 becomes SRAM. It's the difference between cold boot and running: on a cold boot, the flash has to be in memory area 1 (0x00000000) to be at the reset vector. But you want to change the vectors, so we swap it with the fast SRAM (memory area 4) as soon as we're all set up. See MpcForMG5001 for more info.


\begin{verbatim}
\$ arm-elf-gdb
GNU gdb 5.3 (eCosCentric)
Copyright 2002 Free Software Foundation, Inc.
GDB is free software, covered by the GNU General Public License, and you are
welcome to change it and/or distribute copies of it under certain conditions.
Type "show copying" to see the conditions.
There is absolutely no warranty for GDB.  Type "show warranty" for details.
This GDB was configured as "--host=i686-pc-linux-gnu --target=arm-elf".
(gdb) target remote /dev/ttyS0
Remote debugging using /dev/ttyS0
0x2000abec in ?? ()
(gdb) dump memory flash_dump.bin 0x60000000 0x6003FFFF
(gdb) q
The program is running.  Exit anyway? (y or n) y
\$
\end{verbatim}


There. Now you've got the flash memory safely tucked away in a file called \texttt{flash\_dump.bin}

\subsection{Burn redboot into flash}

Assuming you're in \texttt{minicom} with redboot loaded into RAM:


\begin{verbatim}
RedBoot> load -r -b 0x20020000 -m xmodem
CRaw file loaded 0x20020000-0x2002e7d7, assumed entry at 0x20020000
xyzModem - CRC mode, 465(SOH)/0(STX)/0(CAN) packets, 7 retries
\end{verbatim}

\begin{itemize}
   \item In \texttt{minicom}, press \texttt{ctrl-a} then \texttt{s} to send a file; choose the \texttt{xmodem} protocol.
   \item Select \texttt{redboot\_rom/redboot\_rom\_install/bin/redboot\_rom.bin}.
   \item You should see the xmodem status update as the {RedBoot} ROM image loads.
\end{itemize}

And then finally, to burn the file into flash:

\begin{verbatim}
RedBoot> fis write -f 0x60000000 -b 0x20020000 -l 0x10000
* CAUTION * about to program FLASH
            at 0x60000000..0x6000ffff from 0x20020000 - continue (y/n)? y
... Erase from 0x60000000-0x60010000: .
... Program from 0x20020000-0x20030000 at 0x60000000: .
RedBoot>
\end{verbatim}

Now power-cycle or reset the the receiver and you should get:

\begin{verbatim}
RedBoot(tm) bootstrap and debug environment (ROM)
Non-certified release, version UNKNOWN - built 00:44:46, Mar 30 2005

Platform: GPS-4020 (ARM7TDMI)
Copyright (C) 2000, 2001, 2002, 2003, 2004 Red Hat, Inc.

RAM: 0x20000000-0x20040000, (0x200059c0-0x20040000) available
FLASH: 0x60000000 - 0x60040000, 4 blocks of 0x00010000 bytes each.
RedBoot>
\end{verbatim}


CONGRATULATIONS! You've just installed the {RedBoot} boot loader onto your GPS receiver. Now we can move on to the {eCos} and GPS side of things!

\subsection{(Optional and NOT recommend) Loading back in the OEM firmware}

Sometimes you just gotta go back. To do this is a bit annoying, unfortunately. First, the dump you took previously is too big. Using a hex editor, go in and remove all of the blank flash area at the end - everything filled with 0xFF's. Then save this file. Then break the file up into two different chunks, because there's not enough room in the RAM to send the file over and have redboot in RAM. Then you have to x modem the files over and burn them into flash. Here's a transcript of what I did:


\begin{verbatim}
RedBoot>load -r -m xmodem -b 0x20020000
CRaw file loaded 0x20020000-0x2003ffff, assumed entry at 0x20020000
xyzModem - CRC mode, 1025(SOH)/0(STX)/0(CAN) packets, 3 retries
RedBoot> fis write -f 0x60000000 -b 0x20020000 -l 0x20000
** WARNING: RAM address: 0x20020000 may be invalid
   valid range is 0x20000000-0x20040000
* CAUTION * about to program FLASH
            at 0x60000000..0x6001ffff from 0x20020000 - continue (y/n)? y
... Erase from 0x60000000-0x60020000: ..
... Program from 0x20020000-0x20040000 at 0x60000000: ..
RedBoot> load -r -m xmodem -b 0x20020000
CRaw file loaded 0x20020000-0x2002aa1f, assumed entry at 0x20020000
xyzModem - CRC mode, 341(SOH)/0(STX)/0(CAN) packets, 2 retries
RedBoot> fis write -f 0x60020000 -b 0x20020000 -l 0xaa20
* CAUTION * about to program FLASH
            at 0x60020000..0x6002ffff from 0x20020000 - continue (y/n)? y
... Erase from 0x60020000-0x60030000: .
... Program from 0x20020000-0x20030000 at 0x60020000: .
RedBoot>
\end{verbatim}



