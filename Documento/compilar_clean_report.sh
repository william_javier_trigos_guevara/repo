#./compilar_clean_report.sh Propuesta_William_Trigos.ipynb biblio.bib

filename=$(basename "$1")
extension="${filename##*.}"
filename="${filename%.*}"


echo $filename

echo ""> $filename.tex

jupyter nbconvert --to latex --template rev.tplx $filename #rev.tplx latex_basic.tplx clean_report.tplx  citations.tplx 
echo $1
pdflatex $filename.tex
bibtex $2 #
bibtex $filename
pdflatex $filename.tex
pdflatex $filename.tex
pdflatex $filename.tex

rm -rf *.out *.log *.bbl *.blg *.aux *.toc #*files *.tex