STDTEX="/usr/share/texmf"; export STDTEX
MYTEX="./tex"; export MYTEX
TEXINPUTS=".:$MYTEX/inputs/thesis/:$MYTEX/inputs/:$STDTEX/tex//:/etc/texmf/pdftex//"; export TEXINPUTS
BSTINPUTS=".:$MYTEX/bst:$STDTEX/bibtex/bst//"; export BSTINPUTS
BIBINPUTS=".:$MYTEX/bib"; export BIBINPUTS

# commented out for future use
#TEXFONTROOT="/var/lib/texmf"; export TEXFONTROOT
#TFMFONTS="$TEXFONTROOT/tfm//"; export TFMFONTS
#PKFONTS="$TEXFONTROOT/pk//"; export PKFONTS

#TEXINPUTS=".:$MYTEX/inputs:$STDTEX/tex/"; export TEXINPUTS
#TEXFORMATS="$STDTEX/ini"; export TEXFORMATS
#MFINPUTS="$STDTEX/mf:$STDTEX/fonts/src/"; export MFINPUTS
#TFMFONTS=".:$STDTEX/fonts/tfm"; export TFMFONTS
#TEXPOOL="$STDTEX/ini"; export TEXPOOL
#MFPOOL="$STDTEX/ini"; export MFPOOL
#BIBINPUTS=".:$MYTEX/bib:$STDTEX/bibtex/bib"; export BIBINPUTS
#VFFONTS="$STDTEX/fonts/vf"; export VFFONTS
#PKFONTS="$STDTEX/fonts/pk/"; export PKFONTS
