function q = qbuild(eps)
% --------------------------------------------------
% Construct the quaternion from the unit constraint
%
% Author:   Harald N�kland
% Date:     June 2011
% --------------------------------------------------
eta = sqrt(1 - eps'*eps);
q   = [eta; eps];
