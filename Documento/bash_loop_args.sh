#!/bin/bash
## declare an array variable
## https://goo.gl/CiVzeG
## https://goo.gl/Hi7JKp

#declare -a array=("one" "two" "three")
declare -a array_notebooks=("$@")

# get length of an array
arraylength=${#array_notebooks[@]}

# use for loop to read all values and indexes
for (( i=1; i<${arraylength}+1; i++ ));
do
  echo $i " / " ${arraylength} " : " ${array_notebooks[$i-1]}
done

python nbmerge.py ${array_notebooks} > merged.ipynb
