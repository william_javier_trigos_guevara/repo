#!/usr/local/bin/python3.3
# -*- coding: utf8 -*-

{% for worksheet in nb.worksheets -%}
{%- for cell in worksheet.cells -%}
{%- if cell.cell_type in ['code'] and cell.metadata.listing is defined -%}
### {{ cell.metadata.listing.title }} ###

{{ cell.input }}


{% endif -%}
{%- endfor -%}
{%- endfor -%}