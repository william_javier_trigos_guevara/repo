Necesito actualizar este archivo:


# Trabajo pendiente
[ ] contruir metodo [bancroft](http://kom.aau.dk/~borre/matlab/bancroft/bancroft.m) para reemplazar el solver standalone e inicializar cooperativo.

[ ] emplear metodo [baseline](http://kom.aau.dk/~borre/easy/baseline.m) podría encanjar en metodo de [Chang](http://www.cs.mcgill.ca/~chris/pub/ChaPY04.pdf).[Aqui usan lo que necesito para usar baseline](http://geotux.tuxfamily.org/index.php/geo-blogs/item/284-gudux-procesamiento-diferencial-de-datos-gps)

## error ionosférico


# Trabajo desarrollado y estable

[x] función que lee kmz
[x] función que descarga archivos RINEX a la carpeta `new_data` desde: 
> * Unavco
* NASA
* Serv Geo Col

[x] función que crea un diccionario con los archivos comprimidos en el directorio `new_data`

[x] función de descomprime los archivos sobre carpeta `Simulacion`

[x] función que ejecute barridos en tiempo GPS y genere diccionarios que contienen los nieveles de:

> * error en posicionamiento 
* distancia entre los receptores (estimada GPSTk y Solver)
[x] Crear un función capas de crear graficas a partir de los diccionarios, agregando leyendas automaticamente para cada estación o día GPS.