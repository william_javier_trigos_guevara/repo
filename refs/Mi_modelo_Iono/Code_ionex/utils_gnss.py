
import sys
import argparse
import numpy as np
import matplotlib.pyplot as plt
import pylab as pyl
import datetime
import time
import warnings
from sklearn.datasets import load_svmlight_file
from sklearn import linear_model, decomposition, datasets
from sklearn import cross_validation, svm
from scipy.sparse import csr_matrix
from sklearn import preprocessing
from sklearn.ensemble import RandomForestRegressor
from numpy.core.umath_tests import inner1d
warnings.filterwarnings("ignore", category=DeprecationWarning)
import csv # using Python module
import random
from numpy import linalg as LA
import pandas as pd
from numpy import array,fix,floor,logical_not,ones,where,zeros
import sklearn.base as base
from scipy.optimize import fmin
from numpy import linalg,array
import os
import sys,inspect
from math import pi,atan2,cos,sin
import re


#############################################
## Functions from GSim
#############################################

def xyz2llh(r):
#XYZ2LLH        Convert from ECEF cartesian coordinates to 
#               latitude, longitude and height.  WGS-84
#
#       llh = xyz2llh(xyz)
#
#    INPUTS
#       xyz(0) = ECEF x-coordinate in meters
#       xyz(1) = ECEF y-coordinate in meters
#       xyz(2) = ECEF z-coordinate in meters
#
#    OUTPUTS
#       llh(0) = lat    latitude in radians
#       llh(1) = long   longitude in radians
#       llh(2) = height height above ellipsoid in meters
#       Reference: Understanding GPS: Principles and Applications,
#                  Elliott D. Kaplan, Editor, Artech House Publishers,
#                  Boston, 1996.
#
#       M. & S. Braasch 10-96
#       Copyright (c) 1996 by GPSoft
#       All Rights Reserved.
#
    m = r.shape[0]
    x = (r[:,0:1])
    y = (r[:,1:2])
    z = (r[:,2:3])

    x2 = x**2
    y2 = y**2
    z2 = z**2

    a = 6378137.0000
    b = 6356752.3142
    ee = np.sqrt (1-(b/a)**2)
    b2 = b*b
    e2 = ee*ee
    ep = ee*(a/b)
    r = np.sqrt(x2+y2)
    r2 = r*r
    E2 = a*a - b*b
    F = 54 * b2 * z2
    G = r2 + (1 - e2) * z2 - e2 * E2
    c = (e2*e2*F*r2)/(G*G*G)
    s = ( 1 + c + np.sqrt(c*c + 2*c) )**(1/3)
    P = F / (3 * (s+1/s+1)**2 * G*G)
    Q = np.sqrt(1+2*e2*e2*P)
    ro = -(P*e2*r)/(1+Q) + np.sqrt((a*a/2)*(1+1/Q) - (P*(1-e2)*z2)/(Q*(1+Q)) - P*r2/2)
    tmp = (r - e2*ro)**2
    U = np.sqrt( tmp + z2 )
    V = np.sqrt( tmp + (1-e2)*z2 )
    zo = (b2*z)/(a*V)
    height = U*( 1 - b2/(a*V) )

    lat = np.arctan( (z + ep*ep*zo)/r )
    long = np.zeros((m,1))
    temp = np.arctan(y/x)

    long[x>0] = temp[x>0]
    long[(x<0) & ( y>0)] = temp[(x<0) & ( y>0)] + np.pi
    long[(x<0) & ( y<0)] = temp[(x<0) & ( y<0)] - np.pi

    return np.hstack(((lat), (long), height))

def xyz2enu(xyz,orgxyz):
#XYZ2ENU    Convert from WGS-84 ECEF cartesian coordinates to 
#               rectangular local-level-tangent ('East'-'North'-Up)
#               coordinates.
#    enu = XYZ2ENU(xyz,orgxyz)    
#    INPUTS
#    xyz(0) = ECEF x-coordinate in meters
#    xyz(1) = ECEF y-coordinate in meters
#    xyz(2) = ECEF z-coordinate in meters
#    orgxyz(0) = ECEF x-coordinate of local origin in meters
#    orgxyz(1) = ECEF y-coordinate of local origin in meters
#    orgxyz(2) = ECEF z-coordinate of local origin in meters
#
#    OUTPUTS
#       enu
#        enu(0) = 'East'-coordinate relative to local origin (meters)
#        enu(1) = 'North'-coordinate relative to local origin (meters)
#        enu(2) = Up-coordinate relative to local origin (meters)
#    Reference: Alfred Leick, GPS Satellite Surveying, 2nd ed.,
#               Wiley-Interscience, John Wiley & Sons, 
#               New York, 1995.
#    M. & S. Braasch 10-96
#    Copyright (c) 1996 by GPSoft
#    All Rights Reserved.

    #if nargin<2,error('insufficient number of input arguments'),end
    tmpxyz = xyz
    tmporg = orgxyz
    #if size(tmpxyz) ~= size(tmporg), tmporg=tmporg'; end,
    difxyz = tmpxyz - tmporg

    m = difxyz.shape[0]
    n = difxyz.shape[1]
    
    #if m<n: difxyz=difxyz.transpose()
    orgllh = xyz2llh(orgxyz)

    phi = orgllh[:,0:1]
    lam = orgllh[:,1:2]

    sinphi = np.sin(phi)
    cosphi = np.cos(phi)
    sinlam = np.sin(lam)
    coslam = np.cos(lam)

    result = np.zeros((sinphi.shape[0],3))

    for i in range(sinphi.shape[0]):
        R = np.array(([-sinlam[i],coslam[i],0],[-sinphi[i]*coslam[i],-sinphi[i]*sinlam[i],cosphi[i]],[cosphi[i]*coslam[i],cosphi[i]*sinlam[i],sinphi[i]]))
        result[i] = np.dot(R,difxyz[i].transpose()).transpose()

    return result

def llh2xyz(r):
 
    phi    = r[:,0:1]*(np.pi/180)
    lamb   = r[:,1:2]*(np.pi/180)
    h      = r[:,2:3]

    a = 6378137.0000
    b = 6356752.3142
    e = np.sqrt (1-(b/a)**2)
    m = r.shape[0]
    sinphi = np.sin(phi)
    cosphi = np.cos(phi)
    coslam = np.cos(lamb)
    sinlam = np.sin(lamb)
    tan2phi = (np.tan(phi))**2
    tmp = 1 - e*e
    tmpden = np.sqrt( 1 + tmp*tan2phi )

    x = (a*coslam)/tmpden + h*coslam*cosphi
    y = (a*sinlam)/tmpden + h*sinlam*cosphi

    tmp2 = np.sqrt(1 - e*e*sinphi*sinphi)
    z = (a*tmp*sinphi)/tmp2 + h*sinphi

    return np.hstack((x,y,z))
    
def vtec_klobuchar(systime,svxyz,usrxyz,ionoParam):
#IONOCORR   Compute ionospheric correction from the broadcast model parameters
#   ionodel = ionocorr(systime,svxyz,usrxyz)
## INPUTS
#   systime   = time at which iono delay is to be computed; note this is expressed as system time (i.e., GPS time of week in seconds) for each element
#   svxyz     = satellite position expressed in ECEF cartesian coordinates for each element
#   usrxyz    = user position expressed in ECEF cartesian coordinates for each element
#   ionoParam = contains ionosphere parameters ALPHA and BETA for each element
## OUTPUTS
#   ionodel = ionospheric delay correction (meters)
#   Copyright (c) 2002 by GPSoft
#   Modified by Marc Sole (Pildo, Jul'13)
# - mod1: ALPHA BETA provided within the ionoParam input

    ALPHA   = ionoParam[:,0:4]
    BETA    = ionoParam[:,4:8]
    pi = np.pi

    svllh = xyz2llh(svxyz)
    svenu = xyz2enu(svxyz,usrxyz)

    el = np.arctan2(svenu[:,2],np.apply_along_axis(np.linalg.norm, 1, svenu[:,0:2]))
    az = np.arctan2(svenu[:,0],svenu[:,1])
    E = el/pi  # E is elevation angle expressed in semi-circles
    A = az/pi  # A is azimuth angle expressed in semi-circles
    F = 1 + 16*(0.53 - E)**3
    psi = 0.00137/(E + 0.11) - 0.022
    phiu = svllh[:,0]/pi  # user geodetic latitude expressed in semi-circles
    phii = phiu + psi*np.cos(az)
    
    phii[phii >  0.416*np.ones(phii.shape)] =  0.416
    phii[phii < -0.416*np.ones(phii.shape)] = -0.416
    lambdau = svllh[:,1]/pi  # user geodetic longitude expressed in semi-circles
    lambdai = lambdau + psi*np.sin(az)/np.cos(phii*pi)

    phim = phii + 0.064*np.cos((lambdai-1.616)*pi)

    t = 4.32e4*lambdai + systime
    t = np.mod(t,86400)
    phim_matrix = np.column_stack((  np.ones((phim.shape[0])), phim, phim**2, phim**3  ))

    PER = np.sum(BETA*phim_matrix,1)
    #PER = BETA[0] + BETA[1]*phim + BETA[2]*(phim**2) + BETA[3]*(phim**3)

    PER[ PER < 72000*np.ones(PER.shape) ] = 72000

    x = 2*pi*(t - 50400)/PER
    #AMP = ALPHA[0] + ALPHA[1]*phim + ALPHA[2]*pow(phim,2) + ALPHA[3]*pow(phim,3)
    AMP = np.sum(ALPHA*phim_matrix,1)

    AMP[ AMP < np.zeros(AMP.shape)] = 0

    Tiono = F*(5e-9 + AMP*(1 - pow(x,2)/2 + pow(x,4)/24))
    Tiono[ np.absolute(x) >= 1.57*np.ones(x.shape) ] = F[ np.absolute(x) >= 1.57*np.ones(x.shape) ]*5e-9

    K = 1.6240545787086483*(10**-17)

    ionodel = Tiono*299792458
    vtec = ionodel/(F*K)
    return vtec

def v_ionodel_klobuchar(systime,svxyz,usrxyz,ionoParam):
#IONOCORR   Compute ionospheric correction from the broadcast model parameters
#   ionodel = ionocorr(systime,svxyz,usrxyz)
## INPUTS
#   systime   = time at which iono delay is to be computed; note this is expressed as system time (i.e., GPS time of week in seconds) for each element
#   svxyz     = satellite position expressed in ECEF cartesian coordinates for each element
#   usrxyz    = user position expressed in ECEF cartesian coordinates for each element
#   ionoParam = contains ionosphere parameters ALPHA and BETA for each element
## OUTPUTS
#   ionodel = ionospheric delay correction (meters)
#   Copyright (c) 2002 by GPSoft
#   Modified by Marc Sole (Pildo, Jul'13)
# - mod1: ALPHA BETA provided within the ionoParam input

    ALPHA   = ionoParam[:,0:4]
    BETA    = ionoParam[:,4:8]
    pi = np.pi

    svllh = xyz2llh(svxyz)
    svenu = xyz2enu(svxyz,usrxyz)

    el = np.arctan2(svenu[:,2],np.apply_along_axis(np.linalg.norm, 1, svenu[:,0:2]))
    az = np.arctan2(svenu[:,0],svenu[:,1])
    E = el/pi  # E is elevation angle expressed in semi-circles
    A = az/pi  # A is azimuth angle expressed in semi-circles
    F = 1 + 16*(0.53 - E)**3
    psi = 0.00137/(E + 0.11) - 0.022
    phiu = svllh[:,0]/pi  # user geodetic latitude expressed in semi-circles
    phii = phiu + psi*np.cos(az)
    
    phii[phii >  0.416*np.ones(phii.shape)] =  0.416
    phii[phii < -0.416*np.ones(phii.shape)] = -0.416
    lambdau = svllh[:,1]/pi  # user geodetic longitude expressed in semi-circles
    lambdai = lambdau + psi*np.sin(az)/np.cos(phii*pi)

    phim = phii + 0.064*np.cos((lambdai-1.616)*pi)

    t = 4.32e4*lambdai + systime
    t = np.mod(t,86400)
    phim_matrix = np.column_stack((  np.ones((phim.shape[0])), phim, phim**2, phim**3  ))

    PER = np.sum(BETA*phim_matrix,1)
    #PER = BETA[0] + BETA[1]*phim + BETA[2]*(phim**2) + BETA[3]*(phim**3)

    PER[ PER < 72000*np.ones(PER.shape) ] = 72000

    x = 2*pi*(t - 50400)/PER
    #AMP = ALPHA[0] + ALPHA[1]*phim + ALPHA[2]*pow(phim,2) + ALPHA[3]*pow(phim,3)
    AMP = np.sum(ALPHA*phim_matrix,1)

    AMP[ AMP < np.zeros(AMP.shape)] = 0

    Tiono = F*(5e-9 + AMP*(1 - pow(x,2)/2 + pow(x,4)/24))
    Tiono[ np.absolute(x) >= 1.57*np.ones(x.shape) ] = F[ np.absolute(x) >= 1.57*np.ones(x.shape) ]*5e-9

    K = 1.6240545787086483*(10**-17)

    ionodel = Tiono*299792458
    v_ionodel = ionodel/(F)
    return v_ionodel
	
	
	


    
def ExtendFeatures3D(d):
## Returns terms for analytical solution for three intersecting spheres
#  INPUTS:
#  d       = array containing satellite's position vectors SVs and pseudoranges
#  d(0:3)  = SV1 ECEF coordinates xyz
#  d(3)    = Pseudorange from sat 1
#  d(4:7)  = SV2 ECEF coordinates xyz
#  d(7)    = Pseudorange from sat 2
#  d(8:11) = Sat V3 ECEF coordinates xyz
#  d(11)   = Pseudorange from sat 3
#  OUTPUT: Returns an array containing the three vectors required to obtain the analytical solution: 
#  r11, r22, r33: Vectors required to obtain the exact analytical sn for position: 
#  r = SV1 + rr1 + rr2 +/- rr3

    m = d.shape[0]
  
    SV1 = d[:,0:3]
    SV2 = d[:,4:7]
    SV3 = d[:,8:11]
    p1  = d[:,3:4]
    p2  = d[:,7:8]
    p3  = d[:,11:12]

    q2  = np.sqrt(np.sum((SV2-SV1)**2,1));
    q2  = q2.reshape(m,1)
    ex  = (SV2-SV1)/q2
    q3_1=np.zeros((m,1))
    q3_2=np.zeros((m,1))
    q3_1 = inner1d(SV3-SV1,ex).reshape(m,1)

    j   = np.sqrt(np.sum((SV3 - SV1 - q3_1*ex)**2,1)).reshape(m,1)
    ey  = (SV3 - SV1 - q3_1*ex)/j
    ez  = np.cross(ex,ey)
    q3_2 = inner1d(SV3-SV1,ey).reshape(m,1)
    r3_max = 0
    r1  = (p1**2 - p2**2 + q2**2)/(2*q2)
    r2  = (p1**2 - p3**2 + q3_1**2 + q3_2**2)/(2*q3_2) - (q3_1/q3_2) * r1
    if np.all(p1**2 - r1**2 - r2**2 >= 0):
        r3  = np.sqrt((p1**2 - r1**2 - r2**2))
        if np.max((r3)) > r3_max: 
            r3_max = np.max((r3))
    else:
        r3  = np.sqrt(np.abs(p1**2 - r1**2 - r2**2))

    r11 = r1*ex
    r22 = r2*ey
    r33 = r3*ez

    return np.hstack((r11,r22,r33))


def CalculateIPP(svxyz, usrxyz):

    ## Calculation of IPP according to U.S. DEPARTMENT OF TRANSPORTATION FEDERAL AVIATION ADMINISTRATION
    ## SPECIFICATION FOR THE WIDE AREA AUGMENTATION SYSTEM (WAAS), 4.4.10.1 Pierce Point Location Determination

    HEIGHT = 350000
    Re = 6367444.657
    pi = np.pi

    m = usrxyz.shape[0]
    ##Converting positions
    svllh  = xyz2llh(svxyz)
    usrllh = xyz2llh(usrxyz)
    svenu  = xyz2enu(svxyz,usrxyz)

    ## el = elevation angle between the user and satellite
    el = np.arctan2(svenu[:,2],np.apply_along_axis(np.linalg.norm, 1, svenu[:,0:2]))
    el = (el).reshape(m,1)  # E is elevation angle expressed in radians

    ## az = azimuth angle between the user and satellite, measured
    ## clockwise positive from the true North

    temp = np.arctan(svenu[:,0]/svenu[:,1])
    az = temp.copy()
    az[ svenu[:,0]<0 ] = (temp[ svenu[:,0]<0 ] + 2*pi)
    az[ svenu[:,1]<0 ] = (temp[ svenu[:,1]<0 ] + pi)
    az = (az).reshape(m,1)  # A is azimuth angle expressed in radians

    ## IPP algorithm
    ## Known the user (u), satellite (s) and center of Earth (c) we might
    ## calculate ionospheric pierce point (i). The angle between c-u vector and
    ## c-i vector is psi. The last one may be calculated through of c-u-i
    ## triangle and sinus' theorem (a/sinA = b/sinB = c/sinC)
    psi = (pi/2 - el - np.arcsin(Re*np.cos(el)/(Re+HEIGHT)))

    ## lat_ipp is calculated by trigonometry from the original user latitude
    usrlat = (usrllh[:,0]).reshape(m,1)
    lat_ipp = np.arcsin(  np.sin(usrlat)*np.cos(psi) + np.sin(psi)*np.cos(usrlat)*np.cos(az)  )
    lat_ipp = lat_ipp.reshape(m,1)

    ## lon_ipp is calculated by trigonometry from the original user longitude
    usrlon = usrllh[:,1]
    lon_ipp = usrlon.reshape(m,1) + np.arcsin( np.sin(psi).reshape(m,1)*np.sin(az).reshape(m,1)/np.cos(lat_ipp) )
    lon_ipp = lon_ipp.reshape(m,1)

    ## Report xyz coordinates of the IPP
    ipp_xyz = llh2xyz( np.hstack((lat_ipp*180.0/pi,lon_ipp*180.0/pi,HEIGHT*np.ones((m,1))) ) ).reshape(m,3)

    F = 1/np.sqrt( 1 - (Re*np.cos(el)/(HEIGHT+Re))**2 )
    F = F.reshape(m,1)

    ## Change units to the same magnitude than the IONEX grid [degrees]
    #lat_ipp = lat_ipp*180/pi;
    #lon_ipp = lon_ipp*180/pi;

    return np.hstack((ipp_xyz, lat_ipp,lon_ipp, el, az, F ))



def gpst2utc(GPSweek,GPSsec,leapSec):

    GPSsec = GPSsec - leapSec
    idx = where(GPSsec<0)
    if GPSsec[idx]:
        GPSsec = GPSsec%24*7*3600
        GPSweek[idx] = GPSweek[idx]-1

    gpsday = GPSweek*7+GPSsec/86400
    total_days = floor(gpsday)
    if total_days.shape[0]==1:
        total_days = total_days.T

    temp = floor(((total_days+5)%1461-1)/365)
    I_temp = where(temp<0)
    if len(I_temp)>0:
        temp[I_temp] = zeros(temp[I_temp].shape)

    year = 1980+4*floor((total_days+5)/1461)+temp

    leapdays = array([0,31,60,91,121,152,182,213,244,274,305,335,366])

    noleapdays = array([0,31,59,90,120,151,181,212,243,273,304,334,365])

    leap_year = logical_not((year-1980)%4)

    I_leap = where(leap_year == 1)[0]
    I_no_leap = where(leap_year == 0)[0]

    doy = zeros((1,len(leap_year)),int)
    if len(I_leap)>0:
        doy.flat[I_leap] = (total_days[I_leap]+5)%1461+1

    if len(I_no_leap)>0:
        doy.flat[I_no_leap] = ((total_days[I_no_leap]+5)%1461-366)%365+1

    doy = doy.T

    month = zeros((1,len(doy)),int)

    for iii in range(13):
        if len(I_leap)>0:
            I_day = where(doy[I_leap]>leapdays[iii])[0]
            month.flat[I_leap[I_day]]=ones(I_day.T.shape)*iii
            I_day = None

        if len(I_no_leap)>0:
            I_day = where(doy[I_no_leap]>noleapdays[iii])[0]
            month.flat[I_no_leap[I_day]]=ones(I_day.T.shape)*iii
            I_day = None
    month = month.T

    day = zeros((1,len(doy)),int)
    if len(I_leap)>0:
        day.flat[I_leap]=doy.flat[I_leap]-leapdays[month.flat[I_leap]].T

    if len(I_no_leap)>0:
        day.flat[I_no_leap]=doy.flat[I_no_leap]-noleapdays[month.flat[I_no_leap]].T

    day = day.T

    fracday = GPSsec%86400

    hour = fix(fracday/86400.0*24)

    min = fix((fracday-hour*3600)/60.0)

    sec = fracday - hour *3600-min*60
    
    hour = hour.astype(int)
    min = min.astype(int)
    sec = sec.astype(int)
    year = year.astype(int)
    return np.column_stack((doy,year,month+1,day,hour,min,sec))


def utc2gpst(year, month, day, hour, min, sec, leapSec):
    # UTC to Julian Day 
    a   = np.fix((14-month)/12)
    y   = year + 4800 - a
    m   = month + 12*a - 3
    JDN = day + np.fix( (153*m+2)/5) + 365*y + fix(y/4) - fix(y/100) + fix(y/400) - 32045
    JD  = JDN + (hour-12)/24.0 + min/1440.0 + (sec+leapSec)/86400.0

    #   Julian Day to GPS Time      
    GPSweek   = ( JD - 2444244.5 ) / 7.0
    GPSsecond = ( GPSweek - np.fix( GPSweek ) )*86400*7*1e3
    GPSsecond = np.around(GPSsecond)/1e3
    GPSweek   = np.fix(GPSweek)

    # Day of year
    y    = year+4800-1;
    JDNo = 306 + 365*y + np.fix(y/4) - np.fix(y/100) + np.fix(y/400) - 32045   #First day of year
    doy  = JDN - JDNo

    return GPSweek, GPSsecond, doy




def ModelVTEC(X_train, iterations, model):
    global id_uxyz, id_ullh, id_sat1, id_sat4, freq_val, K,n_jobs
    tsize = 0.2
    ptype = "Error"
    features_list = ["TOD","DOY","FREQ","IPPX","IPPY","IPPZ"]
    #X_train["LOCAL_TIME"] = np.mod(4.32e4*X_train["IPP_LON"] + X_train["GPS_SVT"],86400)

    # X_train: VTEC, tod, toy, freq, ipp_x, ipp_y, ipp_z,
    # Features: tod, toy, freq, ipp_x, ipp_y, ipp_z,
    day_ind   = ((X_train["TOD"] >= 6*3600 ) & (X_train["TOD"] <= 18*3600)).as_matrix()
    #day_ind   = ((X_train["LOCAL_TIME"] >= 6*3600) & (X_train["TOD"] <= 18*3600)).as_matrix()
    
    scaler = preprocessing.StandardScaler().fit(X_train[features_list])
#    X_train = pd.DataFrame(scaler.transform(X_train[features_list]),columns=features_list)

    ## Generate models for VTEC:
    ## Day
    model_d = base.clone(model)
    X_feat = scaler.transform(X_train[features_list][ day_ind])
    y      = (X_train.as_matrix(columns=["VTEC"]).ravel())[ day_ind]
    model_d.fit(X_feat, y)
    
    ## Night
    model_n = base.clone(model)
    X_feat = scaler.transform(X_train[features_list][ ~day_ind])
    y      = (X_train.as_matrix(columns=["VTEC"]).ravel())[ ~day_ind]
    
    model_n.fit(X_feat,y)
        
    return model_d, model_n, scaler

def mean_error(a,b):
    if type(a) is pd.core.frame.DataFrame: a = a.as_matrix()
    if type(b) is pd.core.frame.DataFrame: b = b.as_matrix()
    return np.mean(np.sqrt(( a - b )**2)), np.std(np.sqrt(( a - b )**2))


def PredictVTEC(in_data, model_d, model_n, scaler):

    day_ind = ((in_data["TOD"] >= 6*3600) & (in_data["TOD"] <= 18*3600)).as_matrix()

    prediction = np.zeros((in_data.shape[0]))

    features = scaler.transform(in_data.as_matrix(columns=["TOD","DOY","FREQ","IPPX","IPPY","IPPZ"]))

    prediction[ day_ind] = model_d.predict( features[ day_ind,:] ) 
    prediction[~day_ind] = model_n.predict( features[~day_ind,:] ) 
    error,std = mean_error (prediction , in_data["VTEC"])

    return prediction, error, std


def write_feature_importance(model, fet_imp_file, size, trees):
    feat = ''
    for l in range((model.feature_importances_).shape[0]):
        feat   = feat  +','+str(model.feature_importances_[l])
    fet_imp_file.write(str(trees)+','+str(size)+feat+'\n')

    return

def list2csvstring(val_list):
    csvstring = ''
    for i in range(val_list.shape[0]): 
        if i < val_list.shape[0]-1:
            csvstring = csvstring + val_list[i]+',' 
        else:
            csvstring = csvstring + val_list[i]
    return csvstring

def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False

def convert_date(date):
    # date must be provided in format yyyymmdd
    year = date/10000
    month = (date - year*10000) / 100
    day =   (date - year*10000 - month*100)
    return date, year, month, day, int(utc2gpst(year,month,day,0,0,0,0)[2])

def cast_string_float(s):
    if is_number(s):
        return float(s)
    else:
        return 0
    
def cast_string(s):
    if is_number(s):
        return s
    else:
        return 0
    
def cast_string_int(s):
    if is_number(s):
        return int(s)
    else:
        return 0