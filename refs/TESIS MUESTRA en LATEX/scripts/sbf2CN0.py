#!/usr/bin/env python

import sys
import getopt
import os
import numpy as np
import argparse

from SSN import sbf2stf
from Plot import plotCN0
from GNSS import gpstime

__author__ = 'amuls'


# exit codes
E_SUCCESS = 0
E_FILE_NOT_EXIST = 1
E_NOT_IN_PATH = 2
E_UNKNOWN_OPTION = 3
E_TIME_PASSED = 4
E_WRONG_OPTION = 5
E_SIGNALTYPE_MISMATCH = 6
E_DIR_NOT_EXIST = 7
E_FAILURE = 99

# # get startup path
# ospath = sys.path.append(os.path.join(os.path.dirname(sys.argv[0]), "subfolder"))
# print sys.argv[0], ospath


def treatCmdOpts(argv):
    """
    Treats the command line options

    Parameters:
      argv          the options (without argv[0]

    Sets the global variables according to the CLI args
    """
    helpTxt = os.path.basename(__file__) + ' plots the Carrier-to-Noise plots for PRS data'

    # create the parser for command line arguments
    parser = argparse.ArgumentParser(description=helpTxt)
    parser.add_argument('-f','--file', help='Name of SBF file',required=True)
    parser.add_argument('-d', '--dir', help='Directory of SBF file (defaults to .)', required=False, default='.')
    parser.add_argument('-o','--overwrite', help='overwrite intermediate files (default False)', action='store_true', required=False)
    parser.add_argument('-v', '--verbose', help='displays interactive graphs and increase output verbosity (default False)', action='store_true', required=False)
    args = parser.parse_args()

    # # show values
    # print ('SBFFile: %s' % args.file)
    # print ('dir = %s' % args.dir)
    # print ('verbose: %s' % args.verbose)
    # print ('overwrite: %s' % args.overwrite)

    return args.file, args.dir, args.overwrite, args.verbose


if __name__ == "__main__":
    # treat command line options
    nameSBF, dirSBF, overwrite, verbose = treatCmdOpts(sys.argv)

    # change to the directory dirSBF if it exists
    workDir = os.getcwd()
    if dirSBF is not'.':
        workDir = os.path.normpath(os.path.join(workDir, dirSBF))

    # print ('workDir = %s' % workDir)
    if not os.path.exists(workDir):
        sys.stderr.write('Directory %s does not exists. Exiting.\n' % workDir)
        sys.exit(E_DIR_NOT_EXIST)
    else:
        os.chdir(workDir)

    # print ('curDir = %s' % os.getcwd())
    # print ('SBF = %s' % os.path.isfile(nameSBF))

    # check whether the SBF datafile exists
    if not os.path.isfile(nameSBF):
        sys.stderr.write('SBF datafile %s does not exists. Exiting.\n' % nameSBF)
        sys.exit(E_FILE_NOT_EXIST)

    # execute the conversion sbf2stf needed
    SBF2STFOPTS = ['MeasEpoch_2', 'MeasExtra_1']     # options for conversion, ORDER IMPORTANT!!
    sbf2stfConverted = sbf2stf.runSBF2STF(nameSBF, SBF2STFOPTS, overwrite, verbose)

    # print 'SBF2STFOPTS = %s' % SBF2STFOPTS
    for option in SBF2STFOPTS:
        # print 'option = %s - %d' % (option, SBF2STFOPTS.index(option))
        if option == 'MeasEpoch_2':
            # read the MeasEpoch data into a numpy array
            dataMeas = sbf2stf.readMeasEpoch(sbf2stfConverted[SBF2STFOPTS.index(option)], verbose)
        elif option == 'MeasExtra_1':
            # read the MeasExtra data into numpy array
            dataExtra = sbf2stf.readMeasExtra(sbf2stfConverted[SBF2STFOPTS.index(option)], verbose)
        else:
            print '  wrong option %s given.' % option
            sys.exit(E_WRONG_OPTION)

    # check whether the same signaltypes are on corresponsing lines after sorting
    if not sbf2stf.verifySignalTypeOrder(dataMeas['MEAS_SIGNALTYPE'], dataExtra['EXTRA_SIGNALTYPE'], dataMeas['MEAS_TOW'], verbose):
        sys.exit(E_SIGNALTYPE_MISMATCH)

    # determine current weeknumber from SBF data
    WkNr = int(dataMeas['MEAS_WNC'][0])
    print('WkNr = %d' % WkNr)

    # correct the smoothed PR Code and work with the raw PR
    dataMeas['MEAS_CODE'] = sbf2stf.removeSmoothing(dataMeas['MEAS_CODE'], dataExtra['EXTRA_SMOOTHINGCORR'], dataExtra['EXTRA_MPCORR'])
    # print 'rawPR = %s\n' % dataMeas['MEAS_CODE']

    # find list of SVIDs and SIgnalTypes observed
    SVIDs = sbf2stf.observedSatellites(dataMeas['MEAS_SVID'], verbose)
    signalTypes = sbf2stf.observedSignalTypes(dataMeas['MEAS_SIGNALTYPE'], verbose)

    # create the CN0 plots for all SVs and SignalTypes
    indexSignalType = []
    dataMeasSignalType = []

    # treat each SV
    TOWMax = -1
    TOWMin = gpstime.secsInWeek + 1
    # print('TOW min/max = %d/%d' % (TOWMin, TOWMax))

    for SVID in SVIDs:
        if verbose:
            print '  Processing SVID = %d' % SVID

        # find indices with data for this SVID
        indexSVID = sbf2stf.indicesSatellite(SVID, dataMeas['MEAS_SVID'], verbose)
        dataMeasSVID = dataMeas[indexSVID]

        # find indices that correspond to the signalTypes for this SVID
        signalTypesSVID = sbf2stf.observedSignalTypes(dataMeasSVID['MEAS_SIGNALTYPE'], verbose)

        indexSignalType = []
        dataMeasSVIDSignalType = []
        TOW = []

        for index, signalType in enumerate(signalTypesSVID):
            if verbose:
                print ('      Treating signalType = %s (index=%d)' % (signalType, index))

            indexSignalType.extend(np.array(sbf2stf.indicesSignalType(signalType, dataMeasSVID['MEAS_SIGNALTYPE'], verbose)))
            dataMeasSVIDSignalType.append((dataMeasSVID[indexSignalType[index]]))
            # print ('dataMeasSVIDSignalType[%d] =\n%s' % (index,dataMeasSVIDSignalType[index]))

            # get the TOW for this SVID and SignalType
            TOW.append(dataMeasSVIDSignalType[index]['MEAS_TOW'])
            print ('TOW[%d] = %f => %f (%d)' % (index, TOW[index][0], TOW[index][-1], np.size(TOW[index])))

            # serach for min/max TOW
            TOWMin = min(TOWMin, min(TOW[index]))
            TOWMax = max(TOWMax, max(TOW[index]))
            print('min/max = %d  %d' % (TOWMin, TOWMax))

    TOWall = np.arange(TOWMin, TOWMax+1.)
    print ('TOWall = %f => %f (%d)' % (TOWall[0], TOWall[-1], np.size(TOWall)))

    # create with missing values
    UTCall = plotCN0.TOW2UTC(WkNr, TOWall)
    print ('UTCall = %s => %s (%d)' % (UTCall[0], UTCall[-1], np.size(UTCall)))

    sys.exit(0)

    # plot curves for this SVID
    plotCN0.plotCN0(SVID, signalTypesSVID, dataMeasSVIDSignalType, UTCall, True)

    print list(set([0,1,2,3]) | set([2,3,4]))

    sys.exit(0)
