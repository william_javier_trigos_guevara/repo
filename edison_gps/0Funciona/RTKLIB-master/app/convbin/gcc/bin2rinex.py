# -*- coding: utf-8 -*-
import gpstime
import os, datetime
from os import (listdir, getcwd, system)
from os.path import (basename, splitext, abspath,
                     dirname, split, isfile)
           
def renameBin(inputfile, outpath=""):
    
    import os, datetime
    from os import (listdir, getcwd, system)
    from os.path import (basename, splitext, abspath,
                         dirname, split, isfile)

    #filename = "kh03_201810280445.ubx"
    filename, ext  = splitext(inputfile)
    station, UTCdate  = filename.split('_')
    year = int(UTCdate[:8][:4])
    month= int(UTCdate[:8][4:6])
    day  = int(UTCdate[:8][6:8])
    
    
    GPSday = gpstime.julianDay(year,month,day)
    newFilename = ("{0}0{1}{2}".format(station, str(GPSday), ext))
    
    if(outpath==""):
        outpath = getcwd()+"/BinFiles"
        cmd1 = "mkdir -p {0}".format(outpath)
        system(cmd1)
        #print (cmd1)
        
    cmd2 = "cp {0} {1}".format(inputfile, outpath+"/"+newFilename)
    #print (cmd2)
    system(cmd2)
    return outpath

def run_convbin(binpath="", rinpath=""):
    
    if(binpath==""):
        data_folder=getcwd()
    else:
        data_folder=binpath

    dir_files = [fn for fn in listdir(data_folder)]

    # COMANDO CREA CARPETA temporal
    if(binpath==""):
        binpath = getcwd()+"/BinFiles"
        cmd1 = "mkdir -p {0}".format(binpath)
        system(cmd1)
        #print (cmd1)
        
    if(rinpath==""):
        rinpath = getcwd()+"/rinex"
        cmd1 = "mkdir -p {0}".format(rinpath)
        system(cmd1)
        #print (cmd1)

    RINEX_VER=2.11
    FORMAT="nvs" #ubx
    COM1="Data adquired using Intel-Edison by William Trigos"
    COM2="Grupo de Investigación CAGE"
    COM3="Cómputo Avanzado y Gran Escala"
    COM4=" Bucaramanga, Colombia"
    OBSERV="SC3 UIS"
    AGENCY="SC3 UIS"

    cmd2 = '-hc "{0}" -hc "{1}" -hc "{2}" -hc "{3}{4}" \
    -ho "{5}/{6}"'.format(COM1, COM2, COM3,
                        datetime.datetime.now().strftime("%d-%m-%y"),
                        COM4, OBSERV, AGENCY)
    #print (cmd2)

    receivers = {
                "kh01":{"rxtype":"NV08C-CSM",
                         "rxfmt":"nvs",
                         "rxver":"v4.1 1214 fw:2.01"
                     },
                "kh02":{"rxtype":"NV08C-CSM",
                         "rxfmt":"nvs",
                         "rxver":"v4.1 1210 fw:2.01"
                      },
                "kh03":{"rxtype":"Neo-M8N",
                         "rxfmt":"ubx",
                         "rxver":"0-10-fw:2.01"
                     }
        }

    #moviendo y renombrando los binarios
    for filename in listdir(data_folder):
        if filename.endswith((".ubx", "nvs")):
            data_folder = renameBin(filename, binpath)
    
    print (data_folder)
    for filename in listdir(data_folder):
        if filename.endswith((".ubx", "nvs")):
            
            base = splitext(filename)[0].split('_')[0]
            base = base[:4]
            #print (base, filename)
            if(receivers.get(base,"receptor no existe")!=""):
                rx = receivers.get(base,"receptor no existe")
                #print (rx['rxtype'], rx['rxver'])
                #print (cmd2, base, rx)
                cmd3 = '{0} -hr "{1}/{2}/{3}"'.format(cmd2, base, rx['rxtype'], rx['rxver'])
                
                cmd = './convbin {0} -d {1} -r {2} {3}'.format(data_folder+"/"+filename, 
                                                          rinpath, 
                                                          rx["rxfmt"],
                                                          cmd3)
        
                #http://www.rtklib.com/prog/manual_2.4.2.pdf#page=99
                cmdargs = "-od -os -oi -ot -ol"
                cmd = "{0} {1}".format(cmd, cmdargs)
                print (cmd)
                system(cmd)

#===== Main ========================================= 
if __name__ == "__main__": 
    pass 
    run_convbin()