%  Chapter 5: Porting OpenSource GPS

\section{Introduction to OpenSource GPS}

With only a modified GPS receiver mounted on an ISA prototyping card (Figure~\ref{fig:osgpsreceiver}), a 486-based PC, and the Borland C compiler, Dr. Clifford Kelley launched the open source GPS movement by publishing his results in 2002 \cite{kelley} and then releasing his code under the GPL. Since then several projects (including GPL-GPS) have begun from his original ``OpenSource GPS'' (OSGPS) project.

\begin{figure}[h] % place the figure right here
    \begin{center}
        \includegraphics[keepaspectratio,width=0.5\linewidth]{pictures/osgps_hacked_receiver.eps}
        \caption{The original OSGPS ISA PC card (reproduced from \cite{kelley}).}
        \label{fig:osgpsreceiver}
    \end{center}
\end{figure}

OSGPS uses a foreground/background (interrupt/mainline) architecture. The foreground task is driven by a 500~$\mu$s timer interrupt on the host PC. All time dependent functions --- accumulator dump processing, satellite navigation message decoding, and measurement processing --- are called directly from a single interrupt. Navigation algorithms and display code run as the mainline task with communication between tasks accomplished using global variables (Figure~\ref{fig:osgpsflowchart}).

\begin{figure}[h]
    \begin{center}
        \includegraphics[keepaspectratio,width=0.5\linewidth]{pictures/osgps_flowchart.eps}
        \caption{OpenSource GPS software flowchart (reproduced from \cite{kelley}).}
        \label{fig:osgpsflowchart}
    \end{center}
\end{figure}

OSGPS is a ground breaking project, and as such, has several important issues:

\begin{itemize}
    \item OSGPS is not divided into modules by GPS task, making it difficult to follow its control and data flow (e.g., compare Figure~\ref{fig:osgpsflowchart} with Figure~\ref{fig:generic-data-flow}).
    \item The mainline code could be better prioritized and scheduled if it were broken into threads.
    \item OSGPS has very few comments to explain the algorithms used, making it very hard to understand why algorithms and control flow was implemented in certain ways.
    \item While the algorithms used in OSGPS are time honored, they are not necessarily the most efficient or the best for any particular application. Some way to break out the algorithms into modules for easy swapping of algorithms would be useful.
    \item OSGPS relies on the legacy and closed source DOS operating system, and the no longer supported Borland 4.5 C/C++ compiler.
\end{itemize}

\section{ARMGPS: Porting OSGPS to the GP4020}

In 2004, inspired by GPL-GPS, Takuji Ebinuma started his own project, ARMGPS \cite{ebinuma}. In a few months, he ported a stripped down version of OSGPS v1.15 to the SigTec MG5001 receiver using JaysOS \cite{jaysos}, a very simple operating system for ARM7TDMI processors. Ebinuma stripped out all carrier phase, almanac, and file handling features in the process and kept the same foreground/background task model. He attempted to break the background task into threads, but the implementation was no more effective than OSGPS' mainline task because the threads were called by timer delay rather then being event driven.

While ARMGPS does work, it is unstable, prone to dropping accumulator interrupts and to bouts of unpredictable behavior. Ebinuma attributes the instability of ARMGPS to the simple nature of JaysOS which forced a primitive thread architecture with no interprocess communication (IPC). ARMGPS also does not fully implement the changes needed to switch from the OSGPS hardware (486 PC with a GP2021 correlator) to the GP4020-based receiver. Finally, JaysOS lacks any reasonable debugging features, so tuning and debugging ARMGPS is very difficult. After struggling with the code for a few months, Ebinuma gave up on ARMGPS and officially closed the project.

\section{Transforming ARMGPS to GPL-GPS}

In late 2004, I revived the GPL-GPS project and began fine tuning the GP4020 eCos port. In early 2005 I took Ebinuma's ARMGPS code, and using Kelley's most recent OSGPS code (v1.17) as a reference, ported it to eCos. GPL-GPS attained ``first fix'' on May 2nd, 2005, and is now publicly available on the project's web site (\url{http://gps.psas.pdx.edu/}). Although never truly finished because of its development system nature, GPL-GPS now exists as a code-complete development environment for creating extended GPS applications with commercial GPS receivers.

Roughly counted by lines, OSGPS is 6,300 lines of code, ARMGPS is 5,000 lines of code, and GPL-GPS is currently at 6,000 lines. Of these 6,000 lines, an estimated more than 80~\% have been restructured, refactored or replaced in the transition from ARMGPS and OSGPS to GPL-GPS. This section does not attempt to describe all the detailed code changes that have occurred. Instead, I will attempt to list the broad categories of improvements made to the legacy code base.

\subsection{GPL-GPS Computing Environment}

The changes made from OSGPS/ARMGPS to GPL-GPS must be understood within the context of the GP4020 computing environment. Typical GP4020-based receiver boards have:

\begin{itemize}
    \item A 32 bit integer-only ARM7TDMI running at 20~MHz
    \item 8~kB of fast 32~bit wide internal SRAM
    \item 128--512~kB of 16 bit external zero-wait-state SRAM
    \item 128--512~kB of 16 bit external one-to-three wait state flash
    \item 0--512~kB of serial EEPROM
\end{itemize}

Typical throughput for these boards is roughly 5~MIPS from flash memory, 10~MIPS from external SRAM, and 20~MIPS from internal SRAM. The ARM7\-TDMI has a single cycle 32 x 32-bit integer multiply, very sophisticated relative addressing schemes, and a very useful branch-per-instruction capability. Note the ARM7TDMI has a 32-bit wide RISC instruction set and a 16-bit wide alternate instruction set called ``thumb'' mode. Although thumb mode seems optimal for the 16-bit external memory (both SRAM and flash), it is not well supported by eCos. If future versions of eCos support Thumb mode, then it is likely GPL-GPS will begin using it.

\subsection{Optimizing for GPL-GPS}

The GPL-GPS code base attempts to implement the following performance improvements:

\begin{description}
    \item[GPS task-based threads] Possibly the most important optimization has been repartitioning the OSGPS/ARMGPS code to reflect the various GPS receiver tasks (see Figure \ref{fig:generic-data-flow}). Each task has been modularized into its own code and header file, to clarify and prioritize the tasks. Each task usually has one thread and one data structure associated with it, making it a more modular interface for task additions and/or inserting external communication links (e.g., an external flight computer on a UAV). See Figure~\ref{fig:gpl-gps-flowchart} for the repartitioned task and data flow.
    \item[Event driven processing] Instead of timer delays, GPL-GPS uses event-driven threads and IPC, including flags, semaphores and mutexes. This mode of operation decreases both response time and the amount of code scheduled to run at any one time.
    \item[Streamlined critical tasks] Tasks that didn't need to be in higher priority, higher rate threads (or interrupts) have been moved to lower priority, lower rate threads. For example, channel allocation and navigation message processing were moved from the tracking loops to their own low priority threads.
    \item[Prioritized threads] Instead of giving all the tasks the same priority, the threads are partitioned and carefully prioritized in order to let time critical tasks execute before lower priority tasks.
    \item[GP4020-friendly variable types] Since the correlator uses 16~bit integers, much of the integer processing can be moved from 32 bit to 16 bit words to speed up access to the external 16 bit SRAM and to save space in the small but fast internal 32 bit SRAM. Because of the ARM7TDMI's single-cycle 32, 16 and 8~bit memory access abilities this should not slow down processing.
    \item[Fixed point arithmetic] GPL-GPS is slowly moving from double precision floating point to 32 bit fixed point integers in critical code (e.g., the tracking loops). Note that in threads which run in as a low priority, infrequent task (e.g., the position code), floating point may be left in place for coding ease and clarity.
\end{description}

\begin{figure}[h]
\begin{center}
    \includegraphics[keepaspectratio,width=0.9\linewidth]{pictures/gpl-gps_data_flow.eps}
    \caption{GPL-GPS software flowchart.}
    \label{fig:gpl-gps-flowchart}
\end{center}
\end{figure}

\subsection{Improving on OSGPS Algorithms}

GPL-GPS tries to refactor OSGPS algorithms to take a more efficient approach while improving code clarity and readability. Unfortunately these are sometimes mutually exclusive. So far the improvements include:

\begin{itemize}
    \item Streamlined navigation data decoding, and refactored the message finding algorithm to operate on subframes rather than frames. This allows data to be grabbed every subframe period (6~s) rather than once each frame (30~s). The shorter message chunk also improves performance in noisy environments.
    \item Navigation message parity check now uses an improved and clearer algorithm.
    \item Changed hardware register access from a function-based peek-poke interface to memory-mapped structures.
    \item The position calculation has been re-worked to reduce the computational load and to clarify the computation that is being done.
    \item Timing and data flow have been improved by eliminating redundant variables and redundant code, and instituting a more consistent naming convention and coding style.
    \item Added an optimal cold start algorithm based on satellite orbital slot occupancy.
    \item Removed many mysterious constants and replaced them with well defined and documented physical or hardware defined constants. The mysterious constants are still there, but now are computed at compile time with explicit algorithms.
    \item Rewrote from scratch numerous helper routines improving efficiency, correctness and clarity.
    \item Commented and lightly refactored the tracking code.
\end{itemize}

\section{GPL-GPS Current Status}

GPL-GPS became an operational GPS development environment on May 2nd, 2005 with its first successful position fix (see Appendix \ref{app:initialresults} for initial positioning results). While the positioning code is currently crude --- there is no atmospheric correction, carrier phase positioning, or position filtering --- rapid progress can be made now that the software and development infrastructure have been completed.

\section{GPL-GPS Current Performance}

Figure~\ref{fig:scope-zoom} shows the timing characteristics of two time-critical GPL-GPS tasks: the tracking loops, which run every 505~$\mu$s after an accumulator interrupt, and the measurement thread, which runs every 99.9999~ms after a measurement interrupt. The mean time spent in the tracking loops is 355~$\mu$s, or about 70\% of the processor's time. This underscores the need to optimize the tracking loops by moving them into the GP4020's fast internal SRAM and move to fixed point math. The measurement thread takes 380~$\mu$s, but note that it is interrupted and suspended while the tracking loop runs.

\begin{figure}[h]
\begin{center}
    \includegraphics[keepaspectratio,width=0.9\linewidth]{pictures/tracking.eps}
    \caption{GPL-GPS timing: tracking loops and measurement thread.}
    \label{fig:scope-zoom}
\end{center}
\end{figure}

Figure~\ref{fig:scope-wide} shows the tracking loops, measurement thread, and the position thread. In this figure, the position thread is only processing 3 satellites and thus is taking only 70~ms. The position thread takes between 0.3~s (for four satellites) and 0.6~s (for seven satellites) when calculating position.

\begin{figure}[h]
\begin{center}
    \includegraphics[keepaspectratio,width=0.9\linewidth]{pictures/positioning.eps}
    \caption{GPL-GPS timing: tracking loops, measurement thread, and position thread.}
    \label{fig:scope-wide}
\end{center}
\end{figure}

