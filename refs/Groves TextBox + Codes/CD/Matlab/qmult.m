function q = qmult(q1, q2)
% --------------------------------------------------
% Quaternion multiplication
%
% Author:   Harald N�kland
% Date:     June 2011
% --------------------------------------------------
if any(~isreal(q1(:)))
    error('First input elements are not real.');
end
if any(size(q1)~=[4,1])
    error('First input dimension is not 4-by-1.');
end
if any(~isreal(q2(:)))
    error('Second input elements are not real.');
end
if any(size(q2)~=[4,1])
    error('Second input dimension is not 4-by-1.');
end

eta1 = q1(1);
eps1 = q1(2:4);
eta2 = q2(1);
eps2 = q2(2:4);

eta = eta1*eta2 - eps1'*eps2;
eps = eta1*eps2 + eta2*eps1 + cross(eps1,eps2);

q = [eta; eps];
