# -*- coding: utf-8 -*-
import gps_utils
import gps_library
import LS_Standalone
import LS_Coop

def coop_ls_code_phase_multirover(gpstime, receivers, d_est, dat,
                                  Obser="C1",max_iters=50, max_error=1,
                                  apply_earth_rotation=True,
                                  iono_model_master=None,
                                  iono_model_rover =None,
                                  k_a=0.6, k_b=0.009, wls=False):
    
    # initialize vector of variables
    n_rx = len(receivers)  #numero de receptores en cooperativo
    ri = np.hstack((np.zeros(4), np.ones(4*(n_rx-1))))

    svs = dat['prns_pos_master']
    n_sats = len(svs)
    n_vars = 4
    #n, m = 2*n_sats+1, 2*n_vars
    n, m = (n_rx*n_sats + n_rx-1), n_rx*n_vars

    if apply_earth_rotation:

        svs = dat['prns_pos_master']
        prs = dat[Obser+'_master']
        #print np.shape(svs), np.shape(prs)
        svs = apply_earth_rotation_to_svs_position(svs, prs)

    if len(svs)==0 or len(dat[Obser+'_master'])==0:
        return np.zeros(4*n_rx),None, None, None

    delta,i = 1,0
    iono_delays = []

    A = np.zeros((n, m))
    prs = np.zeros(n)
    pranges = np.zeros(n)
    
    #P = np.ones(n)/(max_error)**2
    
    for i in range(1, max_iters):

        rhos_a, pranges_a, io1 = predict_pseudoranges_ionosfe(ri[:4], svs,
                                                      dat['prns_clockbias_master'],
                                                      dat,
                                                      apply_iono_model=iono_model_master)
        iono_delays.append(io1)

        Aa = np.hstack(((ri[:3] - svs)/rhos_a[:,None], np.ones((n_sats, 1))))

        # component asociated to position of local receiver
        A[:n_sats, :n_vars] = Aa

        prs[:n_sats]     = dat[Obser+'_master'] #np.hstack((dat[Obser+'_master'])).reshape(-1,1)
        pranges[:n_sats] = pranges_a - io1

        c = -1#0
        for k, station in enumerate(receivers):
            if k != 0:

                """ MODELO DISTANCIA.
                OJO INVERTIR ESTO PARA CAMBIAR LA DIRECCION DEL VECTOR DISTANCIAS
                """
                d_model = np.linalg.norm(ri[4*k:4*(k+1)-1] - ri[:3])
                Ad = np.hstack(((ri[4*k:4*(k+1)-1] - ri[:3])/d_model))#[:,None]))

                """ MODELO PSEUDORANGES.
                """
                rhos_b, pranges_b, io2 = predict_pseudoranges_ionosfe(ri[4*k:4*(k+1)], svs,
                                                              dat['prns_clockbias'+"_"+station],
                                                              dat,
                                                              apply_iono_model=iono_model_rover)
                """ prs y pranges construction."""
                prs2 = dat[Obser+"_"+station]

                ini = k*n_sats + k-1
                fin = (k+1)*n_sats + k

                pranges_b -= io2
                iono_delays.append(io2)

                prs[ini:fin]     = np.hstack((prs2, np.array(d_est[k-1])))
                pranges[ini:fin] = np.hstack((pranges_b, d_model))

                """ A Matrix construction.
                """
                # get cosine director from each receiver to sats
                Ab = np.hstack(((ri[4*k:4*(k+1)-1]- svs)/rhos_b[:,None], np.ones((n_sats, 1))))

                # component asociated to position of remote receiver
                A[ini:fin, k*n_vars:(k+1)*n_vars] = np.vstack((Ab, np.hstack((Ad[:3], np.zeros(1)))))
                c += 1 # para avanzar la posicion de Ad en cada rover
                #print A

        """ LS computation.
        """
        b     = prs - pranges
        
        if wls:
            W = np.eye(len(b))/np.var(b)**2
            delta =  np.linalg.pinv((A.T.dot(W)).dot(A)).dot(A.T.dot(W)).dot(b)
        else:
            delta =  np.linalg.pinv(A.T.dot(A)).dot(A.T).dot(b)

        ri    += delta#.flatten('F')
        i     += 1

        #if i%5 == 0:
        #    print i, norm(delta), ri

        if norm(delta)<1e-3:
            break

    return ri, A, b, delta, iono_delays

def wls(dat, svs, clocks, prs, snr, wls=False, apply_earth_rotation=True, iono_model="wallas",sigma=10, max_iters=10):
    
    c = 299792458.0
    f1 = 1575420000.0
    lamda = c/f1
    n, m = np.shape(svs)#np.shape(prs)
    
    if apply_earth_rotation:
        svs = apply_earth_rotation_to_svs_position(svs, prs)
        
    x0 = np.zeros(m+1)
    P = np.eye(n)/sigma**2

    delta,i = 1,0
    w = []
    while (norm(delta)>1e-10 and i<max_iters):
        rhos, pranges, io1 = predict_pseudoranges_ionosfe(x0, svs[:,:3], clocks, dat, apply_iono_model=iono_model)
        b = prs - pranges
        ones = np.ones((len(b), 1))
        zero = np.zeros((len(b), len(b)))
        A = np.hstack(((x0[:3]-svs[:,:3])/rhos[:,None], ones))
        
            # agregando modelo iono
        
        b -= io1
        
        if (wls):
            e, az = get_sat_elv_az(svs[:,:3], x0[:3])
            P = np.eye(n)*np.exp(-e/(snr))
            #P = np.eye(n)/np.var(b)**2
            delta =  np.linalg.pinv((A.T.dot(P)).dot(A)).dot(A.T.dot(P)).dot(b)
            #delta =  np.linalg.pinv((A.T.dot(P)).dot(A)).dot(A.T.dot(P)).dot(b)
        else:
            delta =  np.linalg.pinv(A.T.dot(A)).dot(A.T).dot(b)
            
        x0 += delta
        i += 1
        #print "+"*30
        #print i, delta, np.linalg.norm(x0[:3]-rec_pos)
    
    return x0,A,b,delta,io1 



def multirover_test(data_common, pareja, realpos,
                    Obser = "C1", dist = 1.,
                    iono_model_master = None,
                    iono_model_rover = None,
                    k_a=0.6, k_b=0.009,
                    decimal_places=3, wls_flag = False):

    c = 0
    r = []
    station_list = []
    coop_pos, coop_err = [], []
    stdalone_pos, stdalone_err = [], []
    dc_est, d_real  = [], []

    dat = data_common#.iloc[0]
    gps_sow = data_common.gps_sow


    #print pareja
    #print data_common.index

    if len(dat['prns'])>=4:

        # Metodos desarrollados
        ## Compute standalone positions
        for k, station in enumerate(pareja):
            if k == 0:

                #print np.shape(dat.loc['prns_pos_master']), np.shape(dat[Obser+'_master']), np.shape(dat['prns_clockbias_master'])

                #print "d", np.shape(dat.loc['prns_clockbias_master'])
                posstd,_,_,_ = compute_least_squares_position(data_common.loc['prns_pos_master'],
                                                              data_common.loc['prns_clockbias_master'],
                                                              data_common.loc[Obser+'_master'])
                err = np.linalg.norm(posstd[:3] - realpos[k])

                stdalone_pos.append(posstd[:3])
                stdalone_err.append(err)

            else:
                #print "e", np.shape(dat.loc['prns_clockbias_'+station])
                posstd,_,_,_ = compute_least_squares_position(data_common.loc['prns_pos_'+station],
                                                              data_common.loc['prns_clockbias_'+station],
                                                              data_common.loc[Obser+"_"+station])
                err = np.linalg.norm(posstd[:3] - realpos[k])

                stdalone_pos.append(posstd[:3])
                stdalone_err.append(err)

        # compute distances between receivers
        for k, station in enumerate(pareja):
            if k != 0:
                #print k, stdalone_pos[0], stdalone_pos[k]
                dc, teta, dc_vec = d_teta_calc(stdalone_pos[0], stdalone_pos[k])
                dr, teta, dc_vec = d_teta_calc(realpos[0], realpos[k])
                dc_est.append(dc)
                d_real.append(dr)

        start_time = time.time()

        if is_number(dist):
            noise = np.random.normal(-1, 1, len(d_real))
            dist = np.array(d_real) + (dist/100)*noise
            """
            ri,_,_,_ = coop_ls_solver_multirover(data_common["gps_sow"], pareja, dist, data_common,
                                             iono_model_master=iono_model_master,
                                             iono_model_rover =iono_model_rover)
            """
            ri,_,_,_, ionos = coop_ls_code_phase_multirover(data_common["gps_sow"], pareja, dist, data_common,
                                                 iono_model_master=iono_model_master,
                                                 iono_model_rover =iono_model_rover, k_a=k_a, k_b=k_b, wls=wls_flag)
        elif dist == None:
            dist = dc_est
            """
            ri,_,_,_ = coop_ls_solver_multirover(data_common["gps_sow"], pareja, dist, data_common,
                                                 iono_model_master=iono_model_master,
                                                 iono_model_rover =iono_model_rover)
            """
            ri,_,_,_, ionos = coop_ls_code_phase_multirover(data_common["gps_sow"], pareja, dist, data_common,
                                                 iono_model_master=iono_model_master,
                                                 iono_model_rover =iono_model_rover, k_a=k_a, k_b=k_b, wls=wls_flag)
        coop_t = time.time() - start_time

        c = 0

        #print iono_model_rover, iono_model_master
        station_list, iono_model = [], []
        for k, (station, iono) in enumerate(zip(pareja, ionos)):
            station_list.append(station)
            if k == 0:
                coop_pos.append(ri[4*k:4*(k+1)-1])
                coop_err.append(np.linalg.norm(ri[4*k:4*(k+1)-1] - realpos[k]))
                iono_model.append(iono)
            else:
                ini = 4*k #+ c
                fin = 4*(k+1)-1 # + c
                #print
                #print k, ini, fin, ri, np.shape(ri[ini:fin]), np.shape(real_pos[k])
                coop_pos.append(ri[ini:fin])
                coop_err.append(np.linalg.norm(ri[ini:fin] - realpos[k]))
                c += 1
                iono_model.append(iono)

        #print dc_est
        #print "st", stdalone_err
        #print "cp", coop_err

        r = [gps_sow,
                    np.round(realpos, decimal_places),
                    station_list,
                    iono_model,
                    np.round(dist, decimal_places),
                    np.round(d_real, decimal_places),
                    np.round(dc_est, decimal_places),
                    np.round(stdalone_pos, decimal_places),
                    np.round(stdalone_err, decimal_places),
                    np.round(coop_pos, decimal_places),
                    np.round(coop_err, decimal_places),
                    np.round(coop_t, decimal_places),
                    np.round(ri, decimal_places),
                    data_common]
    else:
        return

    names = ["gps_sow" ,"real_pos", "station_list", "iono_model", "d", "dreal", "d_calc",
             "stdlone_pos", "stdlone_err", "coop_pos", "coop_err",
             "coop_time[s]", "rc_sol", "data_common"]

    return r, names #pd.DataFrame(r, columns=names)# Results


def simulation_multirover(Obser, error_max, parejas_kmz, RinexDB, dia1, dian, ano, distances=None,
                         Simu_Folder=None,
                         res_plot=1000,
                         iono_model_master=None,
                         iono_model_rover=None,
                         silent_exec=True,
                         wls = False,
                         k_a=0.6, k_b=0.009):

    def clean_folder(abspath_Folder, silent_mode=True):
            cmd = "rm -rf "+abspath_Folder+"/*"
            if silent_mode == False:
                print ("\n"+cmd)
            system(cmd)


    import re
    # http://stackoverflow.com/questions/2669059/how-to-sort-alpha-numeric-set-in-python
    def sorted_nicely( l ):
        """ Sort the given iterable in the way that humans expect."""
        convert = lambda text: int(text) if text.isdigit() else text
        alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ]
        return sorted(l, key = alphanum_key)

    def extract_rinex_files(RinexDB, k, RX, Simu_Folder, required_files=['n', 'o']):

        Temp = {}
        matches = [x for x in RinexDB.keys() if RX.lower() == x]
        #print matches

        if len(matches)> 0:

            RX = matches[0]
            for Obstype in RinexDB[RX].keys():
                station, ObsType = splitext(RinexDB[RX][Obstype])
                path = RinexDB[RX][Obstype]
                dirname, zipname = split(path)
                fname, ext = splitext(zipname)

                #print fname, ext, path
                cmd = "cp "+path+" "+Simu_Folder+"/"+zipname
                system(cmd)
                cmd2 = "gunzip "+Simu_Folder+"/"+zipname

		if zipname.endswith('zip'):
		     extract_from_zip(zipname, dirname, Simu_Folder)
		else:
		     system(cmd2)
		#print fname
		#break
                if isfile(Simu_Folder+"/"+fname):
                    if k==0:
                        #Temp.setdefault("master_"+RX,{})[Obstype[0]] = Simu_Folder+"/"+fname
                        #Temp["master_"+RX].update({Obstype:path})
                        Temp.setdefault(RX,{})[Obstype[0]] = Simu_Folder+"/"+fname
                        Temp[RX].update({Obstype:path})
                    else:
                        #Temp.setdefault("rover"+str(k)+"_"+RX,{})[Obstype[0]] = Simu_Folder+"/"+fname
                        #Temp["rover"+str(k)+"_"+RX].update({Obstype:path})
                        Temp.setdefault(RX,{})[Obstype[0]] = Simu_Folder+"/"+fname
                        Temp[RX].update({Obstype:path})
                else:
                    if k==0:
                        #Temp.setdefault("master_"+RX,{})[Obstype[0]] = ""
                        #Temp["master_"+RX].update({Obstype:""})
                        Temp.setdefault(RX,{})[Obstype[0]] = ""
                        Temp[RX].update({Obstype:""})
                    else:
                        #Temp.setdefault("rover"+str(k)+"_"+RX,{})[Obstype[0]] = ""
                        #Temp["rover"+str(k)+"_"+RX].update({Obstype:""})
                        Temp.setdefault(RX,{})[Obstype[0]] = ""
                        Temp[RX].update({Obstype:""})

            station = Temp.keys()[0]
            files = [f for f in required_files if f in Temp[station].keys()]

            if (len(files) >= len(required_files)):
                return Temp
            else:
                return {}
        else:
            return {}

    if Simu_Folder==None:
        Simu_Folder = getcwd()+"/Temp_Simul_data"
        cmd1 = "mkdir -p " + Simu_Folder    # COMANDO CREA CARPETA temporal
        system(cmd1)

        #print parejas_kmz
        #print "\t[Station Pair]\t\t[GPS day]\t[year]\t\t[Process status]\n"
        print "[GPS day]   [year]\t[Station Pair]\t\t[Process status]\n"

        Results = {}
        dat_com = {}

        for day in range(int(dia1), int(dian)+1):
            print " %s\t%s" %(str(process_day2(day)[:3]), ano),
            tag = 0

            for k, agrupacion in parejas_kmz.iteritems():

                if tag == 0:
                    print "\t   %s\t"%(str(agrupacion)),
                    tag = 1
                else:
                    print " \t\t %s\t"%(str(agrupacion)),

                agrupacion = [x.lower() for x in agrupacion]
                #print agrupacion
                
                Obsfiles = {}
                for k, station in enumerate(agrupacion):

                    #fname = str(station)+str(process_day2(day))+"."+ano[-2:]
                    fname = str(station[:4])+str(process_day2(day))+"."+ano[-2:]

                    fname = fname.lower()
                    Obsfiles.update(extract_rinex_files(RinexDB, k, fname, Simu_Folder))
                    #print day, agrupacion, station, fname

                    if len([Obsfiles[fname][x] for x in ['n', 'o']]) < 2:
                        error = True
                        #print Obsfiles[fname].keys()
                        if silent_exec==False:
                            print "WARNING! Aborting execution: missing rinex files for %s \n"%(str(agrupacion))

                        break
                    else:
                        error = False
    
                #agrupacion = Obsfiles.keys()
                agrupacion = [y for x in agrupacion for y in Obsfiles.keys() if y[:4]==x]
                #print agrupacion
                
                ## LOS DATOS DE TODA LA AGRUPACION
                data = []
                realpos = []
                for k, station in enumerate(agrupacion):

                    p = station
                    data1, b1, rec_pos1 = rinex_to_dataframe_ionosfe(Obsfiles[p]['o'],
                                                                  Obsfiles[p]['n'],
                                                                  silent_mode=silent_exec)
                    data.append(data1)
                    # almacenando posicion real para calcular error dentro simple_test
                    realpos.append(rec_pos1)

                    
                ## RECORRIENDO LAS EPOCAS DE LOS 
                print " Running... ",
                results = []
                for epoc in range(len(data[0])):

                    if epoc % res_plot == 0:
                        csats = data[0].iloc[epoc]['prns']
                        for k, (d, station) in enumerate(zip(data, agrupacion)): # enumerate(zip(data[1:], pareja[1:])):
                            csats = list( set(csats) & set(d.iloc[epoc]['prns']))

                        csats = sorted_nicely(csats)
                        d_common = pd.DataFrame()
                        #print "c", len(csats)#, csats
                        #print ("\t\t Rinex Observation: "), epoc,
                        gps_time = data[0].iloc[epoc].gps_sow

                        # EXTRAYENDO LOS DATOS EN COMÚN PARA LA EPOCA
                        for k, (d, station) in enumerate(zip(data, agrupacion)):

                            #print gps_time,  d.iloc[epoc].gps_sow
                            if gps_time == d.iloc[epoc].gps_sow:
                                #print station
                                error = False
                                d_common = get_data(d_common, d, epoc, csats, station)

                            else:
                                error = True
                                if silent_exec==False:
                                    print " no existe epoca común para %s la agrupación %s" %(station, agrupacion)
                                
                                #print station, error
                                break
                                
                        if error:
                            break
                        else:
                            d_common = d_common.iloc[0]
                                
                            r,n = multirover_test(d_common, agrupacion, realpos,
                                                  iono_model_master=ion_mas,
                                                  iono_model_rover=ion_rov, k_a=k_a, k_b=k_b, wls_flag = wls)

                            #print ""
                            #for x,y in zip(r[:4], n[:4]):
                            #    print y, np.shape(x)
                            results.append(r)


                print "Finished!!!"
                dat_com.setdefault(day, {})[str(agrupacion)] = d_common
                Results.setdefault(day, {})[str(agrupacion)] = pd.DataFrame(results, columns=n)
                #print Results.keys()

                tag += 1

    else:
        #print "no existe master"
        print "no existe primer estacion"

    print ("\n\tSimulation Finished!")
    print ("\t "+"--"*10)
    #print ("\n")
    #clean_folder(Simu_Folder, silent_mode=False)
    #clean_folder(Simu_Folder)

    return Results, dat_com



def create_multi(df, index=['day'], cols=["mean_std_e", "d", "dr", "dc"]):
    #multi=df.set_index(['stations', 'day', 'scenario'], inplace=False)
    multi=df.set_index(index, inplace=False).sort_index()
    #multi.rename(columns={"index_gen":"index_gen"+"(mean,std)"}, inplace=True)
    return multi[cols]


def gen_df_results3(df_results, dict_results, use_case,
                   xvar="gps_sow",
                   columns = ["stdlone_err", "coop_err", "dreal", "d_calc", "d", "gps_sow", "iono_model"],
                   decimal_places=3):

    df2 = pd.DataFrame()

    for day, data in dict_results.iteritems():

        gps_time, days, dist = [], [], []
        index_improvement, diffs, stations, scheme = [], [], [], []
        mean_station, improve_station = [], []
        iono_delays = []
        
        for pareja in data.keys():
            #print pareja
            header = [x[:4] for x in pareja.translate(None, "[']").replace(" ", "").split(",")]
            header = ','.join(header)
            days.append(day)

            df = dict_results[day][pareja][columns] 
            #display(df)

            diff = [np.array(i)-np.array(j) for i,j in zip(df['stdlone_err'], df['coop_err'])]
            #print diff, np.mean(diff, axis=0)
            index_improvement.append((np.mean(diff), np.std(diff)))

            # mejora para cada estacion
            mean_station.append((np.mean(diff, axis=0), np.std(diff, axis=0)))

            diffs.append(diff)
            stations.append(str(header))
            scheme.append(str(use_case))
            gps_time.append([x for x in df['gps_sow']])

            dist.append((df['d'].mean(), df['dreal'].mean(), df['d_calc'].mean()))

        #print np.shape(gps_time), np.shape(diffs)
        #print diffs
        #print "m", mean_station
        #print "gm", [np.mean(x) for x in np.mean(mean_station, axis=1)]#,mean_station
        #print mean_station, np.mean(mean_station, axis=1)
        
        Index = [(np.mean(x), np.std(x)) for x in np.mean(mean_station, axis=1)]
        
        df2 = df2.assign(gps_day = days)
        df2 = df2.assign(scenario = scheme)
        df2 = df2.assign(stations = stations)
        df2 = df2.assign(improve_by_station = list(np.mean(mean_station, axis=1)))
        df2['improve_by_station'] = df2["improve_by_station"].apply(lambda tupla: tuple(''.join(format(f, '.4f')) for f in tupla))
        
        #df2 = df2.assign(index_gen = mean_std)
        df2 = df2.assign(index_gen = Index)
        df2['index_gen'] = df2["index_gen"].apply(lambda tupla: tuple(''.join(format(f, '.4f')) for f in tupla))
        df2 = df2.assign(mean_e_by_station = mean_station)
        df2 = df2.assign(gps_time = gps_time)
        df2 = df2.assign(d_dreal_dcalc = dist)
        df2 = df2.assign(iono_model = df['iono_model'])
        df2 = df2.assign(diff_e_std_e_coop = diffs)

        df_results = pd.concat([df_results, df2], axis=0)
        #display(df_results)
    
    return df_results


def plot_from_dict(dict_results,
                   xvar = "gps_sow",
                   tags = "station_list",
                   scenario="",
                   vars2plot = ["stdlone_err", "coop_err"],
                   axis_labels = [""]*3,
                   figsize=(20,5),
                   plot_inline = True,
                   plot_in_doc = False):

    def plotter(ax, df, xvar, tags, vars2plot, axis_labels, figsize):

        class Dictlist(dict):
            def __setitem__(self, key, value):
                try:
                    self[key]
                except KeyError:
                    super(Dictlist, self).__setitem__(key, [])
                self[key].append(value)

        #fig = plt.figure(figsize=figsize)
        #ax = fig.add_subplot(111)

        d = Dictlist(dict())
        for i, station in enumerate(df.iloc[0][tags]):

            if station not in d.keys():
                for var in vars2plot:
                    d.setdefault(station, {})[var] = []
                    for epoc in range(len(df)):
                        d[station][var].append((df.iloc[epoc][xvar], df.iloc[epoc][var][i]))


        station_lst = []
        for station, vars2plot in d.iteritems():
            #print station
            station_lst.append(station)
            for k, v in vars2plot.iteritems():
                x, y =  zip(*v)
                #ax.plot(x, y, label=station[7:]+"_"+k)
                ax.plot(x, y, label=station[:7]+"_"+k)

        #station_lst = [x.split("_")[1][:4] for x in station_lst]
        title = axis_labels[2] + " escenario " + str(scenario)

        plt.legend()
        ax.set_xlabel(axis_labels[0])
        ax.set_ylabel(axis_labels[1])
        ax.set_title(title)

        # generated for export to  latex document
        figlabel = str(scenario + '-'.join(station_lst))
        figcaption = "Resultados para " + str('-'.join(station_lst) + " escenario " + scenario)

        # Generate latex code associated to figure plot
        #makeplot_latex(fig, figlabel, figcaption, bShowInline = plot_inline)
        filename = figlabel #+'.png'

        #fig.savefig(filename)
        #plt.close()

        return fig, filename, figlabel, figcaption

    for day, results in dict_results.iteritems():
        for pareja, data in results.iteritems():
            
            #display(day)
            df = dict_results[day][pareja]

            fig = plt.figure(figsize=figsize)
            ax = fig.add_subplot(111)
            #axis_labels = [xvar, "error [m]", vars2plot[0]+" vs "+vars2plot[1]]
            fig, filename, figlabel, figcaption = plotter(ax, df, xvar=xvar, tags=tags, vars2plot=vars2plot,
                                           axis_labels=axis_labels, figsize=figsize)

            """
            fig.savefig(filename+".pdf", format='pdf', bbox_inches='tight')
            plt.close()
            display(Image(filename),Caption(filename+".pdf", figlabel, str(figcaption)))
            """

            if plot_inline:
                plt.show()
            else:
                plt.close()

            fig.savefig(filename+".pdf", format='pdf', bbox_inches='tight')

            if plot_in_doc:
                display(Image(filename),Caption(filename+".pdf", figlabel, str(figcaption)))

def plotting_df3(df_res, scenario,
                 title="diference err_stdlone - err_coop: ",
                 xlabel="gps_sow",
                 ylabel="err_stdlone - err_coop [m]",
                 figsize=(20,5),
                 plot_inline=True,
                 plot_in_doc=True):

    days = set(df_res.gps_day)

    for day in days:
        #
        #Select rows from a DataFrame based on values in a column in pandas
        #https://goo.gl/CW8cZA
        
        df_a = df_res[(df_res['scenario']==scenario) & (df_res['gps_day']==day)]
        #display(df_a)
        
        fig = plt.figure(figsize=figsize)
        ax = fig.add_subplot(111)
        
        labels = df_a["stations"].to_dict()
        labels = {k:v.split(',') for k,v in labels.iteritems()}
        y_var = df_a["diff_e_std_e_coop"].to_dict()
        y_var = {k:np.array(v).T for k,v in y_var.iteritems()}
        x_var = df_a["gps_time"].to_dict()
        
        for idx, idy, idtag in zip(x_var,y_var, labels):
            x = x_var[idx]
            for i, y in enumerate(y_var[idy]):
                ax.plot(x, y, label=labels[idtag][i])

        subtitle = "(scheme {} gpsday {})".format(scenario, str(day))
        ax.set_title(title+" - "+subtitle)
        ax.set_xlabel(xlabel)
        ax.set_ylabel(ylabel)
        ax.legend(bbox_to_anchor=(1.02, 1), loc=1, borderaxespad=0.)

        figlabel = str(day)+"_"+str(scenario)
        figcaption = title+ " " +subtitle
        # #escenario + mean_diference
        filename = figlabel #+'.png'
        
        if plot_inline:
            plt.show()
        else:
            plt.close()

        fig.savefig(filename+".pdf", format='pdf', bbox_inches='tight')

        if plot_in_doc:
            display(Image(filename),Caption(filename+".pdf", figlabel, str(figcaption)))
