#!/bin/bash
filename=$(basename "$1")
extension="${filename##*.}"
filename="${filename%.*}"

#jupyter nbconvert --to latex --template rev.tplx ../Avances/$filename
#mv ../Avances/$filename.tex ../Avances/$filename"_files" .

jupyter nbconvert --to latex --template rev.tplx $filename
#cp -f ../Avances/$filename.tex ../Avances/*png ../Avances/*pdf .

pdflatex $filename.tex
bibtex $2 #
bibtex $filename
pdflatex $filename.tex
pdflatex $filename.tex
pdflatex $filename.tex

rm -rf *.out *.log *.bbl *.blg *.aux *.toc #*files #*.tex