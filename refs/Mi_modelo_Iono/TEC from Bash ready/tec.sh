# Bash script: tec.sh
# Author: Lucas Hjelle
# Created: 31 October 2009
# Last Modified: 23 November 2009
#
# This script downloads the data necessary to generate
# plots for Slant TEC and Vertical TEC, calls Octave to
# analyze the data and create the plots, and calls LaTeX
# to generate the document.
#
# The code is heavily commented, but feel free to direct any
# questions or comments to me at hjelle+tec@gmail.com.

type -P wget   &>/dev/null || { echo "Please install Wget and rerun this script." >&2; exit 1; }
type -P gzip   &>/dev/null || { echo "Please install GZip and rerun this script." >&2; exit 1; }
type -P octave &>/dev/null || { echo "Please install Octave and rerun this script." >&2; exit 1; }
type -P latex  &>/dev/null || { echo "Please install LaTeX and rerun this script." >&2; exit 1; }
[ $(uname)=="CYGWIN*" ] && [ "(type -p xinit)" ] || { echo "Please install Cygwin/X (X11) and rerun this script." >&2; exit 1; }
#[ $(uname)=="CYGWIN*" ] && [ "$(pidof xinit)" ] || { echo "Please run \"startx\" and rerun this script." >&2; exit 1; }

d=$(date --rfc-3339=seconds)  # Get current date
echo Current date: $d  # Display current date
let yyyymmdd=${d:0:4}${d:5:2}${d:8:2}-1  # Get yesterday's date in yyyymmdd format
let day_of_year=$(date +%j)-1  # Get yesterday's day of the year
echo TEC Data first available for yesterday: $yyyymmdd

rinex_file='zmp1'$day_of_year'a.'${yyyymmdd:2:2}o  # Assign rinex data file name

if [ -f $rinex_file ]; then  # If the rinex data file already exists...
	echo You have already downloaded and unzipped $rinex_file...
elif [ -f $rinex_file'.gz' ]; then  # If only the rinex zip file exists...
	gunzip -d -N $rinex_file'.gz'  # Unzip rinex observation file
	cp $rinex_file rinex.txt  # Rename file for use in Octave
	echo Rinex data properly unzipped...	
else  # Else download and unzip the rinex data file...
	wget ftp://www.ngs.noaa.gov/cors/rinex/${yyyymmdd:0:4}/$day_of_year/zmp1/$rinex_file'.gz'  # Get file
	echo Rinex data properly downloaded from www.ngs.noaa.gov...
	gunzip -d -N $rinex_file'.gz'  # Unzip file
	echo Rinex data properly unzipped...
	cp $rinex_file rinex.txt  # Rename file for use in Octave
fi

if [ -f $yyyymmdd'0000_TEC.txt' ]; then  # If the rinex data file already exists...
	echo You have already downloaded $yyyymmdd'0000_TEC.txt'...
else
	wget http://www.ngdc.noaa.gov/stp/IONO/USTEC/products/${yyyymmdd:0:4}/${yyyymmdd:4:2}/${yyyymmdd:6:2}/$yyyymmdd'0000_TEC.txt'
	sed '1,19d' $yyyymmdd'0000_TEC.txt'  > ustec.txt  # Remove 19-line header, rename for use in Octave
	echo US-TEC data properly downlaoded from www.ngdc.noaa.gov...
fi

if [ -f 'coastline.txt' ] && [ -f 'tec.tex' ] && [ -f 'tec.m' ]; then
	echo You have already downloaded coastline data, LaTeX source, and the Octave script.
else
	wget http://lucashjelle.googlepages.com/coastline.txt
	wget http://lucashjelle.googlepages.com/tec.tex
	wget http://lucashjelle.googlepages.com/tec.m
	echo Coastline data, LaTeX source, and Octave script properly downlaoded.
fi

octave -q tec.m  # Invoke Octave and produce plots
echo Successful creation of STEC plot...

latex tec.tex  # Run LaTeX to identify references
latex tec.tex  # Run LaTeX again to correctly number references
echo Generation of tec.tex completed...

dvips -Ppdf tec.dvi -o tec.ps  # Create PostScript file
ps2pdf tec.ps  # Create pdf
echo Output file generated: tec.pdf