%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Delta epsilon plot
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure

% delta eps 1
subplot(3,1,1)
title('Delta epsilon')
hold on
%plot(t, mes_deps(1:N,1),'--g')
plot(t, est_deps(1:N,1),'--r')
legend('q-method', 'EKF')
ylabel('\delta\epsilon_1')

% delta eps 2
subplot(3,1,2)
hold on
%plot(t, mes_deps(1:N+1,2),'--g')
plot(t, est_deps(1:N,2),'--r')
legend('q-method', 'EKF')
ylabel('\delta\epsilon_2')

% delta eps 3
subplot(3,1,3)
hold on
%plot(t, mes_deps(1:N+1,3),'--g')
plot(t, est_deps(1:N,3),'--r')
legend('q-method', 'EKF')
ylabel('\delta\epsilon_3')
xlabel('Time [sec]')

