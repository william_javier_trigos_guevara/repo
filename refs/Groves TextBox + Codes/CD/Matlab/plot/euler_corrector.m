% Break the limit off +-180 deg if 
% that makes it closer to the actual angle.
% To make the plots easier to read.
for i = 1:N
   if act_e(i,1)-est_e(i,1) > 180
       est_e(i,1) = est_e(i,1)+360;
   end
   if act_e(i,1)-est_e(i,1) < -180
       est_e(i,1) = est_e(i,1)-360;
   end
   
   if act_e(i,2)-est_e(i,2) > 180
       est_e(i,2) = est_e(i,2)+360;
   end
   if act_e(i,2)-est_e(i,2) < -180
       est_e(i,2) = est_e(i,2)-360;
   end
   
   if act_e(i,3)-est_e(i,3) > 180
       est_e(i,3) = est_e(i,3)+360;
   end
   if act_e(i,3)-est_e(i,3) < -180
       est_e(i,3) = est_e(i,3)-360;
   end
end
