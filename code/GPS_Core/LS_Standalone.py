# -*- coding: utf-8 -*-
from numba import autojit
from numpy.linalg import norm

@autojit
def apply_earth_rotation_to_svs_position(svs, prs):
      c = 299792458
      we = 7.2921159e-5
      rpos = np.zeros(svs.shape)
      pos = np.array(svs)
      for i in range(len(pos)):
        dt = prs[i]/c
        theta = we*dt
        R = np.array([[np.cos(theta), np.sin(theta),0.],[-np.sin(theta), np.cos(theta),0.],[0.,0.,1.]])
        rpos[i] = R.dot(pos[i])
      svs = np.array(rpos)
      return svs

def compute_least_squares_position(svs, svs_clocks, prs, max_iters=200, apply_earth_rotation=True):

    if apply_earth_rotation:
        #print type(svs), type(prs)
        svs = apply_earth_rotation_to_svs_position(svs, prs)

    if len(svs)==0 or len(prs)==0:
        return np.array([0.,0.,0.,0.]),None, None, None

    ri = np.array([0.,0.,0.,0.])

    #for i in range(max_iters):
    delta,i = 1,0
    while (norm(delta)>1e-8 and i<max_iters):
        rhos, pranges = predict_pseudoranges(ri, svs, svs_clocks)
        b = prs - pranges
        A = np.hstack(((ri[:3]-svs)/rhos[:,None],np.ones((len(b), 1))))
        delta =  np.linalg.pinv(A.T.dot(A)).dot(A.T).dot(b)
        ri += delta
        i+=1
    return ri, A, b, delta

@autojit
def predict_coderanges(x, prns_pos, prns_clockbias):
    c = 299792458.0
    f1 = 1575420000.0
    lamda = c/f1

    rhos    = compute_distances(x[:3], prns_pos)
    pcodes  = rhos + x[3]-c*prns_clockbias + x[4]*lamda
    return rhos, pcodes

def least_squares_position_codephase(svs, svs_clocks, prs, codes=None, elevs=None, snr=None, max_iters=200, apply_earth_rotation=True):

    c = 299792458.0
    f1 = 1575420000.0
    lamda = c/f1

    """
        Para emplear la solucion con codigo+fase se necesitan minimo 5 satelites u observaciones.
    """
    if apply_earth_rotation:
        #print type(svs), type(prs)
        svs = apply_earth_rotation_to_svs_position(svs, prs)

    if len(svs)==0 or len(prs)==0:
        return np.array([0.,0.,0.,0.]),None, None, None

    if len(snr) == 0 or len(elevs)== 0:
        print "SNR observable is needed"
        return np.array([0.,0.,0.,0.]),None, None, None

    ri = np.array([0.,0.,0.,0.])

    if len(codes) != 0 and len(codes)>5:
        ri = np.array([0.]*4 + [0.]*len(codes))
        bandera = True

    #for i in range(max_iters):
    delta,i = 1,0
    while (norm(delta)>1e-8 and i<max_iters):

        rhos, pranges = predict_pseudoranges(ri[:4], svs, svs_clocks)
        b = prs - pranges
        ones = np.ones((len(b), 1))
        zero = np.zeros((len(b), len(b))) #np.zeros((len(b),1))
        A = np.hstack(((ri[:3]-svs)/rhos[:,None], ones, zero))

        if bandera:
            # Considerando observables de fase
            rhos2, pcodes  = predict_coderanges(ri, svs, svs_clocks)
            bc = codes - pcodes
            #print np.shape(b), np.shape(bc)
            b = np.hstack((b, bc))
            #print np.shape(b), np.shape(bc)
            lambdas = lamda*np.eye(len(bc)) # *ones
            #print np.shape((ri[:3]-svs)/rhos[:,None]), np.shape(ones), np.shape(b), np.shape(pcodes)
            ai = np.hstack(((ri[:3]-svs)/rhos[:,None], ones, lambdas))
            #print np.shape(ai), np.shape(b)
            A = np.vstack((A, ai))

        if len(snr) != 0 or len(elevs) != 0:
            #wp = len(pranges)*(0.03 + 0.9*np.exp(-snr/elevs))#elevs/30.))
            wp = len(pranges)*(0.03 + 0.9*np.exp(-snr/elevs))#elevs/30.))
            wc = len(pranges)*(0.07 + 0.6*np.exp(-snr/elevs))#elevs/30.))
            w = np.hstack((wp, wc))
            #print np.shape(w), np.shape((A.T.dot(w*np.eye(len(w))))), np.shape(A.T), np.shape(b)
             #  + 1e-3*np.eye(len(ri))
            delta =  np.linalg.pinv((A.T.dot(w*np.eye(len(w)))).dot(A)).dot(A.T.dot(w*np.eye(len(w)))).dot(b)
        else:
            delta =  np.linalg.pinv(A.T.dot(A)).dot(A.T).dot(b)

        ri += delta
        i+=1
    return ri, A, b, delta

def weight_least_squares_position(svs, svs_clocks, prs, elevs, snr, max_iters=200, apply_earth_rotation=True):

    if apply_earth_rotation:
        svs = apply_earth_rotation_to_svs_position(svs, prs)

    if len(svs)==0 or len(prs)==0 or len(elevs)==0 or len(snr)==0:
        return np.array([0.,0.,0.,0.]),None, None, None

    ri = np.array([0.,0.,0.,0.])

    #for i in range(max_iters):
    delta,i = 1,0
    while (norm(delta)>1e-8 and i<max_iters):
        rhos, pranges = predict_pseudoranges(ri, svs, svs_clocks)
        b = prs - pranges
        A = np.hstack(((ri[:3]-svs)/rhos[:,None],np.ones((len(b), 1))))

        w= len(pranges)*(0.03 + 0.9*np.exp(-snr/elevs))
        #wc = len(pranges)*(0.07 + 0.6*np.exp(-snr/elevs))#elevs/30.))
        #w = np.hstack((wp, wc))
        delta =  np.linalg.pinv((A.T.dot(w*np.eye(len(w)))).dot(A)).dot(A.T.dot(w*np.eye(len(w)))).dot(b)
        #delta =  np.linalg.pinv(A.T.dot(A)).dot(A.T).dot(b)
        ri += delta
        i+=1
    return ri, A, b, delta