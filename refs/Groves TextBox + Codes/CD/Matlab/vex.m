function a = vex(S)
% --------------------------------------------------
% Inverse of the cross-product operator:
% a x b = Smtrx(a)*b
% vex(Smtrx(a)) = a
%
% Author:   Harald N�kland
% Date:     June 2011
% --------------------------------------------------
a = [S(3,2) S(1,3) S(2,1)]';