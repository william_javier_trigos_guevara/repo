% Chapter 4: Selecting and Porting an RTOS

While it was possible to write a custom operating system, or even use a simple C language run-time environment for GPL-GPS, neither allows the developer the flexibility to focus on the application, rather than the underlying code infrastructure. The open systems approach of GPL-GPS also suggests using an already existing standards-based, open software system like an operating system. Thus, I chose to use a standard real time operating system as the base of GPL-GPS' software infrastructure, rather than the seemingly more typical GPS development option of creating a custom solution.

\section{`Soft' vs. `Hard' Real Time}

Operating systems are grouped into three general time-constrained categories: non-real time, `soft' real time, and `hard' real time operating systems. A standard, or non-real time, operating system has no bounds on system response and can not run applications with time constraints. Soft real time systems have low latencies, but still have no bounds on system response time. In many respects, `soft real time' is a misnomer --- if a system can miss deadlines without a failure, then it's not a real time system. Hard real time systems will fail if their time constraints are not always met. For these systems, being late is similar to a logical error.

Real time systems have exacting time constraints that must be met. These constraints may be measured in microseconds, hours, or even days, but they must be met.

GPS receivers are an example of a real time system with critical timing constraints on the order of 100's of microseconds: losing the 1~ms correlator accumulator interrupts will eventually cause the channel to lose lock on a satellite.

\section{RTOS Requirements}

The GPL-GPS project has very strict requirements for a RTOS because of the performance and memory constraints. The requirements for the RTOS are:

\begin{description}
    \item [Hard real time performance] Interrupt latencies must be on the order of 10~$\mu$s in order to handle the 1~ms accumulator interrupts, and context switches must be on the same order of magnitude to enable the use of threads.
    \item [Small memory footprint] With only 256~kB of flash memory, the RTOS may take up no more than 128~kB (based on the OSGPS v1.17 current code size of 180~kB for the x86 processor).
    \item [Open source] As outlined in the GPL-GPS design philosophy, the RTOS must be open source with no licensing restrictions.
    \item [Existing port to ARM7 processors] Porting a RTOS to a new processor architecture is a thesis unto itself, so the RTOS must be already ported to ARM7TDMI processors.
    \item [Integrated debugging tools] Built-in debugging features are one of the most overlooked aspects of an operating system. In order to make cross-platform embedded development tolerable, the RTOS must support remote loading and debugging of applications on the target system.
\end{description}

\noindent Some important considerations for choosing an RTOS include:

\begin{description}
    \item [Rich operating system primitives] A benefit of using an operating system is not only the ubiquitous support of threads, but also that it provides rich timer and interprocess communication (IPC) primitives (e.g., mutexes, semaphores, etc).
    \item [Simple to compile and configure] The more complicated the setup and installation of an OS, the harder it is to have others adopt it and the more time wasted on the code infrastructure rather than the application code.
    \item [Supports multiple architectures] An RTOS which is ported to many other processor architectures is an important consideration for future GPL-GPS ports.
    \item [Good documentation] Poor technical documentation on the complicated inner details of an operating system can be frustrating and waste time. Books, online manuals, and manuals all help clarify the details of using a RTOS.
\end{description}

\section{Applicable Real Time Operating Systems}

Several months were spent research existing real time operating systems. The comparison of the applicable operating systems are in Table~\ref{tab:rtos-comparison}.

\begin{sidewaystable}
    \begin{center}
        \begin{tabular}{l|cccccc}
            RTOS      & ISOS \cite{isos} & $\mu$C/OS-II \cite{ucos} & eCos \cite{ecos} & $\mu$Clinux \cite{uclinux} & Nucleus \cite{nucleus} & Custom \\
            \hline
            Hard real time         & Y  & Y                       & Y               & N                         & Y                     & Y \\
            $<$ 128 kB footprint   & Y  & Y                       & Y               & N                         & Y                     & Y \\
            Open source            & Y  & N (published)           & Y               & Y                         & N                     & Y \\
            Supports ARM7TDMI      & Y  & Y                       & Y               & Y                         & Y                     & Y \\
            Built-in debugging     & N  & N                       & Y               & Y                         & Y                     & N \\
            Rich IPC features      & N  & Y                       & Y               & Y                         & Y                     & N \\
            Simple configuration   & Y  & Y                       & Y               & Y                         & Y                     & N \\
            Supports most proc.    & N  & Y                       & Y               & Y                         & Y                     & N \\
            Existing documentation & N  & Y                       & Y               & Y                         & Y                     & N \\
        \end{tabular}
        \caption{Comparison of real time operating systems.}
        \label{tab:rtos-comparison}
    \end{center}
\end{sidewaystable}

$\mu$Clinux was discarded since it is not a real time operating system. Nucleus turned to out to be thousands of dollars per application, and not open source, which is unfortunate since it's a truly full featured, configurable and light weight RTOS. ISOS was small and simple, but wasn't feature rich and had no debug facilities. As mentioned earlier, a custom-written RTOS was considered, but rejected because of the amount of work necessary. Further, a custom RTOS solution would not provide the feature-rich, standards-based functionality that an existing RTOS provides. This left only $\mu$C/OS-II and eCos. $\mu$C/OS-II turns out to be a well written RTOS with FAA DO-178B certification and a nice thick companion textbook explaining operating system fundamentals using $\mu$C/OS-II as an example. It was the clear choice, until it became evident that it provided no debugging facilities, and that the license agreement doesn't allow redistribution.

\section{Choosing a RTOS}

eCos turned out to be the only choice worth considering. Fortunately, it exactly fits the RTOS requirements and important considerations. eCos is:

\begin{itemize}
    \item A hard real time operating system with low interrupt latencies (approximately 8~$\mu$s for an ARM7TDMI at 20~MHz),
    \item Ported to several existing ARM7TDMI processors,
    \item Bundled with RedBoot, a tiny but powerful boot manager that includes GDB stubs, an open source remote debugging tool,
    \item Tiny enough to fit in under 50~kB of flash (although 80~kB is a more reasonable size),
    \item Free and open source (under the GNU GPL v2),
    \item Ported to more than 10 different processor architectures (including the LEON SPARC v8 processor used in the NemeriX NJ1030 GPS baseband processor)
    \item Well documented via an online reference manual and a published book.
\end{itemize}

Other than Nucleus, eCos has the richest feature set of the surveyed operating systems: a POSIX compatibility layer, several flavors of debug monitors, sophisticated handling of interrupt service routines, and, importantly, a very sophisticated debug and instrumentation environment. eCos has the added benefit that its required toolset is free and open source. It requires the GNU software development system that includes tools such as the GCC compiler and the GDB debugger \cite{ecos}.

eCos's main drawback is that it is a extremely complicated RTOS with the most sophisticated configuration tool of any comparable operating system. Indeed, it is the only configuration tool to have its own language.

\section{eCos Architecture}

eCos is not a standard real time operating system that runs independently on the target system and provides an environment to run applications. It is a ``runtime system'', a static pre-compiled library that the application links against during compile time. The library provides all of the functions of a standard operating system: startup, RTOS kernel, scheduler, etc.

eCos is called the ``embedded Configurable operating system'' because it uses a wxWindows-based graphical configuration tool to control the features compiled into the library. Like many configuration systems, it must carefully orchestrate dependencies amongst the packages. The Atmel 29LV200BB flash memory drivers required for the MG5001 board, for example, require the generic flash support package, which requires a series of file-system-like extensions. This web of dependencies is handled by a TCL-based language written specifically for eCos called the ``configuration description language'' (CDL). Users run the graphical \texttt{configtool} to select eCos packages (Figure~\ref{fig:configtool}), and then choose components inside those packages. On a source level, the configuration tool includes source packages in the eCos library and \texttt{\#defines} component options.

\begin{figure}[h] % place the figure right here
    \begin{center}
        \includegraphics[keepaspectratio,width=0.9\linewidth]{pictures/configtool.eps}
        \caption{The eCos \texttt{configtool} program configuring the \texttt{gps-4020} template.}
        \label{fig:configtool}
    \end{center}
\end{figure}

The configuration system allows eCos to be well ``tuned'' to the target system; full featured systems can be only 50~kB, while including larger packages, like ethernet drivers, POSIX compatibility, and the GoAhead web server can push eCos to several megabytes.

eCos uses a hardware abstraction layer (HAL) to keep its kernel independent of the underlying system architecture. Calls that involve hardware --- turning off the interrupts, for example --- are \texttt{\#defined} by the architecture template into direct hardware calls. This means that the kernel can be almost completely independent of the hardware. This gets more complex the closer one gets to hardware,  but it maintains a reusable code base independent of the underlying hardware \cite{massa}.

\section{Porting eCos to the GP4020}

eCos ports have three levels: architectures, variants, and platforms. Architecture ports are ports to new processor architectures (e.g., x86 vs ARM), variant ports are different versions of an architecture (e.g., ARM7TDMI vs XScale), and platforms are differently configured development boards (e.g., the MG5001 vs the SuperStar II receiver which have different memory sizes). Thus porting eCos to the Zarlink GP4020 baseband processor required a variant port for the ARM7TDMI-based GP4020 and a platform port for the SigTec MG5001 receiver.

The rich feature set and its custom description language makes even eCos platform ports nontrivial. After early attempts at creating a variant port from a more generic ARM7TDMI processor met with failure, Gary Thomas, one of the original authors of eCos and the original author of the ARM architecture port for eCos, completed the variant and platform port. The numerous technical details of the port will not be presented here: for more information, please see the GPL-GPS project web site at \url{http://gps.psas.pdx.edu/}. The GP4020 variant and platform ports includes:

\begin{itemize}
    \item A program to allow minicom, an open source serial terminal application, to use the GP4020 serial port bootloader.
    \item Memory definitions and code for the GP4020's memory peripheral controller (MPC).
    \item Device drivers for the GP4020 serial ports, timers, and interrupt controller.
    \item CDL files to describe the \texttt{gps4020} eCos package.
    \item CDL files to describe RedBoot boot loader RAM and ROM (flash) images.
    \item CDL files to describe \texttt{gps4020} eCos images, including RAM and ROM (flash) images.
\end{itemize}

\section{GPL-GPS Software Infrastructure}

Figure~\ref{fig:ecosdiagram} shows the multi-layered GPL-GPS software infrastructure on a GP4020-based receiver. eCos is first compiled as a stripped-down standalone application, called RedBoot, and installed into the flash memory of the receiver. RedBoot acts as a boot loader, a small program which helps load applications into RAM. RedBoot is used in many commercial development boards, and includes such services as a simple command line interface, serial and ethernet communication with up and download protocols, flash drivers, and debugging executives like GDB stubs, a debug executive for the GDB debugger. The host development PC communicates with RedBoot over a standard serial port to load and debug application programs in the receiver's RAM.

Once RedBoot has been installed in flash memory, the receiver is ready for GPL-GPS application development. The host PC compiles and links the GPL-GPS application with the eCos library, and downloads it using GDB stubs running in the RedBoot flash image. The host PC can now run and debug the program, using the GP4020's second serial port as a serial terminal to display application information.

\begin{figure}[h] % place the figure right here
    \begin{center}
        \includegraphics[keepaspectratio,width=0.9\linewidth]{pictures/software_infrastructure.eps}
        \caption{GPL-GPS software architecture: GPL-GPS, eCos, RedBoot and host system.}
        \label{fig:ecosdiagram}
    \end{center}
\end{figure}

\section{Getting eCos Running on the GP4020}

Detailed instruction on installing RedBoot and the GPL-GPS development system can be found on the GPL-GPS website, \url{http://gps.psas.pdx.edu/}. In summary:

\begin{enumerate}
    \item Compile three eCos libraries: a RedBoot executive for RAM, a RedBoot executive for ROM, and an eCos library for the GPL-GPS application.
    \item Using the GP4020 bootloader and the Minicom terminal program on a host PC, load the RedBoot executive for RAM into the receiver's RAM.
    \item Using the RedBoot image now running in RAM, load the RedBoot executive for ROM and and write it into the receiver's flash memory.
\end{enumerate}

Now the receiver has RedBoot stored in flash. At this point, the receiver is ready for application development. To use the GPL-GPS application, the user must:

\begin{enumerate}
    \item Compile GPL-GPS sources, linking with the eCos library compiled in the previous steps.
    \item Run GDB (\texttt{arm-elf-gdb} in this case) on the host PC and using GDB stubs in the RedBoot image, load the application into the receiver's RAM.
    \item Run and debug the program using the remote GDB protocol over the serial port.
\end{enumerate}
