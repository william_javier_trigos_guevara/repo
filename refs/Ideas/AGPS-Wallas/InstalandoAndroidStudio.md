
# Instalando java y dependencias necesarias 
- http://www.techotopia.com/index.php/Setting_up_a_Windows,_Linux_or_Mac_OS_X_Android_Studio_Development_Environment


## Java (http://tecadmin.net/install-oracle-java-8-jdk-8-ubuntu-via-ppa/)
> $ sudo add-apt-repository ppa:webupd8team/java
> $ sudo apt-get update
> $ sudo apt-get install oracle-java8-installer
* java -version (verificar que tengo la última versión)

## Librerias
- https://github.com/meteor/meteor/wiki/Mobile-Development-Install:-Android-on-Linux
- http://developer.android.com/intl/es/sdk/installing/index.html?pkg=tools
> sudo apt-get install libncurses5 libstdc++6 zlib1g
> sudo apt-get install lib32z1 lib32ncurses5 lib32bz2-1.0 
> sudo apt-get update; sudo apt-get install --yes lib32z1 lib32stdc++6



## Android Studio (http://tecadmin.net/install-android-studio-on-ubuntu-and-linuxmint/)

> $ sudo add-apt-repository ppa:paolorotolo/android-studio
> $ sudo apt-get update
> $ sudo apt-get install android-studio



