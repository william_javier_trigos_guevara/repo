% Chapter 6: Future Work & Conclusion

\section{Future Work}
\label{sec:future-work}

\subsection{Introduction}

The potential of GPL-GPS exists in the extended applications end users can implement because of their complete access to the software and hardware of the GPL-GPS development system. Before attempting extended applications, however, several issues need to be resolved. The following ``laundry list'' is a prioritized list of features I, and the other contributing members of the GPL-GPS community, hope to accomplish:

\subsection{Reimplement Atmospheric Correction}

Atmospheric models of ionosphere and troposphere propagation delays were removed from OSGPS to simplify porting. This should be reimplemented, along with some refactoring of the satellite position calculation.

\subsection{Reimplement Almanac Processing}

Almanac processing was removed from OSGPS to simplify porting. This should be reimplemented, along with more sophisticated satellite-to-channel allocation algorithms based on the almanac and a driver for the GP4020 real time clock (RTC).

\subsection{Reimplement Carrier Phase Tracking}

Carrier phase tracking was removed from OSGPS to simplify porting. This should be reimplemented, along with hooks for future carrier phase-based positioning.

\subsection{Faster Cold Acquisition}

Cold acquisition time --- the time from turn on to position output with no knowledge of position or time --- can be significantly decreased. Once a satellite is acquired and identified, then its orbital slot indicates which other satellites in its orbit might possibly be in view, and which are beyond the horizon and can be ignored.

Another way to enhance cold acquisition time is to skip search bins, counting on the pull-in algorithm to pick up smaller changes in the correlator values than a direct match; this requires careful coordination with the tracking algorithm.

Finally, the obvious acquisition speed enhancement is to include the flash drivers in eCos and storing the almanac data in flash on every successful almanac acquisition. This is more difficult than it sounds because the flash must be erased in sectors, and it's likely that a full sector might not be available. Thus flash will need to be copied to SRAM, and then written back along with the almanac. Note that the almanac is only useful if the real time clock on the GP4020 is backed up by battery (MG5001) or super-capacitor (SuperStar II).

\subsection{Move Tracking Loops to Internal SRAM}

To provide more memory and faster operation, all interrupt and tracking loop code should be moved to the zero wait-state 32-bit internal SRAM in the GP4020. This involves changing linker commands for RedBoot and eCos, as well as special compiler directives.

\subsection{Code Refactoring and Contribution back to OSGPS}

The OSGPS code base needs refactoring to make it more efficient and more clear. This is a long process of stepping through the code and applying comments, code style, and algorithmic refactoring as necessary. Once these changes are implemented and tested in GPL-GPS, these changes will be contributed back to OSGPS. There is even some possibility that OSGPS might move to using POSIX compliant threads, meaning that in many instances the exact same code could be operating on both GPL-GPS and OSGPS.

Some refactoring includes moving to equivalent but clearer algorithms. For example, moving from dithered correlators to tracking (early-late) correlators for the pull-in and tracking functions. While this will not fundamentally change the tracking algorithm, it is the standard way of explaining tracking in the literature and, thus, is easier to understand. similarly, having the tracking loops maximize the in-phase (I) component versus the quadrature (Q) component is more of a standard standard signal processing algorithm than maximizing Q.

\subsection{Better Phase Locked Loop Algorithm}

Research and implement a quasi-coherent PLL to replace the hybrid PLL currently used by OSGPS \cite{bluebook}.

\subsection{Network API (Flight Computer Model)}

Create a network Application Programming Interface (API) to get correlator, high-rate ($\geq$ 10~Hz) pseudorange, and/or navigation data to an offboard PC or ``flight computer'' and get aiding and initial position, velocity and time information back. It's not clear how easily this can be implemented; the only options so far are a high speed serial bus (a 5~Mbps SPI synchronous serial port) or a direct 16~bit parallel interface which may not be possible with the MG5001 receiver board's limited 51~pin connector.

\subsection{Lock Aiding}

Allow external sensor data to aid correlator tracking of satellites. For example, altimeter data can be used to allow fixes on only three satellites, and inertial data can be used to aid tracking of the satellite signals under high dynamic conditions.

\subsection{DGPS}

Implement a module to make GPL-GPS into a DGPS base station: placing the receiver at a known position allows the receiver to calculate atmospheric biases to each satellite. Transmitting this bias to a roving receiver allows that receiver to subtract the propagation errors and obtain sub-meter positioning accuracy.

\subsection{Attitude}

Implement communication amongst GPL-GPS receivers, each doing carrier phase tracking and solving the integer ambiguity problem. With lock and carrier tracking confirmation from each receiver, the parallel system should be able to turn carrier phase into a full attitude.

\subsection{Moving towards Open Hardware}

An exciting, if not distracting, future project is to build an open hardware GPS receiver based on Zarlink's Orion reference design for the GP2015, GP2021, and ARM60 processor. There are real advantages to having open hardware: easy access to the parallel 16~bit bus means enhanced interfaces to peripherals and other processors, and external memory can be sized appropriately. Also, dividing the board into a RF and digital sections (with connectors) may allow for using the board as a front end for software receivers as well.

\section{Conclusion}

By porting open source software to commercial, off-the-shelf GPS receivers, GPL-GPS provides an accessible GPS receiver development environment that is otherwise unavailable. Its inexpensive and open nature lowers the barriers of entry to GPS development, which opens a rich variety of new and interesting GPS applications. Although just in its infancy, GPL-GPS is already being slated for projects such as nanosatellites, UAVs, and small scale launch vehicles. The hope is that these GPL-GPS applications will then contribute their code changes back to the project, enabling GPL-GPS to quickly grow in maturity and features. Furthermore, GPL-GPS may enable academic research and hands-on coursework that was not previously possible. Now even a modest research grant can provide dozens of GPS development kits.

The vision for GPL-GPS is to lower the barrier to GPS development and enable a rich and vibrant community of GPS developers. I look forward to leading GPL-GPS through its first steps toward this vision.
