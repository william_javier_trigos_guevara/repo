%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Gyro bias plot
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure

% gyro bias - roll
subplot(3,1,1)
title('Gyro bias')
hold on
plot(t, act_bgyr(1:N,1),'-b')
plot(t, est_bgyr(1:N,1),'--r')
legend('True', 'EKF')
ylabel('bias_p  [rad/s]')

% gyro bias - pitch
subplot(3,1,2)
hold on
plot(t, act_bgyr(1:N,2),'-b')
plot(t, est_bgyr(1:N,2),'--r')
legend('True', 'EKF')
ylabel('bias_q  [rad/s]')

% gyro bias - yaw
subplot(3,1,3)
hold on
plot(t, act_bgyr(1:N,3),'-b')
plot(t, est_bgyr(1:N,3),'--r')
legend('True', 'EKF')
ylabel('bias_r  [rad/s]')
xlabel('Time [sec]')