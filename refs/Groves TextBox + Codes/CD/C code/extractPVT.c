#include <stdio.h>
#include <stdlib.h>
#include <string.h>


typedef struct __attribute__((packed)) {
    uint8_t     bGPS;       //[-]       GPS status byte or GPS data age. When the value decreases, new GPS data is available.
	uint32_t    Sacc;       //[cm/s]    Speed Accuracy Estimate. Expected error standard deviation.
	uint32_t    Vacc;       //[mm]      Vertical Accuracy Estimate. Expected error standard deviation.
	uint32_t    Hacc;       //[mm]      Horizontal Accuracy Estimate. Expected error standard deviation.
	int32_t     VEL_D;      //[cm/s]    NED down velocity
	int32_t     VEL_E;      //[cm/s]    NED east velocity
	int32_t     VEL_N;      //[cm/s]    NED north velocity
	int32_t     ALT;        //[mm]      Altitude/Height above Ellipsoid/Mean Sea Level
	int32_t     LON;        //[deg]*1e7 Longitude
	int32_t     LAT;        //[deg]*1e7 Latitude
	uint32_t    ITOW;       //[ms]      GPS Millisecond Time of Week
	uint8_t     bPrs;       //[-]       Pressure sensor status. When the value decreases, new pressure data is available.
	uint16_t    Press;      //[Pa]/2    Pressure value in Pascals.
}gps_pvt_t;

typedef struct __attribute__((packed)) {
    uint16_t    temp;       //          Internal temperature of the sensors
    uint16_t    magZ;       //          Raw reading from the AD-converter
    uint16_t    magY;       //          Raw reading from the AD-converter
    uint16_t    magX;       //          Raw reading from the AD-converter
    uint16_t    gyrZ;       //          Raw reading from the AD-converter
    uint16_t    gyrY;       //          Raw reading from the AD-converter
    uint16_t    gyrX;       //          Raw reading from the AD-converter
    uint16_t    accZ;       //          Raw reading from the AD-converter
    uint16_t    accY;       //          Raw reading from the AD-converter
    uint16_t    accX;       //          Raw reading from the AD-converter
}raw_inertial_t;

typedef struct __attribute__((packed)) {
    uint16_t        TS;             //Sample counter (wraps around after 65536)
    uint8_t         status;         //Bit2: GPS Fix
    gps_pvt_t       gps_pvt;
    raw_inertial_t  raw_inertial;
}xsens_data_t;



void xsens_parse(FILE *input, FILE *output)
{
    #define PRE	    0
	#define BID	    1
	#define MID	    2
	#define LEN	    3
	#define DATA    4
	#define CS	    5
    
    int             state = PRE;
    uint8_t         val;
    uint8_t         checksum;
    uint8_t         data_len;
    xsens_data_t    data_buffer;
    uint8_t         *ptr_data_buffer = (uint8_t *)&data_buffer;
    float           lon, lat, alt, vel_n, vel_e, vel_d, press;
    uint32_t        time, bPrs, bGPS, gps_fix, sample;
    
    fprintf(output, "Press   bPrs   ITOW   LON   LAT   ALT   VEL_N   VEL_E   VEL_D   bGPS   gps_fix   sample\n");
    
    while(!feof(input)) {
    
        fread(&val, sizeof(uint8_t), 1, input);
        
        switch (state) {
			case PRE:
				checksum = 0;
				if (val == 0xFA) { //message start
					state = BID;
				}
			    break;
			case BID:
				checksum += val;
				if (val == 0xFF) { //master device
					state = MID;
				}else if (val == 0xFA) { //
				    checksum = 0;
				}else {
					state = PRE;
				}
			    break;
			case MID:
			    checksum += val;
			    if (val == 0x32) { //data message
					state = LEN;
				}else {
					state = PRE;
				}
			    break;
			case LEN:
			    checksum += val;
			    data_len  = val;
			    if (val == 0x43) { //67 bytes
			        state = DATA;
			    }else {
			        state = PRE;
			    }
			    break;
			case DATA:
				checksum += val;
				ptr_data_buffer[--data_len] = val;
				if (data_len <= 0) {
					state = CS;
				}
			    break;
			case CS:	
				checksum += val;
				if (checksum != 0) {
					printf("Checksum error!\n");
				}else {
				    bPrs    = data_buffer.gps_pvt.bPrs;
				    bGPS    = data_buffer.gps_pvt.bGPS;
				    time    = data_buffer.gps_pvt.ITOW;
				    press   = ((float)data_buffer.gps_pvt.Press) * 2;         //[Pa]
				    lon     = ((float)data_buffer.gps_pvt.LON) / 10000000;    //[deg]
				    lat     = ((float)data_buffer.gps_pvt.LAT) / 10000000;    //[deg]
				    alt     = ((float)data_buffer.gps_pvt.ALT) / 1000;        //[m]
				    vel_n   = ((float)data_buffer.gps_pvt.VEL_N) / 100;       //[m/s]
				    vel_e   = ((float)data_buffer.gps_pvt.VEL_E) / 100;       //[m/s]
				    vel_d   = ((float)data_buffer.gps_pvt.VEL_D) / 100;       //[m/s]
				    gps_fix = (data_buffer.status && 0x04) > 0;               //[boolean]
				    sample  = data_buffer.TS;
				    fprintf(output, "%f %i %i %f %f %f %f %f %f %i %i %i\n", press, bPrs, time, lon, lat, alt, vel_n, vel_e, vel_d, bGPS, gps_fix, sample);
				}
                state = PRE;
			    break;
			default:
				state = PRE;
			    break;
		} // end case
    } // end while
}


int main(int argc, char *argv[])
{
    char ifile[80];
    char ofile[80];
    
    if (argc < 2) {
        printf("Please specify filename.\n");
        exit(0);
    }
    strcpy(ifile, argv[1]);
    strcpy(ofile, argv[1]);
    strcat(ifile, ".mtb");
    strcat(ofile, ".txt");
    FILE *input  = fopen(ifile, "r");
    if (!input) {
        printf("Error opening file\n");
        exit(0);
    }
    FILE *output = fopen(ofile, "w+");
    if (!output) {
        printf("Error opening file\n");
        exit(0);
    }
    
    xsens_parse(input, output);
    
    fclose(input);
    fclose(output);
    return 0;
}
