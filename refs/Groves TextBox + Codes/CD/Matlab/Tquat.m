function T = Tquat(q)
% --------------------------------------------------
% For the quaternion differential equation:
% q_dot = Tquat(q)*w
%
% Author:   Harald N�kland
% Date:     June 2011
% --------------------------------------------------
eta  = q(1); 
eps1 = q(2); 
eps2 = q(3); 
eps3 = q(4);

T = [ -eps1 -eps2 -eps3        
       eta  -eps3  eps2
       eps3  eta  -eps1
      -eps2  eps1  eta  ];