def run_simulation_paper(Obser, error_max, parejas_kmz, RinexDB, dia1, dian, ano, dist=None,
                         run_stdlo=True,
                         run_coop=True, 
                         Simu_Folder=None, 
                         res_plot=1000, 
                         apply_iono_model=None):
    
    def clean_folder(abspath_Folder, silent_mode=True):
        cmd = "rm -rf "+abspath_Folder+"/*"
        if silent_mode == False:
            print ("\n"+cmd)
        system(cmd)
        
    def extract_rinex_files(RinexDB, RX, Simu_Folder, required_files=['n', 'o']):

        Temp = {}
        matches = [x for x in RinexDB.keys() if RX.lower() == x]
        
        if len(matches)> 0:
            
            RX = matches[0]
            for Obstype in RinexDB[RX].keys():
                station, ObsType = splitext(RinexDB[RX][Obstype])
                path = RinexDB[RX][Obstype]
                dirname, zipname = split(path)
                fname, ext = splitext(zipname)

                #print fname, ext, path
                cmd = "cp "+path+" "+Simu_Folder+"/"+zipname
                system(cmd)
                cmd2 = "gunzip "+Simu_Folder+"/"+zipname
                system(cmd2)

                if isfile(Simu_Folder+"/"+fname):
                    Temp.setdefault(RX,{})[Obstype[0]] = Simu_Folder+"/"+fname
                    Temp[RX].update({Obstype:path})
                else:
                    Temp.setdefault(RX,{})[Obstype[0]] = ""
                    Temp[RX].update({Obstype:""})

            station = Temp.keys()[0]
            files = [f for f in required_files if f in Temp[station].keys()]
            
            if (len(files) >= len(required_files)):
                return Temp
            else:
                return {}
        else:
            return {}
    
    if Simu_Folder==None:
        Simu_Folder = getcwd()+"/Temp_Simul_data"
        cmd1 = "mkdir -p " + Simu_Folder    # COMANDO CREA CARPETA temporal
        system(cmd1)

    print parejas_kmz
    print "\t [Station Pair] \t [GPS day] \t [year] \t\t [Process status]\n"
    
    Results = {}
    for k, pareja in parejas_kmz.iteritems():
        for d in range(int(dia1), int(dian)+1):

            Obsfiles = {}
            for station in pareja:

                fname = str(station)+str(process_day2(d))+"."+ano[-2:]
                Obsfiles.update(extract_rinex_files(RinexDB, fname, Simu_Folder))

                if any(Obsfiles):
                    error = False
                else:
                    error = True

            if error or len(Obsfiles)<2:
                print "WARNING! %s Aborting execution: missing rinex files for %s \n"%(tick_equis(1), pareja)

            else:
                par = tuple(Obsfiles.keys())
                print "\t %s \t\t %s \t\t %s \t\t %s Running..."%(pareja, d, ano,tick_equis(0))

                try:

                    data1, b1, rec_pos1 = rinex_to_dataframe_iono(Obsfiles[par[0]]['o'], Obsfiles[par[0]]['n'])
                    data2, b2, rec_pos2 = rinex_to_dataframe_iono(Obsfiles[par[1]]['o'], Obsfiles[par[1]]['n'])
                    data_common = extract_common2(data1, data2)

                    Results.setdefault(str(par), {})[d] = simple_test(data_common, pareja, rec_pos1, rec_pos2,  
                    					dc=dist, res_plot=res_plot, apply_iono_model=apply_iono_model)

                except ValueError:
                    msg = "WARNING! %s \t\t %s \t\t %s Aborting execution: Unexpected error"
                    print (msg%(pareja, d, tick_equis(1)))

                except IOError as e:
                    print "I/O error({0}): {1}".format(e.errno, e.strerror)
    
    print ("\n\n\tSimulation Finished!")
    print ("\t "+"*"*30)
    #clean_folder(Simu_Folder, silent_mode=False)
    clean_folder(Simu_Folder)
    
    return Results  

def coop_ls_solver(gpstime, svs, svs_clocks, prs1, prs2, d_est, S11, S12, dat,
                   X0=np.ones(4),
                   X1=np.zeros(4),
                   max_iters=200, 
                   apply_earth_rotation=True, 
                   iono_model=None):
    
    def get_sat_elv_az(svs, rx):
        """
        http://www.naic.edu/aisr/GPSTEC/drewstuff/MATLAB/elavazim.m
        https://goo.gl/2Kt8Pf
        """
        #svs = apply_earth_rotation_to_svs_position(svs, prs)

        #d_svs = r_com[:,:3]/r_mag[:, None]
        svs_enu = ecef2enu(svs, rx)

        east, north, up  = svs_enu[0], svs_enu[1], svs_enu[2]

        elevation = up/np.linalg.norm(svs_enu, axis=0)
        azimuth = np.arctan2(east,north)#radians
        idx = np.where(azimuth<0)
        azimuth[idx] += 2*np.pi;
        return elevation, azimuth # radians

    def apply_earth_rotation_to_svs_position(svs, prs):
        c = 299792458
        we = 7.2921159e-5
        rpos = np.zeros(svs.shape)
        pos = np.array(svs)
        for i in range(len(pos)):
            dt = prs[i]/c
            theta = we*dt
            R = np.array([[np.cos(theta), np.sin(theta),0.],[-np.sin(theta), np.cos(theta),0.],[0.,0.,1.]])
            rpos[i] = R.dot(pos[i])
        svs = np.array(rpos)
        return svs
    
    def compute_distances(rc, svs):
        return np.linalg.norm(rc-svs, axis=1)

    def Klobuchar(A,E,time, rx_pos, alphas, betas): 
        ####klobuchar  model 

        lat, lon, alt = ecef2lla(X=rx_pos[0], Y=rx_pos[1], Z=rx_pos[2])
        C=2.99792458e8 

        ######bla bla 
        phi=(0.0137/(E-0.11))-0.022
        psi=lat+phi*math.cos(A)#2
        if psi > 0.416:
            psi=psi+0.416
        elif psi < -0.416:
            psi=psi-0.416

        lampda=lon+(phi*math.sin(A))/math.cos(psi)#3
        phi_m=lampda+0.064*math.cos(psi-1.617)#4
        t=43200*lampda+time#.getSecondOfDay()#5
        if t > 86400:
            t=t-86400
        elif t < 0:
            t=t+86400
        ##basados en el modelo expuesto en el link, se cambian el exponente de la sumatoria.
        ##http://what-when-how.com/gps/ionospheric-effects-physical-influences-of-gps-surveying-part-2/
        A_I=alphas[0]+alphas[1]*phi_m+alphas[2]*phi_m**2+alphas[3]*phi_m**3 #6
        if A_I < 0:
            A_I=0

        P_I=betas[0]+betas[1]*phi_m+betas[2]*phi_m**2+betas[3]*phi_m**3 #7
        if P_I < 72000:
            P_I=72000

        X_I=2*math.pi*(t-50400)/P_I #8
        F=1+16*(0.53-E)**3#9

        if math.fabs(X_I) > 1.57:#10
            IL1_GPS=((5e-9)+A_I*(1-(((X_I)**2)/2)+((X_I)**4)/24))*F*C
        elif math.fabs(X_I) < 1.57:
            IL1_GPS=(5e-9)*F*C
        else :
            IL1_GPS=(5e-9)*F*C
        return IL1_GPS#/F

    def predict_pseudoranges(x, prns_pos, prns_clockbias):
        c = 299792458
        rhos    = compute_distances(x[:3], prns_pos)
        pranges = rhos + x[3]-c*prns_clockbias
        return rhos, pranges
    
    def predict_pseudoranges_iono(x, svs, prns_clockbias, S1, dat, apply_iono_model=None):
        c = 299792458
        R_earth = 6367444.657  # 6,371 km
        h_iono  = 400e3
        
        def iono_3dmodel(rs, d_iono, elevs):
            num1 = d_iono**2*(np.sqrt(rs**2*np.cos(elevs))*np.sin(elevs)**2)
            den1 = np.power(2*rs*np.cos(elevs)**2, 3) #8*rs**3*np.cos(elev)**6 # 

            num2 = rs
            den2 = rs*np.cos(elevs)

            return num2/den2 + 1/8*(d_iono/rs)*(d_iono/rs)*np.tan(elevs) #num1/den1
        
        if apply_iono_model=='taylor':
            #print "a", apply_iono_model
            rs, d_iono = R_earth+h_iono, 10e3
            elv, az = get_sat_elv_az(svs, x[:3])
            io1 = iono_3dmodel(rs, d_iono, elv)

        elif apply_iono_model=='standard':
            #print apply_iono_model
            R = R_earth
            elevs, az = get_sat_elv_az(svs, x[:3])
            io1 = np.array(1/np.cos(np.arcsin(R/(R+350000))*np.sin(elevs)))
            #io2 = np.array(1/np.cos(np.arcsin(R/(R+350000))*np.sin(elv2)))

        elif apply_iono_model=='wallas':
            e, a = get_sat_elv_az(svs, x[:3])
            #print len(S1), len(e), len(a)
            io1 = (a/40.3)*(S1/e)
            #print len(io1), io1

        elif apply_iono_model=='klobu':
            elv, az = get_sat_elv_az(svs, x[:3])
            io1 = np.array([Klobuchar(a,e, gpstime, ri[:3], dat.alfa_beta_local[:4], dat.alfa_beta_local[4:8]) for a, e in zip(az, elv)])

        elif apply_iono_model=='dual_freq':
            io1 = dat.Iono

        else:
            io1 = 0.
            pass
    
        rhos    = compute_distances(x[:3], svs)
        pranges = rhos + x[3]-c*prns_clockbias + io1 #iono_3dmodel(elevs)
        return rhos, pranges

    if apply_earth_rotation:
        svs = apply_earth_rotation_to_svs_position(svs, prs1)
    
    if len(svs)==0 or len(prs1)==0 or len(prs2)==0:
        return np.array([0.,0.,0.,0.]),None, None, None

    # initialize vector of variables
    #X0 = np.array() #np.ones(4)
    #X1 = np.zeros(4)

    ri = np.hstack((X0, X1))
    
    n_sats = len(svs)
    n_vars = 4
    n, m = 2*n_sats+1, 2*n_vars

    delta,i = 1,0
    while (norm(delta)>1e-8 and i<max_iters):
        
        """ MODELO DISTANCIA.
        OJO INVERTIR ESTO PARA CAMBIAR LA DIRECCION DEL VECTOR DISTANCIAS
        """
        d_model = np.linalg.norm(ri[4:7] - ri[:3])
        Ad = np.hstack(((ri[4:7] - ri[:3])/d_model))#[:,None]))
        
        """ MODELO PSEUDORANGES.
        """
        #rhos_a, pranges_a = predict_pseudoranges(ri[:4],  svs, svs_clocks)
        rhos_a, pranges_a = predict_pseudoranges_iono(ri[:4],  svs, svs_clocks, S11, dat,
                                                      apply_iono_model=iono_model)
        rhos_b, pranges_b = predict_pseudoranges_iono(ri[4:8], svs, svs_clocks, S12, dat,
                                                      apply_iono_model=iono_model)
        
        """ prs y pranges construction."""
        prs     = np.vstack((prs1.reshape(-1,1), prs2.reshape(-1,1), np.array(d_est)))
        pranges = np.vstack((pranges_a.reshape(-1,1), pranges_b.reshape(-1,1), d_model))
        
        """ A Matrix construction.
        """
        # get cosine director from each receiver to sats
        Aa = np.hstack(((ri[:3] - svs)/rhos_a[:,None], np.ones((n_sats, 1))))
        Ab = np.hstack(((ri[4:7]- svs)/rhos_b[:,None], np.ones((n_sats, 1))))
        
        A = np.zeros((n, m))
        # component asociated to position of local receiver
        A[:n_sats,:n_vars:] = Aa
        A[:n_sats, n_vars:] = np.zeros(np.shape(A[:n_sats,n_vars:]))
        # component asociated to position of remote receiver
        A[n_sats:-1,n_vars:] = Ab
        A[n_sats:-1,:n_vars] = np.zeros(np.shape(A[n_sats:-1,:n_vars]))
        A[-1,:] = np.hstack((np.zeros(n_vars), Ad[:3], np.zeros(1)))# np.hstack((zeros(n_vars), Ad[:3], np.zeros(1)))
        
        
        """ LS computation.
        """
        b     = prs - pranges
        delta =  np.linalg.pinv(A.T.dot(A)).dot(A.T).dot(b)
        ri    += delta.flatten('F')
        i     += 1
        
    return ri, A, b, delta

def simple_test(data_common, pareja, rec_pos1, rec_pos2, Obser="C1", local=None, remote=None, dc=None, apply_iono_model=None, res_plot=300):
    
    Results = {}
    r = []
    
    print ("\t\t Rinex Observation: "),
    for i in range(len(data_common)):
        if i % res_plot == 0:
            print i,
            dat = data_common.iloc[i]
            #print dat.keys()
            gps_sow = dat.gps_sow
            
            dreal,_,_ = d_teta_calc(rec_pos1, rec_pos2)
            
            if len(dat['csats'])>=4:

                start_time = time.time()

                # Metodos desarrollados
                if local==None:
                    localstd,_,_,_ = compute_least_squares_position(dat['prns_pos_local'],dat['prns_clockbias_local'], 
                                                                dat[Obser+'_local'])
                if remote==None:
                    remotestd,_,_,_ = compute_least_squares_position(dat['prns_pos_remote'],dat['prns_clockbias_remote'], 
                                                                dat[Obser+'_remote'])
                if dc== None:
                    dc, teta, dc_vec = d_teta_calc(localstd[:3], remotestd[:3])

                elstd, erstd = np.linalg.norm(localstd[:3] - rec_pos1), np.linalg.norm(remotestd[:3] - rec_pos2)
                
                ri,_,_,_ = coop_ls_solver(dat["gps_sow"],
                                          dat["prns_pos_local"], 
                                          dat['prns_clockbias_local'],
                                          dat[Obser+'_local'],
                                          dat[Obser+'_remote'],
                                          dc,
                                          dat['S1_local'],
                                          dat['S1_remote'],
                                          dat,
                                          #X0=np.array(local),
                                          #X1=np.array(remote),
                                          iono_model=apply_iono_model)
                local = ri[:3]
                remote = ri[4:7]
                coop_t = time.time() - start_time

                loc, loc_err = local,  np.linalg.norm(local  - rec_pos1)
                rem, rem_err = remote, np.linalg.norm(remote - rec_pos2)

            r.append([gps_sow, rec_pos1, rec_pos2, dreal, dc, localstd, remotestd, elstd, erstd, loc, rem, loc_err, rem_err, coop_t, ri, data_common])    
    names = ["gps_sow" ,"real_pos_local", "real_pos_remote", "dreal", "d_calc", "stdlone_pos_local", "stdlone_pos_remote", "stdlone_err_local", "stdlone_err_remote", 
             "coop_pos_local", "coop_pos_remote", "coop_err_local", "coop_err_remote", "coop_time[s]", "rc_sol", "data_common"]
    
    #Results.setdefault('cooperative', {})[str(pareja)] = pd.DataFrame(r, columns=names)
    
    return pd.DataFrame(r, columns=names)# Results
