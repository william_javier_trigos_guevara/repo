# -*- coding: utf-8 -*-
from gps_library import *

def Klobuchar(A, E, time, rx_pos, alphas, betas):
    ####klobuchar  model

    lat, lon, alt = ecef2lla(X=rx_pos[0], Y=rx_pos[1], Z=rx_pos[2])
    C = 2.99792458e8

    ######bla bla
    phi = (0.0137/(E-0.11))-0.022
    psi = lat+phi*np.cos(A)#2
    if psi > 0.416:
        psi = psi+0.416
    elif psi < -0.416:
        psi = psi-0.416

    lampda = lon+(phi*np.sin(A))/np.cos(psi)#3
    phi_m = lampda+0.064*np.cos(psi-1.617)#4
    t = 43200*lampda + time#.getSecondOfDay()#5
    if t > 86400:
        t = t-86400
    elif t < 0:
        t = t + 86400
    ##basados en el modelo expuesto en el link, se cambian el exponente de la sumatoria.
    ##http://what-when-how.com/gps/ionospheric-effects-physical-influences-of-gps-surveying-part-2/
    A_I = alphas[0] + alphas[1]*phi_m + alphas[2]*phi_m**2 + alphas[3]*phi_m**3 #6
    if A_I < 0:
        A_I = 0

    P_I = betas[0] + betas[1]*phi_m + betas[2]*phi_m**2 + betas[3]*phi_m**3 #7
    if P_I < 72000:
        P_I = 72000

    X_I = 2*np.pi*(t-50400)/P_I #8
    F = 1+16*(0.53-E)**3#9

    if np.fabs(X_I) > 1.57:#10
        IL1_GPS = ((5e-9)+A_I*(1-(((X_I)**2)/2)+((X_I)**4)/24))*F*C
    elif np.fabs(X_I) < 1.57:
        IL1_GPS = (5e-9)*F*C
    else:
        IL1_GPS = (5e-9)*F*C
    return IL1_GPS#/F
