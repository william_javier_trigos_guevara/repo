
NB_IN="$1.ipynb"
NB_OUT="$2.ipynb"
echo "Input Notebook   = ${NB_IN}"
echo "Output Notebook  = ${NB_OUT}"


# -------------------------------------------------------------------------------
#  SOLO ENVIA MENSAJES SIN ADJUNTOS
# -------------------------------------------------------------------------------
#echo -e "Script Name: $SCRIPTNAME $NB_IN $NB_OUT\n\t 
#		command executed: \n\t${output[*]}. \n
#		Execution start at: $start_timestamp \n
#		Execution finish at: $end_timestamp \n\n
#		Mail generated and with exim4 server and mutt application from Zenbook machine" | mail $MAILADDRESS -s "Script Name: $SCRIPTNAME ${exit_status}  "

# -------------------------------------------------------------------------------
#  ENVIA MENSAJES CON ADJUNTOS
# -------------------------------------------------------------------------------
# Adjuntando archivos
# https://unix.stackexchange.com/questions/329050/how-to-send-mail-with-multiple-attachments
# mutt -s "now" -a Red_Analisis.pdf Makefile -- "william.trigos@gmail.com" <<< "Hola"
from="malliwi88@gmail.com"
to="william.trigos@gmail.com"
subject="Script Name: $SCRIPTNAME ${exit_status}"
body="Script Name: $SCRIPTNAME $NB_IN $NB_OUT\n\t 
		command executed: \n\t${output[*]}. \n
		Execution start at: $start_timestamp \n
		Execution finish at: $end_timestamp \n\n"
		#Mail generated and with exim4 server and mutt application from Zenbook machine"

declare -a attachments
attachments=( "$2.ipynb" "$2.tex" )
 
declare -a attargs
for att in "${attachments[@]}"; do
  #attargs+=( "-a"  "$att" )  
  echo "${att}"
  attargs+=( "$att" )  
done

eval "mutt -s ${subject} -a ${attargs[*]} -- ${to} <<< ${body[*]}"

echo $(pwd)
echo $(ls *tex)
# -------------------------------------------------------------------------------
#  GENERA EL LATEX DEL NOTEBOOK
# -------------------------------------------------------------------------------

gen_latex=( 
'jupyter nbconvert ${NB_OUT}'
'--to latex --template Template_report.tplx'
)	
#eval "${gen_latex[*]}"

rm *.gz *.out *.aux *.toc