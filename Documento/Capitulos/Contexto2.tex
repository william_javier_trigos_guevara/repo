\chapter{Marco Teórico} 
\label{sec:marcoteorico}

Este capítulo tiene como objeto introducir los conocimiento sobre sistemas de posicionamiento Global (GPS) y los aspectos técnicos relacionados con la señales y su recepción en dispositivos de posicionamiento (receptores).



\section{Satellite Positioning}\label{posicionamiento-satelital}

This section presents concepts related to satellite positioning systems, with the purpose of defining a common framework of reference for the reader. This framework lays down the foundation for our approach to cooperative positioning algorithm differential (WLS-DCP) supported by weighted least squares.

\subsection{Global satellite positioning systems}\label{sistema-global-de-posicionamiento-satelital}

The principles behind positioning systems (such as GPS) can be summarized as follows:\newline

\textit{If the distance from three satellites in space to a fixed point on the surface of the Earth (a GPS receiver) is known, along with the position of the satellites at the time of the transmission of a satellite to the receiver, the position of the receiver can be determined through the application of trigonometric concepts, algebraics, about a specific coordinate system \cite{Thompson_1998}}.\newline

Satellite navigation systems are characterized by a unidirectional flow of information from the satellite to the receiver. Hence, the receiver is responsible for receiving and processing these signals to determine its position respecting to the satellites, whose information was obtained at a given point in time.

The key issue behind positioning systems is accurately establishing the distance for each satellite. The final objective is to determine the receiver position as precisely as possible. For this purpose, the observational models work as tools to describe the phenomena involved in the signal voyage. 

More specifically, the observable value measured by the receiver, in terms of clock cycles of satellital signal, can be described through a physical model such as the duration of signal transmission multiplied by the speed of light.


\subsection{The Pseudorange}\label{el-pseudorango-y-su-definiciuxf3n}

Satellite positioning systems work because of the continuous transmission of radio frequency signals through the ionosphere and the troposphere. In the specific case of satellite positioning system (GPS), transmitted signals are shaped by two carrier signals, the code and a navigation  message. These signals are acquired by an antenna capable of perceiving the frequencies in which they are transmitted from the satellites, 1227.60 MHz and 1575.42 MHz.

From the acquisition of these signals, the receiver is able to process and obtain a value representing the distance between its location and the satellite that sent the signal. These numerical values are called observable measurement or pseudorange.

The pseudorange observable is a basic and fundamental concept in GPS. In essence, it is the measured physical range between a satellite and receiver, including  errors attributable to receiver clock bias and error associated with phenomena that cause dispersion and signal delay.

Diversely, to determine the position of a receiver it is necessary to represent the locus of the satellites taking as reference both the coordinates of satellites and the distance to the position of the receiver. This permits us to find the intersection of the geometric planes of the satellites; as it can be seen in the figure \ref{fig:Trilateracion}.

\begin{figure}[H]
    \centering
\includegraphics[width=0.4\textwidth, height=0.4\paperheight,keepaspectratio]{Figs/Trilateracion.png}
    \caption{Figure \ref{fig:Trilateracion} Geometric representation of the concept behind of trilateration}
    \label{fig:Trilateracion}
\end{figure}

Besides, the intersection of the geometrical locations of all the visible satellites hardly coincides in a single point, owing to the fact that the signals that travel from the satellite are affected by the dispersion of the ionosphere and another error sources. This generates signal delays and, consequently, errors in the determination of the distance between each satellite and the receiver. 

However, the main source of error in the observable model is the effect caused by an innacurate local oscillator, which causes a synchronization error with respect to satellital system time reference (GPS time). In order to minimize this error, it is necessary that at least four satellites are visible to the receiver. 


The observable value measured by the receiver, can be algebraically described through a physical model such as the time difference between the signal emission and its reception, multiplied by the speed of light, as expressed in the equation \ref{modelo Observables}.

\begin{equation}
  P_{r}^{s} = (T_r - T_s)*c + \eta
  \label{modelo Observables}
\end{equation}

\noindent Where:

\begin{itemize}
    \item $P_{r}^{s}$ is the value of the observable pseudorange from the receivero $r$ to the satellite $s$
    \item $T_r$ is a time known to the user at the time of the reception
    \item $T_s$ is the time of sending the signal from the satellite, which can be obtained from a navigation message
    \item $c$ is the value of the speed of light in a vacuum
    \item $\eta$ is a term in length units that represents all error sources that in measurement of $P_{r}^{s}$. 
\end{itemize}

However the above model is very basic to represent the different phenomena that occur during the journey of the signal through the atmosphere and avoid determining the distance between the satellite and receiver accurately.

Some pseudorange models proposed in the literature, usually represent the errors associated with orbital satellites as a constant, after considering that the instantaneous change in the position of the satellites is slower with respect to the sampling time of satellite signals at the receiver. In the same way, the ionospheric delay can be modeled using constants or functions with slow exchange rates, to represent the variation of the content of electrons from one measurement to another. 

These models seek to represent some observables in a better way, looking for expressing all phenomena the best possible way, which significantly increases their complexity. This is the case of the generalized equation for the  pseudo-range observable, in which most sources of error affect the satellite signal are taken into consideration \ref{eq:Ec2}.

\begin{equation}
P_{r}^{s}(t) = \rho_{r}^{s}(t) +c*\tau_{r}(t) -c*\tau^{s}(t) -d_{iono}^{s} + d_{trop}^{s} + M_{r, P}^{s}(t) + \eta
\label{eq:Ec2}
\end{equation}

\noindent Where:

\begin{itemize}
    \item $P_{r}^{s}(t)$ is the observable pseudo-range, with all mistakes related to signal adquiring process.
    \item $\rho_{r}^{s}(t)$ represents the geometrical range between receiver and the satellite in time t.
    \item $c$ is the speed of light.
    \item $\tau_{r}$ is the error related to bad time synchronizing in receiver respecting to GPS Time, in time t.
    \item $\tau^{s}$ is the error related to bad time synchronizing in the satellite respecting to GPS Time, in time t.
    \item $d_{iono}^{s}$ is the error related to ionospheric delay.
    \item $d_{trop}^{s}$ is the error related to tropospheric delay.
    \item $M_{r,P}^{s}(t)$ is the error related to the multipath phenomen in time t.
    \item $\eta$ is the error during measurement of the pseudorange $P_{r}^{s}$ due to the noise in receiver, in time t. \newline
\end{itemize}

Approaches and developments in the literature show that it is possible to support the empirical behavior and characteristics of the phenomena to mitigate the complexity of the observation models. Thus, in some cases, it is possible to represent errors associated with satellite orbitals as a constant, when it is considered that the instantaneous change in the position of the satellite is slower compared to the sampling time of signals in the receiver. Similarly, the ionospheric delay can be modeled by constants or functions with slow change rates, representing the variation of the electron content between one measurement or another \cite{klobuchar2003comparative}.

A further consideration has to do with the basis for the differential positioning approach, where the ionospheric delay is considered  similar between nearby receptors and consecutive measurements (time to period). This is evidenced in the case studies in \cite{el1994effect}, \cite{blewitt1997basics}, which show that the ionospheric delay for multiple receivers located relatively close between themselves \(d < 200[km]\), may assume that the ionospheric delay is for all of them. %////REVISAR ESTA ÚLTIMA PARTE/////

    \subsection{Positioning Techniques.}\label{tuxe9cnicas-de-posicionamiento.}

Although many of the techniques of positioning make use of the pseudo-range model, not all of them are able to compensate the effects associated with the error sources. 

\subsubsection{Differential}\label{diferencial.}

Also known as models of baseline positioning. It is a technique that consists of referencing an object with respect to nodes whose location is known. 

\begin{figure}[H]
\begin{center}
\includegraphics[width=0.3\textwidth, height=0.3\paperheight,keepaspectratio]{Figs/rango_satelites.png}
\caption{Figure \ref{fig:2RX1sat} Geometrical representation of Single-difference.}
\label{fig:2RX1sat}
\end{center}
\end{figure}

The differential techniques are very common between pairs of receivers when distances between each other do not exceed 200km. 

The geometrical representation of the single-difference mathematical model shown in figure \ref{fig:2RX1sat}, uses the difference between the observable of two receivers (\(R_{x1}\), \(R_{x2}\)) with respect to a common satellite at the same time (simultaneously), as shown in the equation below \label{eq:Ec4}

\begin{equation}
    \begin{aligned}
        \bigtriangleup P_{AB}^{s} 
        & = P_{B}^{s} - P_{A}^{s}\\
        & =(\rho_{B}^{s}-\rho_{A}^{s}) + c*(\tau_{B} - \tau_{A}) + (\epsilon_{B}^{s} - \epsilon_{A}^{s})\\
        & = \bigtriangleup \rho_{AB}^{s} + c*\bigtriangleup (\tau_{AB}) + \bigtriangleup(\epsilon_{AB}^{s})
    \label{eq:Ec4}
    \end{aligned}
\end{equation}

The pseudorange models for receivers $A$ and $B$ are presented in the equation \label{eq:Ec3}, where the terms \(d_ {iono} ^ {s} \) and \(d_ {trop} ^ {s} \) are assumed to be similar for both receivers, according to what is stated in \cite {el1994effect} and \cite {blewitt1997basics}; consequently, the sources of error cancel each other out.
 
\begin{equation}
    \begin{aligned}
        P_{A}^{s}(t) = \rho_{A}^{s}(t) +c*\tau_{A}(t) - c*\tau^{s}(t) -d_{iono}^{s} + d_{trop}^{s} + \epsilon_{A}^{s}\\
        P_{B}^{s}(t) = \rho_{B}^{s}(t) +c*\tau_{B}(t) - c*\tau^{s}(t) -d_{iono}^{s} + d_{trop}^{s} + \epsilon_{B}^{s}
    \label{eq:Ec3}
    \end{aligned}
\end{equation}
 

\begin{figure}[H]
    \centering
    \adjustimage{max size={0.4\linewidth}{0.4\paperheight}}{Figs/SD_Rhedgecock.png}
    \caption{Figure \ref{fig:SD_Rhedgecock} Geometric interpretation of the single-differencing operation based on \cite{hedgecock2014precise}}
    \label{fig:SD_Rhedgecock}
\end{figure}

According to Rhedgecock \cite{hedgecock2014precise}, the difference between the pseudoranges obtained in two receivers with respect to the same satellite in a single line of sight, is equivalent to the distance vector projection between receivers in the satellite direction, as shown the figure \label{fig:SD_Rhedgecock}.

So, as the distance between the receivers and the satellite is much larger than the distance between receivers, the distance vector projection can be considered equal for both receivers. In this way, Rhedgcock's approach is useful for cosines direction of distance between pairs of receivers, within the WLS-DCP approach presented in the next section. 

\subsubsection{Cooperative}\label{cooperative.}

Contrastingly, the approach of cooperative positioning is aimed at using data exchanges between GNSS receivers, as an alternative to improve positioning accuracy in each one of the interacting devices.

This approach provides the foundation for the hereby study, whose main contribution is presented in the next section.  