import numpy as np
import sys
import argparse
from numpy import int64, float64
from utils_gnss import *

def drange(start, stop, step):
    values = []
    r = start
    if start <= stop:
        while r <= stop:
            values = values + [r]
            r += step
    else:
        while r >= stop:
            values = values + [r]
            r += step
    return values

def ionex_grid(ionex_t):
    #Initialize grid components
    if ( (ionex_t['LATo'] == 0) | (ionex_t['LATn'] == 0) | (ionex_t['DLAT'] == 0)
       | (ionex_t['LONo'] == 0) | (ionex_t['LONn'] == 0) | (ionex_t['DLON'] == 0)):
        print "Incomplete grid description"
        return
    else:
        V_LAT = drange(ionex_t['LATo'][0,0],ionex_t['LATn'][0,0],ionex_t['DLAT'][0,0])
        V_LON = drange(ionex_t['LONo'][0,0],ionex_t['LONn'][0,0],ionex_t['DLON'][0,0])
    return V_LAT, V_LON

def ReadIonex(filename):
    try:
        fid=open(filename,'r+')
    except IOError as e:
        print "IONEX Ionosphere data file not found or permission denied"
        return

#    # Version and satellite constellation detection
#    line = fgetl(fid);
#    ver = str2double(line(6));
#    SAT_CONS = strtrim(line(41:43));
#    # Checking IONEX version compatibility
#    if ~(ver==1):
#        print 'Unknown IONEX Ionosphere version or file'); 
#        return;
#    end

    ## Loop through the file

    nSat = 1
    ionex_t_type = np.dtype([
            # Header
                ('INTERVAL','int64',(1,)), ('NUMMAPS','int64',(1,)),
                ('MAPPING','str',6), ('ELCUTOFF','float64',(1,)),
                ('BASER','float64',(1,)), ('MAP_D','int64',(1,)),
                ('HEIGHT','float64',(1,)), ('LATo','float64',(1)),
                ('LATn','float64',(1)), ('DLAT','float64',(1)),
                ('LONo','float64',(1)), ('LONn','float64',(1)),
                ('DLON','float64',(1)), ('EXP','int64',(1,))
        ])

    ionex_t = np.array(np.zeros((1,1)),ionex_t_type)
    ionex_epochmap = np.zeros((0,8))
    
    ## Parse header
    with fid as object:
        for line in object:
            if len(line)<80: line[len(line):80] == '0'
            
            # To check the end of the header
            if line[60:73] == 'END OF HEADER': break

            if ( (line[69:73] == 'FIRS') | (line[69:73] == 'LAST') ):
                exec(line[69:73]+'_year  =int(line[ 2: 6])')
                exec(line[69:73]+'_month =int(line[10:12])')
                exec(line[69:73]+'_day   =int(line[16:18])')
                exec(line[69:73]+'_hour  =int(line[22:24])')
                exec(line[69:73]+'_minute=int(line[28:30])')
                exec(line[69:73]+'_second=int(line[34:36])')

            if line[60:68] == 'INTERVAL':
                ionex_t['INTERVAL'] = int64(line[0:6])
            if line[60:69] == '# OF MAPS':
                ionex_t['NUMMAPS']  = int64(line[0:6])
            if line[60:67] == 'MAPPING':
                ionex_t['MAPPING'] = line[0:6].strip()
            if line[60:71] == 'ELEVATION C':
                ionex_t['ELCUTOFF'] = float64(line[0:8])
            if line[60:71] == 'BASE RADIUS':
                ionex_t['BASER'] = float64(line[0:8])
            if line[60:69] == 'MAP DIMEN': 
                ionex_t['MAP_D'] = int64(line[0:6])
            if line[60:71] == 'HGT1 / HGT2':
                ionex_t['HEIGHT'] = float64(line[0:8])
            if line[60:71] == 'LAT1 / LAT2': 
                ionex_t['LATo'] = float64(line[0:8])
                ionex_t['LATn'] = float64(line[8:14])
                ionex_t['DLAT'] = float64(line[14:20])
            if line[60:71] == 'LON1 / LON2': 
                ionex_t['LONo'] = float64(line[0:8])
                ionex_t['LONn'] = float64(line[8:14])
                ionex_t['DLON'] = float64(line[14:20])
            if line[60:68] == 'EXPONENT': 
                ionex_t['EXP'] = int64(line[0:6])

            if ionex_t['MAP_D'] == 3:
                print 'Loadionex cannot read 3D IONEX maps'
                return
        ## End of Parse header

        ## Initialize grid components
        if ( (ionex_t['LATo'] == 0) | (ionex_t['LATn'] == 0) | (ionex_t['DLAT'] == 0)
           | (ionex_t['LONo'] == 0) | (ionex_t['LONn'] == 0) | (ionex_t['DLON'] == 0)):
            print "Incomplete grid description"
            return
        else:
            max_lat = int64((ionex_t['LATn'][0,0] - ionex_t['LATo'][0,0])/ionex_t['DLAT'][0,0])+1
            max_lon = int64((ionex_t['LONn'][0,0] - ionex_t['LONo'][0,0])/ionex_t['DLON'][0,0])+1

            ionex_grid = np.array(np.zeros((1,1)), np.dtype([('V_LAT',float64,max_lat), ('V_LON',float64,max_lon)]))

            ionex_grid['V_LAT'] = drange(ionex_t['LATo'][0,0],ionex_t['LATn'][0,0],ionex_t['DLAT'][0,0])
            ionex_grid['V_LON'] = drange(ionex_t['LONo'][0,0],ionex_t['LONn'][0,0],ionex_t['DLON'][0,0])

            
            looplines = int64(np.ceil( (ionex_grid['V_LON'].shape[2])/16 ))
            
            # Initialize Grid data storage
            ionex_ionomap  = np.zeros((0,max_lat,max_lon))

        ## Parse body
        for line in object:
            
            if (type(line) is not str): break
            if len(line)<80: line[len(line):80] == '0'

            # To check the end of the header
            if line[60:71] == 'END OF FILE': break

            # To check number of map and its kind
            if line[60:65] == 'START':
                kind = line[69:72]
                num  = int64(line[0:6])-1
                lat  = 0   #Reseting the latitude pointer
            if kind == 'TEC':
                # To generate XXX_EPOCHMAP(d,e) matrix (epochs' map)
                if line[60:75] == 'EPOCH OF CURREN':
                    ionex_epochmap = np.append(ionex_epochmap,np.zeros((1,8)),axis=0)
                    ionex_ionomap  = np.append(ionex_ionomap, np.zeros((1,max_lat,max_lon)), axis = 0)
                    GPSweek,GPSsecond,doy = utc2gpst( int64(line[2:6]),int64(line[10:12]),int64(line[16:18]),int64(line[22:24]),int64(line[28:30]),int64(line[34:36]),0)
                    exec('ionex_epochmap[num,0] = int64(line[ 2: 6])')
                    exec('ionex_epochmap[num,1] = int64(line[10:12])')
                    exec('ionex_epochmap[num,2] = int64(line[16:18])')
                    exec('ionex_epochmap[num,3] = int64(line[22:24])')
                    exec('ionex_epochmap[num,4] = int64(line[28:30])')
                    exec('ionex_epochmap[num,5] = int64(line[34:36])')
                    exec('ionex_epochmap[num,6] = GPSweek')
                    exec('ionex_epochmap[num,7] = GPSsecond')

                # Loop to generate XXX_IONOMAP(a,b,c)
                if line[60:73] == 'LAT/LON1/LON2':
                    line = object.next()
                    lon  = 0  # Reseting the longitude pointer
                    for ii in range(looplines+1):
                        k=0
                        if ii < looplines: inlineobs = 16   # Getting inline observations number.
                        else: 
                            inlineobs = ionex_grid['V_LON'].shape[2] - lon

                        for jj in range(inlineobs):
                            exec('ionex_ionomap[num,lat,lon] = float64(line[k:5+k])')
                            k   += 5 # Moving inline pointer to read next TEC observable
                            lon += 1 # Increasing longitude pointer

                        if ii < looplines: line = object.next()

                    lat += 1
    
    fid.close()
        
    return ionex_t, ionex_grid, ionex_epochmap, ionex_ionomap


def find_mapnum(time, ionex_t, ionex_epochmap):
    INTERVAL = ionex_t['INTERVAL']
    other_mapnum = 0
    delta = 0
    if   time < ionex_epochmap[ 0,-1]: mapnum = 0
    elif time > ionex_epochmap[-1,-1]: mapnum = ionex_epochmap.shape[0] - 1
    else:
        mapnum = np.where((ionex_epochmap[:,-1] - np.mean(time)) <= 0)[0][-1]
        ## Finding second MAP NUMBER
        # Added by MSG to correct jumps due to the discontinuity in TEC values
        tec_epochs = ionex_epochmap.shape[0]
        dist = ((time)-(ionex_epochmap[mapnum,-1]+INTERVAL/2))

        if (mapnum==0) & (dist<=0) :
            other_mapnum=1
        elif (dist>=0) & (dist<=(INTERVAL/2)):
            other_mapnum = mapnum+1  # It's the next TEC value
            delta  = dist/INTERVAL 
        elif ~(mapnum==0) & (dist<0) & (dist>=(-INTERVAL/2)):
            other_mapnum = mapnum-1 # It's the past TEC value
            mapnum, other_mapnum = np.sort([mapnum, other_mapnum])
            delta = ((dist)+INTERVAL)/INTERVAL
        elif mapnum==tec_epochs:
            if dist>0:
                other_mapnum=0 
    return mapnum, other_mapnum, delta


def tec_from_ionex(usrxyz, svxyz, time, frqc, ionex_t, iono_grid, ionex_epochmap, ionex_ionomap):

    Re = 6367444.657

    HEIGHT   = ionex_t['HEIGHT']
    EXP      = ionex_t['EXP'][0][0][0]
    V_LAT    = iono_grid['V_LAT'][0,0]
    V_LON    = iono_grid['V_LON'][0,0]

    m = svxyz.shape[0]

    mapnum = np.zeros((m,3)).astype(int)
    for i in range(time.shape[0]):
        mapnum[i] = find_mapnum(time[i], ionex_t, ionex_epochmap)
    
    #CalculateIPP output: ipp_xyz, lat_ipp, lon_ipp, el, az, F
    ipp_data = CalculateIPP(svxyz, usrxyz*np.ones((m,3)))
    lat_ipp  = ipp_data[:,3]*180/pi
    lon_ipp  = ipp_data[:,4]*180/pi
    lat_ipp_min = np.zeros((m,1)).astype(int)
    lat_ipp_max = np.zeros((m,1)).astype(int)
    lon_ipp_min = np.zeros((m,1)).astype(int)
    lon_ipp_max = np.zeros((m,1)).astype(int)
    ## Grid interpolation algorithm
    # This loop finds the pointers of the V_LAT and V_LON vectors in order to
    # find the 4 TECUs IONEX grid nearest to ionospheric pierce point

    for i in  range(m):
        if V_LAT[0]>=0:
            lat_ipp_min[i] = np.where(lat_ipp[i] >= V_LAT)[0][ 0]
            lat_ipp_max[i] = np.where(lat_ipp[i] <  V_LAT)[0][-1]
        if V_LAT[0]<0:
            lat_ipp_min[i] = np.where(lat_ipp[i] >= V_LAT)[0][-1]
            lat_ipp_max[i] = np.where(lat_ipp[i] <  V_LAT)[0][ 0]
        if V_LON[0]>=0:
            lon_ipp_min[i] = np.where(lon_ipp[i] >= V_LON)[0][ 0]
            lon_ipp_max[i] = np.where(lon_ipp[i] <  V_LON)[0][-1]
        if V_LON[0]<0:
            lon_ipp_min[i] = np.where(lon_ipp[i] >= V_LON)[0][-1]
            lon_ipp_max[i] = np.where(lon_ipp[i] <  V_LON)[0][ 0]

    # The 4th TECUs IONEX grid nearest to ionospheric pierce point already
    # mentioned. They come from TEC_IONOMAP ('loadionex.m')
    E00 = ionex_ionomap[mapnum[:,0:1],lat_ipp_min,lon_ipp_min]
    E10 = ionex_ionomap[mapnum[:,0:1],lat_ipp_min,lon_ipp_max]
    E01 = ionex_ionomap[mapnum[:,0:1],lat_ipp_max,lon_ipp_min]
    E11 = ionex_ionomap[mapnum[:,0:1],lat_ipp_max,lon_ipp_max]

    # p and q means how many near or far are the Exx (TECUs) to or from the
    # ionospheric pierce point
    q = (V_LAT[lat_ipp_min]-lat_ipp.reshape(V_LAT[lat_ipp_min].shape))/(V_LAT[lat_ipp_min]-V_LAT[lat_ipp_max])
    p = (V_LON[lon_ipp_min]-lon_ipp.reshape(V_LON[lon_ipp_min].shape))/(V_LON[lon_ipp_min]-V_LON[lon_ipp_max])

    # It's the grid interpolation algorithms and it follows this equation:
    # E = (1-p)(1-q)E(0,0) + p(1-q)E(1,0) + q(1-p)E(0,1) + pqE(1,1)
    E = (1-p)*(1-q)*E00 + p*(1-q)*E10 + q*(1-p)*E01 + p*q*E11

    
#    # Storing next or past TEC values
    # Storing next or past TEC values
    for i in range(time.shape[0]):
        if ~(mapnum[i,1:2] == 0): 
            E00_other = ionex_ionomap[mapnum[i,1:2],lat_ipp_min[i],lon_ipp_min[i]]
            E10_other = ionex_ionomap[mapnum[i,1:2],lat_ipp_min[i],lon_ipp_max[i]]
            E01_other = ionex_ionomap[mapnum[i,1:2],lat_ipp_max[i],lon_ipp_min[i]]
            E11_other = ionex_ionomap[mapnum[i,1:2],lat_ipp_max[i],lon_ipp_max[i]]

            E_other = (1-p[i])*(1-q[i])*E00_other + p[i]*(1-q[i])*E10_other + q[i]*(1-p[i])*E01_other + p[i]*q[i]*E11_other

            if mapnum[i,1:2] < mapnum[i,0:1]:
                E[i] = E_other*(1-mapnum[i,2:3]) + E[i] * mapnum[i,2:3]

            else:
                E[i] = E[i]*(1-mapnum[i,2:3]) + E_other * mapnum[i,2:3]


    E = 1.0 * E * pow(10,EXP)

    ## Ionospheric delay
    # Depend on the elevation between the user and satellite the ionosphere
    # affects more or less. Fpp is a factor which measurements it

    # ARiono checks the ionospheric delay in meters. It follows this equation:
    # ASiono = Fpp40.3TEC / f^2

    f = frqc*1e6
    K = 1.6240545787086483*(10**-17)

    #delta =  ipp_data[:,7:8] * 40.3 * E * 1e16  / f**2
    delta =  ipp_data[:,7:8] * E * 1e16* K
    
    return E, delta
        

def Read_Ionex_Bias(filename):
    try:
        fid=open(filename,'r+')
    except IOError as e:
        print "IONEX data file not found or permission denied"
        return
    with fid as object:
        first_line = fid.readline()
        ver  = int(first_line[5])
        if ver not in [1]:
            print "Unknown RINEX Observation version or file"
            return
        
        fid.seek(0)
        sat_bias = pd.DataFrame(columns = ["PRN","SV_TGD"])
        rx_bias  = pd.DataFrame(columns = ["STATION","RX_BIAS"])
        for line in object:
            if len(line)<80: line = line+'0'*(80-len(line))
            if line[60:73] == 'END OF HEADER': break
            # Only RINEX2
            if line[60:76] == 'PRN / BIAS / RMS':
                if line[3:4] == "G":
                    prn  = cast_string_int(line[4:6])
                    bias = cast_string_float(line[6:16])*1e-9
                    sat_bias = sat_bias.append({"PRN":prn,"SV_TGD":bias}, ignore_index=True)
                if line[3:4] == " ":
                    prn  = cast_string_int(line[4:6])
                    bias = cast_string_float(line[6:16])*1e-9
                    sat_bias = sat_bias.append({"PRN":prn,"SV_TGD":bias}, ignore_index=True)
            if line[60:80] == 'STATION / BIAS / RMS':
                station = line[6:10]
                bias = cast_string_float(line[26:36])*1e-9
                rx_bias = rx_bias.append({"STATION":station,"RX_BIAS":bias}, ignore_index=True)
                

    fid.close()
        
    return sat_bias, rx_bias
