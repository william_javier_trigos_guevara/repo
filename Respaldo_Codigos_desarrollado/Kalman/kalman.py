# -*- coding: utf-8 -*-

from __future__ import division
import os
from os import (listdir, getcwd, system)
from os.path import (basename, splitext, abspath,
                     dirname, split, isfile)
from re import search, findall, compile
import re, urllib2, sys, copy, time

import gpstk
import numpy as np
import pandas as pd
from numba import autojit
from numpy.linalg import norm

import matplotlib
import matplotlib.pyplot as plt
from IPython.display import HTML
from IPython.display import display, clear_output, Javascript, Latex

#matplotlib.style.use('ggplot')

#%run -i utils.py
#%run -i utils_enu.py



####################################################################################
##### scripts ######
####################################################################################
def Analisis(data, rec_pos, results, method,
             apply_kalman=False, apply_Iono="",
             resolution=200, max_err_pos=200, sig=1):

    results[method] = {"pos": [],
                       "err": [],
                       "dop": [],
                       "gps_time":[]
                       }

    for i, o in data.iterrows():
        if i % resolution == 0:
            pranges = o[codigo]
            codes = o[fase]
            gps_time = o.gps_sow
            #print gps_time

            idxs = np.arange(len(pranges))
            p1 = np.array(pranges)[idxs]
            l1 = np.array(codes)[idxs]
            pos = np.array(o.prns_pos)[idxs]
            clocks = np.array(o.prns_clockbias)[idxs]
            clk_corr = np.array(o.gps_time_corr)
            p1, pos = pranges[idxs], o.prns_pos[idxs]
            #clocks = o.prns_clockbias[idxs]

            # metodos
            #raim_computed_pos   = compute_raim_position(o.gps_week, o.gps_sow, o.prns, o.prns_pos, o.P1, bcestore)
            position, A, b, _ = compute_least_squares_position(pos, p1, clocks, clk_corr)

            err = np.linalg.norm(position[:3] - rec_pos)
            if err < max_err_pos:
                results[method]["gps_time"].append(gps_time)
                results[method]["pos"].append(position)
                results[method]["err"].append(err)
                results[method]["dop"].append(get_dop(A, sigma=sig))

            if apply_kalman:

                if (len(results[method]["pos"]) > 1):
                    pos_k, cov_k = kalman1(results[method]["pos"], results[method]["dop"])

                    err_k = np.linalg.norm(pos_k[-1][:3] - rec_pos)
                    if err_k < max_err_pos:
                        # obtener el nombre de la funcion ejecutada
                        k_filter = kalman1.__name__
                        key = "{0}_{1}".format(method, k_filter)

                        if not (key in results.keys()):
                            results[key] = {"pos": [],
                                           "err": [],
                                           "dop": [],
                                           "gps_time":[]
                                           }
                            continue
                        else:
                            results[key]["gps_time"].append(gps_time)
                            results[key]["pos"].append(pos_k)
                            results[key]["err"].append(err_k)
                            results[key]["dop"].append(cov_k)
                        
    return results

def Analisis2(data, rec_pos, results,
              Lambda = 0.0,
              apply_kalman=False, apply_iono_models=[""],
              resolution=200, max_err_pos=200, sig=1):

    for method in results.keys():
        for iono_model in apply_iono_models:

            if iono_model != "":
                algortihm = method + "_iono_" + iono_model
                #print algortihm, apply_kalman
                run_method(data, rec_pos, results, algortihm,
                           apply_kalman=apply_kalman,
                           apply_iono=iono_model,
                           resolution=resolution, max_err_pos=max_err_pos, sig=sig)

            else:
                #print method, apply_kalman
                run_method(data, rec_pos, results, method,
                           apply_kalman=apply_kalman,
                           apply_iono="",
                           resolution=resolution, max_err_pos=max_err_pos, sig=sig)

    return results

def run_method(data, rec_pos, results, method,
               Lambda = 0.0,
               apply_kalman=False, apply_iono="",
               resolution=200, max_err_pos=200., sig=1.):

    """
    Metodo que ejecuta los distintos algoritmos de posicionamiento.

    Examples:
            Examples should be written in doctest format, 
            and should illustrate how to use the function.
            
            >>> Res = run_method(data, rec_pos, Res, "WLS")
            >>> print Res.keys()
            WLS
    --------------------------------------------------------------------------------        
    Args:
        data (pd.Dataframe): Observables Rinex del receptor.
        rec_pos (list): Posicion real del receptor GPS.
        results (dict): diccionario de resultados al que se adicionara 
                        la llave `method` junto a los disccionarios con 
                        llaves pos, err, dop y gps_time, para almacenar 
                        resultados del metodo (`method`) ejecutado.
        method (string):nombre del metodo o algoritmo de posicionamiento
                        que se va a ejecutar.
                        
        Optionals:
            apply_kalman (bool)
            apply_iono   (string)
            resolution   (int)
            max_err_pos  (float)
            sig          (float)

    Returns:
        results (dict): diccionario de resultados pos, err, dop y gps_time, 
                        para el metodo ejecutado.

    .. _PEP 484:
        https://www.python.org/dev/peps/pep-0484/

    """
    results[method] = {"A": [],
                       "b": [],
                       "elv":[],
                       "snr":[],
                       "pos": [],
                       "err": [],
                       "dop": [],
                       "gps_time":[]
                       }
    #o = data.iloc[0]
    codigo = "C1"
    fase = "L1"
    for i, o in data.iterrows():
        if i % resolution == 0:
            pranges = o[codigo]
            codes = o[fase]
            gps_time = o.gps_sow
            #print gps_time,

            dat = data[(data["gps_sow"] == gps_time)]

            svs = np.array(dat["prns_pos"], dtype=pd.Series)[0]
            prs = np.array(dat[codigo], dtype=pd.Series)[0]
            codes = np.array(dat[fase], dtype=pd.Series)[0]
            prns = np.array(dat["prns"], dtype=pd.Series)[0]
            clocks = np.array(dat["prns_clockbias"], dtype=pd.Series)[0]
            clk_corr = np.array(dat["gps_time_corr"], dtype=pd.Series)[0]
            SNR1 = np.array(dat["S1"], dtype=pd.Series)[0]
            elevs = np.array(dat["prns_elev"], dtype=pd.Series)[0]
            elv, azim = get_sat_elv_az(svs, rec_pos)

            if apply_iono:
                # print type(svs), type(prs)
                # print apply_iono
                # print prs
                io = get_ionodelay(rec_pos, gps_time, dat,
                                   codigo=codigo,
                                   apply_iono_model=apply_iono)
                prs += io
                # print prs

            if method.split("_")[0] == "WLS":
                #print method
                position,A,b,d = weight_least_squares_position(svs, prs, clocks, clk_corr,
                                                               lamb=Lambda,
                                                               elevs=elevs, snr=SNR1)
            elif method.split("_")[0] == "LS":
                #print method,
                position,A,b,d = compute_least_squares_position(svs, prs, clocks, clk_corr,
                                                               lamb=Lambda)

            elif method.split("_")[0] == "LS+CP":
                #print method,
                position,A,b,d = least_squares_position_codephase(svs, prs, clocks, clk_corr,
                                                                  lamb=Lambda,
                                                                  codes=codes, elevs=elevs,
                                                                  snr=SNR1)
                position = position[:4]
                A = A[:4, :4]                 
            
            #print position
            err = np.linalg.norm(position[:3] - rec_pos)
            #print err,
            #if i == 1900:
            #    print method, err, gps_time
            if err < max_err_pos:
                results[method]["A"].append(A)
                results[method]["b"].append(b)
                results[method]["elv"].append(elevs)
                results[method]["snr"].append(SNR1)
                results[method]["gps_time"].append(gps_time)
                results[method]["pos"].append(position)
                results[method]["err"].append(err)
                results[method]["dop"].append(get_dop(A, sigma=sig))
                #results[method]["dop"].append(get_dopX(svs, elv, A, b))


            if apply_kalman:
                #print gps_time, err, method, 
                #print len(results[method]["pos"]), len(results[method]["dop"])
                if (len(results[method]["pos"]) > 1):
                    pos_k, cov_k = kalman1(results[method]["pos"], results[method]["dop"])
                    
                    A = results[method]["A"][-2]
                    b = results[method]["b"][-2]
                    snr = results[method]["snr"][-2]
                    X = results[method]["pos"][-2]
                    P = results[method]["dop"][-2]
                    Y = results[method]["pos"][-1]
                    
                    #print np.shape(P), np.shape(Y), np.shape(b)
                    #pos_k, cov_k = kalman3(A, X, Y, P, b, snr)
                    
                    err_k = np.linalg.norm(pos_k[-1][:3] - rec_pos)
                    #err_k = np.linalg.norm(pos_k[:3] - rec_pos)
                    
                    #print method, err, err_k
                    if err_k < max_err_pos:
                        # obtener el nombre de la funcion ejecutada
                        #k_filter = kalman1.__name__
                        k_filter = kalman3.__name__
                        
                        key = "{0}_{1}".format(method, k_filter)

                        if not (key in results.keys()):
                            results[key] = {"pos": [],
                                            "err": [],
                                            "dop": [],
                                            "gps_time":[]
                                           }
                            continue
                        else:
                            results[key]["gps_time"].append(gps_time)
                            results[key]["pos"].append(pos_k)
                            results[key]["err"].append(err_k)
                            results[key]["dop"].append(cov_k)

    return results

####################################################################################
##### Geodetic Utils ######
####################################################################################
from numpy import (sin, cos, tan, sqrt, radians, arctan2, hypot, degrees, mod,
                   atleast_2d, atleast_1d, empty_like, array, column_stack)

class EarthEllipsoid:

    def __init__(self):
        self.a = 6378137.  # semi-major axis [m]
        self.f = 1 / 298.2572235630  # flattening
        self.b = self.a * (1 - self.f)  # semi-minor axis

def _uvw2enu(u, v, w, lat0, lon0, deg):
    if deg:
        lat0 = radians(lat0)
        lon0 = radians(lon0)
    t = cos(lon0) * u + sin(lon0) * v
    East = -sin(lon0) * u + cos(lon0) * v
    Up = cos(lat0) * t + sin(lat0) * w
    North = -sin(lat0) * t + cos(lat0) * w
    return East, North, Up

def ecef2enu(x, y, z, rec_pos, ell=EarthEllipsoid(), deg=False):
    #x0, y0, z0 = geodetic2ecef(lat0, lon0, h0, ell, deg=deg)
    x0, y0, z0  = rec_pos[0], rec_pos[1], rec_pos[2]
    lat0, lon0, h0 = ecef2lla(x0, y0, z0)
    return _uvw2enu(x - x0, y - y0, z - z0, lat0, lon0, deg=deg)

import pyproj
ecef = pyproj.Proj(proj='geocent', ellps='WGS84', datum='WGS84')
lla = pyproj.Proj(proj='latlong', ellps='WGS84', datum='WGS84')
def lla2ecef(lat,lon,alt, isradians=True):
    return pyproj.transform(lla, ecef, lon, lat, alt, radians=isradians)

def ecef2lla(X,Y,Z, isradians=True):
    lon, lat, alt = pyproj.transform(ecef, lla, X,Y,Z, radians=isradians)
    return lat, lon, alt

"""
# https://github.com/scottyhq/insar_scripts/blob/master/ALOS/estimate_alos_baselines.py
def ecef2enu2(pos, ref):

    #http://en.wikipedia.org/wiki/Geodetic_datum#Geodetic_versus_geocentric_latitude

    xm,ym,zm = ref.flat #extracts elements of column or row vector
    # duplicate reference vector rows into matrix
    ref = np.vstack((ref,)*pos.shape[0])

    # get geodetic lat/lon/height (above wgs84) of satellite
    ecef = pyproj.Proj(proj='geocent',  ellps='WGS84', datum='WGS84')
    wgs84 = pyproj.Proj(proj='latlong', ellps='WGS84', datum='WGS84')
    lon, lat, h = pyproj.transform(ecef, wgs84, xm, ym, zm, radians=True)

    # make transformation matrix
    transform = np.array([
        [-np.sin(lon), np.cos(lon), 0.0],
        [-np.sin(lat)*np.cos(lon), -np.sin(lat)*np.sin(lon), np.cos(lat)],
        [np.cos(lat)*np.cos(lon), np.cos(lat)*np.sin(lon), np.sin(lat)]
    ])

    # do matrix multiplication
    enu = np.dot(transform, pos.T - ref.T)
    return enu
"""



####################################################################################
##### kalman wallas ######
####################################################################################
from numpy import dot, linalg 
from numpy.linalg import inv 

def kf_predict(X, P, A, Q, B, U): 
    X = dot(A, X) + dot(B, U) 
    P = dot(A, dot(P, A.T)) + Q 
    return(X,P) 

def kf_update (X, P, Y, H, R): 
    IM = dot(H, X) 
    #print np.shape(IM)
    IS = R + dot(H, dot(P, H.T)) 
    K = dot(P, dot(H.T, inv(IS))) 
    X = X + dot(K, (Y-IM)) 
    P = P - dot(K, dot(IS, K.T)) 
    #LH = gauss_pdf(Y, IM, IS) 
    return (X,P,K,IM,IS)#,LH) 

def gauss_pdf(X, M, S): 
    if np.shape(M)[1] == 1: 
        DX = X - np.tile(M, np.shape(X)[1])   
        E = 0.5 * np.sum(DX*(dot(inv(S), DX)), axis=0) 
        E = E + 0.5 * np.shape(M)[0] *np.log(2*np.pi) + 0.5*np.log(np.linalg.det(S)) 
        P = np.exp(-E) 
    elif np.shape(X)[1] == 1: 
        DX = np.tile(X, np.shape(M)[1]) - M   
        E = 0.5 * sum(DX * (dot(inv(S), DX)), axis=0) 
        E = E + 0.5 * np.shape(M)[0] * np.log(2 * np.pi) + 0.5 * np.log(np.linalg.det(S)) 
        P = np.exp(-E) 
    else: 
        DX = X-M   
        E = 0.5*dot(DX.T, dot(inv(S), DX)) 
        E = E + 0.5 * np.shape(M)[0]*np.log(2*np.pi) + 0.5*np.log(np.linalg(S)) 
        P = np.exp(-E) 
    return (P[0],E[0]) 

def kalman2(Xold, Pold, Xnew, R, points=[]):
    
    def update_cov(P, X, n=0.0):
        # http://code.activestate.com/recipes/577213-incremental-pca/
        # http://www.tenpay.tech/questions/155671/incremental-pca
        # Update covariance matrix and mean vector and centralize input around
        oldmean = np.mean(X)
        mean = (n*oldmean + X) / (n + 1.0)
        P = (n*P + X*X.T + n*oldmean*oldmean.T - (n+1)*mean*mean.T) / (n + 1.0)
        #X -= mean
        return P
    
    #Xold = points[0]
    if len(points)==0:
        points = []
        points.append(Xold)
        points.append(Xnew)
    
    A = np.eye(np.shape(Xold)[0])
    B = np.zeros(np.shape(A))
    U = np.ones(np.shape(Xold))
    Q = np.cov(np.asarray(points).T)

    Q[3,3] = gpstk.C_MPS
    H = np.eye(np.shape(Xold)[0])
    
    (Xupd, Pupd) = kf_predict(Xold, Pold, A, Q, B, U) 
    #Pupd = update_cov(Pupd, Xupd)
    (Xnew, Pnew, K, IM, IS) = kf_update(Xupd, Pupd, Xnew, H, R)
    
    return Xnew, Pnew

def kalman3(A, X, Xnew, P, b, snr, points=[]):
    def kf_update3(X, P, b, H, R): 
        IM = dot(H, X) 
        #print np.shape(IM)
        IS = R + dot(H, dot(P, H.T)) 
        K = dot(P, dot(H.T, inv(IS))) 
        X = X + dot(K, b) 
        P = P - dot(K, dot(IS, K.T)) 
        #LH = gauss_pdf(Y, IM, IS) 
        return (X,P,K,IM,IS)#,LH) 

    def update_cov(P, X, n=0.0):
        # http://code.activestate.com/recipes/577213-incremental-pca/
        # http://www.tenpay.tech/questions/155671/incremental-pca
        # Update covariance matrix and mean vector and centralize input around
        oldmean = np.mean(X)
        mean = (n*oldmean + X) / (n + 1.0)
        P = (n*P + X*X.T + n*oldmean*oldmean.T - (n+1)*mean*mean.T) / (n + 1.0)
        #X -= mean
        return P
    
    #Xold = points[0]
    if len(points)==0:
        points = []
        points.append(X)
        points.append(Xnew)
        
    H = np.eye(np.shape(X)[0])
    B = np.zeros(np.shape(H))
    U = np.ones(np.shape(X))
    Q = np.cov(np.asarray(points).T)
    #print Q.diagonal()
    R = np.zeros((np.shape(A)[0], np.shape(A)[0]))
    
    Re = 6367444.657  # 6,371 km
    h_iono  = 400e3
    for ind, el in enumerate(R):
        #print ind,
        #R[ind, ind] = np.array((Re/(Re+h_iono))*np.cos(elv[ind-1])**2)
        R[ind, ind] = 1/snr[ind-1]**2
    #print 
    
    Q[3,3] = gpstk.C_MPS
    #print np.shape(H), np.shape(B)
    
    Xupd = dot(H, X) + dot(B, U) 
    Pupd = dot(H, dot(P, H.T)) + Q 
    #(Xupd, Pupd) = kf_predict(Xnew, P, H, Q, B, U) 
    #Pupd = update_cov(Pupd, Xupd)
    (Xnew, Pnew, K, IM, IS) = kf_update3(Xupd, Pupd, b, A, R)
    
    return Xnew, Pnew



####################################################################################
##### kalman filters ######
####################################################################################
from numpy import dot
from scipy.linalg import inv

def kalman1(points, covars):
    
    #x = points[-1]
    #P = covars[-1]
    x = points[0]
    P = covars[0]
    
    Q = np.cov(np.asarray(points).T)
    Q = Q / np.linalg.norm(Q)

    F = np.eye(4)
    H = np.eye(4)
    
    #print len(F), len(x)

    xs, cov = [], []
    #for z, R in zip(points[:-1], covars[:-1]):
    for z, R in zip(points[1:], covars[1:]):

        # predict
        x = dot(F, x)
        P = dot(F, P).dot(F.T) + Q

        #update
        S = dot(H, P).dot(H.T) + R
        K = dot(P, H.T).dot(inv(S))
        y = z - dot(H, x)
        x += dot(K, y)
        #P = P - dot(K, H).dot(P)
        P = P - dot(K, S).dot(K.T)

        xs.append(x)
        cov.append(P)
    return xs, cov

    
    
    
    
def plot_kalman_vs_method(dict_results, method, station, 
                          title="", labels=[], figsize=(16,6)):
    
    t = dict_results[method]["gps_time"]
    points = dict_results[method]["pos"]
    xs = dict_results[method + "_kalman1"]["pos"]

    fig = plt.figure(figsize=figsize)
    ax = fig.add_subplot(111)
    
    error_ls = []
    for p in points:
        e = np.linalg.norm(p[:3] - rec_pos)
        error_ls.append(e)

    print t
    error_kl = []
    for x in xs:
        #print x
        e = np.linalg.norm(x[:,:3] - rec_pos)
        error_kl.append(e)

    ax.plot(t, error_ls,            marker='.', color='green', linewidth=0, label=method)
    ax.plot(t, np.roll(error_kl,1), marker='.', color='blue', linewidth=0, label=method+" + kalman")
    ax.legend()
    ax.set_title(title)
    ax.set_xlabel(labels[0])
    ax.set_ylabel(labels[1])
        
    filename = station+"_DOP_"+method
    fig.savefig(filename+".pdf", format='pdf', bbox_inches='tight')
    
    
def plot_compair_dop(points, rec_pos, station, 
                     title="", figsize=(16,7), resumen=False):
    
    #points = np.array(dict_results[method]["pos"])

    enu_all = np.array(ecef2enu(points[:,0], points[:,1], points[:,2], rec_pos))
    enu_mean = enu_all.mean(axis=1)
    enu_std = enu_all.std(axis=1)
    
    #enu_all2 = np.array(ecef2enu2(points[:,:3], rec_pos))
    #enu_mean2 = enu_all2.mean(axis=1)
    #print enu_mean, enu_mean2
    
    fig = plt.figure(figsize=figsize)
    fig.suptitle(title, fontsize=14)

    ax1 = fig.add_subplot(121)
    legend = r'$\bf{0}:${1:.3f} '.format("\sigma_x", enu_std[0]) \
            +r'$\bf{0}:${1:.3f}'.format("\sigma_y", enu_std[1]) # np.std(enu_all[:,1])
    
    legendm = r'$\bf{0}:${1:.3f} '.format("\mu_x", enu_mean[0]) \
             +r'$\bf{0}:${1:.3f}'.format("\mu_y", enu_mean[1])
        
    ax1.scatter(enu_mean[1], enu_mean[0], marker='o', c='r', label=legendm)
    ax1.scatter(enu_all[1], enu_all[0], marker='+', c="blue", label=legend)
    ax1.set_xlabel("x-axis [m]")
    ax1.set_ylabel("y-axis [m]")
    ax1.set_title("HDOP", fontsize=12)
    ax1.legend(loc=1, fontsize="x-large")

    ax2 = fig.add_subplot(122)
    legend = r'$\bf{0}:${1:.3f}'.format("\sigma_z", enu_std[2]) #np.std(enu_all[:,2]))
    legendm = r'$\bf{0}:${1:.3f}'.format("\mu_z", enu_mean[2])
    
    mean_z = np.array([(i,enu_mean[2]) for i in range(len(enu_all[2]))])
    ax2.plot(mean_z[:,1], marker='+', c="blue", label=legendm)
    ax2.plot(enu_all[2],  marker='o', c='r', label=legend)
    ax2.set_xlabel("epoch")
    ax2.set_ylabel("z-axis [m]")
    ax2.set_title("VDOP", fontsize=12)
    ax2.legend(loc=1, fontsize="x-large")
    
    filename = "DOP_{0}_{1}".format(station, method)
    fig.savefig(filename+".pdf", format='pdf', bbox_inches='tight')
    
    if(resumen):
        title = "{0} analysis for {1} method {2}".format(filename.split("_")[0], 
                                                         filename.split("_")[1],
                                                         filename.split("_")[2])
        print (title)
        print (" mean(x,y,z)[m]: \t std(x,y,z)[m]: \n{0}\t{1}".format(enu_mean, enu_std))
    
####################################################################################
##### RINEX UTILS ######
####################################################################################
def rinex_to_dataframe_ionosfe(obsfile, navfile, silent_mode=False, ion_max=60):
    c = 299792458.
    observation_types=["P1", "P2", "L1", "L2", "C1", "C2", "S1", "S2"]
    obsHeader, obsData = gpstk.readRinex3Obs(obsfile)
    navHeader, navData = gpstk.readRinex3Nav(navfile)

    nh=gpstk.readRinexNav(navfile)[0]
    alphas = nh.ionAlpha
    betas  = nh.ionBeta
    gamma=gpstk.GAMMA_GPS ##CARGANDO GAMMA

    # setup ephemeris store to look for satellite positions
    bcestore = gpstk.GPSEphemerisStore()
    for navDataObj in navData:
        ephem = navDataObj.toGPSEphemeris()
        bcestore.addEphemeris(ephem)
    bcestore.SearchNear()
    navData.close()

    rec_pos = [obsHeader.antennaPosition[0], obsHeader.antennaPosition[1], obsHeader.antennaPosition[2]]

    requested_obstypes = observation_types
    obsidxs = []
    obstypes = []
    obsdefs = np.array([i for i in obsHeader.R2ObsTypes])
    for i in requested_obstypes:
        w = np.where(obsdefs==i)[0]
        if len(w)!=0:
            obsidxs.append(w[0])
            obstypes.append(i)
        else:
            if silent_mode == False:
                print ("WARNING! observation `"+i+"` no present in file "+obsfile)
            #else:
            #    print ""
    #obsidxs, obstypes

    r = []
    for obsObject in obsData:
        prnlist = []
        obsdict = {}
        prnspos = []
        prns_Timecorr  = []
        prns_clockbias = []
        prns_relcorr   = []
        prnselev       = []
        prnsaz         = []
        iono_delay     = []
        for i in obstypes:
            obsdict[i]=[]

        gpsTime = gpstk.GPSWeekSecond(obsObject.time)

        for satID, datumList in obsObject.obs.iteritems():
            if satID.system == satID.systemGPS:
                prnlist.append("".join(str(satID).split()))
                eph   = bcestore.findEphemeris(satID, obsObject.time)

                for i in range(len(obsidxs)):
                    obsdict[obstypes[i]].append(obsObject.getObs(satID, obsidxs[i]).data)

                P1 = obsObject.getObs(satID, obsidxs[0]).data

                # para reemplazar los valores de P1 que son cero en el rinex
                # por los valores de C1, que deberian ser semejantes
                if np.mean(P1) < 1:
                    #print obstypes[4]
                    P1 = obsObject.getObs(satID, obsidxs[4]).data

                P2 = obsObject.getObs(satID, obsidxs[1]).data
                Id = (1*1/(1-gamma)*(P1-P2))
                if np.abs(Id) < ion_max:
                    iono_delay.append(Id)
                else:
                    iono_delay.append(ion_max)

                svTime = obsObject.time - P1/c
                svXvt = eph.svXvt(svTime)
                
                # svTimeCorr: from http://math.tut.fi/posgroup/icl-gnss2012.pdf
                svClkRelative = svXvt.getRelativityCorr()
                svClkBias = svXvt.getClockBias()
                svToc = gpsTime.getSOW() - P1/c - svClkBias
                svClkDrift = eph.af1
                svFreqDrift = eph.af2
                svTimeCorr = svClkBias + svClkDrift*(svToc) + svFreqDrift*(svToc**2) + svClkRelative - eph.Tgd  
                svTime += svTimeCorr
                #Raul
                #svTime -= svXvt.getClockBias() + svClkRelative
                svXvt = eph.svXvt(svTime)

                prns_Timecorr.append(svTimeCorr)
                prnspos.append([svXvt.x[0], svXvt.x[1], svXvt.x[2]])
                prns_clockbias.append(svXvt.getClockBias())
                prns_relcorr.append(svXvt.getRelativityCorr())

                prnselev.append(obsHeader.antennaPosition.elvAngle(svXvt.getPos()))
                prnsaz.append(obsHeader.antennaPosition.azAngle(svXvt.getPos()))

        r.append([gpsTime.getWeek(), gpsTime.getSOW(), svTimeCorr, alphas, betas, gamma, np.array(prnlist), np.array(prnspos), np.array(prns_clockbias),
                  np.array(prns_relcorr), np.array(prnselev), np.array(prnsaz), np.array(iono_delay)] + [np.array(obsdict[i]) for i in obstypes])

    names=["gps_week", "gps_sow", "gps_time_corr", "alphas", "betas", "gamma", "prns", "prns_pos", "prns_clockbias", "prns_relcorr", "prns_elev", "prns_az", "Iono"] + obstypes
    r = pd.DataFrame(r, columns=names)
    obsData.close()
    return r, bcestore, np.array(rec_pos)


####################################################################################
##### POSITIONING ALGORITHMS ######
####################################################################################
def get_dop(A, sigma=1):
    return np.linalg.pinv(A.T.dot(A)) * sigma**2

def get_dopX(svs, elevs, A, b, num_vars=4):
    w = np.cos(elevs) #np.cos(elevs)**2# + a*np.sin(elevs)**2 
    num = b.T.dot(w*np.eye(len(w))).dot(b)/np.max(b)**2
    den = len(svs) - num_vars
    Cxx = 10*np.linalg.pinv((A.T.dot(w*np.eye(len(w)))).dot(A))*(num/den)
    #print Cxx
    return Cxx

@autojit
def compute_distances(rc, svs):
    # return np.array( [np.sqrt((rc[0]-sv[0])**2 + (rc[1]-sv[1])**2) for sv in svs] )
    return np.linalg.norm(rc-svs, axis=1)

@autojit
def predict_pseudoranges(x, prns_pos, prns_clockbias):
    c = gpstk.C_MPS #299792458
    rhos    = compute_distances(x[:3], prns_pos)
    pranges = rhos + x[3]-c*prns_clockbias
    return rhos, pranges

@autojit
def apply_earth_rotation_to_svs_position(svs, prs):
    c = gpstk.C_MPS #299792458
    we = 7.2921159e-5
    rpos = np.zeros(svs.shape)
    pos = np.array(svs)
    for i in range(len(pos)):
        dt = prs[i]/c
        theta = we*dt
        R = np.array([[np.cos(theta), np.sin(theta),0.],
                      [-np.sin(theta), np.cos(theta),0.],
                      [0.,0.,1.]])
        rpos[i] = R.dot(pos[i])
    svs = np.array(rpos)
    return svs
    
    
    
    
def compute_least_squares_position(svs, prs, clk_bias, clk_corr, 
                                   lamb=0.0,
                                   max_iters=200, apply_earth_rotation=True,
                                   apply_time_correction=True):

    if apply_time_correction:
        clk_bias -= clk_corr
    
    if apply_earth_rotation:
        #print type(svs), type(prs)
        svs = apply_earth_rotation_to_svs_position(svs, prs)

    if len(svs)==0 or len(prs)==0:
        return np.array([0.,0.,0.,0.]),None, None, None

    ri = np.array([0.,0.,0.,0.])

    #for i in range(max_iters):
    delta,i = 1,0
    while (norm(delta)>1e-8 and i<max_iters):
        rhos, pranges = predict_pseudoranges(ri, svs, clk_bias)
        b = prs - pranges
        A = np.hstack(((ri[:3]-svs)/rhos[:,None],np.ones((len(b), 1))))
        G = lamb * np.eye(np.shape(A)[1])
        G[0, 0] = 0 # Don't regularize bias
        delta =  np.linalg.pinv(A.T.dot(A) +np.dot(G.T, G)).dot(A.T).dot(b)
        ri += delta
        i+=1
    return ri, A, b, delta

@autojit
def predict_coderanges(x, prns_pos, prns_clockbias):
    c = gpstk.C_MPS #299792458.0
    f1 = 1575420000.0
    lamda = c/f1

    rhos    = compute_distances(x[:3], prns_pos)
    pcodes  = rhos + x[3]-c*prns_clockbias + x[4]*lamda
    return rhos, pcodes

def least_squares_position_codephase(svs, prs, clk_bias, clk_corr, 
                                     lamb=0.0,
                                     codes=None, elevs=None, snr=None, 
                                     max_iters=200, 
                                     apply_earth_rotation=True,
                                     apply_time_correction=True):

    if apply_time_correction:
        clk_bias -= clk_corr

    c = gpstk.C_MPS #299792458.0
    f1 = gpstk.L1_FREQ_GPS # 1575420000.0
    lamda = c/f1

    """
        Para emplear la solucion con codigo+fase se necesitan minimo 5 satelites u observaciones.
    """
    if apply_earth_rotation:
        #print type(svs), type(prs)
        svs = apply_earth_rotation_to_svs_position(svs, prs)

    if len(svs)==0 or len(prs)==0:
        return np.array([0.,0.,0.,0.]),None, None, None

    if len(snr) == 0 or len(elevs)== 0:
        print "SNR observable is needed"
        return np.array([0.,0.,0.,0.]),None, None, None

    ri = np.array([0.,0.,0.,0.])

    if len(codes) != 0 and len(codes)>5:
        ri = np.array([0.]*4 + [0.]*len(codes))
        bandera = True

    #for i in range(max_iters):
    delta,i = 1,0
    while (norm(delta)>1e-8 and i<max_iters):

        rhos, pranges = predict_pseudoranges(ri[:4], svs, clk_bias)
        b = prs - pranges
        ones = np.ones((len(b), 1))
        zero = np.zeros((len(b), len(b))) #np.zeros((len(b),1))
        A = np.hstack(((ri[:3]-svs)/rhos[:,None], ones, zero))

        if bandera:
            # Considerando observables de fase
            rhos2, pcodes  = predict_coderanges(ri, svs, clk_bias)
            bc = codes - pcodes
            #print np.shape(b), np.shape(bc)
            b = np.hstack((b, bc))
            #print np.shape(b), np.shape(bc)
            lambdas = lamda*np.eye(len(bc)) # *ones
            #print np.shape((ri[:3]-svs)/rhos[:,None]), np.shape(ones), np.shape(b), np.shape(pcodes)
            ai = np.hstack(((ri[:3]-svs)/rhos[:,None], ones, lambdas))
            #print np.shape(ai), np.shape(b)
            A = np.vstack((A, ai))

        G = lamb * np.eye(np.shape(A)[1])
        G[0, 0] = 0 # Don't regularize bias
        
        if len(snr) != 0 or len(elevs) != 0:
            #wp = len(pranges)*(0.03 + 0.9*np.exp(-snr/elevs))#elevs/30.))
            wp = len(pranges)*(0.03 + 0.9*np.exp(-snr/elevs))#elevs/30.))
            wc = len(pranges)*(0.07 + 0.6*np.exp(-snr/elevs))#elevs/30.))
            w = np.hstack((wp, wc))
            #print np.shape(w), np.shape((A.T.dot(w*np.eye(len(w))))), np.shape(A.T), np.shape(b)
             #  + 1e-3*np.eye(len(ri))
            delta =  np.linalg.pinv((A.T.dot(w*np.eye(len(w)))).dot(A) +np.dot(G.T, G)).dot(A.T.dot(w*np.eye(len(w)))).dot(b)
        else:
            delta =  np.linalg.pinv(A.T.dot(A) +np.dot(G.T, G)).dot(A.T).dot(b)

        ri += delta
        i+=1
    return ri, A, b, delta

def weight_least_squares_position(svs, prs, clk_bias, clk_corr, elevs, snr, 
                                  lamb = 0.0,
                                  max_iters=200, 
                                  apply_earth_rotation=True,
                                  apply_time_correction=True):

    if apply_time_correction:
        clk_bias -= clk_corr

    if apply_earth_rotation:
        svs = apply_earth_rotation_to_svs_position(svs, prs)

    if len(svs)==0 or len(prs)==0 or len(elevs)==0 or len(snr)==0:
        return np.array([0.,0.,0.,0.]),None, None, None

    ri = np.array([0.,0.,0.,0.])

    #for i in range(max_iters):
    delta,i = 1,0
    while (norm(delta)>1e-8 and i<max_iters):
        rhos, pranges = predict_pseudoranges(ri, svs, clk_bias)
        b = prs - pranges
        A = np.hstack(((ri[:3]-svs)/rhos[:,None],np.ones((len(b), 1))))

        #w = np.ones(len(elevs))
        w = 1/np.cos(elevs)**2
        #w = 1/(np.cos(elevs)**2 + np.exp(-snr/elevs))# + a*np.sin(elevs)**2 
        #w = np.exp(-snr/elevs)**2        
        #w= len(pranges)*(0.03 + 0.9*np.exp(-snr/elevs))
        #wc = len(pranges)*(0.07 + 0.6*np.exp(-snr/elevs))#elevs/30.))
        #w = np.hstack((wp, wc))
        
        # https://gist.github.com/diogojc/1519756
        G = lamb * np.eye(np.shape(A)[1])
        G[0, 0] = 0 # Don't regularize bias
        delta =  np.linalg.pinv((A.T.dot(w*np.eye(len(w)))).dot(A) +np.dot(G.T, G)).dot(A.T.dot(w*np.eye(len(w)))).dot(b)
        #delta =  np.linalg.pinv(A.T.dot(A)).dot(A.T).dot(b)
        ri += delta
        i+=1
    return ri, A, b, delta
