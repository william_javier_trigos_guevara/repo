
import pandas as pd
def filter_by(df, constraints):
    """Filter MultiIndex by sublevels."""
    indexer = [constraints[name] if name in constraints else slice(None)
               for name in df.index.names]
    return df.loc[tuple(indexer)] if len(df.shape) == 1 else df.loc[tuple(indexer),]

pd.Series.filter_by = filter_by
pd.DataFrame.filter_by = filter_by

def Analisis(Archivo_Analisis, indices, columnas, scenario, num_sub_scenarios=3):

    def create_multi(df, index=['day'], cols=["mean_std_e", "d", "dr", "dc"]):
        multi=df.set_index(index, inplace=False).sort_index()
        return multi[cols]
    
    from IPython.display import display, clear_output, Javascript, Latex
    import pandas as pd
    backup = pd.HDFStore(Archivo_Analisis)

    df_res = backup['df_res']#.set_index("scenario")
    
    df13 = df_res.assign(Enviroment=df_res["scenario"].apply(lambda x: x.split(".")[0]))
    """
    df13 = create_multi(df_res,
                        index=['gps_day', 'stations', 'scenario'],
                        cols=["improve_by_agrupation", "improve_by_station"])
    """
    
    df13 = create_multi(df13, index=indices, cols=columnas)
    
    #display(df13.filter_by(filtro))
    filtro = set(["{0}.{1}".format(scenario, i) for i in range(1, num_sub_scenarios+1)])
    return backup, df13.filter_by(filtro)

def graficaScatter(df_in, agrupacion, scenario, profundidad=2):
    df_scatter = pd.DataFrame()
    df_scatter = df_scatter.assign(index=df_in["improve_by_agrupation"].apply(lambda x: x[0]))
    df_scatter = df_scatter.assign(index_station=df_in["improve_by_station"].apply(lambda improve_stations: [mean_std[0] for mean_std in improve_stations]))

    #display(df)
    num_sub_scenarios=3
    filtro = set(["{0}.{1}".format(scheme, i) for i in range(1,num_sub_scenarios+1) for scheme in scenario ])
    #print filtro
    df = df_scatter.filter_by({'stations': set(agrupacion)})

    df = df[df.index.get_level_values("scenario").isin(filtro)]
    #display(df)
    return df.mean(level=range(profundidad))

    #df[df.index.get_level_values('10').isin(['10', '11', '12'])]

    #print df.index.get_level_values("10").isin(df.index.names)
    #display(df.filter_by({'stations': {'cn20,tgpm'}}))
    
    
def get_stats(group):
    return {'min': group.min(), 'max': group.max(), 'count': group.count(), 'mean': group.mean()}

def get_ind_rov(group):
    return group["index_rovers"].mean()
    
    
def color_negative(value):
    success = value>0
    colors = success.apply(lambda x: "" if x else "color:red")
    return colors

def backgroundColor_negative(value, color):
    success = value>0
    colors = success.apply(lambda x: "" if x else 'background-color:%s'.format(color))
    return colors
