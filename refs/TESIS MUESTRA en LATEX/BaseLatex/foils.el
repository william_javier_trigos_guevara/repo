(TeX-add-style-hook "foils"
 (function
  (lambda ()
    (LaTeX-add-environments
     "boldequation"
     '("thebibliography" 1)
     "titlepage"
     "description"
     "verse"
     "quotation"
     "quote"
     "abstract"
     "Theorem"
     "Lemma"
     "Corollary"
     "Proposition"
     "Definition"
     "Proof")
    (TeX-add-symbols
     '("newnonfloat" 2)
     '("MyLogo" 1)
     '("Restriction" 1)
     '("rightfooter" 1)
     '("rightheader" 1)
     '("leftheader" 1)
     '("descriptionlabel" 1)
     "foiltexversion"
     "marginpar"
     "Black"
     "globalColor"
     "today"
     "sloppyfoils"
     "zerolistvertdimens"
     "zerolistvertdimens"
     "makelabel"
     "maketitle"
     "thefootnote"
     "maketitle"
     "thanks"
     "abstractname"
     "LogoOff"
     "LogoOn"
     "ps"
     "refname"
     "newblock"
     "captionfraction"
     "caption"
     "foilhead"
     "rotatefoilhead"
     "newtheorem"
     "bm"
     "bmstyle")
    (TeX-run-style-hooks
     "leqno"
     "fleqn"
     "fltfonts"))))

