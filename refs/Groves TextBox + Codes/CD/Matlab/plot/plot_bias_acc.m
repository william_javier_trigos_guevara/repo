%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Accelerometer bias plot
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure

% Accelerometer bias - x
subplot(3,1,1)
title('Accelerometer bias')
hold on
plot(t, act_bacc(1:N,1),'-b')
plot(t, est_bacc(1:N,1),'--r')
legend('True', 'EKF')
ylabel('bias_x  [m/s^2]')

% Accelerometer bias - y
subplot(3,1,2)
hold on
plot(t, act_bacc(1:N,2),'-b')
plot(t, est_bacc(1:N,2),'--r')
legend('True', 'EKF')
ylabel('bias_y  [m/s^2]')

% Accelerometer bias - z
subplot(3,1,3)
hold on
plot(t, act_bacc(1:N,3),'-b')
plot(t, est_bacc(1:N,3),'--r')
legend('True', 'EKF')
ylabel('bias_z  [m/s^2]')
xlabel('Time [sec]')