# Procedimiento GNSS Edison (UBLOX-M8N - NV08C-CSM)

## Desde equipo Anfitrion (Zenbook)
### Copiar la carpeta de RTKLIb a /root (Ya debe estar compilado sobre una edison)

scp -r RTKLIB-master root@192.168.0.XX:/root

### Copiar los ejecutables que permiten automatizar la captura de Bin

scp -r run_str2str.sh root@192.168.0.XX:/root

### Copiar archivos de configuración para los receptores (usado por str2str)
scp -r m8n_1hz.cmd nvs_raw_1hz.cmd root@192.168.0.XX:/root



## Desde Edison (Cage.01, Cage.02)

### asignar permisos de ejecución para str2str
chmod +x /root/RTKLIB-master/app/str2str/gcc/str2str

### Crear directorios para almacenamiento de archivos de salida
mkdir /home/root/rinex && mkdir /home/root/logs && ls
mkdir /root/rinex && mkdir /root/logs && ls

### Crear los links simbolicos para ejecución

ln -s /root/RTKLIB-master/app/str2str/gcc/str2str /usr/bin/str2str
ln -s /root/run_str2str.sh /usr/bin/run_str2str

### Crear proceso Crontab (Debe instalarse desde repos Intel Edison)
opkg update && opkg install cronie
export VISUAL=nano; crontab -e

```bash
@reboot	root /sbin/ifconfig | grep "inet addr" | grep -v "127.0.0.1" | awk '{ print $2 }' | awk -F: '{ print $2 }'
@reboot root nohup python -m SimpleHTTPServer 8808 & > /dev/null
59 23 * * * root sleep 58.97;killall str2str;run_str2str
```




#Referencias

* http://offbytwo.com/2008/05/09/show-ip-address-of-vm-as-console-pre-login-message.html
* http://www.elmundoenbits.com/2013/04/date-format-bash-linux.html#.W8fmvxRRdQI
* https://blog.rodrigoramirez.com/agregar-o-quitar-minutoshorassegundos-a-la-fecha-del-sistema/
