function [q bgyr pos vel] = AP_HuaMahony(gyr, mag, acc,...
    gps_pos, gps_vel, init, valid_mag, valid_acc, valid_pos, valid_vel)
% ------------------------------------------------------------------------
% AP_HuaMahony - Nonlinear observer,
% for Position, Velocity and Attitude estimation.
%
% Input     Description         Unit        Frame
% ...........................................................
% gyr       gyro                [rad/s]     body
% mag       magnetometer        [a.u.]      body
% acc       accelerometer       [m/s^2]     body
% gps_pos   gps position        [m]         ned
% gps_vel   gps velocity        [m/s]       ned
%
% Output    Description         Unit        Frame
% ...........................................................
% q         quaternion          []          body to ned
% bgyr      bias gyro           [rad/s]     body
% pos       position            [m]         ned
% vel       velocity            [m/s]       ned
%
% Author:   Harald N�kland
% Date:     June 2011
% ------------------------------------------------------------------------
h = 0.01;                           % Sampling interval
decl = 0.0323;                      % Magnetic declination [rad]
g_ned = [0 0 9.81]';                % Gravity [m/s^2]
gps_arm = [-0.65 -0.08 -0.90]';     % GPS lever arm [m]

% Tuning for experimental data:
% Kquat = 0.5*h; 
% Kbgyr = 0.002*h;
% kmag = 0.26;  kacc = 1;
% kpos = 1*h*25;
% kvel = 1*h*25;
% kQ = 0.12*h*25;

% Tuning for simulations:
Kquat = diag([1 1 10])*h; 
Kbgyr = diag([0.2 0.2 0.5])*h;
kmag = 1;  kacc = 1; 
kpos = 0.0001*h*25;
kvel = 3*h*25;
kQ = 0.06*h*25;
% ------------------------------------------------------------------------
persistent q_bar bgyr_bar pos_bar vel_bar Q_bar;
if init 
%     q_bar    = euler2q([48.4278   48.2415   61.3204]'*pi/180);
    q_bar = [1 0 0 0]';
%     q_bar    = qmethod(100,1,[0 0 1]',[cos(decl) sin(decl) 0]',-acc/norm(acc),mag/norm(mag)); % Initial attitude
    bgyr_bar = [0 0 0]';        % Initial bias gyro
    pos_bar  = -gps_arm;        % Initial position
    vel_bar  = [0 0 0]';        % Initial velocity
    Q_bar    = eye(3);          % Initial Q
end

% Dead-reckoning:
if valid_mag==0, kmag=0; end
if valid_acc==0, kacc=0; end
if valid_pos==0, kpos=0; end
if valid_vel==0, kvel=0; kQ=0; end

% Real measurement:
m = mag/norm(mag);
a = acc/norm(acc);

% Predicted rotation matrix:
R_bar = Rquat(q_bar);

% Lever arm compensation:
gps_pos = gps_pos - R_bar*gps_arm;
gps_vel = gps_vel - R_bar*Smtrx(gyr-bgyr_bar)*gps_arm;

% Magnetic field reference vector:
m_ned = R_bar*mag;
m_ned = [norm(m_ned(1:2))*cos(decl) norm(m_ned(1:2))*sin(decl) m_ned(3)]';
% m_ned = [13605.0 439.0 49863.6]';
% m_ned = m_ned/norm(m_ned);

% Acceleration reference vector:
% a_ned = -g_ned;
a_ned = Q_bar*acc + kvel*(gps_vel - vel_bar);

% Estimated measurement:
m_bar = R_bar'*m_ned/norm(m_ned);
a_bar = R_bar'*a_ned/norm(a_ned);

% Update estimate with measurement:
m_err = 0.5*(m*m_bar' - m_bar*m');
a_err = 0.5*(a*a_bar' - a_bar*a');
w_mes = -vex(kmag*m_err + kacc*a_err);

q_hat    = q_bar    + 0.5*Tquat(q_bar)*Kquat*w_mes;    
bgyr_hat = bgyr_bar - Kbgyr*w_mes;
pos_hat  = pos_bar  + kpos*(gps_pos - pos_bar);
vel_hat  = vel_bar  + kvel*(gps_vel - vel_bar);
Q_hat    = Q_bar    + kQ*(gps_vel - vel_bar)*acc';

% Normalize:
q_hat = q_hat/norm(q_hat);
Q_hat = Q_hat/norm(Q_hat,'fro')*sqrt(3);

% Project ahead:
q_dot    = 0.5*Tquat(q_hat)*(gyr - bgyr_hat);
bgyr_dot = [0 0 0]';
pos_dot  = vel_hat;
vel_dot  = Q_hat*acc + g_ned;
Q_dot    = Q_hat*Smtrx(gyr - bgyr_hat);

q_bar    = q_hat    + h*q_dot;
bgyr_bar = bgyr_hat + h*bgyr_dot;
pos_bar  = pos_hat  + h*pos_dot; 
vel_bar  = vel_hat  + h*vel_dot;
Q_bar    = Q_hat    + h*Q_dot;

% Normalize:
q_bar = q_bar/norm(q_bar);
Q_bar = Q_bar/norm(Q_bar,'fro')*sqrt(3);

% Output:
q    = q_hat;
bgyr = bgyr_hat;
pos  = pos_hat;
vel  = vel_hat;

