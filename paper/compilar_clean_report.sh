#./compilar_clean_report.sh Propuesta_William_Trigos.ipynb biblio.bib

filename=$(basename "$1")
extension="${filename##*.}"
filename="${filename%.*}"


echo $filename

echo ""> $filename.tex

# 
#jupyter nbconvert --to latex --template latex_basic.tplx $filename.ipynb #rev.tplx latex_basic.tplx clean_report.tplx  citations.tplx
jupyter nbconvert --to latex --template ieee-JOURNAL.tplx $filename.ipynb
#jupyter nbconvert --to latex --template ieee-CONFERENCE.tplx $filename.ipynb


pdflatex $filename.tex
bibtex $2 #
bibtex $filename
pdflatex $filename.tex
pdflatex $filename.tex
pdflatex $filename.tex

rm -rf *.out *.log *.bbl *.blg *.aux *.toc #*files *.tex
