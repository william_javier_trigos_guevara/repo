from Utils import get_dist, process_day, unzip_Z_gz, isempty, tick_equis
from os.path import basename, splitext, abspath, dirname
from os import listdir, getcwd, system

def Simulacion(parejas_kmz, RinexDB, day, solve_fun):
    """
    Busca las rutas de los archivos de observacion comprimidos dentro de RinexDB 
    que coinciden con la clave station_day
    """
    funcion_name = "Simulacion"
    arg1 = "station_day"
    arg2 = "RinexDB   "
    arg3 = "day       "
    arg4 = "solve_fun "

    Simu_Folder = getcwd()+"/Temp_Simul_data/"
    cmd1 = "mkdir -p " + Simu_Folder   # COMANDO CREA CARPETA temporal
    print(cmd1)
    system(cmd1)

    results = {}
    distacias = {}
    Sat_Common = {}
    for k, pareja in parejas_kmz.iteritems():

        for station in pareja[:2]:

            # Obteniendo el nombre del archivo Rinex a buscar dentro de RinexDB
            rx_station = station + process_day(day)

            # Buscando el archivo Rinex (rx_station) en la RinexDB
            rx_station = {k:RinexDB[k] for k in RinexDB.keys() if rx_station.lower() == k.lower()}
            results.update(rx_station)
        
        print results
        # verificando qeu existen archivos rinex para ambas estaciones
        if len(results) < 2:
            #print "intentar descargar rinex"
            #cancelar simu si no se encuentra
            #print ("*****"*20)
            print("\t **** EJECUCION CANCELADA No existe %s dentro de %s **** " %(rx_station, arg2))
            #print ("*****"*20)
        
        else:
            
            Simu_Folder = [abspath(x) for x in listdir(getcwd()) if x == "Temp_Simul_data"]

            if len(Simu_Folder) == 0:
                Simu_Folder = getcwd()+"/Temp_Simul_data/"
                cmd1 = "mkdir -p " + Simu_Folder   # COMANDO CREA CARPETA temporal
                print(cmd1)
                os.system(cmd1)  
                continue

            Simu_Folder = getcwd()+"/Temp_Simul_data/"

            #files = [(k, k1, basename(splitext(v1)[0]), dirname(v1)) for k, v in results.iteritems() for k1, v1 in v.iteritems() if k1 not in ['nav', 'obs'] if k1.endswith('zip')]
            files = [(k, k1, basename(splitext(v1)[0]), dirname(v1)) for k, v in results.iteritems() for k1, v1 in v.iteritems() if k1 not in ['nav', 'obs'] if k1.endswith('zip')]

            for f_info in files:
                station = f_info[0]
                ObsType = f_info[1]
                filename = f_info[2]
                path = f_info[3]
                print station

                if path != Simu_Folder and filename[-1].lower() == 'n':
                    #print "h", filename
                    results[station].update({'nav':Simu_Folder+filename})
                    # descomprimir el archivo
                    unzip_Z_gz(results[station]['nav-zip'], results[station]['nav'])

                elif path != Simu_Folder and filename[-1].lower() == 'o':
                    #print "n", filename
                    results[station].update({'obs':Simu_Folder+filename})
                    unzip_Z_gz(results[station]['obs-zip'], results[station]['obs'])
            #"""  

            for i, f_info in enumerate(files):

                if i <=2:
                    station = f_info[0]

                    print station, results[station].keys(), f_info[1], f_info[2]
		    print results[station]['nav']
                    
                    #Inicio de la simulacion GPSTk_Solver
                    
                    try:
                        # Obteniendo informacion para el receptor Rx
                        obs1, cp1, rp1 = GPSTk_Solver(results[station]['obs'], results[station]['nav'], Observable, error_max, resolucion_grafica)
                        results.setdefault(station, {})['informaci'] = obs1
                        results.setdefault(station, {})['Pos_Rinex'] = rp1
                        results.setdefault(station, {})['Pos_GPSTk'] = cp1
                        #print station, " termine\n"

                    except:
                        print "\t %s \t\t %s \t\t %s Cancelando ejecucion (Rinex estacion [%s] esta corrupto!)"%(station, day, tick_equis(1), pareja)
                #""" 
        
        
        ### ejecucion de SOLVER!! !
        
        RX = results.keys()
        
        RX_A = RX[0]
        RX_B = RX[1]
        obs1 = results[RX_A]['informaci']
        obs2 = results[RX_B]['informaci']

        #Sats in common
        Obs_Sat_comm = Info(obs1, obs2)

        rp1 = results[RX_A]['Pos_Rinex']
        cp1 = results[RX_A]['Pos_GPSTk']
        rp2 = results[RX_B]['Pos_Rinex']
        cp2 = results[RX_B]['Pos_GPSTk']

        #Elegir Usar solver minimize (solve_fun = 1) o banhosping (solve_fun = 2)               
        Solver_pos1, Solver_pos2, e_GPSTk, e_Solver, d_calc = ejec_Solver_wallas(Obs_Sat_comm, cp1, cp2, rp1, rp2, solve_fun=solve_fun)

        results.setdefault(RX_A, {})['Pos_Solve'] = Solver_pos1
        results.setdefault(RX_B, {})['Pos_Solve'] = Solver_pos2

        distacias.setdefault(RX_A[:4]+"-"+RX_B, {})['d-calc'] = d_calc
        Sat_Common.setdefault(RX_A[:4]+"-"+RX_B, {})['info']  = Obs_Sat_comm
    
    # Eliminar la carpeta de simulacion
    cmd_clean = "rm -r "+ Simu_Folder
    print cmd_clean
    system(cmd_clean)       

    return results, distacias, Sat_Common



def read_kmz_file(fname, d_min, d_max):
    import sys
    import zipfile
    import glob
    from xml.dom import minidom

    #fname = "sites.kmz"
    path_cnt = 0
    zf = zipfile.ZipFile(fname, 'r')

    #print zf, type(zf)
    for fn in zf.namelist():
        #print fn
        if fn.endswith('.kml'):
            content = zf.read(fn)
            xmldoc = minidom.parseString(content)
            placemarks = xmldoc.getElementsByTagName('Placemark')

            nodes = {}
            for placemark in placemarks:
                nodename = placemark.getElementsByTagName("name")[0].firstChild.data
                coords = placemark.getElementsByTagName("coordinates")[0].firstChild.data
                lst1 = coords.split(",")
                longitude = float(lst1[0])
                latitude = float(lst1[1])
                nodes[nodename] = (latitude, longitude)

            from itertools import combinations
            #print nodes
            parejas = [{j: nodes[j] for j in i} for i in combinations(nodes, 2)]

            #d_min = 1e8

            GPS_Stations = {}
            c = 0
            for n in parejas:
                #print n, type(n), n.keys()[0], n.values()[0]
                rx_1 = n.values()[0]
                rx_2 = n.values()[1]

                d,_ = get_dist(rx_1, rx_2)
                if (d_min < d < d_max):
                    
                    print d_min, d, "entre", n.keys()[0], "y", n.keys()[1]
                    #d_min = d
                    GPS_Stations[c] = [n.keys()[0], n.keys()[1], d]
                    c += 1
    return GPS_Stations



def get_data_stations(f_in, d_min, d_max):
    List_Stations = read_kmz_file(f_in, d_min, d_max)

    stations = {}
    
    for (idx, pareja) in enumerate(List_Stations.iteritems()):
        #print pareja, type(pareja[1])
        par = [str(x) if i != len(pareja[1]) else float(x) for (i, x) in enumerate(pareja[1])]
        #print tuple(par)
        stations[idx] = tuple(par)
        
    return stations


def plot_error2(Resultados, title, x_label, y_label, size_fig=(15,10)):
    import matplotlib.pyplot as plt
    from matplotlib.pyplot import cm
    import numpy as np

    fig = plt.figure(figsize=size_fig)
    ax = fig.add_subplot(211)

    # http://stackoverflow.com/questions/4971269/how-to-pick-a-new-color-for-each-plotted-line-within-a-figure-in-matplotlib
    # https://cheleb.net/ipython/astro/01-Genga.html
    n = len(Resultados.keys())*len(Resultados.values())
    color=iter(cm.rainbow(np.linspace(0,1, n)))

    for estaciondia, datos in Resultados.iteritems():

        Rx_pos = Resultados[estaciondia]['Pos_Rinex']
        #print " estacion %s ubicacion real %s"%(estaciondia, Rx_pos)

        data_plot = {}

        for i, solver_soft in enumerate(['Pos_GPSTk', 'Pos_Solve']):

            key = str(estaciondia)

            if i == 0:
                y_var = np.array([np.linalg.norm(x - Rx_pos) for x in datos[str(solver_soft)].values()])
            elif i == 1:
                y_var = np.array([np.linalg.norm(x[:3] - Rx_pos) for x in datos[str(solver_soft)].values()])

            x_var = datos[solver_soft].keys()

            ax.semilogy(x_var, y_var, 'o', c=next(color), label='e_'+solver_soft+'-'+estaciondia)
            #ax.scatter(x_var, y_var, 'o', c=next(color), label='e_'+solver_soft+'-'+estaciondia)
            ax.legend(loc=3)

            ax.set_xlabel(x_label, size=22)
            ax.set_ylabel(y_label, size=22)
            ax.set_title(title, size=22)



def plot_d(parejas_kmz, distancias, x_label, y_label, title):
    
    import matplotlib.pyplot as plt
    from matplotlib.pyplot import cm
    import numpy as np

    fig = plt.figure(figsize=size_fig)
    ax = fig.add_subplot(211)
    
    for stations, data in distacias.iteritems():
    
        legend = stations
        
        x = data['d-calc'].keys()
        y = data['d-calc'].values()
        
        # extraer la distancia real entre el par de estaciones
        d_real = [parejas_kmz[p][1][2] for p in parejas_kmz.keys() 
                  if str(parejas_kmz[p][1][0]+"-"+parejas_kmz[p][1][1]).lower() == stations[:-4].lower()]
        
        print d_real
        d_real = [d_real]*len(y)
        
        real_legend = stations[:-4]+" dist-real"
        
        
        #ax.scatter(x, y, label=legend)
        ax.semilogy(x, y, 'o', label=legend)
        ax.semilogy(x, d_real, '-', label=real_legend)
        ax.legend(loc=3)

        ax.set_xlabel(x_label, size=22)
        ax.set_ylabel(y_label, size=22)
        ax.set_title(title, size=22)



#######################################################################################################
#######################################################################################################
#######################################################################################################

# FUNCIONES PARA TRABAJAR DESCOMPRESION  !!!!!!!!!!!! NO TOCAR!!!!!!!!!!!!!!!!


# unzip_Z_gz("new_data/BEJA2880.16O.gz", "Simul_data/")
# unzip_Z_gz("new_data/tgdr2870.16o.Z", "Simul_data/")

########################################################################################################
#######################################################################################################
#######################################################################################################

def unzip_Z_gz(i_path=None, o_path=None):
    import gzip
    #from Utils import unlzw
    
    if i_path==None and o_path==None or type(i_path) == int:
        print (" se necesita archivo de entrada o no se encuentra acrchivo")
        
    else:

        filename, ext = splitext(basename(i_path))  
        #o_file = o_path+filename
        o_file = dirname(o_path) + "/" +basename(o_path)
        #print("hola", i_path, o_path, o_file)

        #path = dirname(dirname(abspath()))
        #print filename, ext, path, os.getcwd()
        
        if str(ext).lower() == '.gz':
            #for gzip
            #for compressed file
            inF = gzip.open(i_path, 'rb')
            #file after decompressed
            outF = open(o_file, 'wb')
            outF.write( inF.read() )
            inF.close()
            outF.close()

        if str(ext).lower() == '.z':
            inF = open(i_path, 'r')
            compressed_data = inF.read()
            outF = open(o_file, 'wb')
            outF.write(unlzw(compressed_data))
            inF.close()
            outF.close()
        
        #return o_file


def unlzw(data):
    # This function was adapted for Pythnon from Mark Adler's C implementation
    # https://github.com/umeat/unlzw
    
    # Decompress compressed data generated by the Unix compress utility (LZW
    # compression, files with .Z suffix). Input can be given as any type which
    # can be 'converted' to a bytearray (e.g. string, or bytearray). Returns 
    # decompressed data as string, or raises error.
    
    # Written by Brandon Owen, May 2016, brandon.owen@hotmail.com
    # Adapted from original work by Mark Adler - orginal copyright notice below
    
    # Copyright (C) 2014, 2015 Mark Adler
    # This software is provided 'as-is', without any express or implied
    # warranty.  In no event will the authors be held liable for any damages
    # arising from the use of this software.
    # Permission is granted to anyone to use this software for any purpose,
    # including commercial applications, and to alter it and redistribute it
    # freely, subject to the following restrictions:
    # 1. The origin of this software must not be misrepresented; you must not
    # claim that you wrote the original software. If you use this software
    # in a product, an acknowledgment in the product documentation would be
    # appreciated but is not required.
    # 2. Altered source versions must be plainly marked as such, and must not be
    # misrepresented as being the original software.
    # 3. This notice may not be removed or altered from any source distribution.
    # Mark Adler
    # madler@alumni.caltech.edu
    
    
    # Convert input data stream to byte array, and get length of that array
    try:
        ba_in = bytearray(data)
    except ValueError:
        raise TypeError("Unable to convert inputted data to bytearray")
        
    inlen = len(ba_in)
    prefix = [None] * 65536         # index to LZW prefix string
    suffix = [None] * 65536         # one-character LZW suffix
    
    # Process header
    if inlen < 3:
        raise ValueError("Invalid Input: Length of input too short for processing")
    
    if (ba_in[0] != 0x1f) or (ba_in[1] != 0x9d):
        raise ValueError("Invalid Header Flags Byte: Incorrect magic bytes")
    
    flags = ba_in[2]
    if flags & 0x60:
        raise ValueError("Invalid Header Flags Byte: Flag byte contains invalid data")
        
    max_ = flags & 0x1f
    if (max_ < 9) or (max_ > 16):
        raise ValueError("Invalid Header Flags Byte: Max code size bits out of range")
        
    if (max_ == 9): max_ = 10       # 9 doesn't really mean 9 
    flags &= 0x80                   # true if block compressed
    
    # Clear table, start at nine bits per symbol
    bits = 9
    mask = 0x1ff
    end = 256 if flags else 255
    
    # Ensure stream is initially valid
    if inlen == 3: return 0         # zero-length input is permitted
    if inlen == 4:                  # a partial code is not okay
        raise ValueError("Invalid Data: Stream ended in the middle of a code")
    
    # Set up: get the first 9-bit code, which is the first decompressed byte,
    # but don't create a table entry until the next code
    buf = ba_in[3]
    buf += ba_in[4] << 8
    final = prev = buf & mask       # code
    buf >>= bits
    left = 16 - bits
    if prev > 255: 
        raise ValueError("Invalid Data: First code must be a literal")
    
    # We have output - allocate and set up an output buffer with first byte
    put = [final]
    
    # Decode codes
    mark = 3                        # start of compressed data
    nxt = 5                         # consumed five bytes so far
    while nxt < inlen:
        # If the table will be full after this, increment the code size
        if (end >= mask) and (bits < max_):
            # Flush unused input bits and bytes to next 8*bits bit boundary
            # (this is a vestigial aspect of the compressed data format
            # derived from an implementation that made use of a special VAX
            # machine instruction!)
            rem = (nxt - mark) % bits
            
            if (rem):
                rem = bits - rem
                if rem >= inlen - nxt: 
                    break
                nxt += rem
            
            buf = 0
            left = 0
            
            # mark this new location for computing the next flush
            mark = nxt
            
            # increment the number of bits per symbol
            bits += 1
            mask <<= 1
            mask += 1
        
        # Get a code of bits bits
        buf += ba_in[nxt] << left
        nxt += 1
        left += 8
        if left < bits: 
            if nxt == inlen:
                raise ValueError("Invalid Data: Stream ended in the middle of a code")
            buf += ba_in[nxt] << left
            nxt += 1
            left += 8
        code = buf & mask
        buf >>= bits
        left -= bits
        
        # process clear code (256)
        if (code == 256) and flags:
            # Flush unused input bits and bytes to next 8*bits bit boundary
            rem = (nxt - mark) % bits
            if rem:
                rem = bits - rem
                if rem > inlen - nxt:
                    break
                nxt += rem
            buf = 0
            left = 0
            
            # Mark this location for computing the next flush
            mark = nxt
            
            # Go back to nine bits per symbol
            bits = 9                    # initialize bits and mask
            mask = 0x1ff
            end = 255                   # empty table
            continue                    # get next code
        
        # Process LZW code
        temp = code                     # save the current code
        stack = []                      # buffer for reversed match - empty stack
        
        # Special code to reuse last match
        if code > end:
            # Be picky on the allowed code here, and make sure that the
            # code we drop through (prev) will be a valid index so that
            # random input does not cause an exception
            if (code != end + 1) or (prev > end):
                raise ValueError("Invalid Data: Invalid code detected")
            stack.append(final)
            code = prev

        # Walk through linked list to generate output in reverse order
        while code >= 256:
            stack.append(suffix[code])
            code = prefix[code]

        stack.append(code)
        final = code
        
        # Link new table entry
        if end < mask:
            end += 1
            prefix[end] = prev
            suffix[end] = final
        
        # Set previous code for next iteration
        prev = temp
        
        # Write stack to output in forward order
        put += stack[::-1]

    # Return the decompressed data as string
    return bytes(bytearray(put))
