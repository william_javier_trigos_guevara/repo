%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Velocity plot
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure

% x
subplot(3,1,1)
title('Velocity')
hold on
plot(t, act_vel(1:N,1),'-b')
plot(t, mes_vel(1:N,1),'--g')
plot(t, est_vel(1:N,1),'--r')
legend('True', 'GPS', 'EKF')
ylabel('v_N  [m/s]')

% y
subplot(3,1,2)
hold on
plot(t, act_vel(1:N,2),'-b')
plot(t, mes_vel(1:N,2),'--g')
plot(t, est_vel(1:N,2),'--r')
legend('True', 'GPS', 'EKF')
ylabel('v_E  [m/s]')

% z
subplot(3,1,3)
hold on
plot(t, act_vel(1:N,3),'-b')
plot(t, mes_vel(1:N,3),'--g')
plot(t, est_vel(1:N,3),'--r')
legend('True', 'GPS', 'EKF')
ylabel('v_D  [m/s]')
xlabel('Time [sec]')
