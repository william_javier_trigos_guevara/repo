# http://tritemio.github.io/smbits/2016/01/02/execute-notebooks/
import nbformat
from nbconvert.preprocessors import ExecutePreprocessor

notebook_filename = "merged.ipynb"

nb = nbformat.read(open(notebook_filename), as_version=4)

ep = ExecutePreprocessor(timeout=600, kernel_name='py27')

ep.preprocess(nb, {'metadata': {'path': 'notebooks/'}})

nbformat.write(nb, open('executed_notebook.ipynb', mode='wt'))
