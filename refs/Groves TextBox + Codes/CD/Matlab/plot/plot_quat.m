%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Quaternion plot
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure

% eta
subplot(4,1,1)
title('Quaternion')
hold on
plot(t, act_q(1:N,1),'-b')
plot(t, est_q(1:N,1),'--r')
axis([0 N*h -1 1])
legend('True', 'EKF')
ylabel('\eta')

% eps1
subplot(4,1,2)
hold on
plot(t, act_q(1:N,2),'-b')
plot(t, est_q(1:N,2),'--r')
axis([0 N*h -1 1])
legend('True', 'EKF')
ylabel('\epsilon_1')

% eps2
subplot(4,1,3)
hold on
plot(t, act_q(1:N,3),'-b')
plot(t, est_q(1:N,3),'--r')
axis([0 N*h -1 1])
legend('True', 'EKF')
ylabel('\epsilon_2')

% eps3
subplot(4,1,4)
hold on
plot(t, act_q(1:N,4),'-b')
plot(t, est_q(1:N,4),'--r')
axis([0 N*h -1 1])
legend('True', 'EKF')
ylabel('\epsilon_3')
xlabel('Time [sec]')
