from sympy import *
init_printing(use_latex=True)

import os
from os import (listdir, getcwd, system)
from os.path import (basename, splitext, abspath, 
                     dirname, split, isfile)
from re import search, findall, compile
import re, urllib2, sys, copy, time

import gpstk
import numpy as np
import pandas as pd

def include_observations(expr, O_sats, svs, prs):
    expro = expr
    for j in range(O_sats.shape[1]):
        for i in range(O_sats.shape[0]):
            expro = expro.subs({O_sats[i,j]: svs[i,j]})
            expro = expro.subs({rhos[i]: prs[i]})
    return expro


def include_satpos(expr, symbol_satpos, svs):
    expro = expr
    for j in range(symbol_satpos.shape[1]):
        for i in range(symbol_satpos.shape[0]):
            expro = expro.subs({symbol_satpos[i,j]: svs[i,j]})
    return expro

def include_symbols(expr, syms, vals):
    expro = expr
    for i in range(syms.shape[0]):
        expro = expro.subs({syms[i]: vals[i]})
    return expro

def create_matrix_x_receiver(rx_id, n_sats=4):
    n = n_sats
    
    simbolics = "x_%s y_%s z_%s \\tau_%s c"%(rx_id, rx_id, rx_id, rx_id)
    x,y,z,t,c = symbols(simbolics)
    X       = Matrix(4,1,[x,y,z,t])
    A       = Matrix(MatrixSymbol("A",n,X.shape[0]))
    P       = Matrix(MatrixSymbol("A",n,1))

    vnames  = ["x", "y", "z", "\\tau"]
    O_sats  = Matrix(n, X.shape[0], lambda i,j:var(vnames[j]+'^{%d}' % (i+1)))

    for i in range(n):
        P[i]   = sqrt(sum((X[:3,:].T - O_sats[i,:3]).applyfunc(lambda x: pow(x,2))))+(t-O_sats[i,3])
        A[i,:] = [[diff(P[i],x), diff(P[i],y), diff(P[i], z), diff(P[i], t)]]
    
    return A, P, X

def create_distance_matrix(rx1_id, rx2_id, variable, err_d1=0., err_d2=0., error_d=0.):
    
    n = 1 # quantity of equations of distance
    
    r1_symb = "x_%s y_%s z_%s \\eta_%s"%(rx1_id, rx1_id, rx1_id, err_d1)
    r2_symb = "x_%s y_%s z_%s \\eta_%s"%(rx2_id, rx2_id, rx2_id, err_d2)
    x1,y1,z1,e1 = symbols(r1_symb)
    x2,y2,z2,e2 = symbols(r2_symb)
    X1      = Matrix(4,1,[x1,y1,z1, e1])
    X2      = Matrix(4,1,[x2,y2,z2, e2])
    A       = Matrix(MatrixSymbol("A",n, X2.shape[0]))
    D       = Matrix(MatrixSymbol("A",n, 1))

    if variable == rx2_id:
        for i in range(n):
            D[i]   = sqrt(sum((X2[:3,:].T - X1[:3,:].T).applyfunc(lambda x: pow(x,2))))/2# + error_d
            A[i,:] = [[diff(D[i],x2), diff(D[i],y2), diff(D[i], z2), diff(D[i], e2)]]
    else:
        for i in range(n):
            D[i]   = sqrt(sum((X1[:3,:].T - X2[:3,:].T).applyfunc(lambda x: pow(x,2))))/2# + error_d
            A[i,:] = [[diff(D[i],x1), diff(D[i],y1), diff(D[i], z1), diff(D[i], e1)]]

    return D, A


def extract_info_rx(o, Obser="C1", origin="local", apply_earth_rotation=False):
    c = 299792458.
    
    def apply_earth_rotation_to_svs_position(svs, prs):
        c = 299792458
        we = 7.2921159e-5
        rpos = np.zeros(svs.shape)
        pos = np.array(svs)
        for i in range(len(pos)):
            dt = prs[i]/c
            theta = we*dt
            R = np.array([[np.cos(theta), np.sin(theta),0.],[-np.sin(theta), np.cos(theta),0.],[0.,0.,1.]])
            rpos[i] = R.dot(pos[i])
        svs = np.array(rpos)
        return svs
    
    svs = o['prns_pos_'+origin]
    prs = o[Obser+"_"+origin].reshape(-1,1)
    
    if apply_earth_rotation:
        svs = apply_earth_rotation_to_svs_position(svs, prs)
    
    clocks = c*o['prns_clockbias_'+origin].reshape(-1,1)
    O_sats_vals = np.hstack((svs, clocks))
    
    return O_sats_vals, prs


def extract_info_rx1(o, Obser="C1", origin="local"):
    c = 299792458.
    svs = o['prns_pos_'+origin]
    clocks = c*o['prns_clockbias_'+origin].reshape(-1,1)
    P_observed_vals = o[Obser+"_"+origin].reshape(-1,1)
    O_sats_vals = np.hstack((svs, clocks))
    
    return O_sats_vals, P_observed_vals

# https://tex.stackexchange.com/questions/161299/squares-in-matrix
def get_matrix_cosdir(rx_id = "a", n=4, m=4):

    simbolics = "x_%s y_%s z_%s \\tau_%s c"%(rx_id, rx_id, rx_id, rx_id)
    x,y,z,t,c = symbols(simbolics)
    X       = Matrix(m, 1, [x,y,z,t])
    A       = Matrix(n, X.shape[0], lambda i,j :var('\hat{A_{%s}^{(%s)}}' % (X[j], i)))
    #print type(A)
    A[:,3] = [1]*n #np.ones(4)
    #print type(A)
    if n>4:
        A[4:, 3:] = [0]*(n-4)

    return A, X