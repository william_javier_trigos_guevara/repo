#!/bin/bash

# USAGE
#eval "./run_nb_from_cli.sh fast_report_from_restored_HDF5 fast_report_from_restored_HDF5-out"> fast_report_from_restored_HDF5.log 2>&1 &


SCRIPTNAME="run_nb_from_cli.sh"
MAILADDRESS="william.trigos@gmail.com"

if [ $# -lt 2 ]; then
    usage
fi

NB_IN="$1"
NB_OUT="$2"
echo "Input Notebook   = ${NB_IN}.ipynb"
echo "Output Notebook  = ${NB_OUT}.ipynb"

function usage
{
    echo -e "usage: \n\t $0 in_notebook out_notebook \n"
    #echo $0: usage: myscript name
    exit 1
}


# https://www.reddit.com/r/bash/comments/64vj2a/how_to_split_long_argument_into_multiple_lines_in/?st=j4w2d4lh&sh=96f12dc2
output=( 
'jupyter nbconvert '
'--to notebook '
'--ExecutePreprocessor.kernel_name=python2 '
'--ExecutePreprocessor.timeout=60000 '
'--execute --inplace $NB_IN --output=$NB_OUT'
)

start_timestamp=`date --rfc-3339=seconds`
eval "source ${HOME}/anaconda3/bin/activate py27"
eval "${output[*]}"
OUT=$?

end_timestamp=`date --rfc-3339=seconds`

eval "jupyter nbconvert ${NB_OUT} --to latex --template example_template.tplx"

# Encontrar si se termino ejecucion bien o mal
if [ $OUT -eq 0 ];then
	exit_status="finished successfully!"
	echo ${exit_status} 
else
	exit_status="Exit for errors!"
	echo ${exit_status} 
fi

# -------------------------------------------------------------------------------
#  SOLO ENVIA MENSAJES SIN ADJUNTOS
# -------------------------------------------------------------------------------
#echo -e "Script Name: $SCRIPTNAME $NB_IN $NB_OUT\n\t 
#		command executed: \n\t${output[*]}. \n
#		Execution start at: $start_timestamp \n
#		Execution finish at: $end_timestamp \n\n
#		Mail generated and with exim4 server and mutt application from Zenbook machine" | mail $MAILADDRESS -s "Script Name: $SCRIPTNAME ${exit_status}  "

# -------------------------------------------------------------------------------
#  ENVIA MENSAJES CON ADJUNTOS
# -------------------------------------------------------------------------------
# Adjuntando archivos
# https://unix.stackexchange.com/questions/329050/how-to-send-mail-with-multiple-attachments
# mutt -s "now" -a Red_Analisis.pdf Makefile -- "william.trigos@gmail.com" <<< "Hola"
from="malliwi88@gmail.com"
to="william.trigos@gmail.com"
subject="Script Name: $SCRIPTNAME ${exit_status}"
body="Script Name: $SCRIPTNAME $NB_IN $NB_OUT\n\t 
		command executed: \n\t${output[*]}. \n
		Execution start at: $start_timestamp \n
		Execution finish at: $end_timestamp \n\n"
		#Mail generated and with exim4 server and mutt application from Zenbook machine"

declare -a attachments
attachments=( "$2.ipynb" )
 
declare -a attargs
for att in "${attachments[@]}"; do
  #attargs+=( "-a"  "$att" )  
  attargs+=( "$att" )  
done

eval "mutt -s '$subject' -a '${attargs[*]}' -- '$to' <<< '${body[*]}'"

# -------------------------------------------------------------------------------
#  GENERA EL LATEX DEL NOTEBOOK
# -------------------------------------------------------------------------------

gen_latex=( 
'jupyter nbconvert ${NB_OUT}'
'--to latex --template Template_report.tplx'
)	
#eval "${gen_latex[*]}"

rm *.gz *.out *.aux *.toc
