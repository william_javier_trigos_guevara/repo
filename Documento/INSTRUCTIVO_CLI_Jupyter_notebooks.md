## Verificando el nombre del kernel y la ubicación del ejecutable de python

la idea es darse cuenta del nombre del kernel que contiene el ejecutable de python que necesito para ejecutar mi notebook.

**Comando consola ** 
`jupyter kernelspec list`

**Resultado** 
``` bash
Available kernels:
   python2    /home/zenbook/anaconda3/envs/py27/lib/python2.7/site-packages/ipykernel/resources
```
igualmente se puede verificar los binarios de python disponibles con 

`which -a python`

``` bash
/home/zenbook/anaconda3/bin/python
/usr/bin/python
```

## Activando el ambiente (env)
La idea es activar el ambiente que contiene el kernel que necesito para ejecutar mi notebook.
**Comando consola ** 
`source activate py27`
**Resultado** 
``` bash
(py27) zenbook @ zenbook-UX32VD  /media/DATOS/0_bitbucket/repo/Documento
└─ $ ▶
```