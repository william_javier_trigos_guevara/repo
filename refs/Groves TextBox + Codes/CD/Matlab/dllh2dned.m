function dned = dllh2dned(llh, llh0)
% --------------------------------------------------
% Transformation from longitude, latitude and height
% to NED coordinates. Using flat Earth assumption.
% Ref: Wenstad(2010) - GPS guided r/c car
%
% Author:   Harald N�kland
% Date:     June 2011
% --------------------------------------------------
r_e = 6378137;          % WGS-84 data
r_p = 6356752;
e = 0.08181979099211;

l  = llh0(1)*pi/180;
mu = llh0(2)*pi/180;
h  = llh0(3);

dl  = llh(1)*pi/180 - l;
dmu = llh(2)*pi/180 - mu;
dh  = llh(3) - h;

tmp = 1-(e*sin(mu))^2;

dx  = ( r_e*(1-e^2)/tmp^(3/2)+h )*dmu;
dy  = ( r_e/tmp^(1/2)+h )*cos(mu)*dl;
dz  = -dh;

dned = [dx dy dz]';
