rm -rf merged.tex merged.ipynb *.png

python nbmerge.py ../Avances/0_WLS-Symbolic_Cap_libro.ipynb ../Avances/Coop_simple_010.ipynb > merged.ipynb

# https://github.com/paulgb/runipy
source activate py27
#runipy merged.ipynb merged-output.ipynb

#http://howtoprogram.eu/question/python-jupyter-running-jupyter-with-multiple-python-and-ipython-paths,18720
# http://nbconvert.readthedocs.io/en/latest/execute_api.html#execution-arguments-traitlets
#jupyter nbconvert --to notebook --ExecutePreprocessor.kernel_name=python2 --ExecutePreprocessor.timeout=3000 --execute merged.ipynb
bash -c "time source ${HOME}/anaconda3/bin/activate py27; \
		jupyter nbconvert \
		--to notebook \
		--ExecutePreprocessor.kernel_name=python2 \
		--ExecutePreprocessor.enabled=True \
		--ExecutePreprocessor.timeout=3000 \
		--execute --inplace merged.ipynb --output=Master_Book.ipynb"

source deactivate

./Reporte_localmente.sh Master_Book.ipynb biblio.bib
