function [q deps bgyr pos vel bacc] = AP_MEKF_qmethod(gyr, mag, acc,...
    gps_pos, gps_vel, init, valid_mag, valid_acc, valid_pos, valid_vel)
% ------------------------------------------------------------------------
% AP_MEKF - Multiplicative Extended Kalman Filter (q-method),
% for Position, Velocity and Attitude estimation.
%
% x = [deps bgyr pos vel bacc]
% u = [gyr acc]
% y = [deps gps_pos gps_vel]
%
% Input     Description         Unit        Frame
% ...........................................................
% gyr       gyro                [rad/s]     body
% mag       magnetometer        [a.u.]      body
% acc       accelerometer       [m/s^2]     body
% gps_pos   gps position        [m]         ned
% gps_vel   gps velocity        [m/s]       ned
%
% Output    Description         Unit        Frame
% ...........................................................
% q         quaternion          []          body to ned
% deps      delta epsilon       []          body to body_hat
% bgyr      bias gyro           [rad/s]     body
% pos       position            [m]         ned
% vel       velocity            [m/s]       ned
% bacc      bias accelerometer  [m/s^2]     body
%
% Author:   Harald N�kland
% Date:     June 2011
% ------------------------------------------------------------------------
h = 0.01;                           % Sampling interval
n = 15;                             % Number of states
decl = 0.0323;                      % Magnetic declination [rad]
g_ned = [0 0 9.81]';                % Gravity [m/s^2]
gps_arm = [-0.65 -0.08 -0.90]';     % GPS lever arm [m]

var_deps = [5e-7 5e-7 5e-7];        % f(Gyro variance)
var_bgyr = [1e-9 1e-9 1e-9];        % f(Gyro bias variance)
var_pos  = [1e-0 1e-0 1e-0];
var_vel  = [7e-4 7e-4 7e-4];        % f(Accelerometer variance)
var_bacc = [1e-7 1e-7 1e-7];        % f(Accelerometer bias variance)
process  = [var_deps var_bgyr var_pos var_vel var_bacc];
Q = diag(process);                  % Process noise

var_deps = [4e-4 4e-4 4e-4];        % qmethod variance
var_pos  = [1e-1 1e-1 1e-1];        % GPS position variance
var_vel  = [5e-4 5e-4 5e-4];        % GPS velocity variance
meas     = [var_deps var_pos var_vel];
R = diag(meas);                     % Measurement noise
% ------------------------------------------------------------------------
persistent q_hat deps_bar bgyr_bar pos_bar vel_bar bacc_bar vel_dot P_bar;
if init 
    deps_bar = [0 0 0]';        % Initial delta epsilon
    bgyr_bar = [0 0 0]';        % Initial bias gyro
    pos_bar  = -gps_arm;        % Initial position
    vel_bar  = [0 0 0]';        % Initial velocity
    bacc_bar = [0 0 0]';        % Initial bias accelerometer
    vel_dot  = [0 0 0]';
    q_hat = qmethod(100,1,[0 0 1]',[cos(decl) sin(decl) 0]',...
        -acc/norm(acc),mag/norm(mag));      % Initial attitude
    P_bar = diag([1e-5 1e-5 1e-5   1e-9 1e-9 1e-9   1 1 1 ...
        1e-3 1e-3 1e-3   1e-8 1e-8 1e-8]);  % Initial error covariance
end

% Predicted rotation matrix:
dq_bar = qbuild(deps_bar);
R_bar  = Rquat(q_hat)*Rquat(dq_bar);

% Magnetic field reference vector:
m_ned  = R_bar*mag;
m_ned  = [norm(m_ned(1:2))*cos(decl) norm(m_ned(1:2))*sin(decl) m_ned(3)]';

% Acceleration reference vector:
a_ned = vel_dot - g_ned;
a     = acc - bacc_bar;

% q-method:
q = qmethod(1,1,a_ned/norm(a_ned),m_ned/norm(m_ned),a/norm(a),mag/norm(mag));
if norm(q+q_hat) < norm(q-q_hat) 
    q = -q;
end
dq = qmult(qinv(q_hat),q);

% Real measurement:
y = [dq(2:4); gps_pos; gps_vel];

% Estimated measurement:
y_bar  = [ deps_bar
           pos_bar + R_bar*gps_arm
           vel_bar + R_bar*Smtrx(gyr-bgyr_bar)*gps_arm ];

% Compute Kalman gain:
% H1 = [ eye(3,3)
%        Rquat(q_hat)*Vmtrx(deps_bar, gps_arm))
%        Rquat(q_hat)*Vmtrx(deps_bar, Smtrx(gyr-bgyr_bar)*gps_arm) ];
% H2 = [ zeros(3,3)           zeros(3,3) zeros(3,3) zeros(3,3)
%        zeros(3,3)           eye(3,3)   zeros(3,3) zeros(3,3)
%        R_bar*Smtrx(gps_arm) zeros(3,3) eye(3,3)   zeros(3,3) ];
% H = [H1 H2];
H = [ eye(3,3)      zeros(3,3) zeros(3,3) zeros(3,3) zeros(3,3)
      zeros(3,3)    zeros(3,3) eye(3,3)   zeros(3,3) zeros(3,3)
      zeros(3,3)    zeros(3,3) zeros(3,3) eye(3,3)   zeros(3,3) ];
K = P_bar*H'/(H*P_bar*H' + R);

% Dead-reckoning:
valid_deps = valid_mag & valid_acc;
if valid_deps==0, K(:,1:3)=zeros(n,3); end
if valid_pos==0,  K(:,4:6)=zeros(n,3); end
if valid_vel==0,  K(:,7:9)=zeros(n,3); end

% Update estimate with measurement:
deps_hat = deps_bar + K(1:3,:)  *(y - y_bar);    
bgyr_hat = bgyr_bar + K(4:6,:)  *(y - y_bar);
pos_hat  = pos_bar  + K(7:9,:)  *(y - y_bar);
vel_hat  = vel_bar  + K(10:12,:)*(y - y_bar);
bacc_hat = bacc_bar + K(13:15,:)*(y - y_bar);
if norm(deps_hat)>1, deps_hat=0.99*deps_hat/norm(deps_hat); end  %sat_deps

% Reset:
dq_hat   = qbuild(deps_hat);
q_hat    = qmult(q_hat, dq_hat);
q_hat    = q_hat/norm(q_hat);
deps_hat = [0 0 0]';

% Compute error covariance for updated estimate:
IKH = eye(n) - K*H;
P   = IKH*P_bar*IKH' + K*R*K';

% Project ahead:
deps_dot = 0.5*(gyr - bgyr_hat);
bgyr_dot = [0 0 0]';
pos_dot  = vel_hat;
vel_dot  = Rquat(q_hat)*(acc - bacc_hat) + g_ned;
bacc_dot = [0 0 0]';

deps_bar = deps_hat + h*deps_dot;
bgyr_bar = bgyr_hat + h*bgyr_dot;
pos_bar  = pos_hat  + h*pos_dot; 
vel_bar  = vel_hat  + h*vel_dot;
bacc_bar = bacc_hat + h*bacc_dot;
if norm(deps_bar)>1, deps_bar=0.99*deps_bar/norm(deps_bar); end  %sat_deps

PHI1 = [-0.5*Smtrx(gyr - bgyr_hat)
         zeros(3,3)
         zeros(3,3)
        -2*Rquat(q_hat)*Smtrx(acc - bacc_hat)
         zeros(3,3) ];
PHI2 = [-0.5*eye(3,3) zeros(3,3) zeros(3,3)  zeros(3,3)
         zeros(3,3)   zeros(3,3) zeros(3,3)  zeros(3,3)
         zeros(3,3)   zeros(3,3) eye(3,3)    zeros(3,3)
         zeros(3,3)   zeros(3,3) zeros(3,3) -Rquat(q_hat)
         zeros(3,3)   zeros(3,3) zeros(3,3)  zeros(3,3) ];

PHI   = eye(n,n) + h*[PHI1 PHI2];       
GAMMA = h*eye(n,n);
P_bar = PHI*P*PHI' + GAMMA*Q*GAMMA';

% Output:
q    = q_hat;
deps = deps_hat;
bgyr = bgyr_hat;
pos  = pos_hat;
vel  = vel_hat;
bacc = bacc_hat;

