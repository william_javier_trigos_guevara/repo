function V = Vmtrx(eps, v)
% --------------------------------------------------
% Jacobian of rotation matrix:
% Returns the partial derivative of  R(q)*v  with 
% respect to eps, when q = [sqrt(1-eps'*eps); eps].
%
% Author:   Harald N�kland
% Date:     June 2011
% --------------------------------------------------
V = -2*sqrt(1-eps'*eps)*Smtrx(v) ...
    +2/sqrt(1-eps'*eps)*Smtrx(v)*(eps*eps') ...
    +2*(v'*eps)*eye(3,3) ...
    +2*eps*v' ...
    -4*v*eps';