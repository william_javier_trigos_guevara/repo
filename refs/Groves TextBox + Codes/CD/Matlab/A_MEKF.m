function [q deps bgyr] = A_MEKF(gyr, mag, acc, init, valid_mag, valid_acc)
% ------------------------------------------------------------------------
% A_MEKF - Multiplicative Extended Kalman Filter for Attitude estimation
%
% x = [deps bgyr]
% u = [gyr]
% y = [mag acc]
%
% Input     Description         Unit        Frame
% ...........................................................
% gyr       gyro                [rad/s]     body
% mag       magnetometer        [a.u.]      body
% acc       accelerometer       [m/s^2]     body
%
% Output    Description         Unit        Frame
% ...........................................................
% q         quaternion          []          body to ned
% deps      delta epsilon       []          body to body_hat
% bgyr      bias gyro           [rad/s]     body
%
% Author:   Harald N�kland
% Date:     June 2011
% ------------------------------------------------------------------------
h = 0.01;                           % Sampling interval
n = 6;                              % Number of states
decl = 0.0323;                      % Magnetic declination [rad]
g_ned = [0 0 9.81]';                % Gravity [m/s^2] 

var_deps = [1e-6 1e-6 1e-6];        % f(Gyro variance)
var_bgyr = [1e-8 1e-8 1e-8];        % f(Gyro bias variance)
process  = [var_deps var_bgyr];
Q = diag(process);                  % Process noise

var_mag  = [7e-5 7e-5 7e-5];        % Magnetometer variance
var_acc  = [1e-2 1e-2 1e-2];        % Accelerometer variance
meas     = [var_mag var_acc];
R = diag(meas);                     % Measurement noise
% ------------------------------------------------------------------------
persistent q_hat deps_bar bgyr_bar P_bar;
if init 
    deps_bar = [0 0 0]';        % Initial delta epsilon
    bgyr_bar = [0 0 0]';        % Initial bias gyro
    q_hat = qmethod(100,1,[0 0 1]',[cos(decl) sin(decl) 0]',...
        -acc/norm(acc),mag/norm(mag));               % Initial attitude
    P_bar = diag([1e-5 1e-5 1e-5   1e-9 1e-9 1e-9]); % Initial error covariance
end

% Real measurement:
y = [mag; acc];

% Predicted rotation matrix:
dq_bar = qbuild(deps_bar);
R_bar  = Rquat(q_hat)*Rquat(dq_bar);

% Magnetic field reference vector:
m_ned  = R_bar*mag;
m_ned  = [norm(m_ned(1:2))*cos(decl) norm(m_ned(1:2))*sin(decl) m_ned(3)]';
% m_ned = [13605.0 439.0 49863.6]';
% m_ned = m_ned/norm(m_ned);

% Estimated measurement:
y_bar  = [ R_bar'*m_ned
          -R_bar'*g_ned ];

% Compute Kalman gain:
H = [ Wmtrx(deps_bar, Rquat(q_hat)'*m_ned) zeros(3,3)
     -Wmtrx(deps_bar, Rquat(q_hat)'*g_ned) zeros(3,3) ];
K = P_bar*H'/(H*P_bar*H' + R);

% Dead-reckoning:
if valid_mag==0, K(:,1:3)=zeros(n,3); end
if valid_acc==0, K(:,4:6)=zeros(n,3); end

% Update estimate with measurement:
deps_hat = deps_bar + K(1:3,:)*(y - y_bar);    
bgyr_hat = bgyr_bar + K(4:6,:)*(y - y_bar);
if norm(deps_hat)>1, deps_hat=0.99*deps_hat/norm(deps_hat); end  %sat_deps

% Reset:
dq_hat   = qbuild(deps_hat);
q_hat    = qmult(q_hat, dq_hat);
q_hat    = q_hat/norm(q_hat);
deps_hat = [0 0 0]';

% Compute error covariance for updated estimate:
IKH = eye(n) - K*H;
P   = IKH*P_bar*IKH' + K*R*K';

% Project ahead:
deps_dot = 0.5*(gyr - bgyr_hat);
bgyr_dot = [0 0 0]';

deps_bar = deps_hat + h*deps_dot;
bgyr_bar = bgyr_hat + h*bgyr_dot;
if norm(deps_bar)>1, deps_bar=0.99*deps_bar/norm(deps_bar); end  %sat_deps

PHI = eye(n,n) + h*[ -0.5*Smtrx(gyr - bgyr_hat) -0.5*eye(3,3)
                      zeros(3,3)                 zeros(3,3) ];        
GAMMA = h*eye(n,n);
P_bar = PHI*P*PHI' + GAMMA*Q*GAMMA';

% Output:
q    = q_hat;
deps = deps_hat;
bgyr = bgyr_hat;

