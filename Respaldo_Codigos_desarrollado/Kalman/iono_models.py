# -*- coding: utf-8 -*-
"""
def get_sat_elv_az(svs, rx):
    #http://www.naic.edu/aisr/GPSTEC/drewstuff/MATLAB/elavazim.m
    #https://goo.gl/2Kt8Pf
    # https://sourceforge.isae.fr/projects/building-blocks-for-a-leo-ground-station/wiki/Distance_Elevation_and_Azimuth_Calculation
    svs_enu = ecef2enu(svs[:,0], svs[:,1], svs[:,2], rx)
    east, north, up  = svs_enu[0], svs_enu[1], svs_enu[2]

    elevation = up/np.linalg.norm(svs_enu, axis=0)
    azimuth = np.arctan2(east,north)#radians
    idx = np.where(azimuth<0)
    azimuth[idx] += 2*np.pi;
    return elevation, azimuth # radians
"""


def get_sat_elv_az(svs, rx):
    """
    http://www.naic.edu/aisr/GPSTEC/drewstuff/MATLAB/elavazim.m
    https://goo.gl/2Kt8Pf
    # https://sourceforge.isae.fr/projects/building-blocks-for-a-leo-ground-station/wiki/Distance_Elevation_and_Azimuth_Calculation
    """
    #svs = apply_earth_rotation_to_svs_position(svs, prs)

    #d_svs = r_com[:,:3]/r_mag[:, None]
    svs_enu = ecef2enu(svs, rx)

    east, north, up  = svs_enu[0], svs_enu[1], svs_enu[2]

    elevation = up/np.linalg.norm(svs_enu, axis=0)
    azimuth = np.arctan2(east,north)#radians
    idx = np.where(azimuth<0)
    azimuth[idx] += 2*np.pi;
    return elevation, azimuth # radians

def Klobuchar(A,E,time, rx_pos, alphas, betas):
        ####klobuchar  model

        lat, lon, alt = ecef2lla(X=rx_pos[0], Y=rx_pos[1], Z=rx_pos[2])
        C = gpstk.C_MPS #2.99792458e8

        ######bla bla
        phi=(0.0137/(E-0.11))-0.022
        psi=lat+phi*np.cos(A)#2
        if psi > 0.416:
            psi=psi+0.416
        elif psi < -0.416:
            psi=psi-0.416

        lampda=lon+(phi*np.sin(A))/np.cos(psi)#3
        phi_m=lampda+0.064*np.cos(psi-1.617)#4
        t=43200*lampda+time#.getSecondOfDay()#5
        if t > 86400:
            t=t-86400
        elif t < 0:
            t=t+86400
        ##basados en el modelo expuesto en el link, se cambian el exponente de la sumatoria.
        ##http://what-when-how.com/gps/ionospheric-effects-physical-influences-of-gps-surveying-part-2/
        A_I=alphas[0]+alphas[1]*phi_m+alphas[2]*phi_m**2+alphas[3]*phi_m**3 #6
        if A_I < 0:
            A_I=0

        P_I=betas[0]+betas[1]*phi_m+betas[2]*phi_m**2+betas[3]*phi_m**3 #7
        if P_I < 72000:
            P_I=72000

        X_I=2*np.pi*(t-50400)/P_I #8
        F=1+16*(0.53-E)**3#9

        if np.fabs(X_I) > 1.57:#10
            IL1_GPS=((5e-9)+A_I*(1-(((X_I)**2)/2)+((X_I)**4)/24))*F*C
        elif np.fabs(X_I) < 1.57:
            IL1_GPS=(5e-9)*F*C
        else :
            IL1_GPS=(5e-9)*F*C
        return IL1_GPS#/F
    
    
    
def get_ionodelay(rx_pos, gps_time, dat, 
                  codigo="C1", 
                  apply_iono_model="", apply_earth_rotation=True):
    
    c = gpstk.C_MPS #299792458
    R_earth = 6367444.657  # 6,371 km
    h_iono  = 400e3
    
    svs = np.array(dat["prns_pos"], dtype=pd.Series)[0]
    prs = np.array(dat[codigo], dtype=pd.Series)[0]
    if apply_earth_rotation:
        svs = apply_earth_rotation_to_svs_position(svs, prs)
        
    clocks = dat["prns_clockbias"]
    SNR1 = np.array(dat["S1"], dtype=pd.Series)[0]
    alfas = np.array(dat["alphas"], dtype=pd.Series)[0]
    betas = np.array(dat["betas"], dtype=pd.Series)[0]
    iono_dual = np.array(dat["Iono"], dtype=pd.Series)[0]
    
    def iono_3dmodel(rs, d_iono, elevs):
        num1 = d_iono**2*(np.sqrt(rs**2*np.cos(elevs))*np.sin(elevs)**2)
        den1 = np.power(2*rs*np.cos(elevs)**2, 3) #8*rs**3*np.cos(elev)**6 #

        num2 = rs
        den2 = rs*np.cos(elevs)

        return num2/den2 + 1/8*(d_iono/rs)*(d_iono/rs)*np.tan(elevs) #num1/den1

    if apply_iono_model=='taylor':
        #print "a", apply_iono_model
        rs, d_iono = R_earth+h_iono, 10e3
        elv, az = get_sat_elv_az(svs, rx_pos[:3])
        io1 = iono_3dmodel(rs, d_iono, elv)

    elif apply_iono_model=='standard':
        R = R_earth
        elevs, az = get_sat_elv_az(svs, rx_pos[:3])
        io1 = np.array(1/np.cos(np.arcsin(R/(R+350000))*np.sin(elevs)))
        #io1 = np.array(np.sqrt(1 - (R/(R+350000))*np.cos(elevs)**2))

    elif apply_iono_model=='empiric2':
        e, a = get_sat_elv_az(svs, rx_pos[:3])
        #print len(S1), len(e), len(a)
        #io1 = (a/40.3)*(dat["S1_master"]/e)
        #io1 = (a/40.3)*(0.1 + np.exp(-((e/dat["S1_master"]))))
        N = np.shape(svs)[0]
        #io1 = (1./2)*(N - e/SNR1 + np.random.normal(N, 1)*np.exp(-e/SNR1))
        #io1 = (- e/SNR1 + np.power(np.random.normal(SNR1, 2), .5)*np.exp(-e/SNR1)**3)
        io1 = (np.power(np.random.normal(SNR1, 2), 3.5)*np.exp(-e/SNR1)**3)/40.3e3
        #print len(io1), io1

    elif apply_iono_model=='wallas':
        e, a = get_sat_elv_az(svs, rx_pos[:3])
        #print len(S1), len(e), len(a)
        #io1 = (a/40.3)*(dat["S1_master"]/e)
        #io1 = (a/40.3)*(0.1 + np.exp(-((e/dat["S1_master"]))))
        N = np.shape(svs)[0]
        #SNR1 = dat["S1_master"]
        N = 0
        io1 = 0.7*(-N + np.random.normal(SNR1, 1)*np.exp(-e/SNR1))/2
        #print len(io1), io1

    elif apply_iono_model=='klobu':
        elv, az = get_sat_elv_az(svs, rx_pos[:3])
        io1 = np.array([Klobuchar(az,elv, gps_time, x[:3], alfas, betas) for az, elv in zip(az, elv)])

    elif apply_iono_model=='dual_freq':
        #print apply_iono_model, dat["Iono_master"][:4], dat["P1_master"][:4], dat["P2_master"][:4]
        io1 = iono_dual

    else:
        io1 = np.zeros(len(svs))
        pass
    
    return io1