# -*- coding: utf-8 -*-
"""
Editor de Spyder

Este es un archivo temporal
"""

from utils import *
from multirover import *
from os import (listdir, getcwd, system)

dia1 = "010"
dian = "010"
ano = "2017"

Observable = "C1"
# Variables de la grafica
error_max = 100

# Ubicacion de la Rinex local DB 
Rinex_dataBase      = "./../new_data/"
Simu_Folder         = getcwd() +"/" + "Temp_Simul_data"

# agregando nuevos pares de estaciones (que no estan en el kmz)
parejas_kmz = {}
#parejas_kmz[0] = ("CN20", "TGPM",)
#parejas_kmz[0] = ("p553", "p554", "ljrn",)#, "fzhs",)
parejas_kmz[1] = ("mida", "land", "pomm",)#, "cand",)#, "p790",)

RinexDB = descarga_Rinex_Compri(Rinex_dataBase, parejas_kmz, dia1, dian, ano)
## Leer los rinex almacenado en mi equipo
RinexDB = read_Rinex_Compri(Rinex_dataBase, parejas_kmz)


R, d = experiment_multirover(Observable, error_max, parejas_kmz, RinexDB, dia1, dian, ano, 
                             distances=None,
                             Simu_Folder=None,
                             res_plot=1000,
                             iono_model_master=None,
                             iono_model_rover =None)


