function [q bgyr] = A_EKF(gyr, mag, acc, init, valid_mag, valid_acc)
% ------------------------------------------------------------------------
% A_EKF - Extended Kalman Filter for Attitude estimation
%
% x = [q bgyr]
% u = [gyr]
% y = [mag acc]
%
% Input     Description         Unit        Frame
% ...........................................................
% gyr       gyro                [rad/s]     body
% mag       magnetometer        [a.u.]      body
% acc       accelerometer       [m/s^2]     body
%
% Output    Description         Unit        Frame
% ...........................................................
% q         quaternion          []          body to ned
% bgyr      bias gyro           [rad/s]     body
%
% Author:   Harald N�kland
% Date:     June 2011
% ------------------------------------------------------------------------
h = 0.01;                           % Sampling interval
n = 7;                              % Number of states
decl = 0.0323;                      % Magnetic declination [rad]
g_ned = [0 0 9.81]';                % Gravity [m/s^2] 

var_q    = [1e-6 1e-6 1e-6 1e-6];   % f(Gyro variance)
var_bgyr = [1e-8 1e-8 1e-8];        % f(Gyro bias variance)
process  = [var_q var_bgyr];
Q = diag(process);                  % Process noise

var_mag  = [7e-5 7e-5 7e-5];        % Magnetometer variance
var_acc  = [1e-2 1e-2 1e-2];        % Accelerometer variance
meas     = [var_mag var_acc];
R = diag(meas);                     % Measurement noise
% ------------------------------------------------------------------------
persistent q_bar bgyr_bar P_bar;
if init 
    bgyr_bar = [0 0 0]';        % Initial bias gyro
    q_bar = [1 0 0 0]';         % Initial attitude
    P_bar = diag([1e-5 1e-5 1e-5 1e-5   1e-9 1e-9 1e-9]); % Initial error covariance
end

% Real measurement:
y = [mag; acc];

% Predicted rotation matrix:
R_bar  = Rquat(q_bar);

% Magnetic field reference vector:
m_ned  = R_bar*mag;
m_ned  = [norm(m_ned(1:2))*cos(decl) norm(m_ned(1:2))*sin(decl) m_ned(3)]';
% m_ned = [13605.0 439.0 49863.6]';
% m_ned = m_ned/norm(m_ned);

% Estimated measurement:
y_bar  = [ R_bar'*m_ned
          -R_bar'*g_ned ];

% Compute Kalman gain:
H = [ eye(3,4) zeros(3,3)       %%H skal v�re noe annet!!!
     -eye(3,4) zeros(3,3) ];
K = P_bar*H'/(H*P_bar*H' + R);

% Dead-reckoning:
if valid_mag==0, K(:,1:3)=zeros(n,3); end
if valid_acc==0, K(:,4:6)=zeros(n,3); end

% Update estimate with measurement:
q_hat    = q_bar    + K(1:4,:)*(y - y_bar);    
bgyr_hat = bgyr_bar + K(5:7,:)*(y - y_bar);

% Normalize unit quaternion:
q_hat = q_hat/norm(q_hat);

% Compute error covariance for updated estimate:
IKH = eye(n) - K*H;
P   = IKH*P_bar*IKH' + K*R*K';

% Project ahead:
q_dot    = 0.5*Tquat(q_hat)*(gyr - bgyr_hat);
bgyr_dot = [0 0 0]';

q_bar    = q_hat    + h*q_dot;
bgyr_bar = bgyr_hat + h*bgyr_dot;

PHI = eye(n,n) + h*[ 0.5*Omega(gyr - bgyr_hat) -0.5*Tquat(q_hat)
                     zeros(3,4)                 zeros(3,3) ];        
GAMMA = h*eye(n,n);
P_bar = PHI*P*PHI' + GAMMA*Q*GAMMA';

% Normalize unit quaternion:
q_bar = q_bar/norm(q_bar);

% Output:
q    = q_hat;
bgyr = bgyr_hat;

