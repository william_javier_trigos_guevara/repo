# -*- coding: utf-8 -*-
import os
from os import (listdir, getcwd, system)
from os.path import (basename, splitext, abspath,
                     dirname, split, isfile)
from re import search, findall, compile
import re, urllib2, sys, copy, time

import gpstk
import numpy as np
import pandas as pd
from numba import autojit

import matplotlib.pyplot as plt
from IPython.display import HTML

global lab
lab = 0


# PARA EL MANEJO DE CONVERSION DATAFRAMES A LATEX
# https://stackoverflow.com/questions/20685635/pandas-dataframe-as-latex-or-html-table-nbconvert
import pandas as pd
pd.set_option('display.notebook_repr_html', True)
pd.set_option('precision', 3)
pd.set_option('max_colwidth',44)

def _repr_latex_(self):
    return "\centering{%s}" % self.to_latex()
pd.DataFrame._repr_latex_ = _repr_latex_  # monkey patch pandas DataFrame

# Make figures to pdf
# https://goo.gl/DhQHFL
# http://blog.rtwilson.com/how-to-get-nice-vector-graphics-in-your-exported-pdf-ipython-notebooks/
from IPython.display import set_matplotlib_formats
set_matplotlib_formats('png','pdf')


#https://stackoverflow.com/questions/34106622/how-to-convert-html-output-for-pandas-dataframes-to-latex-when-using-nbconvert
#https://stackoverflow.com/questions/20685635/pandas-dataframe-as-latex-or-html-table-nbconvert
#from IPython.display import Latex
#Latex(df_stations.to_latex())

# https://stackoverflow.com/questions/32855828/how-to-add-latex-figure-captions-below-jupyter-matplotlib-figures
from IPython.display import display, clear_output, Javascript, Latex
def makeplot_latex( plt, figlabel, figcaption, bShowInline = False):
    figname = figlabel+'.png'

    #plt.savefig(figname, format='pdf')
    plt.savefig(figname, format='png')

    if bShowInline:
        plt.show()
    else:
        plt.close()

    strLatex="""
    \\begin{figure}[b]
    \centering
        \includegraphics[totalheight=10.0cm]{%s}
        \caption{%s}
        \label{fig:%s}
    \end{figure}"""%(figname, figcaption, figlabel)

    strLatex2="""
    \\begin{figure}
        \\begin{center}
        	\\adjustimage{max size={0.9\linewidth}{0.6\paperheight}}{%s}
        \\end{center}
        \\caption{%s}
        \\label{%s}
    \\end{figure}"""%(figname, figcaption, figlabel)

    #print figname, figcaption, figlabel
    return display(Latex(strLatex2))


from IPython.display import display, Math, Latex, Image

def save(path, ext='png', close=True, verbose=True, axis_state='off'):
    """Save a figure from pyplot.
    http://www.jesshamrick.com/2012/09/03/saving-figures-from-pyplot/

    Parameters
    ----------
    path : string
        The path (and filename, without the extension) to save the
        figure to.
    ext : string (default='png')
        The file extension. This must be supported by the active
        matplotlib backend (see matplotlib.backends module).  Most
        backends support 'png', 'pdf', 'ps', 'eps', and 'svg'.
    close : boolean (default=True)
        Whether to close the figure after saving.  If you want to save
        the figure multiple times (e.g., to multiple formats), you
        should NOT close it in between saves or you will have to
        re-plot it.
    verbose : boolean (default=True)
        Whether to print information about when and where the image
        has been saved.
    """

    # Extract the directory and filename from the given path
    directory = os.path.split(path)[0]
    filename = "%s.%s" % (os.path.split(path)[1], ext)
    if directory == '':
        directory = '.'

    # If the directory does not exist, create it
    if not os.path.exists(directory):
        os.makedirs(directory)

    # The final path to save to
    savepath = os.path.join(directory, filename)

    if verbose:
        print("Saving figure to '%s'..." % savepath),

    plt.axis(axis_state)
    # Actually save the figure
    plt.savefig(savepath, bbox_inches='tight', dpi=300)

    # Close it
    if close:
        plt.close()

    if verbose:
        print("Done")

def section_title(placement, scenario=0):

    title_x_section = [r"""\subsection{\'Indice de mejora para escenario %s}"""%(scenario),
                       r"""\subsection{Gr\'aficas para escenario %s}"""%(scenario)]

    if isinstance(placement, str):
        return placement
    else:
        return title_x_section[placement]


class Table_latex():
    def __init__(self, table, ref, cap, landscape=False):
        self.cap = cap
        self.tab = table
        self.ref = ref
        self.landscape = landscape
    def _repr_html_(self):
        return '<center>{0}</center>'.format(self.cap)
    def _repr_latex_(self):
        #return '\\begin{figure}\n \includegraphics{'+self.fig+'}\n \caption{'+self.s+'}\n\\end{figure}'
        # \\adjustimage{max size={0.9\linewidth}{0.6\paperheight}}{%s}
        if self.landscape==True:
            self.begin = r"""
                            \begin{landscape}
                            \global\pdfpageattr\expandafter{\the\pdfpageattr/Rotate 90}"""
            self.end   = r"""
                            \global\pdfpageattr\expandafter{\the\pdfpageattr/Rotate 0}
                            \end{landscape}"""
        else:
            self.begin = ""
            self.end   = ""

        a = r"""
            %s
            \begin{table}[h!]
            \begin{minipage}{\linewidth}
            \begin{center}
            %s
            \caption{Tabla \ref{tab:%s} %s}
            \label{tab:%s}
            \end{center}
            \end{minipage}
            \end {table}
            %s"""%(self.begin, self.tab, self.ref, self.cap, self.ref, self.end)
        return a

class Caption():
    def __init__(self, fig, ref, cap, size=(1.05, 0.17)):
        self.cap = cap
        self.fig = fig
        self.ref = ref
        self.w   = size[0]
        self.h   = size[1]
    def _repr_html_(self):
        return '<center>{0}</center>'.format(self.cap)
    def _repr_latex_(self):
        #return '\\begin{figure}\n \includegraphics{'+self.fig+'}\n \caption{'+self.s+'}\n\\end{figure}'
        # \\adjustimage{max size={0.9\linewidth}{0.6\paperheight}}{%s}
        a = r"""
            \begin{figure}[h!]
            \begin{center}
            \adjustimage{width={%s\linewidth}, height={%s\paperheight}}{%s}
            \caption{Figura \ref{fig:%s} %s}
            \label{fig:%s}
            \end{center}
            \end{figure}"""%(self.w, self.h, self.fig, self.ref, self.cap, self.ref)
        return a


def new_section(title):
    style = "text-align:center;background:#66aa33;padding:30px;color:#ffffff;font-size:3em;"
    return HTML('<div style="{}">{}</div>'.format(style, title))

# SIMBOLOS DE ERROR
def tick_equis(c):
    if c == 0:  #imprime chulito
        return "✔ "
    else:
        return "✘ "


def is_number(s):
    # http://stackoverflow.com/questions/354038/how-do-i-check-if-a-string-is-a-number-float-in-python
    try:
        float(s) # for int, long and float
    except ValueError:
        try:
            complex(s) # for complex
        except ValueError:
            return False

    return True

def isempty(var):

    if var != None:# or str(var) != '':
        return True
    else:
        return False


@autojit
def process_day(day):
    if   day<10:
         day="00"+str(day)
    elif day<100:
         day="0"+str(day)
    else:
         day=str(day)+str("0")

    return day

@autojit
def process_day2(day):
    if   day<10:
         day="00"+str(day)+"0"
    elif day>9 and day<100:
         day="0"+str(day)+"0"
    else:
         day=str(day)+str("0")

    return day

#######################################################

# Full Guide to open files with python
# https://goo.gl/DWxWqR

def download(url, filename, dest_dir=os.getcwd()+"/"+"new_data/"):
    import urllib2

    if not os.path.exists(dest_dir):
        print dest_dir
        os.makedirs(dest_dir)

    filename = dest_dir+filename
    #page=urllib2.urlopen(url)
    #open(filename,'wb').write(page.read())

    testfile = urllib.URLopener()
    testfile.retrieve(url, filename)

def extract_from_zip(zip_file, in_path, outpath):
    """
        Descompresor funcional
    """
    import zipfile

    in_path = str(in_path)+"/"+zip_file
    #print in_path
    archive = zipfile.ZipFile(in_path, 'r')

    for f in archive.namelist():
        if f.endswith(('n', 'o')):
            makedir(Simu_Folder)
            archive.extract(f, Simu_Folder)
            fname, ext = splitext(zip_file)
            cmd = "mv "+Simu_Folder+"/"+str(f)+" "+Simu_Folder+"/"+fname[:-1]+f[-1]
            #print cmd
            system(cmd)


def unzip(filename, source_dir, dest_dir):
    """
	Descompresor no probado
    """
    import zipfile, os.path

    source_filename = source_dir+filename
    print source_filename

    if not os.path.exists(dest_dir):
        print dest_dir
        os.makedirs(dest_dir)

    with zipfile.ZipFile(source_filename) as zf:
        for member in zf.infolist():
            words = member.filename.split('/')
            path = dest_dir
            for word in words[:-1]:
                drive, word = os.path.splitdrive(word)
                head, word = os.path.split(word)
                if word in (os.curdir, os.pardir, ''): continue
                path = os.path.join(path, word)
            zf.extract(member, path)

def makedir(path):
    if not os.path.exists(path):
        print path
        os.makedirs(path)

def file_url_exist(location):
    request = urllib2.Request(location)
    request.get_method = lambda : 'HEAD'
    try:
        response = urllib2.urlopen(request)
        return True
    except urllib2.URLError, e:
        #print(e.args)
        return False
    except urllib2.HTTPError:
        return False

def urlretrieve(urllib2_request, filepath, reporthook=None, chunk_size=4096):
    """
    https://stackoverflow.com/questions/51212/how-to-write-a-download-progress-indicator-in-python
    """
    req = urllib2.urlopen(urllib2_request)

    if reporthook:
        # ensure progress method is callable
        if hasattr(reporthook, '__call__'):
            reporthook = None

        try:
            # get response length
            total_size = req.info().getheaders('Content-Length')[0]
        except KeyError:
            reporthook = None

    data = ''
    num_blocks = 0

    with open(filepath, 'w') as f:
        while True:
            data = req.read(chunk_size)
            num_blocks += 1
            if reporthook:
                # report progress
                reporthook(num_blocks, chunk_size, total_size)
            if not data:
                break
            f.write(data)

    # return downloaded length
    return len(data)

    #######################################################

def d_teta_calc(rx_1, rx_2):
    import numpy as np
    import nvector as nv
    from nvector import deg

    wgs84n = dict(a=6378137.0, f=1.0/298.257223563)
    p_EB_E1 = 6371e3 * np.vstack((rx_1))
    p_EB_E2 = 6371e3 * np.vstack((rx_2))

    n_EB_E1, z_EB1 = nv.p_EB_E2n_EB_E(p_EB_E1, **wgs84n)
    n_EB_E2, z_EB2 = nv.p_EB_E2n_EB_E(p_EB_E2, **wgs84n)

    lat_EB1, lon_EB1 = nv.n_E2lat_lon(n_EB_E1)
    lat_EB2, lon_EB2 = nv.n_E2lat_lon(n_EB_E2)
    h = -z_EB1
    lat1, lon1 = deg(lat_EB1), deg(lon_EB1)
    lat2, lon2 = deg(lat_EB2), deg(lon_EB2)
    #print lat1, lon1, lat2, lon2

    wgs84 = nv.FrameE(name='WGS84')

    pointA = wgs84.GeoPoint(latitude=lat1, longitude=lon1, degrees=True)
    pointB = wgs84.GeoPoint(latitude=lat2, longitude=lon2, degrees=True)

    p_AB_E = nv.diff_positions(pointA, pointB)

    frame_N = nv.FrameN(pointA)
    p_AB_N = p_AB_E.change_frame(frame_N)
    p_AB_N = p_AB_N.pvector.ravel()

    azimuth = np.arctan2(p_AB_N[1], p_AB_N[0])
    #teta = np.rad2deg(azimuth)
    #print 'azimuth = {%4.2f} deg' %(teta)

    d = np.linalg.norm(p_AB_E.pvector)

    return d, azimuth, p_AB_E.pvector.ravel()

def get_dist(rx_1, rx_2):
    import numpy as np
    import nvector as nv
    from nvector import deg

    lat1, lon1 = rx_1[0], rx_1[1]
    lat2, lon2 = rx_2[0], rx_2[1]
    #print lat1, lon1, lat2, lon2

    wgs84 = nv.FrameE(name='WGS84')

    pointA = wgs84.GeoPoint(latitude=lat1, longitude=lon1, degrees=True)
    pointB = wgs84.GeoPoint(latitude=lat2, longitude=lon2, degrees=True)

    p_AB_E = nv.diff_positions(pointA, pointB)

    frame_N = nv.FrameN(pointA)
    p_AB_N = p_AB_E.change_frame(frame_N)
    p_AB_N = p_AB_N.pvector.ravel()

    azimuth = np.arctan2(p_AB_N[1], p_AB_N[0])
    #teta = np.rad2deg(azimuth)
    #print 'azimuth = {%4.2f} deg' %(teta)

    d = np.linalg.norm(p_AB_E.pvector)

    return d, azimuth#, p_AB_E.pvector.ravel()

def read_kmz_file(fname, max_distancia):
    import sys
    import zipfile
    import glob
    from xml.dom import minidom

    #fname = "sites.kmz"
    path_cnt = 0
    zf = zipfile.ZipFile(fname, 'r')

    #print zf, type(zf)
    for fn in zf.namelist():
        #print fn
        if fn.endswith('.kml'):
            content = zf.read(fn)
            xmldoc = minidom.parseString(content)
            placemarks = xmldoc.getElementsByTagName('Placemark')

            nodes = {}
            for placemark in placemarks:
                nodename = placemark.getElementsByTagName("name")[0].firstChild.data
                coords = placemark.getElementsByTagName("coordinates")[0].firstChild.data
                lst1 = coords.split(",")
                longitude = float(lst1[0])
                latitude = float(lst1[1])
                nodes[nodename] = (latitude, longitude)

            from itertools import combinations
            #print nodes
            parejas = [{j: nodes[j] for j in i} for i in combinations(nodes, 2)]

            d_min = 1e8

            GPS_Stations = {}
            c = 0
            for n in parejas:
                #print n, type(n), n.keys()[0], n.values()[0]
                rx_1 = n.values()[0]
                rx_2 = n.values()[1]

                d,_ = get_dist(rx_1, rx_2)
                if (d_min > d):
                    if (d < max_distancia):
                        print d_min, d, "entre", n.keys()[0], "y", n.keys()[1]
                        #d_min = d
                        GPS_Stations[c] = [n.keys()[0], n.keys()[1]]
                        c += 1
    return GPS_Stations


def get_data_stations(f_in, max_dis):
    
    List_Stations = read_kmz_file(f_in, max_dis)

    stations = {}
    
    for (idx, two) in enumerate(List_Stations.iteritems()):
        #print two, type(two[1])
        two = [str(x) for x in two[1]]
        #print tuple(two)
        stations[idx] = tuple(two)
        
    return stations

def readAllStored_Rinex_Compri(data_folder, parejas_estaciones):
    import os, re

    # Nombre de la carpeta donde se guardara la descarga
    stations = [x.lower() for par in parejas_estaciones.values() for x in par]
    #verificando las parejas de estaciones de las que se tiene archivos rinex

    estaciones = {}

    dir_files = [fn for fn in listdir(data_folder)]
    #print dir_files

    for filename in dir_files: #os.listdir(data_folder):
        base, ext = splitext(filename)

        if base[:4].lower() in stations:
            station, ObsType = splitext(base)

            #print station
            path = data_folder+"/"

            # GRACIAS A ESTA RESPUESTO, EL CODIGO FUNCIONA
            # http://stackoverflow.com/questions/21613038/adding-new-keys-to-a-python-dictionary-from-variables?answertab=votes#tab-top
            # http://stackoverflow.com/questions/14012918/passing-dictionary-keys-to-a-new-dictionary
            if base[-1].lower() == 'o':
                estaciones.setdefault(station, {})['name_station'] = station[:4]
                estaciones.setdefault(station, {})['obs-zip'] = path+filename

            else:

                estaciones.setdefault(station, {})['name_station'] = station[:4]
                estaciones.setdefault(station, {})['nav-zip'] = path+filename

    return estaciones

def descarga_Rinex_Compri(data_folder, parejas_kmz, dia1, dian, ano):
    #import descargador_RINEX as dl_RINEX
    #import os
    #import urllib2
    #import re

    # Nombre de la carpeta donde se guardara la descarga
    #data_folder = os.getcwd() +"/" + data_folder      #"new_data"
    cmd1 = "mkdir -p " + data_folder    # COMANDO CREA CARPETA DE DESCARGA
    system(cmd1)

    dir_files = [fn for fn in listdir(data_folder)]
    #print dir_files

    # http://stackoverflow.com/questions/2486145/python-check-if-url-to-jpg-exists
    def file_exists(url):
        request = urllib2.Request(url)
        request.get_method = lambda : 'HEAD'
        try:
            response = urllib2.urlopen(request)
            return True
        except:
            return False

    def valid_extension(x, valid={'.gz', '.Z'}):
        return splitext(x)[1].lower() in valid

    # MAIN FUNCTION
    ###########################################################################S
    print "\n Descargando Nuevos Archivos ...."
    print "************************\n"

    #rint "Buscando Archivos para: "
    print "\t [Estacion] \t [dia GPS] \t\t [RESULTADO]"

    for key, value in parejas_kmz.iteritems():
        #print k, v[0], v[1], type(v[0]), str(v[0])
        for station in value:
            #print stat, type(stat)
            # BASADO EN EL CALENDARIO http://gps.topografia.upm.es/www/2015G.htm


            # RED Gadner tiene met, tropo, rinex files de casi todo
            # USADOS EN LA TESIS DE
            # http://www.inpe.br/pos_graduacao/cursos/geo/arquivos/dissertacoes/dissertacao_olusegun_folarin_2013.pdf


            # ftp://garner.ucsd.edu/pub/troposphere/0990/
            # ftp://garner.ucsd.edu/pub/met/2017/010/
            # ftp://garner.ucsd.edu/pub/rinex/2017/010/
            # ftp://garner.ucsd.edu/pub/
            # http://madrigal.haystack.mit.edu/madrigal/siteSpecific/tec_sources.html

            Servers = {
                'garner':{'ftp':'ftp://garner.ucsd.edu/pub/rinex/',
                        },
                'lox':{   'ftp':'ftp://lox.ucsd.edu/pub/rinex/',
                        },
                #
                'cors1' :{'ftp':'ftp://geodesy.noaa.gov/cors/rinex/',
                        },
                'cors2' :{'ftp':'ftp://alt.ngs.noaa.gov/cors/rinex/',
                        },
                #
                'igac-gov':{'ftp':'ftp://anonimo:anonimo@132.255.20.140/',
                        },
                'nasa'  :{'ftp':'ftp://cddis.gsfc.nasa.gov/pub/gps/data/daily/',
                        },
                'unavco':{'ftp':'ftp://data-out.unavco.org/pub/rinex/',
                        },
                'rbmc'  :{'ftp':'ftp://geoftp.ibge.gov.br/informacoes_sobre_posicionamento_geodesico/rbmc/dados/',
                        }
            }

            tipos_Rinex  = ['obs', 'nav', 'd']
            station = str(station.lower())

            print "\t %s" %(station)


            for i in range(int(dia1), int(dian)+1):
                """
                if i<10:
                    dia="00"+str(i)
                elif i<100:
                    dia="0"+str(i)
                else:
                    dia=str(i)
                """
                dia = process_day(i)

                c = 0
                flag = 0
                for server in Servers.keys():
                    c += 1
                    if flag == 1 or flag == 2:
                        # hacer que se acabe la busqueda en los servidores
                        c == len(Servers[server])

                    else:
                        fail = []     # guarda tipo de archivo que no se encontro para descarga
                        f_exist = []  # contar los archivos que existen

                        for tipo in tipos_Rinex:
                            #print tipo

                            #print "\t", tipo,
                            f_name = station+dia+'0.'+ano[-2:]+tipo[:1]

                            #http://stackoverflow.com/questions/17777311/filter-file-list-in-python-lowercase-and-uppercase-extension-files
                            pat = compile(r'[.](gz|Z)$', re.IGNORECASE)
                            filenames = [filename for filename in listdir(data_folder)
                                         if search(pat, filename)]

                            #matches = [re.search("("+f_name+".*)", f, flags=re.I) for f in filenames]
                            matches = [f for f in filenames if findall("("+f_name+".*)", f, flags=re.I)]

                            #print matches
                            if len(matches) != 0:
                                f_exist.append(tipo)
                                #print "esta en dir"

                                if len(f_exist) == len(tipos_Rinex):
                                    flag = 2
                                    #break

                            else:
                                #print "No esta en dir"

                                #Servers['igac-gov']['ftp']    = 'ftp://anonimo:anonimo@132.255.20.140/'

                                Servers['igac-gov']['path']    = dia+'/'+ano[-2:]+tipo[:1]+'/'
                                Servers['igac-gov']['archivo'] = str(f_name).upper()+'.gz'

                                #Servers['nasa']['ftp']         ='ftp://cddis.gsfc.nasa.gov/pub/gps/data/daily/'
                                Servers['nasa']['path']        = ano+'/'+dia+'/'+ano[-2:]+tipo[:1]+'/'
                                Servers['nasa']['archivo']     = f_name+'.Z'

                                #Servers['unavco']['ftp']       ='ftp://data-out.unavco.org/pub/rinex/'
                                Servers['unavco']['path']      = tipo+'/'+ano+'/'+dia+'/'
                                Servers['unavco']['archivo']   = f_name+'.Z'

                                Servers['garner']['path']      = ano+'/'+dia+'/'
                                Servers['garner']['archivo']   = f_name+'.Z'

                                Servers['lox']['path']         = ano+'/'+dia+'/'
                                Servers['lox']['archivo']      = f_name+'.Z'

                                #""
                                Servers['cors1']['path']      = ano+'/'+dia+'/'+station[:4]
                                Servers['cors1']['archivo']   = f_name+'.Z'

                                Servers['cors2']['path']      = ano+'/'+dia+'/'+station[:4]
                                Servers['cors2']['archivo']   = f_name+'.Z'
                                #""

                                ##################################################################################
                                if server == 'rbmc':

                                    #2017/010/ifsc0101.zip
                                    f_name = station+dia+'0.'+ano[-2:]+tipo[:1]
                                    Servers[server]['path'] = str(ano+'/'+dia+'/')
                                    Servers[server]['archivo'] = str(f_name).lower()+'.zip'

                                    ## convirtiendo a la convencion de nombres standar que yo manejo
                                    ## dentro de mi carpeta new_data

                                    filename = Servers['rbmc']['archivo']
                                    url = Servers[server]['ftp']+Servers[server]['path']+station+dia+'1'+".zip"

                                    ## Check if not exist in new_data folder
                                    if not os.path.exists(data_folder+filename):

                                        # Check if exist in webpage
                                        if file_url_exist(url):

                                            #print url
                                            makedir(data_folder)
                                            #makedir(simu_path)

                                            # FIRST VERSION
                                            #download(url, filename, dest_dir=folderpath)

                                            # OPTIMIZED VERSION
                                            urlretrieve(urllib2_request=url, filepath=data_folder+filename)
                                            #print "finish"
                                            #unzip(filename=filename, source_dir=down_path, dest_dir=simu_path)

                                            f_exist.append(tipo)

                                        else:
                                            if tipo not in fail:
                                                fail.append(tipo)
                                                pass

                                    else:
                                        flag = 2
                                        #print "Red RBMC"
                                        break

                                ##################################################################################


                                url = Servers[server]['ftp']+Servers[server]['path']
                                archivo = Servers[server]['archivo']

                                # Red gps new zeland
                                #https://apps.linz.govt.nz/ftp/positionz/2016/026/
                                #ftp://ftp.geonet.org.nz/gps/rinex/2016/003/

                                # GLONAS DESDE SERVIDOR NASA
                                #ftp://igscb.jpl.nasa.gov/igscb/glonass/products/1920/


                                if not os.path.exists(data_folder+archivo):
                                    #https://code-maven.com/urllib-vs-urllib2
                                    if file_exists(url+archivo):
                                        #print "existe url"
                                        #print url
                                        html_content = urllib2.urlopen(url).read()
                                        matches = re.findall(archivo, html_content, flags=re.I)

                                        if len(matches) != 0:

                                            #print archivo, url+archivo
                                            if flag == 0:

                                                cmd = "wget " + str(url+archivo) + " -P " + data_folder
                                                #print "\t", cmd
                                                system(cmd)

                                            f_exist.append(tipo)

                                    else:

                                        #print "No encontrado el archivo en ", server
                                        if tipo not in fail:
                                            fail.append(tipo)
                                            pass
                                        #print fail

                                else:
                                        flag = 2
                                        #print "Red RBMC"
                                        break


                        #if len(f_exist) == len(tipos_Rinex) and len(fail) == 0:
                        #    flag = 1


                    if c == len(Servers[server]):
                        if flag == 0:
                            print "\t\t\t %s \t\t No encontrados Ficheros %s" %(dia, fail)
                            #print dia, "

                        elif flag ==2:
                            print "\t\t\t %s \t\t Archivos ya disponibles!!!" %(dia)
                        else:
                            print "\t\t\t %s \t\t Descarga Exitosa!!!" %(dia)

    print "Descarga Finalizada!! \n"

    DB_Rinex_Compri = readAllStored_Rinex_Compri(data_folder, parejas_kmz)

    return DB_Rinex_Compri


def read_Rinex_Compri(data_folder, parejas):
    import os, re
    
    # Nombre de la carpeta donde se guardara la descarga
    stations = [x.lower() for par in parejas.values() for x in par]
    #verificando las parejas de estaciones de las que se tiene archivos rinex
    
    estaciones = {}

    dir_files = [fn for fn in listdir(data_folder)]

    for filename in listdir(data_folder):
        base, ext = splitext(filename)
        
        key = base[:-1]
        for par in parejas.values(): 
            for station in par:

                if base[:4].lower() == station.lower():
                    
                    path = data_folder+"/"
                    
                    obstype = str(base[-1]+'-zip').lower()
                    estaciones.setdefault(key, {})[obstype] = path+filename

    return estaciones