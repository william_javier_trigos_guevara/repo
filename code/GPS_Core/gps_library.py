# -*- coding: utf-8 -*-
from gps_utils import *

def is_number(s):
    # http://stackoverflow.com/questions/354038/how-do-i-check-if-a-string-is-a-number-float-in-python
    try:
        float(s) # for int, long and float
    except ValueError:
        try:
            complex(s) # for complex
        except ValueError:
            return False

    return True

import re
# http://stackoverflow.com/questions/2669059/how-to-sort-alpha-numeric-set-in-python
def sorted_nicely( l ):
    """ Sort the given iterable in the way that humans expect."""
    convert = lambda text: int(text) if text.isdigit() else text
    alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ]
    return sorted(l, key = alphanum_key)


@autojit
def compute_distances(rc, svs):
    # return np.array( [np.sqrt((rc[0]-sv[0])**2 + (rc[1]-sv[1])**2) for sv in svs] )
    return np.linalg.norm(rc-svs, axis=1)

def get_sat_elv_az(svs, rx):
    """
    http://www.naic.edu/aisr/GPSTEC/drewstuff/MATLAB/elavazim.m
    https://goo.gl/2Kt8Pf
    # https://sourceforge.isae.fr/projects/building-blocks-for-a-leo-ground-station/wiki/Distance_Elevation_and_Azimuth_Calculation
    """
    #svs = apply_earth_rotation_to_svs_position(svs, prs)

    #d_svs = r_com[:,:3]/r_mag[:, None]
    svs_enu = ecef2enu(svs, rx)

    east, north, up  = svs_enu[0], svs_enu[1], svs_enu[2]

    elevation = up/np.linalg.norm(svs_enu, axis=0)
    azimuth = np.arctan2(east,north)#radians
    idx = np.where(azimuth<0)
    azimuth[idx] += 2*np.pi;
    return elevation, azimuth # radians

@autojit
def predict_pseudoranges(x, prns_pos, prns_clockbias):
    c = 299792458
    rhos    = compute_distances(x[:3], prns_pos)
    pranges = rhos + x[3]-c*prns_clockbias
    return rhos, pranges

import pyproj
ecef = pyproj.Proj(proj='geocent', ellps='WGS84', datum='WGS84')
lla = pyproj.Proj(proj='latlong', ellps='WGS84', datum='WGS84')
def lla2ecef(lat,lon,alt, isradians=True):
    return pyproj.transform(lla, ecef, lon, lat, alt, radians=isradians)

def ecef2lla(X,Y,Z, isradians=True):
    lon, lat, alt = pyproj.transform(ecef, lla, X,Y,Z, radians=isradians)
    return lat, lon, alt

# https://github.com/scottyhq/insar_scripts/blob/master/ALOS/estimate_alos_baselines.py
def ecef2enu(pos, ref):
    """
    http://en.wikipedia.org/wiki/Geodetic_datum#Geodetic_versus_geocentric_latitude
    """
    xm,ym,zm = ref.flat #extracts elements of column or row vector
    # duplicate reference vector rows into matrix
    ref = np.vstack((ref,)*pos.shape[0])

    # get geodetic lat/lon/height (above wgs84) of satellite
    ecef = pyproj.Proj(proj='geocent',  ellps='WGS84', datum='WGS84')
    wgs84 = pyproj.Proj(proj='latlong', ellps='WGS84', datum='WGS84')
    lon, lat, h = pyproj.transform(ecef, wgs84, xm, ym, zm, radians=True)

    # make transformation matrix
    transform = np.array([
        [-np.sin(lon), np.cos(lon), 0.0],
        [-np.sin(lat)*np.cos(lon), -np.sin(lat)*np.sin(lon), np.cos(lat)],
        [np.cos(lat)*np.cos(lon), np.cos(lat)*np.sin(lon), np.sin(lat)]
    ])

    # do matrix multiplication
    enu = np.dot(transform, pos.T - ref.T)
    return enu


def get_data(dat_common, data_rx, epoc, csats, station_name):
    import types
    requested_obstypes = ["C1", "C2", "P1", "P2", "L1", "L2", "S1", "S2"]
    columns2search = ["prns", "alphas", "betas", "prns_elev", "prns_az", "prns_clockbias", "prns_pos", "Iono"]

    col_rename = {k:k+"_master" for k in columns2search+requested_obstypes}
    col_rename["prns"] = "prns"

    r = []
    data = {}

    # http://thomas-cokelaer.info/blog/2014/06/pandas-how-to-compare-dataframe-with-none/
    if len(dat_common.columns) == 0: #isinstance(dat_common, types.NoneType) == True: #dat_common.empty:
        #print('DataFrame is empty!')
        obstypes = [c for c in data_rx.columns if c in requested_obstypes]

        #print "csats", len(csats)

        idx1 = []
        for k, sat in enumerate(data_rx.iloc[epoc].prns):
            if sat in csats:
                idx1.append(k)

        columns = ["prns", "prns_elev", "prns_az", "prns_clockbias", "prns_pos", "Iono"]+obstypes
        names = []

        for col in data_rx.columns:
            if col in columns:
                names.append(col)
                values = data_rx.iloc[epoc][col][idx1]
            elif col == "prns":
                names.append(col)
                values = csats
            else:
                names.append(col)
                values = np.array(data_rx.iloc[epoc][col])

            r.append(values)

        r = pd.Series(r, index=names).to_frame()
        df_temp =pd.DataFrame(r.T, columns=names)
        df_temp.rename(columns=col_rename, inplace=True)
        return df_temp

    else:

        obstypes = [c for c in data_rx.columns if c in requested_obstypes]

        idx1 = []
        for k, sat in enumerate(data_rx.iloc[epoc].prns):
            if sat in csats:
                idx1.append(k)

        for obstype in obstypes:
            dat_common[obstype+"_"+station_name] = pd.Series([data_rx.iloc[epoc][obstype][idx1]], index=dat_common.index)

        columns = ["prns_elev", "prns_az", "prns_clockbias", "prns_pos", "Iono"]

        for col in columns2search:
            if col in columns:
                values = data_rx.iloc[epoc][col][idx1]
                key = col+"_"+station_name
            elif col == "prns":
                values = csats
                key = "csats"
            else:
                key = col+"_"+station_name
                values = np.array(data_rx.iloc[epoc][col])

            dat_common[key] = pd.Series([values], index=dat_common.index)

    return dat_common

def inspect_rinex(obsfile, navfile, silent_mode=False):
    obsHeader, obsData = gpstk.readRinex3Obs(obsfile)
    navHeader, navData = gpstk.readRinex3Nav(navfile)
    # setup ephemeris store to look for satellite positions
    bcestore = gpstk.GPSEphemerisStore()
    for navDataObj in navData:
        ephem = navDataObj.toGPSEphemeris()
        bcestore.addEphemeris(ephem)
    bcestore.SearchNear()
    navData.close()

    rec_pos = np.array([obsHeader.antennaPosition[0], obsHeader.antennaPosition[1], obsHeader.antennaPosition[2]])
    lat, lon, alt = ecef2lla(rec_pos[0], rec_pos[1], rec_pos[2], isradians=True)
    print ("receiver position lat=%f, lon=%f, alt=%f"%(np.rad2deg(lat), np.rad2deg(lon), alt))
    silent_mode = False
    requested_obstypes = ["P1", "P2", "L1", "L2", "C1", "C2", "S1", "S2"]
    obsidxs = []
    obstypes = []
    obsdefs = np.array([i for i in obsHeader.R2ObsTypes])
    for i in requested_obstypes:
        w = np.where(obsdefs==i)[0]
        if len(w)!=0:
            obsidxs.append(w[0])
            obstypes.append(i)
        else:
            if silent_mode == False:
                print ("WARNING! observation `"+i+"` no present in file "+obsfile)
            #else:
                #print ""

    obsObject = obsData.next()
    print ("Time of observation", obsObject.time)
    print ("SatID GPS  GLON"),
    for P1_idx, otype in zip(obsidxs, obstypes):
            print ("\t  ", otype),
    print ("")

    for satID, datumList in obsObject.obs.iteritems():

        isGPS     = True if satID.system==satID.systemGPS else False
        isGlonass = True if satID.system==satID.systemGlonass else False

        print ("  %2d %5s %5s"%(satID.id, isGPS, isGlonass)),

        for P1_idx, otype in zip(obsidxs, obstypes):
            P1 = obsObject.getObs(satID, P1_idx).data
            print ("%10.3f"%( P1)),

        print ("")
        #print "  %2d %5s %5s %10.3f %10.3f %10.3f %10.3f"%(satID.id, isGPS, isGlonass, P1, P2, L1, L2)

#inspect_rinex(obsfile1, navfile1)

def check_get_data(data1, data2, d_c):
    variables = ["gps_sow", "prns", "C1", "Iono"]

    c = 0
    for epoc in range(len(data1)):
        if epoc % res_plot == 0:
            for var in variables[:2]:
                print (np.array(data1.iloc[epoc][var]))
                print (np.array(data2.iloc[epoc][var]))
                d = d_c[c]
                print (np.array(d.iloc[0][var]))

            for var in variables[2:]:
                print (np.array(data1.iloc[epoc][var]))
                print (np.array(data2.iloc[epoc][var]))
                d = d_c[c]
                print (np.array(d.iloc[0][var+"_master"]))
                print (np.array(d.iloc[0][var+"_"+pareja[1]]))

            c += 1
            print ("*"*70)
    print (len(data1), len(data2), len(d_c))

#check_get_data(data1, data2, d_c)

def rinex_to_dataframe_ionosfe(obsfile, navfile, silent_mode=False, ion_max=60):
    c = 299792458.

    observation_types=["P1", "P2", "L1", "L2", "C1", "C2", "S1", "S2"]
    obsHeader, obsData = gpstk.readRinex3Obs(obsfile)
    navHeader, navData = gpstk.readRinex3Nav(navfile)

    nh=gpstk.readRinexNav(navfile)[0]
    alphas = nh.ionAlpha
    betas  = nh.ionBeta
    gamma=gpstk.GAMMA_GPS ##CARGANDO GAMMA

    # setup ephemeris store to look for satellite positions
    bcestore = gpstk.GPSEphemerisStore()
    for navDataObj in navData:
        ephem = navDataObj.toGPSEphemeris()
        bcestore.addEphemeris(ephem)
    bcestore.SearchNear()
    navData.close()

    rec_pos = [obsHeader.antennaPosition[0], obsHeader.antennaPosition[1], obsHeader.antennaPosition[2]]

    requested_obstypes = observation_types
    obsidxs = []
    obstypes = []
    obsdefs = np.array([i for i in obsHeader.R2ObsTypes])
    for i in requested_obstypes:
        w = np.where(obsdefs==i)[0]
        if len(w)!=0:
            obsidxs.append(w[0])
            obstypes.append(i)
        else:
            if silent_mode == False:
                print ("WARNING! observation `"+i+"` no present in file "+obsfile)
            #else:
            #    print ""
    #obsidxs, obstypes

    r = []
    for obsObject in obsData:
        prnlist = []
        obsdict = {}
        prnspos = []
        prns_clockbias = []
        prns_relcorr   = []
        prnselev       = []
        prnsaz         = []
        iono_delay     = []
        for i in obstypes:
            obsdict[i]=[]

        gpsTime = gpstk.GPSWeekSecond(obsObject.time)

        for satID, datumList in obsObject.obs.iteritems():
            if satID.system == satID.systemGPS:
                prnlist.append("".join(str(satID).split()))
                eph   = bcestore.findEphemeris(satID, obsObject.time)

                for i in range(len(obsidxs)):
                    obsdict[obstypes[i]].append(obsObject.getObs(satID, obsidxs[i]).data)

                P1 = obsObject.getObs(satID, obsidxs[0]).data

                # para reemplazar los valores de P1 que son cero en el rinex
                # por los valores de C1, que deberian ser semejantes
                if np.mean(P1) < 1:
                    #print obstypes[4]
                    P1 = obsObject.getObs(satID, obsidxs[4]).data

                P2 = obsObject.getObs(satID, obsidxs[1]).data
                Id = (1*1/(1-gamma)*(P1-P2))
                if np.abs(Id) < ion_max:
                    iono_delay.append(Id)
                else:
                    iono_delay.append(ion_max)

                svTime = obsObject.time - P1/c
                svXvt = eph.svXvt(svTime)
                svTime += - svXvt.getClockBias() + svXvt.getRelativityCorr()
                svXvt = eph.svXvt(svTime)

                prnspos.append([svXvt.x[0], svXvt.x[1], svXvt.x[2]])
                prns_clockbias.append(svXvt.getClockBias())
                prns_relcorr.append(svXvt.getRelativityCorr())

                prnselev.append(obsHeader.antennaPosition.elvAngle(svXvt.getPos()))
                prnsaz.append(obsHeader.antennaPosition.azAngle(svXvt.getPos()))

        r.append([gpsTime.getWeek(), gpsTime.getSOW(), alphas, betas, gamma, np.array(prnlist), np.array(prnspos), np.array(prns_clockbias),
                  np.array(prns_relcorr), np.array(prnselev), np.array(prnsaz), np.array(iono_delay)] + [np.array(obsdict[i]) for i in obstypes])

    names=["gps_week", "gps_sow", "alphas", "betas", "gamma", "prns", "prns_pos", "prns_clockbias", "prns_relcorr", "prns_elev", "prns_az", "Iono"] + obstypes
    r = pd.DataFrame(r, columns=names)
    obsData.close()
    return r, bcestore, np.array(rec_pos)


def predict_pseudoranges_ionosfe(x, svs, prns_clockbias, dat, apply_iono_model=None):
    c = 299792458
    R_earth = 6367444.657  # 6,371 km
    h_iono  = 350e3

    def iono_3dmodel(rs, d_iono, elevs):
        num1 = d_iono**2*(np.sqrt(rs**2*np.cos(elevs))*np.sin(elevs)**2)
        den1 = np.power(2*rs*np.cos(elevs)**2, 3) #8*rs**3*np.cos(elev)**6 #

        num2 = rs
        den2 = rs*np.cos(elevs)

        return num2/den2 + 1/8*(d_iono/rs)*(d_iono/rs)*np.tan(elevs) #num1/den1

    if apply_iono_model=='taylor':
        #print "a", apply_iono_model
        rs, d_iono = R_earth+h_iono, 10e3
        elv, az = get_sat_elv_az(svs, x[:3])
        io1 = iono_3dmodel(rs, d_iono, elv)

    elif apply_iono_model=='standard':
        #print apply_iono_model
        R = R_earth
        elevs, az = get_sat_elv_az(svs, x[:3])
        io1 = np.array(1/np.cos(np.arcsin(R/(R+350000))*np.sin(elevs)))
        #io2 = np.array(1/np.cos(np.arcsin(R/(R+350000))*np.sin(elv2)))

    elif apply_iono_model=='empiric2':
        e, a = get_sat_elv_az(svs, x[:3])
        #print len(S1), len(e), len(a)
        #io1 = (a/40.3)*(dat["S1_master"]/e)
        #io1 = (a/40.3)*(0.1 + np.exp(-((e/dat["S1_master"]))))
        N = np.shape(svs)[0]
        SNR1 = dat["S1_master"]
        io1 = (1./2)*(N - e/SNR1 + np.random.normal(N, 1)*np.exp(-e/SNR1))
        #print len(io1), io1

    elif apply_iono_model=='wallas':
        e, a = get_sat_elv_az(svs, x[:3])
        #print len(S1), len(e), len(a)
        #io1 = (a/40.3)*(dat["S1_master"]/e)
        #io1 = (a/40.3)*(0.1 + np.exp(-((e/dat["S1_master"]))))
        N = np.shape(svs)[0]
        SNR1 = dat["S1_master"]
        io1 = np.random.normal(SNR1, 1)*np.exp(-e/SNR1)
        #print len(io1), io1

    elif apply_iono_model=='klobu':
        elv, az = get_sat_elv_az(svs, x[:3])
        io1 = np.array([Klobuchar(a,e, dat["gps_sow"], x[:3], dat.alphas, dat.betas) for a, e in zip(az, elv)])

    elif apply_iono_model=='dual_freq':
        #print apply_iono_model, dat["Iono_master"][:4], dat["P1_master"][:4], dat["P2_master"][:4]
        io1 = dat["Iono_master"]

    elif apply_iono_model=='pondered':
        rs, d_iono = R_earth+160e3, 320e3
        elv, az = get_sat_elv_az(svs, x[:3])
        SNR1 = dat["S1_master"]

        io1 = np.random.normal(SNR1, 1)*np.exp(-elv/SNR1)#+ elv/SNR1  
        io1 += iono_3dmodel(rs, d_iono, elv)

    else:
        io1 = np.zeros(len(prns_clockbias))
        #print len(io1)
        pass

    rhos    = compute_distances(x[:3], svs)
    #print np.shape(rhos), np.shape(x[3]), np.shape(svs), np.shape(prns_clockbias)
    pranges = rhos + x[3]-c*prns_clockbias# - io1 #iono_3dmodel(elevs)
    return rhos, pranges, io1

    c = 299792458.

    observation_types=["P1", "P2", "L1", "L2", "C1", "C2", "S1", "S2"]
    obsHeader, obsData = gpstk.readRinex3Obs(obsfile)
    navHeader, navData = gpstk.readRinex3Nav(navfile)

    nh=gpstk.readRinexNav(navfile)[0]
    alphas = nh.ionAlpha
    betas  = nh.ionBeta
    gamma=gpstk.GAMMA_GPS ##CARGANDO GAMMA

    # setup ephemeris store to look for satellite positions
    bcestore = gpstk.GPSEphemerisStore()
    for navDataObj in navData:
        ephem = navDataObj.toGPSEphemeris()
        bcestore.addEphemeris(ephem)
    bcestore.SearchNear()
    navData.close()

    rec_pos = [obsHeader.antennaPosition[0], obsHeader.antennaPosition[1], obsHeader.antennaPosition[2]]

    requested_obstypes = observation_types
    obsidxs = []
    obstypes = []
    obsdefs = np.array([i for i in obsHeader.R2ObsTypes])
    for i in requested_obstypes:
        w = np.where(obsdefs==i)[0]
        if len(w)!=0:
            obsidxs.append(w[0])
            obstypes.append(i)
        else:
            if silent_mode == False:
                print ("WARNING! observation `"+i+"` no present in file "+obsfile)
            #else:
            #    print ""
    #obsidxs, obstypes

    r = []
    for obsObject in obsData:
        prnlist = []
        obsdict = {}
        prnspos = []
        prns_clockbias = []
        prns_relcorr   = []
        prnselev       = []
        prnsaz         = []
        iono_delay     = []
        for i in obstypes:
            obsdict[i]=[]

        gpsTime = gpstk.GPSWeekSecond(obsObject.time)

        for satID, datumList in obsObject.obs.iteritems():
            if satID.system == satID.systemGPS:
                prnlist.append("".join(str(satID).split()))
                eph   = bcestore.findEphemeris(satID, obsObject.time)

                for i in range(len(obsidxs)):
                    obsdict[obstypes[i]].append(obsObject.getObs(satID, obsidxs[i]).data)

                P1 = obsObject.getObs(satID, obsidxs[0]).data

                # para reemplazar los valores de P1 que son cero en el rinex
                # por los valores de C1, que deberian ser semejantes
                if np.mean(P1) < 1:
                    #print obstypes[4]
                    P1 = obsObject.getObs(satID, obsidxs[4]).data

                P2 = obsObject.getObs(satID, obsidxs[1]).data
                Id = (1*1/(1-gamma)*(P1-P2))
                if np.abs(Id) < ion_max:
                    iono_delay.append(Id)
                else:
                    iono_delay.append(ion_max)

                svTime = obsObject.time - P1/c
                svXvt = eph.svXvt(svTime)
                svTime += - svXvt.getClockBias() + svXvt.getRelativityCorr()
                svXvt = eph.svXvt(svTime)

                prnspos.append([svXvt.x[0], svXvt.x[1], svXvt.x[2]])
                prns_clockbias.append(svXvt.getClockBias())
                prns_relcorr.append(svXvt.getRelativityCorr())

                prnselev.append(obsHeader.antennaPosition.elvAngle(svXvt.getPos()))
                prnsaz.append(obsHeader.antennaPosition.azAngle(svXvt.getPos()))

        r.append([gpsTime.getWeek(), gpsTime.getSOW(), alphas, betas, gamma, np.array(prnlist), np.array(prnspos), np.array(prns_clockbias),
                  np.array(prns_relcorr), np.array(prnselev), np.array(prnsaz), np.array(iono_delay)] + [np.array(obsdict[i]) for i in obstypes])

    names=["gps_week", "gps_sow", "alphas", "betas", "gamma", "prns", "prns_pos", "prns_clockbias", "prns_relcorr", "prns_elev", "prns_az", "Iono"] + obstypes
    r = pd.DataFrame(r, columns=names)
    obsData.close()
    return r, bcestore, np.array(rec_pos)